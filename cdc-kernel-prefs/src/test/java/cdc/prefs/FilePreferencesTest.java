package cdc.prefs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.prefs.file.FilePreferencesFactory;

public class FilePreferencesTest {
    private static final Logger LOGGER = LogManager.getLogger(FilePreferencesTest.class);

    @Test
    public void test() throws BackingStoreException {
        System.setProperty("java.util.prefs.PreferencesFactory", FilePreferencesFactory.class.getName());
        Prefs.setPrefsRootName("cdc-prefs-test");

        LOGGER.info("preference file: {}", FilePreferencesFactory.getPreferencesFile());
        LOGGER.info("root name: {}", Prefs.getRootName());
        final Preferences prefs = Prefs.getUserRoot();
        prefs.put("key1", "value");
        prefs.put("key2", "value");
        assertEquals("value", prefs.get("key1", null));
        assertEquals("value", prefs.get("key2", null));
        prefs.remove("key1");
        assertEquals(null, prefs.get("key1", null));
        assertEquals("value", prefs.get("key2", null));
        LOGGER.info("prefs: {}", prefs);
        for (final String key : prefs.keys()) {
            LOGGER.info("key: {}", key);
        }
        for (final String name : prefs.childrenNames()) {
            LOGGER.info("child: {}", name);
        }
    }
}