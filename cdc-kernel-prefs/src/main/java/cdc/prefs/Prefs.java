package cdc.prefs;

import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Prefs {
    private static final Logger LOGGER = LogManager.getLogger(Prefs.class);
    private static String rootName = null;

    private Prefs() {
    }

    public static void setPrefsRootName(String name) {
        LOGGER.info("setPrefsRootName({})", name);
        rootName = name;
    }

    public static String getRootName() {
        if (rootName == null) {
            LOGGER.warn("setPrefsRootName() was not called.");
        }
        return rootName == null ? "cdc" : rootName;
    }

    public static Preferences getUserRoot() {
        return Preferences.userRoot().node(rootName);
    }

    public static Preferences getSystemRoot() {
        return Preferences.systemRoot().node(rootName);
    }
}