package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * String preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class StringPreference extends AbstractPreference<String> {
    public StringPreference(Preferences node,
                            String key,
                            String def) {
        super(String.class,
              node,
              key,
              def,
              s -> s,
              s -> s);
    }
}