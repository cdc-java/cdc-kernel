package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Float preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class FloatPreference extends AbstractPreference<Float> {
    public FloatPreference(Preferences node,
                           String key,
                           Float def) {
        super(Float.class,
              node,
              key,
              def,
              Object::toString,
              Float::parseFloat);
    }
}