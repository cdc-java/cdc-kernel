package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Byte preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class BytePreference extends AbstractPreference<Byte> {
    public BytePreference(Preferences node,
                          String key,
                          Byte def) {
        super(Byte.class,
              node,
              key,
              def,
              Object::toString,
              Byte::parseByte);
    }
}