package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Enum preference (node, key) pair.
 *
 * @author Damien Carbonne
 * @param <E> The enum type.
 */
public class EnumPreference<E extends Enum<E>> extends AbstractPreference<E> {
    public EnumPreference(Class<E> valueClass,
                          Preferences node,
                          String key,
                          E def) {
        super(valueClass,
              node,
              key,
              def,
              Object::toString,
              s -> {
                  for (final E value : valueClass.getEnumConstants()) {
                      if (value.toString().equals(s)) {
                          return value;
                      }
                  }
                  return null;
              });
    }
}