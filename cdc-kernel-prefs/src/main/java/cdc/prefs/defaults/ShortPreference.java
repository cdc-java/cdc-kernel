package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Short preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class ShortPreference extends AbstractPreference<Short> {
    public ShortPreference(Preferences node,
                           String key,
                           Short def) {
        super(Short.class,
              node,
              key,
              def,
              Object::toString,
              Short::parseShort);
    }
}