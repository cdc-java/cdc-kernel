package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Boolean preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class BooleanPreference extends AbstractPreference<Boolean> {
    public BooleanPreference(Preferences node,
                             String key,
                             Boolean def) {
        super(Boolean.class,
              node,
              key,
              def,
              Object::toString,
              Boolean::parseBoolean);
    }
}