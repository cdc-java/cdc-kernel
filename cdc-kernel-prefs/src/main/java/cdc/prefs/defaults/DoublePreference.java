package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Double preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class DoublePreference extends AbstractPreference<Double> {
    public DoublePreference(Preferences node,
                            String key,
                            Double def) {
        super(Double.class,
              node,
              key,
              def,
              Object::toString,
              Double::parseDouble);
    }
}