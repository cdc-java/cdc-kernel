package cdc.prefs.defaults;

import java.io.File;
import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * File preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class FilePreference extends AbstractPreference<File> {
    public FilePreference(Preferences node,
                          String key,
                          File def) {
        super(File.class,
              node,
              key,
              def,
              File::toString,
              File::new);
    }
}