package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Long preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class LongPreference extends AbstractPreference<Long> {
    public LongPreference(Preferences node,
                          String key,
                          Long def) {
        super(Long.class,
              node,
              key,
              def,
              Object::toString,
              Long::parseLong);
    }
}