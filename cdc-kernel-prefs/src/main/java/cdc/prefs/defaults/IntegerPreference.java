package cdc.prefs.defaults;

import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Integer preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class IntegerPreference extends AbstractPreference<Integer> {
    public IntegerPreference(Preferences node,
                             String key,
                             Integer def) {
        super(Integer.class,
              node,
              key,
              def,
              Object::toString,
              Integer::parseInt);
    }
}