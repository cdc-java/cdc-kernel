package cdc.prefs;

import java.util.function.Function;
import java.util.prefs.Preferences;

/**
 * Base implementation of Preference.
 *
 * @author Damien Carbonne
 *
 * @param <T> The preference type.
 */
public abstract class AbstractPreference<T> implements Preference<T> {
    private final Class<T> valueClass;
    private final Preferences node;
    private final String key;
    private final T def;
    private final Function<T, String> toString;
    private final Function<String, T> fromString;

    /**
     * Creates an abstract preference.
     * <p>
     * If {@code toString } is {@code null}, {@link #toString} must be implemented.<br>
     * If {@code fromString } is {@code null}, {@link #fromString} must be implemented.
     *
     * @param valueClass The value class.
     * @param node The Preferences node.
     * @param key The preferences key.
     * @param def The default value.
     * @param toString Optional function used to convert from value type to string.
     * @param fromString Optional function used to convert from string to value type.
     */
    protected AbstractPreference(Class<T> valueClass,
                                 Preferences node,
                                 String key,
                                 T def,
                                 Function<T, String> toString,
                                 Function<String, T> fromString) {
        this.valueClass = valueClass;
        this.node = node;
        this.key = key;
        this.def = def;
        this.toString = toString;
        this.fromString = fromString;
    }

    /**
     * Creates an abstract preference.
     * <p>
     * {@link #toString} and {@link #fromString} must be implemented.
     *
     * @param valueClass The value class.
     * @param node The Preferences node.
     * @param key The preferences key.
     * @param def The default value.
     */
    protected AbstractPreference(Class<T> valueClass,
                                 Preferences node,
                                 String key,
                                 T def) {
        this(valueClass, node, key, def, null, null);
    }

    /**
     * Converts a value to a string using {@code toString}.
     * <p>
     * If {@code value} is {@code null}, returns {@code ""}.
     *
     * @param value The value.
     * @return The corresponding string.
     * @throws IllegalStateException When {@code toString} is {@code null} and {@code value} is not {@code null}.
     */
    protected String toString(T value) {
        if (value == null) {
            return "";
        } else if (toString != null) {
            return toString.apply(value);
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Converts a string to a value using {@code fomString}.
     * <p>
     * If {@code s} is {@code null} or empty, returns {@code null}.
     *
     * @param s The string.
     * @return The corresponding value.
     * @throws IllegalStateException When {@code fromString} is {@code null} and{@code s} is neither {@code null} or empty.
     */
    protected T fromString(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        } else if (fromString != null) {
            return fromString.apply(s);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public final Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public final Preferences getNode() {
        return node;
    }

    @Override
    public final String getKey() {
        return key;
    }

    @Override
    public final T getDefaultValue() {
        return def;
    }

    @Override
    public final void put(T value) {
        if (value == null) {
            getNode().put(getKey(), "");
        } else {
            getNode().put(getKey(), toString(value));
        }
    }

    @Override
    public final T get(T def) {
        final String value = getNode().get(getKey(), null);
        if (value == null) {
            return def;
        } else {
            try {
                return fromString(value);
            } catch (final Exception e) {
                return def;
            }
        }
    }
}