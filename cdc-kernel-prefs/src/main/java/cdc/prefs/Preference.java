package cdc.prefs;

import java.util.prefs.Preferences;

/**
 * Interface giving access to one (node, key) preference.
 *
 * @author Damien Carbonne
 *
 * @param <T> The preference type.
 */
public interface Preference<T> {
    /**
     * @return The value class.
     */
    public Class<T> getValueClass();

    /**
     * @return The associated node.
     */
    public Preferences getNode();

    /**
     * @return The associated key.
     */
    public String getKey();

    /**
     * @return The default value.
     */
    public T getDefaultValue();

    /**
     * Puts a value into this (node, key).
     *
     * @param value The value.
     */
    public void put(T value);

    /**
     * Returns the value associated to this (node, key).
     *
     * @param def The default value to return when there is
     *            no associated value or value processing raises an exception
     * @return The associated value or {@code def}.
     */
    public T get(T def);

    /**
     * @return the value associated to this (node, key) or the default value.
     */
    public default T get() {
        return get(getDefaultValue());
    }
}