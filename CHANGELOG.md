# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - org.junit-5.12.0


## [0.51.3] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-io-0.53.0
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3


## [0.51.2] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-io-0.52.1
    - cdc-util-0.53.0
    - org.junit-5.11.1


## [0.51.1] - 2024-05-18
### Changed
- Updated dependencies:
    - cdc-io-0.52.0
    - cdc-util-0.52.1
    - org.apache.log4j-2.23.1
    - org.junit-5.10.2
- Updated maven plugins.


## [0.51.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-util-0.52.0
- Updated maven plugins.


## [0.50.0] - 2023-11-25
### Changed
- Moved to Java 17.  
  Note that the non-breaking space used by Java formatters has changed.
- Updated dependencies:
    - cdc-io-0.50.0
    - cdc-util-0.50.0


## [0.23.2] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-io-0.27.3
    - cdc-util-0.33.2
    - org.junit-5.10.1


## [0.23.1] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-io-0.27.2
    - cdc-util-0.33.1


## [0.23.0] - 2023-10-01
### Changed
- Updated dependencies:
    - cdc-io-0.27.1
    - org.junit-5.10.0
    - org.openjdk.jmh-1.37
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom.
- Now `Checkers.elaborate()` and `Converters.elaborate()` return `void`.

### Added
- Added `Converters.convertRaw(Object, Class, Class)` and `Converters.convert(Object, Class, Class)`. #6 


## [0.22.1] - 2023-07-15
- Updated dependencies:
    - cdc-io-0.27.0
    - cdc-util-0.33.0


## [0.22.0] - 2023-06-11
- Updated dependencies:
    - cdc-io-0.26.2
    - cdc-util-0.32.0

### Removed
- Removed old deprecated code


## [0.21.4] - 2023-04-19
### Fixed
- Fixed bug in `Args.isEmptyOrAllValuesAreNull()`. #5


## [0.21.3] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-io-0.26.0


## [0.21.2] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-io-0.25.1
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0


## [0.21.1] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-io-0.24.0
    - cdc-util-0.29.0
    - org.junit-5.9.2


## [0.21.0] - 2023-01-02
### Added
- Added `MatchesPattern.ASCII`, `HasNoOuterWhiteSpaces`, `IsNull` and `IsNullOrEmpty`.
- Added `of` construction functions.

### Changed
- Updated dependencies:
    - cdc-io-0.23.2
    - cdc-util-0.28.2
    - org.openjdk.jmh-1.36


## [0.20.7] - 2022-11-09
### Changed
- Updated dependencies:
    - cdc-io-0.23.1
    - cdc-util-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1


## [0.20.6] - 2022-08-23
### Changed
- Updated dependencies:
    - cdc-io-0.23.0
    - cdc-util-0.28.0
    - org.junit-5.9.0


## [0.20.5] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-io-0.22.0
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.20.4] - 2022-06-18
### Changed
- Updated dependencies:
    - cdc-io-0.21.3
    - cdc-util-0.26.0


## [0.20.3] - 2022-05-19
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-io-0.21.2
    - cdc-util-0.25.0
    - org.openjdk.jmh-1.35


## [0.20.2] - 2022-03-11
### Changed
- Updated dependencies:
    - cdc-io-0.21.1
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2


## [0.20.1] - 2022-02-13
### Changed
- Updated dependencies:
    - cdc-io-0.21.0
    - cdc-util-0.20.0


## [0.20.0] - 2022-02-04
### Added
- Added `ListType.size()`. #4

### Changed
- Updated maven plugins
- Upgraded to Java 11
- Updated dependencies:
    - cdc-io-0.20.0


## [0.14.2] - 2022-01-03
### Security
- Updated dependencies:
    - cdc-util-0.14.2
    - cdc-io-0.13.2
    - org.apache.log4j-2.17.1. #3


## [0.14.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-util-0.14.1
    - cdc-io-0.13.1
    - org.apache.log4j-2.17.0. #3
    - org.openjdk.jmh-1.34


## [0.14.0] - 2021-12-14
### Security
- Updated dependencies:
    - cdc-util-0.14.0
    - cdc-io-0.13.0
    - org.apache.log4j-2.16.0. #3


## [0.13.1] - 2021-10-02
### Changed
- Updated dependencies


## [0.13.0] - 2021-07-23
### Added
- Added handling of first converter in IO. #2

### Changed
- Updated dependencies
- Merged NaryOrElseConverter and OrElseConverter. #2


## [0.12.0] - 2021-06-05
### Added
- Created `cdc-kernel-app` module. It contains code to save data about an application.


## [0.11.0] - 2021-05-10
### Added
- **benches** module #1

### Fixed
- **AbstractMask** performances have been improved. #1


## [0.10.0] - 2021-05-03
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util).
    - **enums** cdc-java/cdc-util#40
    - **prefs** cdc-java/cdc-util#42
    - **converters** cdc-java/cdc-util#44
    - **args** cdc-java/cdc-util#45
    - **validation** cdc-java/cdc-util#46
    - **informers** cdc-java/cdc-util#47
    - **rids** cdc-java/cdc-util#48