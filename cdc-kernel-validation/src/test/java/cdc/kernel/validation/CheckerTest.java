package cdc.kernel.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.converters.defaults.StringToInteger;
import cdc.validation.checkers.Checker;
import cdc.validation.checkers.defaults.IsInRange;

public class CheckerTest {
    private static final Logger LOGGER = LogManager.getLogger(CheckerTest.class);

    @Test
    public void testNegate() {
        final Checker<Integer> checker = IsInRange.from(0, 100).negate();
        assertEquals(true, checker.test(200));
        LOGGER.debug(checker.explain());
    }

    @Test
    public void testAnd() {
        final Checker<Integer> checker = IsInRange.from(0, 100).and(IsInRange.from(50, 150));
        assertEquals(true, checker.test(75));
        assertEquals(false, checker.test(200));
        LOGGER.debug(checker.explain());
    }

    @Test
    public void testOr() {
        final Checker<Integer> checker = IsInRange.from(0, 100).or(IsInRange.from(50, 150));
        assertEquals(true, checker.test(75));
        assertEquals(false, checker.test(200));
        LOGGER.debug(checker.explain());
    }

    @Test
    public void testAfter() {
        final Checker<String> checker = IsInRange.from(10, 20).after(StringToInteger.INSTANCE);
        assertEquals(true, checker.test("15"));
        assertEquals(false, checker.test("30"));
        assertEquals(false, checker.test("Hello"));
        LOGGER.debug(checker.explain());
    }
}