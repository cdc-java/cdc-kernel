package cdc.kernel.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.validation.checkers.Checker;
import cdc.validation.checkers.defaults.IsInRange;
import cdc.validation.checkers.defaults.IsInstanceOf;
import cdc.validation.checkers.defaults.IsNotNull;

public class CheckersDefaultTest {
    private static final Logger LOGGER = LogManager.getLogger(CheckersDefaultTest.class);

    @Test
    public void testIsNotNull() {
        assertEquals(false, IsNotNull.INSTANCE.test(null));
        assertEquals(true, IsNotNull.INSTANCE.test("Hello"));
        assertEquals(true, IsNotNull.INSTANCE.negate().test(null));
        assertEquals(false, IsNotNull.INSTANCE.negate().test("Hello"));
        LOGGER.debug(IsNotNull.INSTANCE.explain());
        LOGGER.debug(IsNotNull.INSTANCE.negate().explain());
        LOGGER.debug(IsNotNull.INSTANCE.testAndExplain(null));
        LOGGER.debug(IsNotNull.INSTANCE.testAndExplain("Hello"));
        LOGGER.debug(IsNotNull.INSTANCE.negate().testAndExplain(null));
        LOGGER.debug(IsNotNull.INSTANCE.negate().testAndExplain("Hello"));
    }

    @Test
    public void testIsInRange() {
        assertEquals(true, IsInRange.from(0, 100).test(10));
        assertEquals(false, IsInRange.from(0, 100).test(200));
        LOGGER.debug(IsInRange.from(0, 100).explain());
        LOGGER.debug(IsInRange.from(0, 100).negate().explain());
        LOGGER.debug(IsInRange.from(0, 100).testAndExplain(10));
        LOGGER.debug(IsInRange.from(0, 100).testAndExplain(200));
        LOGGER.debug(IsInRange.from(0, 100).negate().testAndExplain(10));
        LOGGER.debug(IsInRange.from(0, 100).negate().testAndExplain(200));
    }

    @Test
    public void testeIsInstanceOf() {
        final Checker<Object> checker = IsInstanceOf.from(String.class);
        assertEquals(false, checker.test(null));
        assertEquals(true, checker.test("Hello"));
        LOGGER.debug(checker.explain());
    }
}