package cdc.kernel.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.validation.Validity;

public class ValidityTest {
    @Test
    public void testIsValidOrDubious() {
        assertTrue(Validity.VALID.isValidOrDubious());
        assertTrue(Validity.DUBIOUS.isValidOrDubious());
        assertFalse(Validity.ERRONEOUS.isValidOrDubious());
    }

    @Test
    public void testWorse() {
        assertEquals(Validity.VALID, Validity.worse(Validity.VALID, Validity.VALID));
        assertEquals(Validity.DUBIOUS, Validity.worse(Validity.VALID, Validity.DUBIOUS));
        assertEquals(Validity.ERRONEOUS, Validity.worse(Validity.VALID, Validity.ERRONEOUS));

        assertEquals(Validity.DUBIOUS, Validity.worse(Validity.DUBIOUS, Validity.VALID));
        assertEquals(Validity.DUBIOUS, Validity.worse(Validity.DUBIOUS, Validity.DUBIOUS));
        assertEquals(Validity.ERRONEOUS, Validity.worse(Validity.DUBIOUS, Validity.ERRONEOUS));

        assertEquals(Validity.ERRONEOUS, Validity.worse(Validity.ERRONEOUS, Validity.VALID));
        assertEquals(Validity.ERRONEOUS, Validity.worse(Validity.ERRONEOUS, Validity.DUBIOUS));
        assertEquals(Validity.ERRONEOUS, Validity.worse(Validity.ERRONEOUS, Validity.ERRONEOUS));
    }

}