package cdc.kernel.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.converters.defaults.StringToDouble;
import cdc.validation.ValidationRecord;
import cdc.validation.Validator;
import cdc.validation.ValidatorImpl;
import cdc.validation.Validity;
import cdc.validation.checkers.Checker;
import cdc.validation.checkers.defaults.IsInRange;
import cdc.validation.checkers.defaults.IsNotNull;
import cdc.validation.checkers.defaults.LengthIsInRange;

class ValidatorTest {
    private static final Logger LOGGER = LogManager.getLogger(ValidatorTest.class);

    private static <T> void checkValidate(Validator<T> validator,
                                          T value,
                                          Validity expected) {
        LOGGER.debug("checkValidate({}, {})", validator, value);
        final List<ValidationRecord> records = validator.validate(value, "value");
        for (final ValidationRecord record : records) {
            LOGGER.debug("   {}", record);
        }
        assertEquals(expected, ValidationRecord.getValidity(records));
        assertEquals(expected, validator.getValidity(value));
    }

    private static <T> void checkValidateRaw(Validator<?> validator,
                                             Object value,
                                             Validity expected) {
        LOGGER.debug("checkValidateRaw({}, {})", validator, value);
        final List<ValidationRecord> records = validator.validateRaw(value, "value");
        for (final ValidationRecord record : records) {
            LOGGER.debug("   {}", record);
        }
        assertEquals(expected, ValidationRecord.getValidity(records));
        assertEquals(expected, validator.getValidityRaw(value));
    }

    @Test
    void testNone() {
        final ValidatorImpl<String> validator = new ValidatorImpl<>(String.class, null, null);
        assertEquals(String.class, validator.getValueClass());
        assertEquals(null, validator.getChecker(Validity.ERRONEOUS));
        assertEquals(null, validator.getChecker(Validity.DUBIOUS));
        assertEquals(null, validator.getChecker(Validity.VALID));

        checkValidate(validator, null, Validity.VALID);
        checkValidate(validator, "", Validity.VALID);
        checkValidate(validator, "Hello", Validity.VALID);
    }

    @Test
    void testError() {
        final ValidatorImpl<String> validator = new ValidatorImpl<>(String.class,
                                                                    IsNotNull.INSTANCE,
                                                                    null);
        assertEquals(String.class, validator.getValueClass());
        assertEquals(IsNotNull.INSTANCE, validator.getChecker(Validity.ERRONEOUS));
        assertEquals(null, validator.getChecker(Validity.DUBIOUS));
        assertEquals(null, validator.getChecker(Validity.VALID));

        checkValidate(validator, null, Validity.ERRONEOUS);
        checkValidate(validator, "", Validity.VALID);
        checkValidate(validator, "Hello", Validity.VALID);
    }

    @Test
    void testWarning() {
        final ValidatorImpl<String> validator = new ValidatorImpl<>(String.class,
                                                                    null,
                                                                    IsNotNull.INSTANCE);
        assertEquals(String.class, validator.getValueClass());
        assertEquals(null, validator.getChecker(Validity.ERRONEOUS));
        assertEquals(IsNotNull.INSTANCE, validator.getChecker(Validity.DUBIOUS));
        assertEquals(null, validator.getChecker(Validity.VALID));

        checkValidate(validator, null, Validity.DUBIOUS);
        checkValidate(validator, "", Validity.VALID);
        checkValidate(validator, "Hello", Validity.VALID);
    }

    @Test
    void testErrorAndWarning() {
        final Checker<String> warning = new LengthIsInRange(5, 10);
        final ValidatorImpl<String> validator = new ValidatorImpl<>(String.class,
                                                                    IsNotNull.INSTANCE,
                                                                    warning);
        assertEquals(String.class, validator.getValueClass());
        assertEquals(IsNotNull.INSTANCE, validator.getChecker(Validity.ERRONEOUS));
        assertEquals(warning, validator.getChecker(Validity.DUBIOUS));
        assertEquals(null, validator.getChecker(Validity.VALID));

        assertTrue(validator.hasChecker(Validity.ERRONEOUS));
        assertTrue(validator.hasChecker(Validity.DUBIOUS));
        assertFalse(validator.hasChecker(Validity.VALID));

        checkValidate(validator, null, Validity.ERRONEOUS);
        checkValidate(validator, "", Validity.DUBIOUS);
        checkValidate(validator, "Hello", Validity.VALID);
        checkValidate(validator, "Much too long", Validity.DUBIOUS);
    }

    @Test
    void testAfter() {
        final Checker<Object> error = IsNotNull.INSTANCE;
        final Checker<Double> warning = new IsInRange<>(Double.class, 0.0, 10.0);
        final ValidatorImpl<Double> doubleValidator = new ValidatorImpl<>(Double.class,
                                                                          error,
                                                                          warning);
        final Validator<String> validator = doubleValidator.after(StringToDouble.INSTANCE);

        assertEquals(String.class, validator.getValueClass());

        checkValidate(validator, null, Validity.ERRONEOUS);
        checkValidate(validator, "5.0", Validity.VALID);
        checkValidate(validator, "-10.0", Validity.DUBIOUS);
        checkValidate(validator, "20.0", Validity.DUBIOUS);
        checkValidate(validator, "Number", Validity.ERRONEOUS);
    }

    @Test
    void testAfterRaw() {
        final Checker<Object> error = IsNotNull.INSTANCE;
        final Checker<Double> warning = new IsInRange<>(Double.class, 0.0, 10.0);
        final ValidatorImpl<Double> doubleValidator = new ValidatorImpl<>(Double.class,
                                                                          error,
                                                                          warning);
        final Validator<?> validator = doubleValidator.afterRaw(StringToDouble.INSTANCE);

        assertEquals(String.class, validator.getValueClass());

        checkValidateRaw(validator, null, Validity.ERRONEOUS);
        checkValidateRaw(validator, "5.0", Validity.VALID);
        checkValidateRaw(validator, "-10.0", Validity.DUBIOUS);
        checkValidateRaw(validator, "20.0", Validity.DUBIOUS);
        checkValidateRaw(validator, "Number", Validity.ERRONEOUS);
    }
}