package cdc.validation;

import java.util.List;

public class ValidationRecord {
    private final Validity validity;
    private final String message;

    public ValidationRecord(Validity validity,
                            String message) {
        this.validity = validity;
        this.message = message;
    }

    public Validity getValidity() {
        return validity;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "[" + getValidity() + ": " + getMessage() + "]";
    }

    public static Validity getValidity(List<ValidationRecord> records) {
        Validity result = Validity.VALID;
        for (final ValidationRecord record : records) {
            result = Validity.worse(result, record.getValidity());
        }
        return result;
    }
}