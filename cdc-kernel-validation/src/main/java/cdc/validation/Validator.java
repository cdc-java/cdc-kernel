package cdc.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import cdc.converters.Converter;
import cdc.util.lang.Checks;
import cdc.util.lang.ValueHolder;

/**
 * Validator of values.
 *
 * @author Damien Carbonne
 *
 * @param <T> Value type.
 */
public interface Validator<T> {

    /**
     * @return The class of values validated by this validator.
     */
    public Class<T> getValueClass();

    /**
     * Returns an explanation of the rule that must be respected to avoid errors.
     *
     * @param name The name that must be given to value in messages.
     * @return An explanation of the rule that must be respected to avoid errors.
     */
    public String explainError(String name);

    /**
     * Returns an explanation of the rule that must be respected to avoid warnings.
     *
     * @param name The name that must be given to value in messages.
     * @return An explanation of the rule that must be respected to avoid warnings.
     */
    public String explainWarning(String name);

    /**
     * Validates a value.
     *
     * @param value The value.
     * @param name The name that must be given to {@code value} in messages.
     * @param handler The validation messages handler.
     */
    public void validate(T value,
                         String name,
                         ValidationHandler handler);

    /**
     * Validates a value and adds validation records to a list.
     *
     * @param value The value.
     * @param name The name that must be given to {@code value} in messages.
     * @param records The list of validation records that must be filled.
     * @throws IllegalArgumentException When {@code records} is {@code null}.
     */
    public default void validate(T value,
                                 String name,
                                 List<ValidationRecord> records) {
        Checks.isNotNull(records, "records");
        validate(value,
                 name,
                 (s,
                  m) -> records.add(new ValidationRecord(s, m)));
    }

    /**
     * Validates a value and returns a list of validation records.
     *
     * @param value The value.
     * @param name The name that must be given to {@code value} in messages.
     * @return A list of validation records, possibly empty.
     */
    public default List<ValidationRecord> validate(T value,
                                                   String name) {
        final List<ValidationRecord> records = new ArrayList<>();
        validate(value, name, records);
        return records;
    }

    /**
     * Validates a raw value.
     *
     * @param value The value.
     * @param name The name that must be given to {@code value} in messages.
     * @param handler The validation messages handler.
     * @throws ClassCastException If {@code value} is not {@code null} and cannot be assigned to value type {@code T}.
     */
    public default void validateRaw(Object value,
                                    String name,
                                    ValidationHandler handler) {
        final T v = getValueClass().cast(value);
        validate(v, name, handler);
    }

    /**
     * Validates a raw value and adds validation records to a list.
     *
     * @param value The value.
     * @param name The name that must be given to {@code value} in messages.
     * @param records The list of validation records that must be filled.
     * @throws IllegalArgumentException When {@code records} is {@code null}.
     * @throws ClassCastException If {@code value} is not {@code null} and cannot be assigned to value type {@code T}.
     */
    public default void validateRaw(Object value,
                                    String name,
                                    List<ValidationRecord> records) {
        Checks.isNotNull(records, "records");
        validateRaw(value,
                    name,
                    (s,
                     m) -> records.add(new ValidationRecord(s, m)));
    }

    /**
     * Validates a raw value and returns a list of validation records.
     *
     * @param value The value.
     * @param name The name that must be given to {@code value} in messages.
     * @return A list of validation records, possibly empty.
     * @throws ClassCastException If {@code value} is not {@code null} and cannot be assigned to value type {@code T}.
     */
    public default List<ValidationRecord> validateRaw(Object value,
                                                      String name) {
        final List<ValidationRecord> records = new ArrayList<>();
        validateRaw(value, name, records);
        return records;
    }

    /**
     * Returns the validity of a value.
     *
     * @param value The value.
     * @return The validity of {@code value}.
     */
    public default Validity getValidity(T value) {
        final ValueHolder<Validity> max = new ValueHolder<>(Validity.VALID);
        validate(value,
                 null,
                 (s,
                  m) -> max.value = Validity.worse(max.value, s));
        return max.value;
    }

    /**
     * Returns the validity of a raw value.
     *
     * @param value The value.
     * @return The validity of {@code value}.
     * @throws ClassCastException If {@code value} is not {@code null} and cannot be assigned to value type {@code T}.
     */
    public default Validity getValidityRaw(Object value) {
        final T v = getValueClass().cast(value);
        return getValidity(v);
    }

    /**
     * Creates a validator composed of this one applied after a converter.
     *
     * @param <S> The source type.
     * @param converter The converter from source type {@code S} to {@code T}.
     * @return A new validator composed of this one applied after {@code converter}.
     * @throws IllegalArgumentException When {@code converter} is {@code null}.
     */
    public default <S> Validator<S> after(Converter<S, ? extends T> converter) {
        Checks.isNotNull(converter, "converter");
        return after(converter, converter.getSourceClass());
    }

    /**
     *
     * @param converter The converter.
     * @return A new validator composed of this one applied after {@code converter}.
     * @throws IllegalArgumentException When {@code converter} is {@code null}.
     */
    public default Validator<?> afterRaw(Converter<?, ?> converter) {
        Checks.isNotNull(converter, "converter");
        final Converter<?, ? extends T> tmp = converter.cast(converter.getSourceClass(), getValueClass());
        return after(tmp);
    }

    /**
     * Creates a validator composed of this one applied after a conversion function.
     *
     * @param <S> The source type.
     * @param function The function that converts values from source type {@code S} to {@code T}.
     * @param sourceClass The source class.
     * @return A new validator composed of this one applied conversion {@code function}.
     * @throws IllegalArgumentException When {@code function} or {@code sourceClass} is {@code null}.
     */
    public default <S> Validator<S> after(Function<S, ? extends T> function,
                                          Class<S> sourceClass) {
        Checks.isNotNull(function, "function");
        Checks.isNotNull(sourceClass, "sourceClass");

        return new Validator<S>() {
            @Override
            public Class<S> getValueClass() {
                return sourceClass;
            }

            @Override
            public String explainError(String name) {
                final String s1 = name + " must be convertible to " + Validator.this.getValueClass().getSimpleName();
                final String s2 = Validator.this.explainError(name);
                if (s2 == null || s2.isEmpty()) {
                    return s1;
                } else {
                    return s1 + " and " + s2;
                }
            }

            @Override
            public String explainWarning(String name) {
                final String s1 = name + " must be convertible to " + Validator.this.getValueClass().getSimpleName();
                final String s2 = Validator.this.explainWarning(name);
                if (s2 == null || s2.isEmpty()) {
                    return s1;
                } else {
                    return s1 + " and " + s2;
                }
            }

            @Override
            public void validate(S value,
                                 String name,
                                 ValidationHandler handler) {
                T target;
                try {
                    target = function.apply(value);
                } catch (final Exception e) {
                    handler.begin();
                    handler.processIssue(Validity.ERRONEOUS, "Failed to convert '" + value + "'");
                    handler.end();
                    return;
                }
                Validator.this.validate(target, name, handler);
            }

            @Override
            public String toString() {
                return "convert from " + sourceClass.getSimpleName() + " followed by: " + Validator.this;
            }
        };
    }
}