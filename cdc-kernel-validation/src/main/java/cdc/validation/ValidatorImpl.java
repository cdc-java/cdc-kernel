package cdc.validation;

import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;
import cdc.validation.checkers.Checker;

public class ValidatorImpl<T> implements Validator<T> {
    private final Class<T> valueClass;
    private final Checker<? super T> error;
    private final Checker<? super T> warning;

    public ValidatorImpl(Class<T> valueClass,
                         Checker<? super T> error,
                         Checker<? super T> warning) {
        Checks.isNotNull(valueClass, "valueClass");
        this.valueClass = valueClass;
        this.error = error;
        this.warning = warning;
    }

    public static ValidatorImpl<?> createUnchecked(Checker<?> error,
                                                   Checker<?> warning) {
        final Class<?> valueClass;
        if (error == null) {
            if (warning == null) {
                valueClass = Object.class;
            } else {
                valueClass = warning.getValueClass();
            }
        } else {
            if (warning == null) {
                valueClass = error.getValueClass();
            } else {
                valueClass = Introspection.mostGeneralized(error.getValueClass(), warning.getValueClass());
                if (valueClass == null) {
                    throw new IllegalArgumentException("Can not create a validator with " + error.getValueClass().getCanonicalName()
                            + " and " + warning.getValueClass().getCanonicalName());
                }
            }
        }

        return new ValidatorImpl<>(Introspection.uncheckedCast(valueClass),
                                   Introspection.uncheckedCast(error),
                                   Introspection.uncheckedCast(warning));
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public String explainError(String name) {
        if (error == null) {
            return null;
        } else {
            return error.explain(name);
        }
    }

    @Override
    public String explainWarning(String name) {
        if (warning == null) {
            return null;
        } else {
            return warning.explain(name);
        }
    }

    @Override
    public void validate(T value,
                         String name,
                         ValidationHandler handler) {
        handler.begin();
        final boolean hasNoError = isAccepted(value, name, handler, error, Validity.ERRONEOUS);
        if (hasNoError) {
            isAccepted(value, name, handler, warning, Validity.DUBIOUS);
        }
        handler.end();
    }

    private boolean isAccepted(T value,
                               String name,
                               ValidationHandler handler,
                               Checker<? super T> checker,
                               Validity validity) {
        if (checker == null) {
            return true;
        } else {
            final boolean accepted = checker.test(value);
            if (!accepted) {
                final String message = checker.explain(name);
                handler.processIssue(validity, message);
            }
            return accepted;
        }
    }

    public Checker<? super T> getChecker(Validity validity) {
        Checks.isNotNull(validity, "validity");
        switch (validity) {
        case ERRONEOUS:
            return error;
        case DUBIOUS:
            return warning;
        default:
            return null;
        }
    }

    public boolean hasChecker(Validity validity) {
        Checks.isNotNull(validity, "validity");
        return getChecker(validity) != null;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[value class: ");
        builder.append(getValueClass().getSimpleName());
        if (error != null) {
            builder.append(", error: ");
            builder.append(explainError("'value'"));
        }
        if (warning != null) {
            builder.append(", warning: ");
            builder.append(explainWarning("'value'"));
        }
        builder.append("]");
        return builder.toString();
    }
}