package cdc.validation.checkers;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import cdc.converters.ConversionException;
import cdc.converters.Converter;
import cdc.util.lang.Checks;

/**
 * Extension of Predicate that explains results.
 *
 * @author Damien Carbonne
 *
 * @param <T> The value type.
 */
public interface Checker<T> extends Predicate<T> {

    /**
     * @return The class of tested values.
     */
    public Class<T> getValueClass();

    @Override
    public boolean test(T value);

    public default boolean testRaw(Object value) {
        final T v = getValueClass().cast(value);
        return test(v);
    }

    /**
     * Explains the conditions at which a positive or negative result is obtained.
     *
     * @param result The result to explain.
     * @param arg The argument name to use in explanations.
     * @return A string explaining the conditions leading to {@code result}.
     */
    public String explain(boolean result,
                          String arg);

    public default String explain(String arg) {
        return "expects " + explain(true, arg);
    }

    public default String explain() {
        return explain("value");
    }

    /**
     * Tests a value and explains the result.
     *
     * @param value The tested value.
     * @param arg The argument name to use in explanations.
     * @return A string explaining why {@code value} passes or fails the test.
     *         {@code value} is represented by {@code arg}.
     */
    public default String testAndExplain(T value,
                                         String arg) {
        final boolean result = test(value);
        return explain(result, arg);
    }

    public default String testAndExplainRaw(Object value,
                                            String arg) {
        final boolean result = testRaw(value);
        return explain(result, arg);
    }

    /**
     * Tests a value and explains the result.
     *
     * @param value The tested value.
     * @return A string explaining why {@code value} passes or fails the test.
     *         {@code value} is represented by its default string representation.
     */
    public default String testAndExplain(T value) {
        return testAndExplain(value, Objects.toString(value));
    }

    public default String testAndExplainRaw(Object value) {
        return testAndExplainRaw(value, Objects.toString(value));
    }

    /**
     * @return A checker that is the logical negation of this checker.
     */
    @Override
    public default Checker<T> negate() {
        return new Checker<T>() {
            @Override
            public boolean test(T value) {
                return !Checker.this.test(value);
            }

            @Override
            public Class<T> getValueClass() {
                return Checker.this.getValueClass();
            }

            @Override
            public String explain(boolean result,
                                  String arg) {
                return Checker.this.explain(!result, arg);
            }
        };
    }

    /**
     * Returns a checker that is the logical AND composition of this checker
     * with another one.
     * <p>
     * This checker is evaluated before {@code other}.<br>
     * If this checker evaluates to {@code false}, {@code other} is not
     * evaluated.
     *
     * @param other The other checker.
     * @return A checker that is the logical AND composition of this checker
     *         with {@code other}.
     */
    public default Checker<T> and(Checker<? super T> other) {
        Checks.isNotNull(other, "other");

        return new Checker<T>() {
            @Override
            public boolean test(T value) {
                return Checker.this.test(value) && other.test(value);
            }

            @Override
            public Class<T> getValueClass() {
                return Checker.this.getValueClass();
            }

            @Override
            public String explain(boolean result,
                                  String arg) {
                return Checker.this.explain(result, arg)
                        + (result ? " and " : " or ")
                        + other.explain(result, arg);
            }
        };
    }

    public default <V> Checker<V> cast(Class<V> cls) {
        Checks.isNotNull(cls, "cls");
        if (cls.isAssignableFrom(getValueClass())) {
            @SuppressWarnings("unchecked")
            final Checker<V> tmp = (Checker<V>) this;
            return tmp;
        } else {
            throw new IllegalArgumentException(cls.getCanonicalName()
                    + " not compliant with "
                    + getValueClass().getCanonicalName());
        }
    }

    public default Checker<T> cast(Checker<?> other) {
        Checks.isNotNull(other, "other");
        return other.cast(getValueClass());
    }

    public default Checker<T> andRaw(Checker<?> other) {
        return and(this.cast(other));
    }

    public default Checker<T> orRaw(Checker<?> other) {
        return or(this.cast(other));
    }

    /**
     * Returns a checker that is the logical OR composition of this checker
     * with another one.
     * <p>
     * This checker is evaluated before {@code other}.<br>
     * If this checker evaluates to {@code true}, {@code other} is not evaluated.
     *
     * @param other The other checker.
     * @return A checker that is the logical OR composition of this checker
     *         with {@code other}.
     */
    public default Checker<T> or(Checker<? super T> other) {
        Checks.isNotNull(other, "other");

        return new Checker<T>() {
            @Override
            public boolean test(T value) {
                return Checker.this.test(value) || other.test(value);
            }

            @Override
            public Class<T> getValueClass() {
                return Checker.this.getValueClass();
            }

            @Override
            public String explain(boolean result,
                                  String arg) {
                return Checker.this.explain(result, arg)
                        + (result ? " or " : " and ")
                        + other.explain(result, arg);
            }
        };
    }

    /**
     * Creates a checkers that checks that a value is convertible from a source type
     * to a target type, then matches a checker of the target type.
     *
     * @param <S> The source type.
     * @param converter A converter from a source type to a type
     *            that is compliant with this checker.
     * @return A new checker that first applies the {@code converter},
     *         then this checker.
     */
    public default <S> Checker<S> after(Converter<S, ? extends T> converter) {
        Checks.isNotNull(converter, "converter");

        return after(converter, converter.getSourceClass());
    }

    public default Checker<?> afterRaw(Converter<?, ?> converter) {
        final Converter<?, ? extends T> tmp =
                converter.cast(converter.getSourceClass(), getValueClass());
        return after(tmp);
    }

    /**
     * Creates a checkers that checks that a value is convertible from a source
     * type to a target type, then matches a checker of the target type.
     *
     * @param <S> The source type.
     * @param function A function from a source type to a type
     *            that is compliant with this checker.
     * @param sourceClass The source class.
     * @return A new checker that first applies the {@code function},
     *         then this checker.
     */
    public default <S> Checker<S> after(Function<S, ? extends T> function,
                                        Class<S> sourceClass) {
        Checks.isNotNull(function, "function");

        return new Checker<S>() {
            @Override
            public boolean test(S value) {
                try {
                    final T target = function.apply(value);
                    return Checker.this.test(target);
                } catch (final ConversionException e) {
                    return false;
                }
            }

            @Override
            public Class<S> getValueClass() {
                return sourceClass;
            }

            @Override
            public String explain(boolean result,
                                  String arg) {
                if (result) {
                    return arg + " is convertible to "
                            + Checker.this.getValueClass().getCanonicalName()
                            + " and " + Checker.this.explain(true, arg);
                } else {
                    return arg + " is not convertible to "
                            + Checker.this.getValueClass().getCanonicalName()
                            + " or " + Checker.this.explain(false, arg);
                }
            }
        };
    }

    public default String wrap(String s) {
        return "'" + s + "'";
    }

    /**
     * Creates a checker from a converter.
     * <p>
     * The checker checks that the argument is convertible.
     *
     * @param <S> The source type.
     * @param converter The converter.
     * @return A checker from {@code converter}.
     */
    public static <S> Checker<S> fromConverter(Converter<S, ?> converter) {
        Checks.isNotNull(converter, "converter");

        return CheckerSupport.fromFunctionUnchecked(converter,
                                                    converter.getSourceClass(),
                                                    converter.getTargetClass());
    }

    /**
     * Creates a checker from a function.
     * <p>
     * The checker checks that the argument is accepted (convertible) by the function.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param function The function.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return A checker from {@code function}.
     */
    public static <S, T> Checker<S> fromFunction(Function<S, ?> function,
                                                 Class<S> sourceClass,
                                                 Class<T> targetClass) {
        Checks.isNotNull(function, "function");
        Checks.isNotNull(sourceClass, "sourceClass");
        Checks.isNotNull(targetClass, "targetClass");

        return CheckerSupport.fromFunctionUnchecked(function,
                                                    sourceClass,
                                                    targetClass);
    }
}