package cdc.validation.checkers;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Factories;
import cdc.util.debug.Printable;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;
import cdc.validation.checkers.defaults.HasNoDoubleSpaces;
import cdc.validation.checkers.defaults.HasNoInnerSpaces;
import cdc.validation.checkers.defaults.HasNoOuterSpaces;
import cdc.validation.checkers.defaults.HasNoOuterWhiteSpaces;
import cdc.validation.checkers.defaults.HasNoSpaces;
import cdc.validation.checkers.defaults.IsConvertibleUsing;
import cdc.validation.checkers.defaults.IsExistingDirectory;
import cdc.validation.checkers.defaults.IsExistingFile;
import cdc.validation.checkers.defaults.IsGreaterOrEqual;
import cdc.validation.checkers.defaults.IsGreaterThan;
import cdc.validation.checkers.defaults.IsInRange;
import cdc.validation.checkers.defaults.IsInstanceOf;
import cdc.validation.checkers.defaults.IsLessOrEqual;
import cdc.validation.checkers.defaults.IsLessThan;
import cdc.validation.checkers.defaults.IsNotNull;
import cdc.validation.checkers.defaults.IsNull;
import cdc.validation.checkers.defaults.IsNullOrEmpty;
import cdc.validation.checkers.defaults.IsPattern;
import cdc.validation.checkers.defaults.LengthIsInRange;
import cdc.validation.checkers.defaults.MatchesPattern;

public final class Checkers {
    private static final Logger LOGGER = LogManager.getLogger(Checkers.class);
    private static final Map<String, Checker<?>> MAP = new HashMap<>();
    public static final Printable PRINTER = new Printer();

    static {
        Printables.register(Checkers.class, PRINTER);
    }

    private Checkers() {
    }

    static {
        Factories.register(HasNoDoubleSpaces.FACTORY);
        Factories.register(HasNoInnerSpaces.FACTORY);
        Factories.register(HasNoOuterSpaces.FACTORY);
        Factories.register(HasNoSpaces.FACTORY);
        Factories.register(IsConvertibleUsing.FACTORY);
        Factories.register(IsExistingDirectory.FACTORY);
        Factories.register(IsExistingFile.FACTORY);
        Factories.register(IsGreaterOrEqual.FACTORY);
        Factories.register(IsGreaterThan.FACTORY);
        Factories.register(IsInRange.FACTORY);
        Factories.register(IsInstanceOf.FACTORY);
        Factories.register(IsLessOrEqual.FACTORY);
        Factories.register(IsLessThan.FACTORY);
        Factories.register(IsNotNull.FACTORY);
        Factories.register(IsNull.FACTORY);
        Factories.register(IsNullOrEmpty.FACTORY);
        Factories.register(IsPattern.FACTORY);
        Factories.register(LengthIsInRange.FACTORY);
        Factories.register(MatchesPattern.FACTORY);

        Checkers.register(HasNoDoubleSpaces.INSTANCE, "HasNoDoubleSpaces");
        Checkers.register(HasNoInnerSpaces.INSTANCE, "HasNoInnerSpaces");
        Checkers.register(HasNoOuterSpaces.INSTANCE, "HasNoOuterSpaces");
        Checkers.register(HasNoOuterWhiteSpaces.INSTANCE, "HasNoOuterWhiteSpaces");
        Checkers.register(HasNoSpaces.INSTANCE, "HasNoSpaces");
        Checkers.register(IsConvertibleUsing.STRING_IS_DOUBLE, "IsDouble");
        Checkers.register(IsConvertibleUsing.STRING_IS_FLOAT, "IsFloat");
        Checkers.register(IsConvertibleUsing.STRING_IS_LONG, "IsLong");
        Checkers.register(IsConvertibleUsing.STRING_IS_INTEGER, "IsInteger");
        Checkers.register(IsConvertibleUsing.STRING_IS_SHORT, "IsShort");
        Checkers.register(IsConvertibleUsing.STRING_IS_BYTE, "IsByte");
        Checkers.register(IsConvertibleUsing.STRING_IS_BOOLEAN, "IsBoolean");
        Checkers.register(IsExistingDirectory.INSTANCE, "IsExistingDirectory");
        Checkers.register(IsExistingFile.INSTANCE, "IsExistingFile");
        Checkers.register(IsNotNull.INSTANCE, "IsNotNull");
        Checkers.register(IsNull.INSTANCE, "IsNull");
        Checkers.register(IsNullOrEmpty.INSTANCE, "IsNullOrEmpty");
        Checkers.register(IsPattern.INSTANCE, "IsPattern");
        Checkers.register(MatchesPattern.LETTERS, "Letters");
        Checkers.register(MatchesPattern.DIGITS, "Digits");
        Checkers.register(MatchesPattern.LETTERS_OR_DIGITS, "LettersOrDigits");
        Checkers.register(MatchesPattern.LC_LETTERS, "LowerCaseLetters");
        Checkers.register(MatchesPattern.LC_LETTERS_OR_DIGITS, "LowerCaseLettersOrDigits");
        Checkers.register(MatchesPattern.UC_LETTERS, "UpperCaseLetters");
        Checkers.register(MatchesPattern.UC_LETTERS_OR_DIGITS, "UpperCaseLettersOrDigits");
    }

    public static void elaborate() {
        // Ignore
    }

    /**
     * Register a checker with a name.
     *
     * @param checker The checker.
     * @param name The name.
     * @throws IllegalArgumentException When {@code name} or {@code checker} is invalid.
     */
    public static void register(Checker<?> checker,
                                String name) {
        LOGGER.debug("register({}, {})", name, checker);
        Checks.isNotNullOrEmpty(name, "name");
        Checks.isNotNull(checker, "checker");
        if (MAP.containsKey(name)) {
            throw new IllegalArgumentException("A checker named '" + name + "' is already registered");
        }

        MAP.put(name, checker);
    }

    /**
     * @return A set of registered checkers names.
     */
    public static Set<String> getNames() {
        return MAP.keySet();
    }

    public static boolean hasChecker(String name) {
        return MAP.containsKey(name);
    }

    public static Checker<?> getChecker(String name,
                                        FailureReaction reaction) {
        return NotFoundException.onResult(MAP.get(name),
                                          "No '" + name + "' checker found",
                                          LOGGER,
                                          reaction,
                                          null);
    }

    public static Checker<?> getChecker(String name) {
        return getChecker(name, FailureReaction.FAIL);
    }

    public static <T> Checker<? super T> getChecker(Class<T> valueClass,
                                                    String name,
                                                    FailureReaction reaction) {
        Checks.isNotNull(valueClass, "valueClass");
        @SuppressWarnings("unchecked")
        final Checker<? super T> tmp = (Checker<? super T>) getChecker(name, reaction);
        if (tmp != null && !tmp.getValueClass().isAssignableFrom(valueClass)) {
            throw new IllegalArgumentException("Non compliant class " + valueClass + " with " + tmp.getValueClass());
        }
        return tmp;
    }

    protected static class Printer implements Printable {
        @Override
        public void print(PrintStream out,
                          int level) {
            indent(out, level);
            out.println("Checkers (" + Checkers.getNames().size() + ")");
            for (final String name : CollectionUtils.toSortedList(Checkers.getNames())) {
                final Checker<?> checker = Checkers.getChecker(name);
                indent(out, level + 1);
                out.println(checker.getValueClass());
                indent(out, level + 2);
                out.println("explanation: " + checker.explain());
            }
        }
    }
}