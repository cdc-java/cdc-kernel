package cdc.validation.checkers;

import java.util.function.Function;

import cdc.util.lang.Checks;

final class CheckerSupport {
    static <S> Checker<S> fromFunctionUnchecked(Function<S, ?> function,
                                                Class<S> sourceClass,
                                                Class<?> targetClass) {
        Checks.isNotNull(function, "function");
        Checks.isNotNull(sourceClass, "sourceClass");
        Checks.isNotNull(targetClass, "targetClass");

        return new Checker<S>() {
            @Override
            public boolean test(S value) {
                try {
                    function.apply(value);
                    return true;
                } catch (final Exception e) {
                    return false;
                }
            }

            @Override
            public Class<S> getValueClass() {
                return sourceClass;
            }

            @Override
            public String explain(boolean result,
                                  String arg) {
                if (result) {
                    return wrap(arg) + " is convertible to " + targetClass.getCanonicalName();
                } else {
                    return wrap(arg) + " is not convertible to " + targetClass.getCanonicalName();
                }
            }
        };
    }

    private CheckerSupport() {
    }
}