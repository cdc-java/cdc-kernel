package cdc.validation.checkers.defaults;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class IsPattern extends AbstractStringChecker {
    public static final IsPattern INSTANCE = new IsPattern();

    public static final Factory<IsPattern> FACTORY =
            new AbstractFactory<IsPattern>(IsPattern.class) {
                @Override
                protected IsPattern create(Args args,
                                           FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        if (value == null) {
            return false;
        } else {
            try {
                Pattern.compile(value);
            } catch (final PatternSyntaxException e) {
                return false;
            }
            return true;
        }
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is a valid pattern";
        } else {
            return wrap(arg) + " is not a valid pattern";
        }
    }
}