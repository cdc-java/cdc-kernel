package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public class IsGreaterThan<T extends Comparable<T>> implements Checker<T> {
    private final Class<T> valueClass;
    private final T min;

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = ComparablesSupport.CLASS;
    public static final FormalArg<String> SMIN = ComparablesSupport.MANDATORY_STRING_MIN;
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> CMIN = ComparablesSupport.MANDATORY_COMPARABLE_MIN;
    public static final FormalArgs SFARGS = new FormalArgs(CLASS, SMIN);
    public static final FormalArgs CFARGS = new FormalArgs(CLASS, CMIN);

    @SuppressWarnings("rawtypes")
    public static final Factory<IsGreaterThan> FACTORY =
            new AbstractFactory<IsGreaterThan>(IsGreaterThan.class,
                                               SFARGS,
                                               CFARGS) {

                @Override
                protected IsGreaterThan<?> create(Args args,
                                                  FormalArgs fargs) {
                    if (fargs.equals(SFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable minValue = ComparablesSupport.getMinFromString(args, cls);
                        return new IsGreaterThan<>(cls, minValue);
                    } else if (fargs.equals(CFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable minValue = ComparablesSupport.getMinFromComparable(args, cls);
                        return new IsGreaterThan<>(cls, minValue);
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            };

    public IsGreaterThan(Class<T> valueClass,
                         T min) {
        this.valueClass = valueClass;
        this.min = min;
    }

    public static <T extends Comparable<T>> IsGreaterThan<T> of(Class<T> valueClass,
                                                                T min) {
        return new IsGreaterThan<>(valueClass, min);
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public boolean test(T value) {
        return value != null
                && value.compareTo(min) > 0;
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + ">" + min + ComparablesSupport.valueInfo(arg, this);
        } else {
            return wrap(arg) + "<=" + min + ComparablesSupport.valueInfo(arg, this);
        }
    }

    public T getMin() {
        return min;
    }

    public static IsGreaterThan<String> from(String min) {
        return new IsGreaterThan<>(String.class, min);
    }

    public static IsGreaterThan<Character> from(char min) {
        return new IsGreaterThan<>(Character.class, min);
    }

    public static IsGreaterThan<Double> from(double min) {
        return new IsGreaterThan<>(Double.class, min);
    }

    public static IsGreaterThan<Float> from(float min) {
        return new IsGreaterThan<>(Float.class, min);
    }

    public static IsGreaterThan<Long> from(long min) {
        return new IsGreaterThan<>(Long.class, min);
    }

    public static IsGreaterThan<Integer> from(int min) {
        return new IsGreaterThan<>(Integer.class, min);
    }

    public static IsGreaterThan<Short> from(short min) {
        return new IsGreaterThan<>(Short.class, min);
    }

    public static IsGreaterThan<Byte> from(byte min) {
        return new IsGreaterThan<>(Byte.class, min);
    }
}