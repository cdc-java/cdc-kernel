package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class IsNullOrEmpty extends AbstractStringChecker {
    public static final IsNullOrEmpty INSTANCE = new IsNullOrEmpty();

    public static final Factory<IsNullOrEmpty> FACTORY =
            new AbstractFactory<IsNullOrEmpty>(IsNullOrEmpty.class) {
                @Override
                protected IsNullOrEmpty create(Args args,
                                               FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || value.isEmpty();
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is null or empty";
        } else {
            return wrap(arg) + " is neither null nor empty";
        }
    }
}