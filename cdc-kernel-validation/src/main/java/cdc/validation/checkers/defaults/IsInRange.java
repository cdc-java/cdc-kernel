package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public class IsInRange<T extends Comparable<T>> implements Checker<T> {
    private final Class<T> valueClass;
    private final T min;
    private final T max;

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = ComparablesSupport.CLASS;
    public static final FormalArg<String> SMIN = ComparablesSupport.OPTIONAL_STRING_MIN;
    public static final FormalArg<String> SMAX = ComparablesSupport.OPTIONAL_STRING_MAX;
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> CMIN = ComparablesSupport.OPTIONAL_COMPARABLE_MIN;
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> CMAX = ComparablesSupport.OPTIONAL_COMPARABLE_MAX;
    public static final FormalArgs SFARGS = new FormalArgs(CLASS, SMIN, SMAX);
    public static final FormalArgs CFARGS = new FormalArgs(CLASS, CMIN, CMAX);

    @SuppressWarnings("rawtypes")
    public static final Factory<IsInRange> FACTORY =
            new AbstractFactory<IsInRange>(IsInRange.class,
                                           SFARGS,
                                           CFARGS) {
                @Override
                protected IsInRange<?> create(Args args,
                                              FormalArgs fargs) {
                    if (fargs.equals(SFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable minValue = ComparablesSupport.getMinFromString(args, cls);
                        final Comparable maxValue = ComparablesSupport.getMaxFromString(args, cls);
                        return new IsInRange<>(cls, minValue, maxValue);
                    } else if (fargs.equals(CFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable minValue = ComparablesSupport.getMinFromComparable(args, cls);
                        final Comparable maxValue = ComparablesSupport.getMaxFromComparable(args, cls);
                        return new IsInRange<>(cls, minValue, maxValue);
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            };

    public IsInRange(Class<T> valueClass,
                     T min,
                     T max) {
        if (min == null && max == null) {
            throw new IllegalArgumentException("At least one of min or max must be set.");
        }
        this.valueClass = valueClass;
        this.min = min;
        this.max = max;
    }

    public static <T extends Comparable<T>> IsInRange<T> of(Class<T> valueClass,
                                                            T min,
                                                            T max) {
        return new IsInRange<>(valueClass, min, max);
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public boolean test(T value) {
        return value != null
                && (min == null || value.compareTo(min) >= 0)
                && (max == null || value.compareTo(max) <= 0);
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            if (min != null || max != null) {
                return (min == null ? "" : min + "<=") + wrap(arg) + (max == null ? "" : "<=" + max)
                        + ComparablesSupport.valueInfo(arg, this);
            } else {
                return "empty range";
            }
        } else {
            if (min != null && max != null) {
                return wrap(arg) + "<" + min + " or " + max + "<" + wrap(arg) + ComparablesSupport.valueInfo(arg, this);
            } else if (min != null) {
                return wrap(arg) + "<" + min + ComparablesSupport.valueInfo(arg, this);
            } else if (max != null) {
                return max + "<" + wrap(arg) + ComparablesSupport.valueInfo(arg, this);
            } else {
                return "empty range";
            }
        }
    }

    public T getMin() {
        return min;
    }

    public T getMax() {
        return max;
    }

    public static IsInRange<String> from(String min,
                                         String max) {
        return new IsInRange<>(String.class, min, max);
    }

    public static IsInRange<Character> from(char min,
                                            char max) {
        return new IsInRange<>(Character.class, min, max);
    }

    public static IsInRange<Double> from(double min,
                                         double max) {
        return new IsInRange<>(Double.class, min, max);
    }

    public static IsInRange<Float> from(float min,
                                        float max) {
        return new IsInRange<>(Float.class, min, max);
    }

    public static IsInRange<Long> from(long min,
                                       long max) {
        return new IsInRange<>(Long.class, min, max);
    }

    public static IsInRange<Integer> from(int min,
                                          int max) {
        return new IsInRange<>(Integer.class, min, max);
    }

    public static IsInRange<Short> from(short min,
                                        short max) {
        return new IsInRange<>(Short.class, min, max);
    }

    public static IsInRange<Byte> from(byte min,
                                       byte max) {
        return new IsInRange<>(Byte.class, min, max);
    }
}