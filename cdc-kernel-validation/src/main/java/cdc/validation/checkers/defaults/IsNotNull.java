package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public final class IsNotNull implements Checker<Object> {
    public static final IsNotNull INSTANCE = new IsNotNull();

    public static final Factory<IsNotNull> FACTORY =
            new AbstractFactory<IsNotNull>(IsNotNull.class) {
                @Override
                protected IsNotNull create(Args args,
                                           FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public Class<Object> getValueClass() {
        return Object.class;
    }

    @Override
    public boolean test(Object value) {
        return value != null;
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is not null";
        } else {
            return wrap(arg) + " is null";
        }
    }
}