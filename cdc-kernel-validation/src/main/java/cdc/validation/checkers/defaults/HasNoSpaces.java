package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class HasNoSpaces extends AbstractStringChecker {
    public static final HasNoSpaces INSTANCE = new HasNoSpaces();

    public static final Factory<HasNoSpaces> FACTORY =
            new AbstractFactory<HasNoSpaces>(HasNoSpaces.class) {
                @Override
                protected HasNoSpaces create(Args args,
                                             FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || !value.contains(" ");
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " has no spaces";
        } else {
            return wrap(arg) + " is null or has spaces";
        }
    }
}