package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;
import cdc.io.xml.XmlUtils;

public final class IsXml10String extends AbstractStringChecker {
    public static final IsXml10String INSTANCE = new IsXml10String();

    public static final Factory<IsXml10String> FACTORY =
            new AbstractFactory<IsXml10String>(IsXml10String.class) {
                @Override
                protected IsXml10String create(Args args,
                                               FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || XmlUtils.isValidXml10(value);
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is XML 1.0 String";
        } else {
            return wrap(arg) + " is not XML 1.0 String";
        }
    }
}