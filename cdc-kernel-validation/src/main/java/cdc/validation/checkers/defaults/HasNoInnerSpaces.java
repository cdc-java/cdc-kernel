package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class HasNoInnerSpaces extends AbstractStringChecker {
    public static final HasNoInnerSpaces INSTANCE = new HasNoInnerSpaces();

    public static final Factory<HasNoInnerSpaces> FACTORY =
            new AbstractFactory<HasNoInnerSpaces>(HasNoInnerSpaces.class) {
                @Override
                protected HasNoInnerSpaces create(Args args,
                                                  FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || !value.trim().contains(" ");
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " has no inner spaces";
        } else {
            return wrap(arg) + " is null or has inner spaces";
        }
    }
}