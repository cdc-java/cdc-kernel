package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class HasNoDoubleSpaces extends AbstractStringChecker {
    public static final HasNoDoubleSpaces INSTANCE = new HasNoDoubleSpaces();

    public static final Factory<HasNoDoubleSpaces> FACTORY =
            new AbstractFactory<HasNoDoubleSpaces>(HasNoDoubleSpaces.class) {
                @Override
                protected HasNoDoubleSpaces create(Args args,
                                                   FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || !value.contains("  ");
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " has no double spaces";
        } else {
            return wrap(arg) + " is null or has double spaces";
        }
    }
}