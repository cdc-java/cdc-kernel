package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class HasNoOuterSpaces extends AbstractStringChecker {
    public static final HasNoOuterSpaces INSTANCE = new HasNoOuterSpaces();

    public static final Factory<HasNoOuterSpaces> FACTORY =
            new AbstractFactory<HasNoOuterSpaces>(HasNoOuterSpaces.class) {
                @Override
                protected HasNoOuterSpaces create(Args args,
                                                  FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || value.trim().equals(value);
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " has no outer spaces";
        } else {
            return wrap(arg) + " is null or has outer spaces";
        }
    }
}