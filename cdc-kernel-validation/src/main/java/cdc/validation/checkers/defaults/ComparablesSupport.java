package cdc.validation.checkers.defaults;

import cdc.args.Args;
import cdc.args.FormalArg;
import cdc.args.Necessity;
import cdc.converters.Converters;
import cdc.validation.checkers.Checker;

public final class ComparablesSupport {
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = new FormalArg<>("class", Class.class, Necessity.MANDATORY);
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> MANDATORY_COMPARABLE_MIN =
            new FormalArg<>("min", Comparable.class, Necessity.MANDATORY);
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> OPTIONAL_COMPARABLE_MIN =
            new FormalArg<>("min", Comparable.class, Necessity.OPTIONAL);

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> MANDATORY_COMPARABLE_MAX =
            new FormalArg<>("max", Comparable.class, Necessity.MANDATORY);

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> OPTIONAL_COMPARABLE_MAX =
            new FormalArg<>("max", Comparable.class, Necessity.OPTIONAL);

    public static final FormalArg<String> MANDATORY_STRING_MIN = new FormalArg<>("min", String.class, Necessity.MANDATORY);
    public static final FormalArg<String> MANDATORY_STRING_MAX = new FormalArg<>("max", String.class, Necessity.MANDATORY);

    public static final FormalArg<String> OPTIONAL_STRING_MIN = new FormalArg<>("min", String.class, Necessity.OPTIONAL);
    public static final FormalArg<String> OPTIONAL_STRING_MAX = new FormalArg<>("max", String.class, Necessity.OPTIONAL);

    private ComparablesSupport() {
    }

    @SuppressWarnings("rawtypes")
    static Class<Comparable> getClass(Args args) {
        final Class<?> cls = args.getValue(CLASS);
        if (!Comparable.class.isAssignableFrom(cls)) {
            throw new IllegalArgumentException();
        }
        @SuppressWarnings("unchecked")
        final Class<Comparable> result = (Class<Comparable>) cls;
        return result;
    }

    @SuppressWarnings("rawtypes")
    static Comparable getMinFromComparable(Args args,
                                           Class<Comparable> cls) {
        return Converters.convert(args.getValue(OPTIONAL_COMPARABLE_MIN, null), cls);
    }

    @SuppressWarnings("rawtypes")
    static Comparable getMinFromString(Args args,
                                       Class<Comparable> cls) {
        return Converters.convert(args.getValue(OPTIONAL_STRING_MIN, null), cls);
    }

    @SuppressWarnings("rawtypes")
    static Comparable getMaxFromComparable(Args args,
                                           Class<Comparable> cls) {
        return Converters.convert(args.getValue(OPTIONAL_COMPARABLE_MAX, null), cls);
    }

    @SuppressWarnings("rawtypes")
    static Comparable getMaxFromString(Args args,
                                       Class<Comparable> cls) {
        return Converters.convert(args.getValue(OPTIONAL_STRING_MAX, null), cls);
    }

    public static String valueInfo(String name,
                                   Checker<?> checker) {
        return ", '" + name + "' being a " + checker.getValueClass().getSimpleName();
    }
}