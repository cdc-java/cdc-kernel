package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public class IsLessOrEqual<T extends Comparable<T>> implements Checker<T> {
    private final Class<T> valueClass;
    private final T max;

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = ComparablesSupport.CLASS;
    public static final FormalArg<String> SMAX = ComparablesSupport.MANDATORY_STRING_MAX;
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> CMAX = ComparablesSupport.MANDATORY_COMPARABLE_MAX;
    public static final FormalArgs SFARGS = new FormalArgs(CLASS, SMAX);
    public static final FormalArgs CFARGS = new FormalArgs(CLASS, CMAX);

    @SuppressWarnings("rawtypes")
    public static final Factory<IsLessOrEqual> FACTORY =
            new AbstractFactory<IsLessOrEqual>(IsLessOrEqual.class,
                                               SFARGS,
                                               CFARGS) {
                @Override
                protected IsLessOrEqual<?> create(Args args,
                                                  FormalArgs fargs) {
                    if (fargs.equals(SFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable maxValue = ComparablesSupport.getMaxFromString(args, cls);
                        return new IsLessOrEqual<>(cls, maxValue);
                    } else if (fargs.equals(CFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable maxValue = ComparablesSupport.getMaxFromComparable(args, cls);
                        return new IsLessOrEqual<>(cls, maxValue);
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            };

    public IsLessOrEqual(Class<T> valueClass,
                         T max) {
        this.valueClass = valueClass;
        this.max = max;
    }

    public static <T extends Comparable<T>> IsLessOrEqual<T> of(Class<T> valueClass,
                                                                T min) {
        return new IsLessOrEqual<>(valueClass, min);
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public boolean test(T value) {
        return value != null
                && value.compareTo(max) <= 0;
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + "<=" + max + ComparablesSupport.valueInfo(arg, this);
        } else {
            return wrap(arg) + ">" + max + ComparablesSupport.valueInfo(arg, this);
        }
    }

    public T getMax() {
        return max;
    }

    public static IsLessOrEqual<String> from(String max) {
        return new IsLessOrEqual<>(String.class, max);
    }

    public static IsLessOrEqual<Character> from(char max) {
        return new IsLessOrEqual<>(Character.class, max);
    }

    public static IsLessOrEqual<Double> from(double max) {
        return new IsLessOrEqual<>(Double.class, max);
    }

    public static IsLessOrEqual<Float> from(float max) {
        return new IsLessOrEqual<>(Float.class, max);
    }

    public static IsLessOrEqual<Long> from(long max) {
        return new IsLessOrEqual<>(Long.class, max);
    }

    public static IsLessOrEqual<Integer> from(int max) {
        return new IsLessOrEqual<>(Integer.class, max);
    }

    public static IsLessOrEqual<Short> from(short max) {
        return new IsLessOrEqual<>(Short.class, max);
    }

    public static IsLessOrEqual<Byte> from(byte max) {
        return new IsLessOrEqual<>(Byte.class, max);
    }
}