package cdc.validation.checkers.defaults;

import cdc.validation.checkers.Checker;

public abstract class AbstractStringChecker implements Checker<String> {
    @Override
    public final Class<String> getValueClass() {
        return String.class;
    }
}