package cdc.validation.checkers.defaults;

import java.io.File;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public final class IsExistingFile implements Checker<File> {
    public static final IsExistingFile INSTANCE = new IsExistingFile();

    public static final Factory<IsExistingFile> FACTORY =
            new AbstractFactory<IsExistingFile>(IsExistingFile.class) {
                @Override
                protected IsExistingFile create(Args args,
                                                FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    private IsExistingFile() {
    }

    @Override
    public Class<File> getValueClass() {
        return File.class;
    }

    @Override
    public boolean test(File value) {
        return value != null && value.isFile();
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is an existing file";
        } else {
            return wrap(arg) + " is not an existing file";
        }
    }
}