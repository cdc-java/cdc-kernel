package cdc.validation.checkers.defaults;

import java.util.regex.Pattern;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.util.lang.Checks;

public final class MatchesPattern extends AbstractStringChecker {
    private final Pattern pattern;

    public static final FormalArg<String> PATTERN = new FormalArg<>("pattern", String.class, Necessity.MANDATORY);
    public static final FormalArgs FARGS = new FormalArgs(PATTERN);

    public static final Factory<MatchesPattern> FACTORY =
            new AbstractFactory<MatchesPattern>(MatchesPattern.class, FARGS) {
                @Override
                protected MatchesPattern create(Args args,
                                                FormalArgs fargs) {
                    return new MatchesPattern(args.getValue(PATTERN));
                }
            };

    public static final MatchesPattern LETTERS = new MatchesPattern("[A-Za-z]*");
    public static final MatchesPattern DIGITS = new MatchesPattern("[0-9]*");
    public static final MatchesPattern LETTERS_OR_DIGITS = new MatchesPattern("[A-Za-z0-9]*");
    public static final MatchesPattern UC_LETTERS = new MatchesPattern("[A-Z]*");
    public static final MatchesPattern UC_LETTERS_OR_DIGITS = new MatchesPattern("[A-Z0-9]*");
    public static final MatchesPattern LC_LETTERS = new MatchesPattern("[a-z]*");
    public static final MatchesPattern LC_LETTERS_OR_DIGITS = new MatchesPattern("[a-z0-9]*");
    public static final MatchesPattern ASCII = new MatchesPattern("^\\p{ASCII}*$");
    public static final MatchesPattern HAS_WHITE_SPACES = new MatchesPattern("^.*\\p{Space}.*$");
    public static final MatchesPattern HAS_LEADING_WHITE_SPACES = new MatchesPattern("^\\p{Space}.*$");
    public static final MatchesPattern HAS_TRAILING_WHITE_SPACES = new MatchesPattern("^.*\\p{Space}$");

    public MatchesPattern(Pattern pattern) {
        Checks.isNotNull(pattern, "pattern");
        this.pattern = pattern;
    }

    public MatchesPattern(String regex) {
        this(Pattern.compile(regex));
    }

    public static MatchesPattern of(Pattern pattern) {
        return new MatchesPattern(pattern);
    }

    public static MatchesPattern of(String regex) {
        return new MatchesPattern(regex);
    }

    @Override
    public boolean test(String value) {
        return value != null && pattern.matcher(value).matches();
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " matches " + wrap(pattern.pattern());
        } else {
            return wrap(arg) + " does not match " + wrap(pattern.pattern());
        }
    }

    public Pattern getPattern() {
        return pattern;
    }
}