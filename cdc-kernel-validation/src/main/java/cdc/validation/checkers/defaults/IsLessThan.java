package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public class IsLessThan<T extends Comparable<T>> implements Checker<T> {
    private final Class<T> valueClass;
    private final T max;

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = ComparablesSupport.CLASS;
    public static final FormalArg<String> SMAX = ComparablesSupport.MANDATORY_STRING_MAX;
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> CMAX = ComparablesSupport.MANDATORY_COMPARABLE_MAX;
    public static final FormalArgs SFARGS = new FormalArgs(CLASS, SMAX);
    public static final FormalArgs CFARGS = new FormalArgs(CLASS, CMAX);

    @SuppressWarnings("rawtypes")
    public static final Factory<IsLessThan> FACTORY =
            new AbstractFactory<IsLessThan>(IsLessThan.class,
                                            SFARGS,
                                            CFARGS) {
                @Override
                protected IsLessThan<?> create(Args args,
                                               FormalArgs fargs) {
                    if (fargs.equals(SFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable maxValue = ComparablesSupport.getMaxFromString(args, cls);
                        return new IsLessThan<>(cls, maxValue);
                    } else if (fargs.equals(CFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable maxValue = ComparablesSupport.getMaxFromComparable(args, cls);
                        return new IsLessThan<>(cls, maxValue);
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            };

    public IsLessThan(Class<T> valueClass,
                      T max) {
        this.valueClass = valueClass;
        this.max = max;
    }

    public static <T extends Comparable<T>> IsLessThan<T> of(Class<T> valueClass,
                                                             T min) {
        return new IsLessThan<>(valueClass, min);
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public boolean test(T value) {
        return value != null
                && value.compareTo(max) < 0;
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + "<" + max + ComparablesSupport.valueInfo(arg, this);
        } else {
            return wrap(arg) + ">=" + max + ComparablesSupport.valueInfo(arg, this);
        }
    }

    public T getMax() {
        return max;
    }

    public static IsLessThan<String> from(String max) {
        return new IsLessThan<>(String.class, max);
    }

    public static IsLessThan<Character> from(char max) {
        return new IsLessThan<>(Character.class, max);
    }

    public static IsLessThan<Double> from(double max) {
        return new IsLessThan<>(Double.class, max);
    }

    public static IsLessThan<Float> from(float max) {
        return new IsLessThan<>(Float.class, max);
    }

    public static IsLessThan<Long> from(long max) {
        return new IsLessThan<>(Long.class, max);
    }

    public static IsLessThan<Integer> from(int max) {
        return new IsLessThan<>(Integer.class, max);
    }

    public static IsLessThan<Short> from(short max) {
        return new IsLessThan<>(Short.class, max);
    }

    public static IsLessThan<Byte> from(byte max) {
        return new IsLessThan<>(Byte.class, max);
    }
}