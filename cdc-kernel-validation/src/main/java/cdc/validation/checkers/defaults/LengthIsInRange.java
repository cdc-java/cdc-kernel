package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;

/**
 * Check that the length of string is in a given range.
 *
 * @author Damien Carbonne
 *
 */
public class LengthIsInRange extends AbstractStringChecker {
    private final int min;
    private final int max;

    public static final FormalArg<Integer> MIN = new FormalArg<>("min", Integer.class, Necessity.OPTIONAL);
    public static final FormalArg<Integer> MAX = new FormalArg<>("max", Integer.class, Necessity.OPTIONAL);
    public static final FormalArgs FARGS = new FormalArgs(MIN, MAX);

    public static final Factory<LengthIsInRange> FACTORY =
            new AbstractFactory<LengthIsInRange>(LengthIsInRange.class, FARGS) {
                @Override
                protected LengthIsInRange create(Args args,
                                                 FormalArgs fargs) {
                    return new LengthIsInRange(args.getValue(MIN, 0),
                                               args.getValue(MAX, Integer.MAX_VALUE));
                }
            };

    public LengthIsInRange(int min,
                           int max) {
        this.min = min;
        this.max = max;
    }

    public static LengthIsInRange of(int min,
                                     int max) {
        return new LengthIsInRange(min, max);
    }

    @Override
    public boolean test(String value) {
        return value != null
                && value.length() >= min
                && value.length() <= max;
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " length in [" + min + ".." + max + "]";
        } else {
            return wrap(arg) + " length not in [" + min + ".." + max + "]";
        }
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}