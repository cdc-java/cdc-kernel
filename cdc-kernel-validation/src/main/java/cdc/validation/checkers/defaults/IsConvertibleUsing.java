package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.ConversionException;
import cdc.converters.Converter;
import cdc.converters.Converters;
import cdc.converters.defaults.StringToBoolean;
import cdc.converters.defaults.StringToByte;
import cdc.converters.defaults.StringToDouble;
import cdc.converters.defaults.StringToFloat;
import cdc.converters.defaults.StringToInteger;
import cdc.converters.defaults.StringToLong;
import cdc.converters.defaults.StringToShort;
import cdc.util.lang.Introspection;
import cdc.validation.checkers.Checker;

public class IsConvertibleUsing<T> implements Checker<T> {
    private final Converter<T, ?> converter;

    public static final FormalArg<Converter<?, ?>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);
    public static final FormalArgs FARGS1 = new FormalArgs(CONVERTER);
    public static final FormalArg<String> NAME = new FormalArg<>("converter", String.class, Necessity.MANDATORY);
    public static final FormalArgs FARGS2 = new FormalArgs(NAME);

    @SuppressWarnings("rawtypes")
    public static final Factory<IsConvertibleUsing> FACTORY =
            new AbstractFactory<IsConvertibleUsing>(IsConvertibleUsing.class,
                                                    FARGS1,
                                                    FARGS2) {
                @Override
                protected IsConvertibleUsing create(Args args,
                                                    FormalArgs fargs) {
                    if (fargs.equals(FARGS1)) {
                        final Converter<?, ?> rconverter = args.getValue(CONVERTER);
                        final Converter<?, ?> tmp = Converter.class.cast(rconverter);
                        return new IsConvertibleUsing<>(tmp);
                    } else if (fargs.equals(FARGS2)) {
                        final String name = args.getValue(NAME);
                        final Converter<?, ?> rconverter = Converters.getConverter(name);
                        final Converter<?, ?> tmp = Converter.class.cast(rconverter);
                        return new IsConvertibleUsing<>(tmp);
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            };

    public static final IsConvertibleUsing<String> STRING_IS_DOUBLE = new IsConvertibleUsing<>(StringToDouble.INSTANCE);
    public static final IsConvertibleUsing<String> STRING_IS_FLOAT = new IsConvertibleUsing<>(StringToFloat.INSTANCE);
    public static final IsConvertibleUsing<String> STRING_IS_LONG = new IsConvertibleUsing<>(StringToLong.INSTANCE);
    public static final IsConvertibleUsing<String> STRING_IS_INTEGER = new IsConvertibleUsing<>(StringToInteger.INSTANCE);
    public static final IsConvertibleUsing<String> STRING_IS_SHORT = new IsConvertibleUsing<>(StringToShort.INSTANCE);
    public static final IsConvertibleUsing<String> STRING_IS_BYTE = new IsConvertibleUsing<>(StringToByte.INSTANCE);
    public static final IsConvertibleUsing<String> STRING_IS_BOOLEAN = new IsConvertibleUsing<>(StringToBoolean.INSTANCE);

    public IsConvertibleUsing(Converter<T, ?> converter) {
        this.converter = converter;
    }

    public static <T> IsConvertibleUsing<T> of(Converter<T, ?> converter) {
        return new IsConvertibleUsing<>(converter);
    }

    @Override
    public Class<T> getValueClass() {
        return converter.getSourceClass();
    }

    @Override
    public boolean test(T value) {
        try {
            converter.apply(value);
            return true;
        } catch (final ConversionException e) {
            return false;
        }
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is convertible to " + converter.getTargetClass().getSimpleName();
        } else {
            return wrap(arg) + " is not convertible to " + converter.getTargetClass().getSimpleName();
        }
    }
}