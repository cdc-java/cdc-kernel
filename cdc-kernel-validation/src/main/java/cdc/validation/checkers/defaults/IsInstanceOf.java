package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.util.lang.Checks;
import cdc.validation.checkers.Checker;

public final class IsInstanceOf implements Checker<Object> {
    private final Class<?> cls;

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = new FormalArg<>("class", Class.class, Necessity.MANDATORY);
    public static final FormalArgs FARGS = new FormalArgs(CLASS);

    public static final Factory<IsInstanceOf> FACTORY =
            new AbstractFactory<IsInstanceOf>(IsInstanceOf.class, FARGS) {
                @Override
                protected IsInstanceOf create(Args args,
                                              FormalArgs fargs) {
                    return new IsInstanceOf(args.getValue(CLASS));
                }
            };

    public IsInstanceOf(Class<?> cls) {
        Checks.isNotNull(cls, "cls");
        this.cls = cls;
    }

    public static IsInstanceOf of(Class<?> cls) {
        return new IsInstanceOf(cls);
    }

    @Override
    public Class<Object> getValueClass() {
        return Object.class;
    }

    @Override
    public boolean test(Object value) {
        return cls.isInstance(value);
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is instance of " + cls.getCanonicalName();
        } else {
            return wrap(arg) + " is not instance of " + cls.getCanonicalName();
        }
    }

    public static IsInstanceOf from(Class<?> cls) {
        return new IsInstanceOf(cls);
    }
}