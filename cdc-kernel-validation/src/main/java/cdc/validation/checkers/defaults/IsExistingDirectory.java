package cdc.validation.checkers.defaults;

import java.io.File;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public final class IsExistingDirectory implements Checker<File> {
    public static final IsExistingDirectory INSTANCE = new IsExistingDirectory();

    public static final Factory<IsExistingDirectory> FACTORY =
            new AbstractFactory<IsExistingDirectory>(IsExistingDirectory.class) {
                @Override
                protected IsExistingDirectory create(Args args,
                                                     FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    private IsExistingDirectory() {
    }

    @Override
    public Class<File> getValueClass() {
        return File.class;
    }

    @Override
    public boolean test(File value) {
        return value != null && value.isDirectory();
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " is an existing directory";
        } else {
            return wrap(arg) + " is not an existing directory";
        }
    }
}