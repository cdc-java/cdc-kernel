package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.validation.checkers.Checker;

public class IsGreaterOrEqual<T extends Comparable<T>> implements Checker<T> {
    private final Class<T> valueClass;
    private final T min;

    @SuppressWarnings("rawtypes")
    public static final FormalArg<Class> CLASS = ComparablesSupport.CLASS;
    public static final FormalArg<String> SMIN = ComparablesSupport.MANDATORY_STRING_MIN;
    @SuppressWarnings("rawtypes")
    public static final FormalArg<Comparable> CMIN = ComparablesSupport.MANDATORY_COMPARABLE_MIN;
    public static final FormalArgs SFARGS = new FormalArgs(CLASS, SMIN);
    public static final FormalArgs CFARGS = new FormalArgs(CLASS, CMIN);

    @SuppressWarnings("rawtypes")
    public static final Factory<IsGreaterOrEqual> FACTORY =
            new AbstractFactory<IsGreaterOrEqual>(IsGreaterOrEqual.class, SFARGS, CFARGS) {
                @Override
                protected IsGreaterOrEqual<?> create(Args args,
                                                     FormalArgs fargs) {
                    if (fargs.equals(SFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable minValue = ComparablesSupport.getMinFromString(args, cls);
                        return new IsGreaterOrEqual<>(cls, minValue);
                    } else if (fargs.equals(CFARGS)) {
                        final Class<Comparable> cls = ComparablesSupport.getClass(args);
                        final Comparable minValue = ComparablesSupport.getMinFromComparable(args, cls);
                        return new IsGreaterOrEqual<>(cls, minValue);
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
            };

    public IsGreaterOrEqual(Class<T> valueClass,
                            T min) {
        this.valueClass = valueClass;
        this.min = min;
    }

    public static <T extends Comparable<T>> IsGreaterOrEqual<T> of(Class<T> valueClass,
                                                                   T min) {
        return new IsGreaterOrEqual<>(valueClass, min);
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public boolean test(T value) {
        return value != null
                && value.compareTo(min) >= 0;
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + ">=" + min + ComparablesSupport.valueInfo(arg, this);
        } else {
            return wrap(arg) + "<" + min + ComparablesSupport.valueInfo(arg, this);
        }
    }

    public T getMin() {
        return min;
    }

    public static IsGreaterOrEqual<String> from(String min) {
        return new IsGreaterOrEqual<>(String.class, min);
    }

    public static IsGreaterOrEqual<Character> from(char min) {
        return new IsGreaterOrEqual<>(Character.class, min);
    }

    public static IsGreaterOrEqual<Double> from(double min) {
        return new IsGreaterOrEqual<>(Double.class, min);
    }

    public static IsGreaterOrEqual<Float> from(float min) {
        return new IsGreaterOrEqual<>(Float.class, min);
    }

    public static IsGreaterOrEqual<Long> from(long min) {
        return new IsGreaterOrEqual<>(Long.class, min);
    }

    public static IsGreaterOrEqual<Integer> from(int min) {
        return new IsGreaterOrEqual<>(Integer.class, min);
    }

    public static IsGreaterOrEqual<Short> from(short min) {
        return new IsGreaterOrEqual<>(Short.class, min);
    }

    public static IsGreaterOrEqual<Byte> from(byte min) {
        return new IsGreaterOrEqual<>(Byte.class, min);
    }
}