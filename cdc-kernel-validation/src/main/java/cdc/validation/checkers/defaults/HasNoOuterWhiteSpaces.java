package cdc.validation.checkers.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArgs;

public final class HasNoOuterWhiteSpaces extends AbstractStringChecker {
    public static final HasNoOuterWhiteSpaces INSTANCE = new HasNoOuterWhiteSpaces();

    public static final Factory<HasNoOuterWhiteSpaces> FACTORY =
            new AbstractFactory<HasNoOuterWhiteSpaces>(HasNoOuterWhiteSpaces.class) {
                @Override
                protected HasNoOuterWhiteSpaces create(Args args,
                                                       FormalArgs fargs) {
                    return INSTANCE;
                }
            };

    @Override
    public boolean test(String value) {
        return value == null || value.strip().equals(value);
    }

    @Override
    public String explain(boolean result,
                          String arg) {
        if (result) {
            return wrap(arg) + " has no outer white spaces";
        } else {
            return wrap(arg) + " is null or has outer white spaces";
        }
    }
}