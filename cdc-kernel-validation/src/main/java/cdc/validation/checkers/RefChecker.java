package cdc.validation.checkers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;
import cdc.util.refs.ResolutionException;
import cdc.util.refs.ResolutionStatus;

/**
 * Checker that lazily delegates its task to a named checker.
 * <p>
 * The delegate must be compliant with the type of this checker.
 *
 * @author Damien Carbonne
 *
 * @param <T> The value type.
 */
public final class RefChecker<T> implements Checker<T> {
    private static final Logger LOGGER = LogManager.getLogger(RefChecker.class);
    private final Class<T> valueClass;
    private final String name;
    private ResolutionStatus status = ResolutionStatus.PENDING;
    private Checker<? super T> delegate = null;

    public RefChecker(Class<T> valueClass,
                      String name) {
        this.valueClass = valueClass;
        this.name = name;
    }

    /**
     * @return The delegate name.
     */
    public String getName() {
        return name;
    }

    /**
     * Resolves the delegate.
     * <p>
     * The delegate is searched only the first time this method is called.
     *
     * @param reaction The reaction to adopt if resolution fails.
     */
    public void resolve(FailureReaction reaction) {
        if (status == ResolutionStatus.PENDING) {
            this.delegate = Checkers.getChecker(valueClass, name, FailureReaction.WARN);
            if (this.delegate == null) {
                status = ResolutionStatus.FAILURE;
            } else {
                status = ResolutionStatus.SUCCESS;
            }
        }
        if (status == ResolutionStatus.FAILURE) {
            if (reaction == FailureReaction.FAIL) {
                throw new ResolutionException("Failed to resolve " + name);
            } else if (reaction == FailureReaction.WARN) {
                LOGGER.warn("Failed to resolve {}", name);
            }
        }
    }

    public ResolutionStatus getResolutionStatus() {
        return status;
    }

    /**
     * @return The associated delegate, possibly {@code null} if resolution is not yet done or failed.
     */
    public Checker<? super T> getDelegate() {
        return delegate;
    }

    @Override
    public Class<T> getValueClass() {
        return valueClass;
    }

    @Override
    public boolean test(T value) {
        resolve(FailureReaction.FAIL);
        return delegate.test(value);
    }

    @Override
    public String explain(boolean result,
                          String argName) {
        resolve(FailureReaction.FAIL);
        return delegate.explain(result, argName);
    }
}