package cdc.validation.checkers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;
import cdc.util.refs.ResolutionException;
import cdc.util.refs.ResolutionStatus;

public final class RawRefChecker implements Checker<Object> {
    private static final Logger LOGGER = LogManager.getLogger(RawRefChecker.class);
    private final String name;
    private ResolutionStatus status = ResolutionStatus.PENDING;
    private Checker<Object> delegate = null;

    public RawRefChecker(String name) {
        this.name = name;
    }

    /**
     * @return The delegate name.
     */
    public String getName() {
        return name;
    }

    /**
     * Resolves the delegate.
     * <p>
     * The delegate is searched only the first time this method is called.
     *
     * @param reaction The reaction to adopt if resolution fails.
     */
    public void resolve(FailureReaction reaction) {
        if (status == ResolutionStatus.PENDING) {
            @SuppressWarnings("unchecked")
            final Checker<Object> tmp = (Checker<Object>) Checkers.getChecker(name, FailureReaction.FAIL);
            this.delegate = tmp;
            if (this.delegate == null) {
                status = ResolutionStatus.FAILURE;
            } else {
                status = ResolutionStatus.SUCCESS;
            }
        }
        if (status == ResolutionStatus.FAILURE) {
            if (reaction == FailureReaction.FAIL) {
                throw new ResolutionException("Failed to resolve " + name);
            } else if (reaction == FailureReaction.WARN) {
                LOGGER.warn("Failed to resolve {}", name);
            }
        }
    }

    public ResolutionStatus getResolutionStatus() {
        return status;
    }

    /**
     * @return The associated delegate, possibly {@code null} if resolution is not yet done or failed.
     */
    public Checker<?> getDelegate() {
        return delegate;
    }

    @Override
    public Class<Object> getValueClass() {
        resolve(FailureReaction.FAIL);
        return delegate.getValueClass();
    }

    @Override
    public boolean test(Object value) {
        resolve(FailureReaction.FAIL);
        final Object tmp = delegate.getValueClass().cast(value);
        return delegate.test(tmp);
    }

    @Override
    public String explain(boolean result,
                          String argName) {
        resolve(FailureReaction.FAIL);
        return delegate.explain(result, argName);
    }
}