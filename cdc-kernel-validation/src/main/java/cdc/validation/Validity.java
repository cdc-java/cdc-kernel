package cdc.validation;

import cdc.util.lang.EnumUtils;

/**
 * Enumeration of validities.
 *
 * @author Damien Carbonne
 *
 */
public enum Validity {
    /**
     * The value is valid.
     */
    VALID,

    /**
     * The value is technically valid, but should be changed from a user point of view.
     */
    DUBIOUS,

    /**
     * The value is invalid.
     */
    ERRONEOUS;

    public boolean isValidOrDubious() {
        return this != ERRONEOUS;
    }

    public static Validity worse(Validity value1,
                                 Validity value2) {
        return EnumUtils.max(value1, value2);
    }
}