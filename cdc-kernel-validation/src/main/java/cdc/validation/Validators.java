package cdc.validation;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.debug.Printable;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;

/**
 * Registry of validators.
 *
 * @author Damien Carbonne
 *
 */
public class Validators {
    private static final Logger LOGGER = LogManager.getLogger(Validators.class);
    public static final Printable PRINTER = new Printer();

    static {
        Printables.register(Validators.class, PRINTER);
    }

    private static final Map<String, Validator<?>> MAP = new HashMap<>();

    private Validators() {
    }

    /**
     * Register a validator with a name.
     *
     * @param validator The validator.
     * @param name The name.
     * @throws IllegalArgumentException When {@code validator} or {@code name} is {@code null},
     *             or when a validator with that {@code name} is already registered.
     */
    public static void register(Validator<?> validator,
                                String name) {
        LOGGER.debug("register({}, {})", name, validator);
        Checks.isNotNullOrEmpty(name, "name");
        Checks.isNotNull(validator, "validator");

        if (MAP.containsKey(name)) {
            throw new IllegalArgumentException("A validator named '" + name + "' is already registered");
        }

        MAP.put(name, validator);
    }

    /**
     * @return A set of registered validators names.
     */
    public static Set<String> getNames() {
        return MAP.keySet();
    }

    /**
     * Returns {@code true} if a validator with a name exists.
     *
     * @param name The name.
     * @return {@code true} if a validator named {@code name} exists.
     */
    public static boolean hasValidator(String name) {
        return MAP.containsKey(name);
    }

    /**
     * Returns the validator that has a name.
     *
     * @param name The name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @return The validator named {@code name} or {@code null} or an exception, depending on {@code reaction}.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     */
    public static Validator<?> getValidator(String name,
                                            FailureReaction reaction) {
        return NotFoundException.onResult(MAP.get(name),
                                          "No '" + name + "' checker found",
                                          LOGGER,
                                          reaction,
                                          null);
    }

    /**
     * Returns the validator that has a name.
     *
     * @param name The name.
     * @return The validator named {@code name}.
     * @throws NotFoundException When no validator is found.
     */
    public static Validator<?> getValidator(String name) {
        return getValidator(name, FailureReaction.FAIL);
    }

    /**
     * Returns the validator that has a name and a value type.
     *
     * @param <T> The validator value type.
     * @param valueClass The validator value class.
     * @param name The name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @return the validator named {@code name} or {@code null} or an exception, depending on {@code reaction}.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws IllegalArgumentException When a validator is found but its value type does not match {@code valueClass}.
     */
    public static <T> Validator<? super T> getValidator(Class<T> valueClass,
                                                        String name,
                                                        FailureReaction reaction) {
        Checks.isNotNull(valueClass, "valueClass");
        @SuppressWarnings("unchecked")
        final Validator<? super T> tmp = (Validator<? super T>) getValidator(name, reaction);
        if (tmp != null && !tmp.getValueClass().isAssignableFrom(valueClass)) {
            throw new IllegalArgumentException("Non compliant class " + valueClass + " with " + tmp.getValueClass());
        }
        return tmp;
    }

    /**
     * Returns the validator that has a name and a value type.
     *
     * @param <T> The validator value type.
     * @param valueClass The validator value class.
     * @param name The name.
     * @return the validator named {@code name} or {@code null} or an exception.
     * @throws NotFoundException When no validator is found.
     * @throws IllegalArgumentException When a validator is found but its value type does not match {@code valueClass}.
     */
    public static <T> Validator<? super T> getValidator(Class<T> valueClass,
                                                        String name) {
        return getValidator(valueClass, name, FailureReaction.FAIL);
    }

    public static Validator<? super String> getStringValidator(String name,
                                                               FailureReaction reaction) {
        return getValidator(String.class, name, reaction);
    }

    public static Validator<? super String> getStringValidator(String name) {
        return getValidator(String.class, name);
    }

    /**
     * Validates a value with a validator.
     *
     * @param <T> The value type.
     * @param valueClass The value class.
     * @param validatorName The validator name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @param value The value.
     * @param valueName The name that must be given to {@code value} in messages.
     * @param handler The validation messages handler.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws IllegalArgumentException When a validator is found but its value type does not match {@code valueClass}.
     */
    public static <T> void validate(Class<T> valueClass,
                                    String validatorName,
                                    FailureReaction reaction,
                                    T value,
                                    String valueName,
                                    ValidationHandler handler) {
        final Validator<? super T> validator = getValidator(valueClass, validatorName, reaction);
        if (validator != null) {
            validator.validate(value, valueName, handler);
        }
    }

    /**
     * Validates a value with a validator and fills a list of validation records.
     *
     * @param <T> The value type.
     * @param valueClass The value class.
     * @param validatorName The validator name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @param value The value.
     * @param valueName The name that must be given to {@code value} in messages.
     * @param records The list of validation records that must be filled.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws IllegalArgumentException When a validator is found but its value type does not match {@code valueClass}.
     */
    public static <T> void validate(Class<T> valueClass,
                                    String validatorName,
                                    FailureReaction reaction,
                                    T value,
                                    String valueName,
                                    List<ValidationRecord> records) {
        final Validator<? super T> validator = getValidator(valueClass, validatorName, reaction);
        if (validator != null) {
            validator.validate(value, valueName, records);
        }
    }

    /**
     * Validates a value with a validator and returns a list of validation records.
     *
     * @param <T> The value type.
     * @param valueClass The value class.
     * @param validatorName The validator name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @param value The value.
     * @param valueName The name that must be given to {@code value} in messages.
     * @return A list of validation records, possibly empty.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws IllegalArgumentException When a validator is found but its value type does not match {@code valueClass}.
     */
    public static <T> List<ValidationRecord> validate(Class<T> valueClass,
                                                      String validatorName,
                                                      FailureReaction reaction,
                                                      T value,
                                                      String valueName) {
        final Validator<? super T> validator = getValidator(valueClass, validatorName, reaction);
        final List<ValidationRecord> records = new ArrayList<>();
        if (validator != null) {
            validator.validate(value, valueName, records);
        }
        return records;
    }

    /**
     * Validates a raw value with a validator.
     *
     * @param validatorName The validator name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @param value The value.
     * @param valueName The name that must be given to {@code value} in messages.
     * @param handler The validation messages handler.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws ClassCastException If {@code value} is not {@code null} and cannot be assigned to value type of the found validator.
     */
    public static void validateRaw(String validatorName,
                                   FailureReaction reaction,
                                   Object value,
                                   String valueName,
                                   ValidationHandler handler) {
        final Validator<?> validator = getValidator(validatorName, reaction);
        if (validator != null) {
            validator.validateRaw(value, valueName, handler);
        }
    }

    /**
     * Validates a raw value with a validator and fills a list of validation records.
     *
     * @param validatorName The validator name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @param value The value.
     * @param valueName The name that must be given to {@code value} in messages.
     * @param records The list of validation records that must be filled.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws ClassCastException If {@code value} is not {@code null} and cannot be assigned to value type of the found validator.
     */
    public static void validateRaw(String validatorName,
                                   FailureReaction reaction,
                                   Object value,
                                   String valueName,
                                   List<ValidationRecord> records) {
        final Validator<?> validator = getValidator(validatorName, reaction);
        if (validator != null) {
            validator.validateRaw(value, valueName, records);
        }
    }

    /**
     * Validates a raw value with a validator and returns a list of validation records.
     *
     * @param validatorName The validator name.
     * @param reaction The reaction to adopt if no matching validator is found.
     * @param value The value.
     * @param valueName The name that must be given to {@code value} in messages.
     * @return A list of validation records, possibly empty.
     * @throws NotFoundException When no validator is found and {@code reaction} is {@link FailureReaction#FAIL}.
     * @throws IllegalArgumentException When a validator is found but its value type does not match {@code valueClass}.
     */
    public static List<ValidationRecord> validatRaw(String validatorName,
                                                    FailureReaction reaction,
                                                    Object value,
                                                    String valueName) {
        final Validator<?> validator = getValidator(validatorName, reaction);
        final List<ValidationRecord> records = new ArrayList<>();
        if (validator != null) {
            validator.validateRaw(value, valueName, records);
        }
        return records;
    }

    protected static class Printer implements Printable {
        @Override
        public void print(PrintStream out,
                          int level) {
            indent(out, level);
            out.println("Validators (" + Validators.getNames().size() + ")");
            for (final String name : CollectionUtils.toSortedList(Validators.getNames())) {
                final Validator<?> validator = Validators.getValidator(name);
                indent(out, level + 1);
                out.println(validator.getValueClass());
                indent(out, level + 2);
                final String error = validator.explainError("'value'");
                out.println("error: " + (error == null ? "" : error));
                indent(out, level + 2);
                final String warning = validator.explainWarning("'value'");
                out.println("warning: " + (warning == null ? "" : warning));
            }
        }
    }
}