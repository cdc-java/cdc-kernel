package cdc.kernel.demos;

import java.io.File;
import java.io.IOException;

import cdc.app.AppInfo;
import cdc.app.AppInfoIo;

public class AppInfoDemo {

    private static void foo() {
        throw new IllegalArgumentException("Hello from foo()");
    }

    private static void bar() {
        try {
            foo();
        } catch (final RuntimeException e) {
            throw new IllegalArgumentException("Hello from bar()", e);
        }
    }

    public static void main(String[] args) throws IOException {
        AppInfo.setMainArgs(args);
        try {
            bar();
        } catch (final RuntimeException e) {
            final AppInfo info = AppInfo.builder()
                                        .appProp("Name", "Value")
                                        .stacktrace(e)
                                        .build();
            AppInfoIo.saveAsJson(info, new File("target/app-info-demo.json"));
            info.print(System.out);
        }
    }
}