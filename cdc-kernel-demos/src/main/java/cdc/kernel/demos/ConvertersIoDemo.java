package cdc.kernel.demos;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Factories;
import cdc.converters.ArgsConversion;
import cdc.converters.Converter;
import cdc.converters.Converters;
import cdc.converters.SpecialConverterKind;
import cdc.converters.io.ConvertersIo;
import cdc.io.xml.XmlWriter;
import cdc.util.function.IterableUtils;
import cdc.util.lang.FailureReaction;
import cdc.util.time.Chronometer;

public final class ConvertersIoDemo {
    private static final Logger LOGGER = LogManager.getLogger(ConvertersIoDemo.class);
    protected static boolean useDataLoader = true;

    private ConvertersIoDemo() {
    }

    private static String special(Converter<?, ?> converter) {
        return (Converters.isSpecialConverter(converter, SpecialConverterKind.DEFAULT) ? "D" : " ")
                + (Converters.isSpecialConverter(converter, SpecialConverterKind.DEFAULT_OR_SINGLE) ? "S" : " ");

    }

    public static void main(String[] args) throws IOException {
        Converters.elaborate();
        Factories.setConverter(ArgsConversion::convert);
        final Chronometer chrono = new Chronometer();
        chrono.start();
        if (useDataLoader) {
            final ConvertersIo.DataLoader loader = new ConvertersIo.DataLoader(FailureReaction.WARN);
            loader.loadXml("src/test/resources/converters.xml");
        } else {
            final ConvertersIo.StAXLoader loader = new ConvertersIo.StAXLoader(FailureReaction.WARN);
            loader.load("src/test/resources/converters.xml");
        }
        chrono.suspend();
        LOGGER.info("elapsed: {}", chrono);

        LOGGER.info("===================================================");
        for (final String name : IterableUtils.toSortedList(Converters.getNames())) {
            if (name.startsWith("Demo")) {
                final Converter<?, ?> converter = Converters.getConverter(name);
                LOGGER.info("{} {}: {}", special(converter), name, converter);
                final StringWriter buffer = new StringWriter();
                final XmlWriter writer = new XmlWriter(buffer);
                writer.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML,
                                  XmlWriter.Feature.PRETTY_PRINT);
                ConvertersIo.write(writer, converter);
                LOGGER.info("converter:\n{}", buffer.toString());
            }
        }
    }
}