package cdc.kernel.demos;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

import cdc.args.Factories;
import cdc.args.Factory;
import cdc.converters.ArgsConversion;
import cdc.converters.Converters;
import cdc.informers.Informer;
import cdc.informers.Informers;
import cdc.informers.StringConversionInformer;
import cdc.informers.io.InformersIo;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;
import cdc.util.strings.StringConversion;

public final class InformersIoDemo {
    private static final Logger LOGGER = LogManager.getLogger(InformersIoDemo.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    protected static class A {
        // Ignore
    }

    protected static class AInformer implements StringConversionInformer<A> {
        public static final AInformer INSTANCE = new AInformer();
        public static final Factory<AInformer> FACTORY = Factory.singleton(INSTANCE);

        private AInformer() {
        }

        @Override
        public Class<A> getObjectClass() {
            return A.class;
        }

        @Override
        public String toString(A object) {
            return null;
        }

        @Override
        public A fromString(String s) {
            return null;
        }
    }

    protected static class B extends A {
        // Ignore
    }

    protected static class DemoInformer implements StringConversionInformer<Integer> {
        public static final DemoInformer INSTANCE = new DemoInformer();
        public static final Factory<DemoInformer> FACTORY = Factory.singleton(INSTANCE);

        private DemoInformer() {
        }

        @Override
        public Class<Integer> getObjectClass() {
            return Integer.class;
        }

        @Override
        public String toString(Integer object) {
            return StringConversion.asString(object);
        }

        @Override
        public Integer fromString(String s) {
            return StringConversion.asOptionalInt(s);
        }
    }

    private InformersIoDemo() {
    }

    private static <O, T extends Informer<O>> void show(Class<O> objectClass,
                                                        Class<T> topic) {
        LOGGER.info("show({}, {}):", objectClass.getSimpleName(), topic.getSimpleName());
        try {
            final T informer = Informers.getInformer(objectClass,
                                                     topic,
                                                     FailureReaction.DEFAULT);
            LOGGER.info("   exact: {}", Informer.toString(informer));
        } catch (final RuntimeException e) {
            LOGGER.info("   exact: NONE");
        }
        try {
            final T informer = Informers.getBestInformer(objectClass, topic, FailureReaction.DEFAULT);
            LOGGER.info("   best: {}", Informer.toString(informer));
        } catch (final RuntimeException e) {
            LOGGER.info("   best: NONE");
        }
    }

    private static void convert(Object object) {
        try {
            final String s = StringConversionInformer.Service.toString(object);
            LOGGER.info("convert({}): {}", object, s);
        } catch (final RuntimeException e) {
            LOGGER.info("convert({}): FAILED", object);
        }
    }

    public static void main(String[] args) throws IOException {
        Converters.elaborate();
        Factories.setConverter(ArgsConversion::convert);

        final InformersIo.DataLoader loader = new InformersIo.DataLoader(FailureReaction.WARN);
        loader.loadXml("src/test/resources/informers.xml");

        Informers.register(AInformer.INSTANCE);

        Informers.PRINTER.print(OUT);

        show(Integer.class, Introspection.uncheckedCast(StringConversionInformer.class));
        show(Float.class, Introspection.uncheckedCast(StringConversionInformer.class));
        show(A.class, Introspection.uncheckedCast(StringConversionInformer.class));
        show(B.class, Introspection.uncheckedCast(StringConversionInformer.class));

        convert(10);
        convert(10.0);
    }
}