package cdc.args;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class ArgsTest {
    private static final Logger LOGGER = LogManager.getLogger(ArgsTest.class);
    private static final String AAA = "AAA";
    private static final String BBB = "BBB";
    private static final String CCC = "CCC";
    private static final String ARG0 = "arg0";
    private static final String ARG1 = "arg1";
    private static final String ARG2 = "arg2";

    private static final Arg OBJ_ARG0 = new Arg(ARG0, 10);
    private static final Arg STR_ARG0 = new Arg(ARG0, "Hello");

    private static final FormalArg<Integer> OINT_MAN_FARG0 = new FormalArg<>(ARG0, Integer.class, Necessity.MANDATORY);
    private static final FormalArg<Integer> PINT_MAN_FARG0 = new FormalArg<>(ARG0, int.class, Necessity.MANDATORY);

    private static final FormalArg<Object> OBJ_MAN_FARG0 = new FormalArg<>(ARG0, Object.class, Necessity.MANDATORY);
    private static final FormalArg<Object> OBJ_OPT_FARG0 = new FormalArg<>(ARG0, Object.class, Necessity.OPTIONAL);
    private static final FormalArg<Object> OBJ_MAN_FARG1 = new FormalArg<>(ARG1, Object.class, Necessity.MANDATORY);
    private static final FormalArg<Object> OBJ_OPT_FARG1 = new FormalArg<>(ARG1, Object.class, Necessity.OPTIONAL);
    private static final FormalArg<String> STR_MAN_FARG0 = new FormalArg<>(ARG0, String.class, Necessity.MANDATORY);
    private static final FormalArg<String> STR_OPT_FARG0 = new FormalArg<>(ARG0, String.class, Necessity.OPTIONAL);
    private static final FormalArg<String> STR_MAN_FARG1 = new FormalArg<>(ARG1, String.class, Necessity.MANDATORY);
    private static final FormalArg<String> STR_OPT_FARG1 = new FormalArg<>(ARG1, String.class, Necessity.OPTIONAL);
    private static final FormalArgs FARGS = new FormalArgs(STR_MAN_FARG0, STR_OPT_FARG1);

    @Test
    void testConstructors() {
        Args args = new Args();

        assertTrue(args.isEmpty());
        assertEquals(null, args.getValue(ARG0));
        assertEquals(AAA, args.getValue(ARG0, AAA));
        assertEquals(null, args.getValue(ARG0, String.class));
        assertEquals(AAA, args.getValue(ARG0, String.class, AAA));

        args = Args.builder()
                   .arg(ARG0, BBB)
                   .build();
        assertFalse(args.isEmpty());
        assertEquals(1, args.size());
        assertEquals(BBB, args.getValue(ARG0));
        assertEquals(BBB, args.getValue(ARG0, AAA));
        assertEquals(null, args.getValue(ARG2));
        assertEquals(null, args.getValue(ARG2, String.class));
        assertEquals(AAA, args.getValue(ARG2, String.class, AAA));

        args = Args.builder()
                   .arg(ARG0, BBB)
                   .arg(ARG0, CCC)
                   .arg(ARG1, BBB)
                   .build();
        assertEquals(2, args.size());
        assertEquals(CCC, args.getValue(ARG0));
        assertEquals(CCC, args.getValue(ARG0, String.class));
        assertEquals(CCC, args.getValue(ARG0, String.class, AAA));
        assertEquals(BBB, args.getValue(ARG1));
        assertEquals(BBB, args.getValue(ARG1, String.class));
        assertEquals(BBB, args.getValue(ARG1, String.class, AAA));
        assertEquals(null, args.getValue(ARG2));
        assertEquals(null, args.getValue(ARG2, String.class));
        assertEquals(AAA, args.getValue(ARG2, String.class, AAA));
        assertTrue(args.hasArg(ARG0));
        assertFalse(args.hasArg(ARG2));

        args = Args.builder()
                   .arg(ARG0, BBB)
                   .arg(ARG0, CCC)
                   .arg(ARG1, BBB)
                   .arg(ARG0, null)
                   .build();
        assertEquals(2, args.size());
        assertEquals(null, args.getValue(ARG0));

        final Args args0 = new Args(args.getArgs());
        assertEquals(args, args0);

        final Args args1 = new Args();
        assertTrue(args1.isEmpty());
        // final Args args2 = new Args(args);
        // assertEquals(args2, args);

        final Args args2 = new Args(args1.getArgs());
        assertEquals(args1, args2);
    }

    @Test
    void testFormalAccessors() {
        Args args = Args.builder().build();
        assertEquals(null, args.getValue(STR_MAN_FARG0));
        assertEquals("ZZZ", args.getValue(STR_MAN_FARG0, "ZZZ"));

        args = Args.builder().arg(STR_MAN_FARG0, "Hello").build();
        assertEquals("Hello", args.getValue(STR_MAN_FARG0));
        assertEquals("Hello", args.getValue(STR_MAN_FARG0, "AAA"));
        assertTrue(args.hasArgWithMatchingName(STR_MAN_FARG0));
        assertThrows(IllegalArgumentException.class, () -> {
            Args.NO_ARGS.hasArgWithMatchingName((FormalArg<?>) null);
        });
    }

    @Test
    void testStaticMethods() {
        Args args = new Args();

        assertEquals(AAA, Args.NO_ARGS.getValue(STR_MAN_FARG0, AAA));
        assertEquals(AAA, args.getValue(STR_MAN_FARG0, AAA));

        assertTrue(Args.NO_ARGS.isLooselyCompliantWith(FormalArgs.NO_FARGS));
        assertTrue(Args.NO_ARGS.isStrictlyCompliantWith(FormalArgs.NO_FARGS));

        assertTrue(args.isLooselyCompliantWith(FormalArgs.NO_FARGS));
        assertTrue(args.isStrictlyCompliantWith(FormalArgs.NO_FARGS));

        assertFalse(Args.NO_ARGS.isLooselyCompliantWith(FARGS));
        assertFalse(Args.NO_ARGS.isStrictlyCompliantWith(FARGS));

        args = Args.builder()
                   .arg(ARG0, 10)
                   .build();

        assertTrue(args.isLooselyCompliantWith(FormalArgs.NO_FARGS));
        assertFalse(args.isStrictlyCompliantWith(FormalArgs.NO_FARGS));

        assertFalse(args.isLooselyCompliantWith(FARGS));
    }

    @Test
    void testBasic() {
        Args args0 = new Args();
        Args args1 = new Args();
        assertEquals(args0, args0);
        assertNotEquals(args0, null);
        assertNotEquals(args0, "Hello");
        assertEquals(args0, args1);

        args0 = Args.builder()
                    .arg(ARG0, AAA)
                    .arg(ARG1, BBB)
                    .build();
        assertNotEquals(args0, args1);

        args1 = Args.builder()
                    .arg(ARG0, AAA)
                    .arg(ARG1, BBB)
                    .build();
        assertEquals(args0, args1);
        assertEquals(ARG0, args1.getNames().get(0));
        assertEquals(ARG1, args1.getNames().get(1));

        args1 = Args.builder()
                    .arg(ARG1, BBB)
                    .arg(ARG0, AAA)
                    .build();
        assertEquals(args0, args1);
        assertEquals(ARG1, args1.getNames().get(0));
        assertEquals(ARG0, args1.getNames().get(1));

        LOGGER.debug("{} {}", args0, args0.hashCode());
        args0 = Args.builder()
                    .arg(ARG0, "XXX")
                    .arg(ARG1, "XXX")
                    .build();
        LOGGER.debug("{} {}", args0, args0.hashCode());
    }

    @Test
    void testBuilder() {
        final Args args0 = Args.builder()
                               .arg("arg0", null)
                               .arg("arg1", null)
                               .arg("arg0", "Hello")
                               .build();
        assertEquals("arg0", args0.getNames().get((0)));
        assertEquals("arg1", args0.getNames().get((1)));
        assertEquals("Hello", args0.getValue("arg0"));
        assertEquals(null, args0.getValue("arg1"));
    }

    private static void testCompliance(Arg arg,
                                       FormalArg<?> farg,
                                       Strictness strictness,
                                       boolean expected) {
        final Args args = Args.builder().arg(arg).build();
        final FormalArgs fargs = new FormalArgs(farg);

        assertEquals(expected, args.isCompliantWith(fargs, strictness));
    }

    @Test
    void testCompliance() {
        assertTrue(Args.NO_ARGS.isLooselyCompliantWith(FormalArgs.NO_FARGS));
        assertTrue(Args.NO_ARGS.isStrictlyCompliantWith(FormalArgs.NO_FARGS));
        assertFalse(Args.NO_ARGS.isLooselyCompliantWith(FARGS));
        assertFalse(Args.NO_ARGS.isStrictlyCompliantWith(FARGS));

        testCompliance(OBJ_ARG0, OBJ_MAN_FARG0, Strictness.LOOSE, true);
        testCompliance(OBJ_ARG0, OBJ_MAN_FARG0, Strictness.STRICT, true);
        testCompliance(OBJ_ARG0, OBJ_OPT_FARG0, Strictness.LOOSE, true);
        testCompliance(OBJ_ARG0, OBJ_OPT_FARG0, Strictness.STRICT, true);

        testCompliance(OBJ_ARG0, STR_MAN_FARG0, Strictness.LOOSE, false);
        testCompliance(OBJ_ARG0, STR_MAN_FARG0, Strictness.STRICT, false);
        testCompliance(OBJ_ARG0, STR_OPT_FARG0, Strictness.LOOSE, false);
        testCompliance(OBJ_ARG0, STR_OPT_FARG0, Strictness.STRICT, false);

        testCompliance(STR_ARG0, OBJ_MAN_FARG0, Strictness.LOOSE, true);
        testCompliance(STR_ARG0, OBJ_MAN_FARG0, Strictness.STRICT, true);
        testCompliance(STR_ARG0, OBJ_OPT_FARG0, Strictness.LOOSE, true);
        testCompliance(STR_ARG0, OBJ_OPT_FARG0, Strictness.STRICT, true);

        testCompliance(STR_ARG0, STR_MAN_FARG0, Strictness.LOOSE, true);
        testCompliance(STR_ARG0, STR_MAN_FARG0, Strictness.STRICT, true);
        testCompliance(STR_ARG0, STR_OPT_FARG0, Strictness.LOOSE, true);
        testCompliance(STR_ARG0, STR_OPT_FARG0, Strictness.STRICT, true);

        testCompliance(OBJ_ARG0, OBJ_MAN_FARG1, Strictness.LOOSE, false);
        testCompliance(OBJ_ARG0, OBJ_MAN_FARG1, Strictness.STRICT, false);
        testCompliance(OBJ_ARG0, OBJ_OPT_FARG1, Strictness.LOOSE, true);
        testCompliance(OBJ_ARG0, OBJ_OPT_FARG1, Strictness.STRICT, false);

        testCompliance(OBJ_ARG0, STR_MAN_FARG1, Strictness.LOOSE, false);
        testCompliance(OBJ_ARG0, STR_MAN_FARG1, Strictness.STRICT, false);
        testCompliance(OBJ_ARG0, STR_OPT_FARG1, Strictness.LOOSE, true);
        testCompliance(OBJ_ARG0, STR_OPT_FARG1, Strictness.STRICT, false);

        testCompliance(STR_ARG0, OBJ_MAN_FARG1, Strictness.LOOSE, false);
        testCompliance(STR_ARG0, OBJ_MAN_FARG1, Strictness.STRICT, false);
        testCompliance(STR_ARG0, OBJ_OPT_FARG1, Strictness.LOOSE, true);
        testCompliance(STR_ARG0, OBJ_OPT_FARG1, Strictness.STRICT, false);

        testCompliance(STR_ARG0, STR_MAN_FARG1, Strictness.LOOSE, false);
        testCompliance(STR_ARG0, STR_MAN_FARG1, Strictness.STRICT, false);
        testCompliance(STR_ARG0, STR_OPT_FARG1, Strictness.LOOSE, true);
        testCompliance(STR_ARG0, STR_OPT_FARG1, Strictness.STRICT, false);

        final Args args = Args.builder()
                              .arg(STR_MAN_FARG0, "Hello")
                              .build();
        assertTrue(args.isLooselyCompliantWith(FARGS));
        assertTrue(args.isStrictlyCompliantWith(FARGS));

        assertTrue(Args.NO_ARGS.isCompliantWith(FormalArgs.NO_FARGS, Strictness.LOOSE));
        assertTrue(Args.NO_ARGS.isCompliantWith(FormalArgs.NO_FARGS, Strictness.STRICT));
    }

    @Test
    void testWrappedCompliance() {
        testCompliance(OBJ_ARG0, OINT_MAN_FARG0, Strictness.LOOSE, true);
        testCompliance(OBJ_ARG0, OINT_MAN_FARG0, Strictness.STRICT, true);
        testCompliance(OBJ_ARG0, PINT_MAN_FARG0, Strictness.LOOSE, true);
        testCompliance(OBJ_ARG0, PINT_MAN_FARG0, Strictness.STRICT, true);
    }

    @Test
    void testSet() {
        final Args argsA = Args.builder()
                               .arg("arg0", 10)
                               .arg("arg1", "Hello")
                               .arg("arg2", null)
                               .build();
        LOGGER.debug("argsA: {}", argsA);
        final Args argsAA = argsA.set(Args.NO_ARGS);
        assertEquals(argsA, argsAA);
        final Args argsAB = Args.NO_ARGS.set(argsA);
        assertEquals(argsA, argsAB);

        final Args argsB = Args.builder()
                               .arg("arg0", 20)
                               .arg("arg2", 2.0)
                               .arg("arg3", 'a')
                               .build();
        LOGGER.debug("argsB: {}", argsB);

        final Args argsC = argsA.set(argsB);
        LOGGER.debug("argsC: {}", argsC);
        assertEquals(4, argsC.size());
        assertEquals(20, argsC.getValue("arg0"));
        assertEquals("Hello", argsC.getValue("arg1"));
        assertEquals(2.0, argsC.getValue("arg2"));
        assertEquals('a', argsC.getValue("arg3"));
    }

    @Test
    void testNoArg() {
        assertEquals(0, Args.NO_ARGS.size());
        assertTrue(Args.NO_ARGS.isEmpty());
        assertEquals(null, Args.NO_ARGS.getValue("Hello"));
        assertEquals("World", Args.NO_ARGS.getValue("Hello", "World"));
    }

    @Test
    void testObject() {
        final Args args = Args.builder()
                              .arg("Hello", null)
                              .build();
        assertTrue(args.hasArg("Hello"));
        assertEquals(null, args.getValue("Hello"));
        assertEquals(null, args.getValue("Hello", "World"));
    }

    @Test
    void testClass() {
        final Args args = Args.builder()
                              .arg("Hello", 10)
                              .build();
        assertTrue(args.hasArg("Hello"));
        assertEquals(10, (int) args.getValue("Hello", Integer.class));
        assertEquals(null, args.getValue("World", Integer.class));
        assertEquals(20, (int) args.getValue("World", Integer.class, 20));
    }

    @Test
    void testGetArg() {
        final Args args = Args.builder()
                              .arg("arg0", 10)
                              .arg("arg1", "Hello")
                              .arg("arg2", null)
                              .build();
        assertEquals(new Arg("arg0", 10), args.getArg("arg0"));
        assertEquals(null, args.getArg("ARG0"));
    }

    @Test
    void testGetArgs() {
        final Args args = Args.builder()
                              .arg("arg0", 10)
                              .arg("arg1", "Hello")
                              .arg("arg2", null)
                              .build();
        assertEquals(3, args.getArgs().size());
    }

    @Test
    void testGetValue() {
        final FormalArg<Integer> farg = new FormalArg<>("x", int.class, Necessity.MANDATORY);
        LOGGER.info("farg: {}", farg);

        final Args args = Args.builder().arg(farg, 10).build();
        LOGGER.info("args: {}", args);
        final int x = args.getValue(farg);
        LOGGER.info("x: {}", x);

        assertTrue(true);
    }

    @Test
    void testBug5() {
        assertFalse(Args.builder().arg("a", "").build().isEmptyOrAllValuesAreNull());
        assertTrue(Args.builder().arg("a", null).build().isEmptyOrAllValuesAreNull());
        assertFalse(Args.builder().arg("a", null).arg("b", "").build().isEmptyOrAllValuesAreNull());
    }
}