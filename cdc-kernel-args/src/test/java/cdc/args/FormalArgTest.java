package cdc.args;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class FormalArgTest {
    private static final Logger LOGGER = LogManager.getLogger(FormalArgTest.class);

    @Test
    void testConstructor() {
        final FormalArg<String> farg0 = new FormalArg<>("Arg1", String.class, Necessity.MANDATORY);
        assertEquals("Arg1", farg0.getName());
        assertEquals(String.class, farg0.getType());
        assertEquals(Necessity.MANDATORY, farg0.getNecessity());
        assertTrue(farg0.isMandatory());
        assertFalse(farg0.isOptional());

        final FormalArg<String> farg1 = new FormalArg<>("Arg1", String.class, Necessity.OPTIONAL);
        assertFalse(farg1.isMandatory());
        assertTrue(farg1.isOptional());

        final FormalArg<String> farg2 = new FormalArg<>("Arg1", String.class);
        assertTrue(farg2.isMandatory());
        assertFalse(farg2.isOptional());
    }

    @Test
    void testIsValidName() {
        assertTrue(FormalArg.isValidName("Hello"));
        assertTrue(FormalArg.isValidName("Hello world"));
        assertFalse(FormalArg.isValidName(null));
    }

    @Test
    void testCheckName() {
        assertThrows(IllegalArgumentException.class, () -> {
            FormalArg.checkName(null);
        });
        FormalArg.checkName("");
        FormalArg.checkName("   aaa aaa   ");
    }

    @Test
    void testIsCompliantWith() {
        final FormalArg<Object> farg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        assertTrue(farg0.isCompliantWith("Hello"));
        assertTrue(farg0.isCompliantWith(10));
        assertFalse(farg0.isCompliantWith(null));
        final FormalArg<String> farg1 = new FormalArg<>("Arg1", String.class, Necessity.OPTIONAL);
        assertTrue(farg1.isCompliantWith("Hello"));
        assertFalse(farg1.isCompliantWith(10));
        assertTrue(farg1.isCompliantWith(null));
        final FormalArg<Float> farg2 = new FormalArg<>("Arg2", float.class, Necessity.OPTIONAL);
        assertTrue(farg2.isCompliantWith(10.0f));
        final FormalArg<Float> farg3 = new FormalArg<>("Arg2", Float.class, Necessity.OPTIONAL);
        assertTrue(farg3.isCompliantWith(10.0f));
    }

    @Test
    void testIsWeakerThan() {
        final FormalArg<Object> marg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        final FormalArg<Object> oarg0 = new FormalArg<>("Arg0", Object.class, Necessity.OPTIONAL);
        final FormalArg<String> marg1 = new FormalArg<>("Arg0", String.class, Necessity.MANDATORY);
        final FormalArg<String> oarg1 = new FormalArg<>("Arg0", String.class, Necessity.OPTIONAL);
        assertTrue(marg0.isWeakerThan(marg0));
        assertFalse(marg0.isWeakerThan(oarg0));
        assertTrue(oarg0.isWeakerThan(marg0));
        assertTrue(oarg0.isWeakerThan(oarg0));

        assertTrue(marg0.isWeakerThan(marg1));
        assertFalse(marg0.isWeakerThan(oarg1));
        assertTrue(oarg0.isWeakerThan(marg1));
        assertTrue(oarg0.isWeakerThan(oarg1));

        assertTrue(marg1.isWeakerThan(marg1));
        assertFalse(marg1.isWeakerThan(oarg1));
        assertTrue(oarg1.isWeakerThan(marg1));
        assertTrue(oarg1.isWeakerThan(oarg1));

        assertFalse(marg1.isWeakerThan(marg0));
        assertFalse(marg1.isWeakerThan(oarg0));
        assertFalse(oarg1.isWeakerThan(marg0));
        assertFalse(oarg1.isWeakerThan(oarg0));

        assertFalse(oarg0.isWeakerThan(null));
    }

    @Test
    void testIsWeakerThanWrap() {
        final FormalArg<Double> margD = new FormalArg<>("Arg0", Double.class, Necessity.MANDATORY);
        final FormalArg<Double> oargD = new FormalArg<>("Arg0", Double.class, Necessity.OPTIONAL);
        final FormalArg<Double> margd = new FormalArg<>("Arg0", double.class, Necessity.MANDATORY);
        final FormalArg<Double> oargd = new FormalArg<>("Arg0", double.class, Necessity.OPTIONAL);

        assertTrue(margD.isWeakerThan(margD));
        assertFalse(margD.isWeakerThan(oargD));
        assertTrue(margD.isWeakerThan(margd));
        assertFalse(margD.isWeakerThan(oargd));

        assertTrue(margd.isWeakerThan(margD));
        assertFalse(margd.isWeakerThan(oargD));
        assertTrue(margd.isWeakerThan(margd));
        assertFalse(margd.isWeakerThan(oargd));

        assertTrue(oargD.isWeakerThan(margD));
        assertTrue(oargD.isWeakerThan(oargD));
        assertTrue(oargD.isWeakerThan(margd));
        assertTrue(oargD.isWeakerThan(oargd));

        assertTrue(oargd.isWeakerThan(margD));
        assertTrue(oargd.isWeakerThan(oargD));
        assertTrue(oargd.isWeakerThan(margd));
        assertTrue(oargd.isWeakerThan(oargd));
    }

    @Test
    void testMatchesName() {
        final FormalArg<Object> farg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        assertTrue(farg0.matchesName("Arg0"));
        assertFalse(farg0.matchesName("arg0"));
    }

    @Test
    void testHasWeakerType() {
        final FormalArg<Object> farg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        assertTrue(farg0.hasWeakerType(Object.class));
        assertTrue(farg0.hasWeakerType(String.class));
        assertTrue(farg0.hasWeakerType(Float.class));
        assertTrue(farg0.hasWeakerType(float.class));
        final FormalArg<String> farg1 = new FormalArg<>("Arg1", String.class, Necessity.MANDATORY);
        assertFalse(farg1.hasWeakerType(Object.class));
        assertTrue(farg1.hasWeakerType(String.class));
        final FormalArg<Float> farg2 = new FormalArg<>("Arg2", float.class, Necessity.MANDATORY);
        assertTrue(farg2.hasWeakerType(float.class));
        assertTrue(farg2.hasWeakerType(Float.class));
    }

    @Test
    void testHasWeakerNecessity() {
        final FormalArg<Object> farg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        assertTrue(farg0.hasWeakerNecessity(Necessity.MANDATORY));
        assertFalse(farg0.hasWeakerNecessity(Necessity.OPTIONAL));
        final FormalArg<Object> farg1 = new FormalArg<>("Arg0", Object.class, Necessity.OPTIONAL);
        assertTrue(farg1.hasWeakerNecessity(Necessity.MANDATORY));
        assertTrue(farg1.hasWeakerNecessity(Necessity.OPTIONAL));
    }

    @Test
    void testEquals() {
        final FormalArg<Object> farg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        final FormalArg<Object> farg1 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        final FormalArg<Object> farg2 = new FormalArg<>("Arg0", Object.class, Necessity.OPTIONAL);
        assertTrue(farg0.equals(farg0));
        assertTrue(farg0.equals(farg1));
        assertFalse(farg0.equals(farg2));
        assertFalse(farg0.equals(null));
    }

    @Test
    void testBasic() {
        final FormalArg<Object> farg0 = new FormalArg<>("Arg0", Object.class, Necessity.MANDATORY);
        LOGGER.debug("{} {}", farg0, farg0.hashCode());
        assertTrue(true);
    }

    @Test
    void testMerge() {
        final FormalArg<Number> optionalNumber = new FormalArg<>("number", Number.class, Necessity.OPTIONAL);
        final FormalArg<Number> mandatoryNumber = new FormalArg<>("number", Number.class, Necessity.MANDATORY);
        final FormalArg<Integer> optionalInteger = new FormalArg<>("number", Integer.class, Necessity.OPTIONAL);
        final FormalArg<Integer> mandatoryInteger = new FormalArg<>("number", Integer.class, Necessity.MANDATORY);
        final FormalArg<String> mandatoryString = new FormalArg<>("number", String.class, Necessity.MANDATORY);
        final FormalArg<Float> optionalFloat = new FormalArg<>("float", Float.class, Necessity.OPTIONAL);

        assertEquals(null, FormalArg.merge(null, null, Necessity.MANDATORY));
        assertEquals(mandatoryNumber, FormalArg.merge(mandatoryNumber, null, Necessity.MANDATORY));
        assertEquals(mandatoryNumber, FormalArg.merge(null, mandatoryNumber, Necessity.MANDATORY));
        assertEquals(mandatoryNumber, FormalArg.merge(mandatoryNumber, null, Necessity.OPTIONAL));

        assertEquals(mandatoryNumber, FormalArg.merge(null, mandatoryNumber, Necessity.OPTIONAL));
        assertEquals(optionalNumber, FormalArg.merge(optionalNumber, null, Necessity.MANDATORY));
        assertEquals(optionalNumber, FormalArg.merge(null, optionalNumber, Necessity.MANDATORY));
        assertEquals(optionalNumber, FormalArg.merge(optionalNumber, null, Necessity.OPTIONAL));
        assertEquals(optionalNumber, FormalArg.merge(null, optionalNumber, Necessity.OPTIONAL));

        assertEquals(mandatoryNumber, FormalArg.merge(mandatoryNumber, mandatoryNumber, Necessity.OPTIONAL));
        assertEquals(mandatoryNumber, FormalArg.merge(mandatoryNumber, mandatoryNumber, Necessity.MANDATORY));
        assertEquals(optionalNumber, FormalArg.merge(optionalNumber, optionalNumber, Necessity.OPTIONAL));
        assertEquals(optionalNumber, FormalArg.merge(optionalNumber, optionalNumber, Necessity.MANDATORY));

        assertEquals(optionalNumber, FormalArg.merge(mandatoryNumber, optionalNumber, Necessity.OPTIONAL));
        assertEquals(optionalNumber, FormalArg.merge(optionalNumber, mandatoryNumber, Necessity.OPTIONAL));
        assertEquals(mandatoryNumber, FormalArg.merge(mandatoryNumber, optionalNumber, Necessity.MANDATORY));
        assertEquals(mandatoryNumber, FormalArg.merge(optionalNumber, mandatoryNumber, Necessity.MANDATORY));

        assertEquals(mandatoryInteger, FormalArg.merge(mandatoryNumber, mandatoryInteger, Necessity.OPTIONAL));
        assertEquals(optionalInteger, FormalArg.merge(mandatoryNumber, optionalInteger, Necessity.OPTIONAL));
        assertEquals(mandatoryInteger, FormalArg.merge(mandatoryNumber, mandatoryInteger, Necessity.MANDATORY));
        assertEquals(mandatoryInteger, FormalArg.merge(mandatoryNumber, optionalInteger, Necessity.MANDATORY));

        assertThrows(IllegalArgumentException.class, () -> {
            FormalArg.merge(mandatoryNumber, mandatoryString, Necessity.MANDATORY);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            FormalArg.merge(optionalFloat, optionalInteger, Necessity.MANDATORY);
        });
    }
}