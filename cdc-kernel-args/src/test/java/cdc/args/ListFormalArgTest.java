package cdc.args;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class ListFormalArgTest {
    private static final String NAME = "name";

    @Test
    void test() {
        final ListFormalArg<String> farg = new ListFormalArg<>(NAME, String.class);
        assertEquals(NAME, farg.getName());
        assertEquals(List.class, farg.getType());
        assertEquals(String.class, farg.getElementType());
        assertEquals(Necessity.MANDATORY, farg.getNecessity());
    }
}