package cdc.args;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class FormalArgsTest {
    private static final Logger LOGGER = LogManager.getLogger(FormalArgsTest.class);
    private static final FormalArg<Integer> IMARG0 = new FormalArg<>("arg0", Integer.class, Necessity.MANDATORY);
    private static final FormalArg<Integer> PIMARG0 = new FormalArg<>("arg0", int.class, Necessity.MANDATORY);
    // private static final FormalArg<Integer> IOARG0 = new FormalArg<>("arg0", Integer.class, Necessity.OPTIONAL);
    private static final FormalArg<String> SMARG0 = new FormalArg<>("arg0", String.class, Necessity.MANDATORY);
    private static final FormalArg<String> SOARG0 = new FormalArg<>("arg0", String.class, Necessity.OPTIONAL);
    private static final FormalArg<Object> OMARG0 = new FormalArg<>("arg0", Object.class, Necessity.MANDATORY);
    private static final FormalArg<Object> OOARG0 = new FormalArg<>("arg0", Object.class, Necessity.OPTIONAL);
    private static final FormalArg<Object> OMARG1 = new FormalArg<>("arg1", Object.class, Necessity.MANDATORY);
    private static final FormalArg<Object> OOARG1 = new FormalArg<>("arg1", Object.class, Necessity.OPTIONAL);

    @Test
    void testConstructors() {
        final FormalArgs fargs0 = new FormalArgs();
        assertEquals(0, fargs0.size());

        final FormalArgs fargs1 = new FormalArgs(SMARG0, OOARG1);
        assertEquals(2, fargs1.size());
        assertEquals(SMARG0, fargs1.getArg(0));
        assertEquals(OOARG1, fargs1.getArg(1));

        assertEquals(0, fargs1.getArgIndex("arg0"));
        assertEquals(1, fargs1.getArgIndex("arg1"));
        assertEquals(-1, fargs1.getArgIndex("arg2"));
        assertEquals(-1, fargs1.getArgIndex("Arg0"));

        final FormalArgs m = fargs1.getArgs(Necessity.MANDATORY);
        assertEquals(1, m.size());
    }

    @Test
    void testConstructors2() {
        final FormalArgs fargs0 = new FormalArgs();
        assertEquals(0, fargs0.size());
        final FormalArgs fargs1 = new FormalArgs(SMARG0, OOARG1);
        assertEquals(2, fargs1.size());
        final FormalArgs fargs00 = new FormalArgs(fargs0, fargs0);
        assertEquals(0, fargs00.size());
        assertThrows(IllegalArgumentException.class, () -> {
            new FormalArgs(null, null);
        });
        final FormalArgs fargs01 = new FormalArgs(fargs0, fargs1);
        assertEquals(2, fargs01.size());
        final FormalArgs fargs10 = new FormalArgs(fargs1, fargs0);
        assertEquals(2, fargs10.size());
        assertThrows(IllegalArgumentException.class, () -> {
            new FormalArgs(fargs1, fargs1);
        });
    }

    @Test
    void testEquals() {
        assertEquals(new FormalArgs(), new FormalArgs());
        assertEquals(new FormalArgs(SMARG0), new FormalArgs(SMARG0));
        assertNotEquals(new FormalArgs(SMARG0), new FormalArgs(SOARG0));
        assertNotEquals(new FormalArgs(SMARG0), null);
        final FormalArgs fargs = new FormalArgs(SMARG0);
        assertEquals(fargs, fargs);
    }

    @Test
    void testAccepts() {
        assertTrue(FormalArgs.NO_FARGS.accepts(FormalArgs.NO_FARGS));
        assertTrue(FormalArgs.NO_FARGS.accepts(new FormalArgs(SMARG0)));
        assertFalse(new FormalArgs(SMARG0).accepts(FormalArgs.NO_FARGS));

        assertTrue(new FormalArgs(SOARG0).accepts(FormalArgs.NO_FARGS));
        assertTrue(new FormalArgs(SOARG0).accepts(new FormalArgs(SOARG0)));
        assertTrue(new FormalArgs(SOARG0).accepts(new FormalArgs(SMARG0)));
        assertTrue(new FormalArgs(SOARG0).accepts(new FormalArgs(SMARG0, OMARG1)));
        assertTrue(new FormalArgs(SOARG0).accepts(new FormalArgs(SMARG0, OOARG1)));
        assertTrue(new FormalArgs(SOARG0).accepts(new FormalArgs(SOARG0, OMARG1)));
        assertTrue(new FormalArgs(SOARG0).accepts(new FormalArgs(SOARG0, OOARG1)));

        assertFalse(new FormalArgs(SMARG0).accepts(FormalArgs.NO_FARGS));
        assertTrue(new FormalArgs(SMARG0).accepts(new FormalArgs(SOARG0)));
        assertTrue(new FormalArgs(SMARG0).accepts(new FormalArgs(SMARG0)));
        assertTrue(new FormalArgs(SMARG0).accepts(new FormalArgs(SMARG0, OMARG1)));
        assertTrue(new FormalArgs(SMARG0).accepts(new FormalArgs(SMARG0, OOARG1)));
        assertTrue(new FormalArgs(SMARG0).accepts(new FormalArgs(SOARG0, OMARG1)));
        assertTrue(new FormalArgs(SMARG0).accepts(new FormalArgs(SOARG0, OOARG1)));
    }

    @Test
    void testAcceptsWrap() {
        assertFalse(new FormalArgs(IMARG0).accepts(FormalArgs.NO_FARGS));
        assertTrue(new FormalArgs(IMARG0).accepts(new FormalArgs(IMARG0)));
        assertTrue(new FormalArgs(IMARG0).accepts(new FormalArgs(PIMARG0)));
        assertTrue(new FormalArgs(PIMARG0).accepts(new FormalArgs(PIMARG0)));
        assertTrue(new FormalArgs(PIMARG0).accepts(new FormalArgs(IMARG0)));
    }

    private static void testMerge(FormalArgs left,
                                  FormalArgs right,
                                  FormalArg<?> expected) {
        final FormalArgs fargs = FormalArgs.merge(left, right);
        assertEquals(1, fargs.size());
        assertEquals(expected, fargs.getArg(expected.getName()));
    }

    @Test
    void testMerge() {
        testMerge(new FormalArgs(SMARG0), FormalArgs.NO_FARGS, SMARG0);
        testMerge(FormalArgs.NO_FARGS, new FormalArgs(SMARG0), SMARG0);

        testMerge(new FormalArgs(SMARG0), new FormalArgs(SMARG0), SMARG0);
        testMerge(new FormalArgs(SMARG0), new FormalArgs(SOARG0), SMARG0);
        testMerge(new FormalArgs(SOARG0), new FormalArgs(SMARG0), SMARG0);
        testMerge(new FormalArgs(SOARG0), new FormalArgs(SOARG0), SOARG0);

        testMerge(new FormalArgs(SMARG0), new FormalArgs(OMARG0), SMARG0);
        testMerge(new FormalArgs(SMARG0), new FormalArgs(OOARG0), SMARG0);
        testMerge(new FormalArgs(SOARG0), new FormalArgs(OMARG0), SMARG0);
        testMerge(new FormalArgs(SOARG0), new FormalArgs(OOARG0), SOARG0);

        testMerge(new FormalArgs(OMARG0), new FormalArgs(SMARG0), SMARG0);
        testMerge(new FormalArgs(OOARG0), new FormalArgs(SMARG0), SMARG0);
        testMerge(new FormalArgs(OMARG0), new FormalArgs(SOARG0), SMARG0);
        testMerge(new FormalArgs(OOARG0), new FormalArgs(SOARG0), SOARG0);
    }

    private static void testInvalidMerge(FormalArgs left,
                                         FormalArgs right) {
        FormalArgs.merge(left, right);
    }

    @Test
    void testInvalidMerge() {
        assertThrows(IllegalArgumentException.class, () -> {
            testInvalidMerge(new FormalArgs(IMARG0), new FormalArgs(SMARG0));
        });
    }

    @Test
    void testBasic() {
        final FormalArgs fargs1 = new FormalArgs(SMARG0, OOARG1);
        LOGGER.debug("{} {}", fargs1, fargs1.hashCode());
    }

    @Test
    void testBuilder() {
        final FormalArgs fargs1 = FormalArgs.builder().build();
        assertTrue(fargs1.isEmpty());

        final FormalArgs fargs2 = FormalArgs.builder()
                                            .add("Hello", String.class, Necessity.MANDATORY)
                                            .build();
        assertEquals(1, fargs2.size());

        final FormalArgs fargs3 = FormalArgs.builder()
                                            .add("Hello", String.class, Necessity.MANDATORY)
                                            .clear()
                                            .build();
        assertTrue(fargs3.isEmpty());
    }

    @Test
    void testHasArg() {
        assertFalse(FormalArgs.NO_FARGS.hasArgs(Necessity.MANDATORY));
        assertFalse(FormalArgs.NO_FARGS.hasArgs(Necessity.OPTIONAL));
        final FormalArgs fargs = FormalArgs.builder()
                                           .add("Hello", String.class, Necessity.MANDATORY)
                                           .build();
        assertTrue(fargs.hasArgs(Necessity.MANDATORY));
        assertFalse(fargs.hasArgs(Necessity.OPTIONAL));
    }
}