package cdc.args;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ArgTest {
    private static final String NAME = "name";
    private static final String VALUE = "value";
    private static final String FOO = "foo";

    @Test
    void testEquals() {
        final Arg arg1 = new Arg(NAME, VALUE);
        final Arg arg2 = new Arg(NAME, VALUE);
        final Arg arg3 = new Arg(NAME, FOO);
        final Arg arg4 = new Arg(FOO, VALUE);
        assertEquals(NAME, arg1.getName());
        assertEquals(VALUE, arg1.getValue());
        assertEquals(arg1, arg1);
        assertEquals(arg1, arg2);
        assertNotEquals(arg1, "Hello");
        assertNotEquals(arg1, arg3);
        assertNotEquals(arg3, arg4);
    }

    @Test
    void testConstructors() {
        final Arg arg0 = new Arg(NAME, VALUE);
        assertEquals(NAME, arg0.getName());

        final FormalArg<Integer> farg = new FormalArg<>(NAME, Integer.class, Necessity.OPTIONAL);
        final Arg arg1 = new Arg(farg, null);
        assertEquals(NAME, arg1.getName());
    }

    @Test
    void testClassAccessors() {
        final Arg arg0 = new Arg(NAME, null);
        assertEquals(null, arg0.getValue(String.class));
        assertEquals(VALUE, arg0.getValue(String.class, VALUE));

        final Arg arg1 = new Arg(NAME, VALUE);
        assertEquals(VALUE, arg1.getValue(String.class));
        assertEquals(VALUE, arg1.getValue(String.class, FOO));
    }

    @Test
    void testObjectAccessors() {
        final Arg arg0 = new Arg(NAME, null);
        assertEquals(null, arg0.getValue());
        assertEquals(null, arg0.getValue((Object) null));
        assertEquals(VALUE, arg0.getValue(VALUE));

        final Arg arg1 = new Arg(NAME, VALUE);
        assertEquals(VALUE, arg1.getValue());
        assertEquals(VALUE, arg1.getValue((Object) null));
        assertEquals(VALUE, arg1.getValue(FOO));
    }

    @Test
    void testIsCompliantWithFormalArg() {
        final Arg arg0 = new Arg(NAME, null);
        final Arg arg1 = new Arg(NAME, VALUE);
        final FormalArg<String> snm = new FormalArg<>(NAME, String.class, Necessity.MANDATORY);
        final FormalArg<String> sno = new FormalArg<>(NAME, String.class, Necessity.OPTIONAL);
        final FormalArg<String> sfm = new FormalArg<>(FOO, String.class, Necessity.MANDATORY);
        final FormalArg<String> sfo = new FormalArg<>(FOO, String.class, Necessity.OPTIONAL);
        final FormalArg<Integer> inm = new FormalArg<>(NAME, Integer.class, Necessity.MANDATORY);
        final FormalArg<Integer> ino = new FormalArg<>(NAME, Integer.class, Necessity.OPTIONAL);
        assertFalse(arg0.isCompliantWith(snm));
        assertTrue(arg0.isCompliantWith(sno));
        assertFalse(arg0.isCompliantWith(sfm));
        assertFalse(arg0.isCompliantWith(sfo));
        assertFalse(arg0.isCompliantWith(inm));
        assertTrue(arg0.isCompliantWith(ino));

        assertTrue(arg1.isCompliantWith(snm));
        assertTrue(arg1.isCompliantWith(sno));
        assertFalse(arg1.isCompliantWith(sfm));
        assertFalse(arg1.isCompliantWith(sfo));
        assertFalse(arg1.isCompliantWith(inm));
        assertFalse(arg1.isCompliantWith(ino));
    }

    @Test
    void testIsCompliantWithFormalArgs() {
        final Arg arg0 = new Arg(NAME, null);
        final Arg arg1 = new Arg(NAME, VALUE);
        final FormalArgs fargs0 = new FormalArgs(new FormalArg<>(NAME, String.class, Necessity.MANDATORY));
        assertFalse(arg0.isCompliantWith(fargs0));
        assertTrue(arg1.isCompliantWith(fargs0));
        final FormalArgs fargs1 = new FormalArgs(new FormalArg<>(NAME, String.class, Necessity.OPTIONAL));
        assertTrue(arg0.isCompliantWith(fargs1));
        assertTrue(arg1.isCompliantWith(fargs1));
    }

    void testFormalConstructor() {
        final FormalArg<Integer> farg1 = new FormalArg<>("name", Integer.class, Necessity.MANDATORY);
        final FormalArg<Integer> farg2 = new FormalArg<>("name", Integer.class, Necessity.OPTIONAL);
        final Arg arg1 = new Arg(farg1, 10);
        assertEquals(10, (int) arg1.getValue(Integer.class));

        final Arg arg2 = new Arg(farg2, 10);
        assertEquals(10, (int) arg2.getValue(Integer.class));
        final Arg arg3 = new Arg(farg2, null);
        assertEquals(null, arg3.getValue());

        assertThrows(IllegalArgumentException.class, () -> {
            new Arg(farg1, null);
        });
    }
}