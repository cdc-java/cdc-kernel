package cdc.args;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import org.junit.jupiter.api.Test;

class SetFormalArgTest {
    private static final String NAME = "name";

    @Test
    void test() {
        final SetFormalArg<String> farg = new SetFormalArg<>(NAME, String.class);
        assertEquals(NAME, farg.getName());
        assertEquals(Set.class, farg.getType());
        assertEquals(String.class, farg.getElementType());
        assertEquals(Necessity.MANDATORY, farg.getNecessity());
    }
}