package cdc.args;

import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Class used to describe a formal argument.
 * <p>
 * A formal argument has a name, a type and may be optional or mandatory.
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of the formal argument.
 */
public class FormalArg<T> {
    /**
     * Name of the argument.
     */
    private final String name;

    /**
     * Type of the argument.
     */
    private final Class<T> type;

    /**
     * Necessity of the argument.
     */
    private final Necessity necessity;

    private final String description;

    /**
     * Creates a formal argument.
     *
     * @param name The formal argument name.
     * @param type The formal argument type.
     * @param necessity The formal argument necessity.
     * @param description The formal argument description.
     * @throws IllegalArgumentException when {@code name} is invalid,
     *             or when {@code type} or {@code necessity} is {@code null}.
     */
    private FormalArg(String name,
                      Class<T> type,
                      Necessity necessity,
                      String description) {
        checkName(name);
        Checks.isNotNull(type, "type");
        Checks.isNotNull(necessity, "necessity");

        this.name = name;
        this.type = type;
        this.necessity = necessity;
        this.description = description;
    }

    /**
     * Creates a formal argument.
     *
     * @param name The formal argument name.
     * @param type The formal argument type.
     * @param necessity The formal argument necessity.
     * @throws IllegalArgumentException when {@code name} is invalid,
     *             or when {@code type} or {@code necessity} is {@code null}.
     */
    public FormalArg(String name,
                     Class<T> type,
                     Necessity necessity) {
        this(name,
             type,
             necessity,
             null);
    }

    /**
     * Creates a MANDATORY formal argument.
     *
     * @param name The formal argument name.
     * @param type The formal argument type.
     * @throws IllegalArgumentException when {@code name} is invalid,
     *             or when {@code type} is {@code null}.
     */
    public FormalArg(String name,
                     Class<T> type) {
        this(name, type, Necessity.MANDATORY);
    }

    /**
     * Returns true when a string can be used as a valid name.
     * <p>
     * Rules are very relaxed: only {@code null} names are forbidden.<br>
     * Stronger rules should be applied.
     *
     * @param s The string
     * @return True when {@code s} is a valid name.
     */
    public static boolean isValidName(String s) {
        return s != null;
    }

    /**
     * Checks a name.
     *
     * @param name The name
     * @throws IllegalArgumentException when {@code name} is invalid.
     */
    public static void checkName(String name) {
        if (!isValidName(name)) {
            throw new IllegalArgumentException("Invalid name: '" + name + "'");
        }
    }

    /**
     * @return The formal argument name.
     */
    public final String getName() {
        return name;
    }

    /**
     * @return The formal argument type.
     */
    public final Class<T> getType() {
        return type;
    }

    /**
     * @return The formal argument description.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * @return The formal argument wrapped type.
     */
    public final Class<T> getWrappedType() {
        return Introspection.wrap(type);
    }

    /**
     * @return The formal argument necessity.
     */
    public final Necessity getNecessity() {
        return necessity;
    }

    /**
     * @return {@code true} if this formal argument is mandatory.
     */
    public final boolean isMandatory() {
        return necessity == Necessity.MANDATORY;
    }

    /**
     * @return {@code true} if this formal argument is optional.
     */
    public final boolean isOptional() {
        return necessity == Necessity.OPTIONAL;
    }

    /**
     * Returns whether a value is compliant with this formal argument.
     * <p>
     * If {@code value} is null, then this formal argument must be optional.<br>
     * Otherwise, {@code value} must be assignable to {@code wrappedType} of the formal argument.
     *
     * @param value The value to check.
     * @return Whether {@code value} is compliant with this formal argument.
     */
    public final boolean isCompliantWith(Object value) {
        return value == null ? isOptional() : getWrappedType().isInstance(value);
    }

    /**
     * Returns {@code true} if the name of this formal argument equals another name.
     *
     * @param other The other name.
     * @return {@code true} if the name of this formal argument equals {@code other}.
     */
    public final boolean matchesName(String other) {
        return name.equals(other);
    }

    /**
     * Returns {@code true} if the (wrapped) type of this formal argument can be assigned by another type.
     *
     * @param other The other type.
     * @return {@code true} if the (wrapped) type of this formal argument can be assigned by  {@code other}.
     */
    public final boolean hasWeakerType(Class<?> other) {
        return getWrappedType().isAssignableFrom(Introspection.wrap(other));
    }

    /**
     * Returns {@code true} if the necessity of this formal argument is weaker than (or equal to) another necessity.
     *
     * @param other The other necessity.
     * @return {@code true} if the necessity of this formal argument is weaker than (or equal to) {@code other}.
     */

    public final boolean hasWeakerNecessity(Necessity other) {
        return necessity.isWeakerThan(other);
    }

    /**
     * Returns {@code true} if this formal argument is weaker than another one.
     * <p>
     * This is the case when any argument that is compliant with {@code other} is
     * compliant with this formal argument.
     * <p>
     * It must have:
     * <ul>
     * <li>The same name.
     * <li>A weaker necessity.
     * <li>A weaker (more general) type.
     * </ul>
     *
     * @param other The other formal argument.
     * @return {@code true} if this formal argument is weaker than {@code other}.
     */
    public final boolean isWeakerThan(FormalArg<?> other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else {
            return matchesName(other.name)
                    && hasWeakerType(other.type)
                    && hasWeakerNecessity(other.necessity);
        }
    }

    /**
     * Returns the most specialized class of 2 classes.
     *
     * @param cls1 The first class.
     * @param cls2 The second class.
     * @return The most specialized class.
     * @throws IllegalArgumentException When there is no inheritance hierarchy between {@code cls1} and {@code cls2}.
     */
    private static Class<?> mergeMostSpecialized(Class<?> cls1,
                                                 Class<?> cls2) {
        final Class<?> result = Introspection.mostSpecialized(cls1, cls2);
        if (result == null) {
            throw new IllegalArgumentException("Non compliant types: " + cls1.getSimpleName() + " " + cls2.getSimpleName());
        } else {
            return result;
        }
    }

    /**
     * Merges 2 formal arguments.
     * <p>
     * This is possible if one of them is {@code null}, or they have same name and compliant types.<br>
     * In that case, if necessity are different, the resulting necessity is MANDATORY.<br>
     * When types are compliant, the most specialized one is chosen.
     * <p>
     * For example:
     * <ul>
     * <li>{@code merge(null, null) = null}
     * <li>{@code merge(null, w) = x}
     * <li>{@code merge(x, null) = x}
     * <li>{@code merge((name1, ...), (name2, ...)) throws IllegalArgumentException}
     * <li>{@code merge((name, String.class, OPTIONAL), (name, Object, OPTIONAL)) = (name, String.class, OPTIONAL)}
     * <li>{@code merge((name, String.class, OPTIONAL), (name, Object, MANDATORY)) = (name, String.class, MANDATORY)}
     * <li>{@code merge((name, String.class, OPTIONAL), (name, Integer, OPTIONAL)) = throws IllegalArgumentException}
     * </ul>
     *
     * @param farg1 The first formal argument (possibly {@code null}).
     * @param farg2 The second formal argument (possibly {@code null}).
     * @param privileged The privileged necessity.
     * @return The merge of {@code farg1} and {@code farg2}.
     * @throws IllegalArgumentException When merge is impossible (names conflict or types conflict).
     */
    public static FormalArg<?> merge(FormalArg<?> farg1,
                                     FormalArg<?> farg2,
                                     Necessity privileged) {
        if (farg1 == null) {
            return farg2;
        } else if (farg2 == null) {
            return farg1;
        } else if (!farg1.name.equals(farg2.name)) {
            throw new IllegalArgumentException("Non compliant names '" + farg1.name + "' '" + farg2.name + "'");
        } else {
            if (farg1.getNecessity() == farg2.getNecessity()) {
                return new FormalArg<>(farg1.getName(),
                                       mergeMostSpecialized(farg1.getType(), farg2.getType()),
                                       farg1.getNecessity());
            } else {
                return new FormalArg<>(farg1.getName(),
                                       mergeMostSpecialized(farg1.getType(), farg2.getType()),
                                       privileged);
            }
        }
    }

    @Override
    public int hashCode() {
        return name.hashCode()
                + type.hashCode()
                + necessity.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof FormalArg)) {
            return false;
        }
        final FormalArg<?> o = (FormalArg<?>) other;
        return name.equals(o.name)
                && type.equals(o.type)
                && necessity.equals(o.necessity);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(name);
        builder.append(" => ");
        builder.append(type.getSimpleName());
        builder.append(" ");
        builder.append(necessity);
        builder.append("]");
        return builder.toString();
    }

    public static <T> Builder<T> builder(Class<T> type) {
        return new Builder<>(type);
    }

    public static class Builder<T> {
        private String name;
        private final Class<T> type;
        private Necessity necessity = Necessity.MANDATORY;
        private String description = null;

        protected Builder(Class<T> type) {
            this.type = type;
        }

        public Builder<T> name(String name) {
            this.name = name;
            return this;
        }

        public Builder<T> necessity(Necessity necessity) {
            this.necessity = necessity;
            return this;
        }

        public Builder<T> description(String description) {
            this.description = description;
            return this;
        }

        public FormalArg<T> build() {
            return new FormalArg<>(name,
                                   type,
                                   necessity,
                                   description);
        }
    }
}