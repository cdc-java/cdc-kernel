package cdc.args;

import java.util.List;

/**
 * Interface dedicated to objects creation.
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of created objects.
 */
public interface Factory<T> {
    /**
     * @return The class of built objects.
     */
    public Class<T> getObjectClass();

    /**
     * @return The meta data associated to this factory.
     */
    public default Args getMeta() {
        return Args.NO_ARGS;
    }

    /**
     * Returns a list of supported creation arguments.
     * <p>
     * This list must not be empty.<br>
     * If creation does not need any arguments, a list containing FormalArgs.NO_FARGS should be returned.
     *
     * @return A list of supported creation arguments.
     */
    public List<FormalArgs> getCreationFormalArgsList();

    /**
     * Creates a {@code T} instance.
     *
     * @param args The creation arguments.
     * @return A new {@code T} instance.
     * @throws IllegalArgumentException When {@code args} is not valid.
     */
    public T create(Args args);

    /**
     * Creates a {@code T} instance.
     * <p>
     * Uses empty args.
     *
     * @return A new {@code T} instance.
     * @throws IllegalArgumentException When empty args is not valid.
     */
    public default T create() {
        return create(Args.NO_ARGS);
    }

    /**
     * Creates a Singleton factory.
     *
     * @param <E> The created objects type.
     * @param objectClass The created objects class.
     * @param object The object that will be returned when a creation is asked.
     * @param meta The meta data associated to the created factory.
     * @return A Factory that will always return {@code object} when a creation is asked.
     */
    public static <E> Factory<E> singleton(Class<E> objectClass,
                                           E object,
                                           Args meta) {
        return new Singleton<>(objectClass, object, meta);
    }

    /**
     * Creates a Singleton factory with no (empty) meta data.
     *
     * @param <E> The created objects type.
     * @param objectClass The created objects class.
     * @param object The object that will be returned when a creation is asked.
     * @return A Factory that will always return {@code object} when a creation is asked.
     */
    public static <E> Factory<E> singleton(Class<E> objectClass,
                                           E object) {
        return singleton(objectClass, object, Args.NO_ARGS);
    }

    /**
     * Creates a Singleton factory.
     * <p>
     * The class of created objects is that of {@code object}.<br>
     * Meta data can be associated to the factory.
     *
     * @param <E> The created objects type.
     * @param object The object that will be returned when a creation is asked.
     * @param meta The meta data associated to the created factory.
     * @return A Factory that will always return {@code object} when a creation is asked.
     */
    public static <E> Factory<E> singleton(E object,
                                           Args meta) {
        return new Singleton<>(object, meta);
    }

    /**
     * Creates a Singleton factory with no (empty) meta data.
     * <p>
     * The class of created objects is that of {@code object}.<br>
     *
     * @param <E> The created objects type.
     * @param object The object that will be returned when a creation is asked.
     * @return A Factory that will always return {@code object} when a creation is asked.
     */
    public static <E> Factory<E> singleton(E object) {
        return singleton(object, Args.NO_ARGS);
    }
}