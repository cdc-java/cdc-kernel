package cdc.args;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.IllegalCallException;
import cdc.util.lang.Introspection;

public class AutoFactory<T> extends AbstractFactory<T> {
    private static final Logger LOGGER = LogManager.getLogger(AutoFactory.class);
    private final Field instance;
    private final Constructor<T> ctor;

    public AutoFactory(Class<T> objectClass) {
        super(objectClass);
        instance = getInstanceField(objectClass);
        ctor = Introspection.getConstructor(objectClass);
        if (instance == null && ctor == null) {
            throw new IllegalArgumentException("Can not create auto factory for " + objectClass.getCanonicalName());
        } else if (instance != null) {
            LOGGER.debug("Found instance {} for {}", instance, objectClass.getCanonicalName());
        } else {
            LOGGER.debug("Found constructor {} for {}", ctor, objectClass.getCanonicalName());
        }
    }

    @Override
    protected T create(Args args,
                       FormalArgs fargs) {
        try {
            if (instance != null) {
                return getObjectClass().cast(instance.get(null));
            } else {
                return ctor.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new IllegalCallException("Failed to create a " + getObjectClass().getCanonicalName() + " instance", e);
        }
    }

    private static Field getInstanceField(Class<?> cls) {
        final Set<Field> fields = Introspection.getFieldsMatching(cls,
                                                                  f -> Modifier.isPublic(f.getModifiers())
                                                                          && Modifier.isStatic(f.getModifiers())
                                                                          && f.getType().equals(cls));
        if (fields.size() == 1) {
            return fields.iterator().next();
        } else {
            return null;
        }
    }
}