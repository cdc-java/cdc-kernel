package cdc.args;

import cdc.util.lang.Introspection;

/**
 * Formal argument for arrays.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class ArrayFormalArg<E> extends CompositeFormalArg<E[], E> {

    /**
     * Creates an array formal argument.
     *
     * @param name The argument name.
     * @param elementType The array element type.
     * @param necessity The argument necessity.
     */
    public ArrayFormalArg(String name,
                          Class<E> elementType,
                          Necessity necessity) {
        super(name,
              Introspection.getArrayClass(elementType),
              elementType,
              necessity);
    }

    /**
     * Creates a MANDATORY array formal argument.
     *
     * @param name The argument name.
     * @param elementType The array element type.
     */
    public ArrayFormalArg(String name,
                          Class<E> elementType) {
        this(name, elementType, Necessity.MANDATORY);
    }
}