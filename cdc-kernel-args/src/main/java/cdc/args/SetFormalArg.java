package cdc.args;

import java.util.Set;

import cdc.util.lang.Introspection;

/**
 * Formal argument for sets.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class SetFormalArg<E> extends CompositeFormalArg<Set<E>, E> {
    /**
     * Creates a set formal argument.
     *
     * @param name The argument name.
     * @param elementType The set element type.
     * @param necessity The argument necessity.
     */
    public SetFormalArg(String name,
                        Class<E> elementType,
                        Necessity necessity) {
        super(name,
              Introspection.uncheckedCast(Set.class),
              elementType,
              necessity);
    }

    /**
     * Creates a MANDATORY set formal argument.
     *
     * @param name The argument name.
     * @param elementType The set element type.
     */
    public SetFormalArg(String name,
                        Class<E> elementType) {
        this(name, elementType, Necessity.MANDATORY);
    }
}