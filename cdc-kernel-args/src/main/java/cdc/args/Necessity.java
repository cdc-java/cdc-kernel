package cdc.args;

/**
 * Necessity of an argument.
 * <p>
 * <b>WARNING:</b> Order of declarations matters.
 *
 * @author D. Carbonne
 *
 */
public enum Necessity {
    /**
     * The argument is optional.
     * <p>
     * It can be null.
     */
    OPTIONAL,

    /**
     * The argument is mandatory.
     * <p>
     * It can not be nulL.
     */
    MANDATORY;

    /**
     * Returns {@code true} if this necessity is weaker than (or equal to) another one.
     * <p>
     * {@code OPTIONAL < MANDATORY}
     *
     * @param other The other necessity.
     * @return {@code true} if this necessity is weaker than {@code other}.
     */
    public boolean isWeakerThan(Necessity other) {
        return ordinal() <= other.ordinal();
    }
}