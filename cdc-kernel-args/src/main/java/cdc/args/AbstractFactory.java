package cdc.args;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * Base class for factory implementation.
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of created objects.
 */
public abstract class AbstractFactory<T> implements Factory<T> {
    private final Class<T> objectClass;
    private final List<FormalArgs> creationFormalArgs;
    private final Args meta;

    /**
     * Creates a factory with a list of possible tuples for creation.
     *
     * @param objectClass The class of created objects.
     * @param meta The meta parameters associated to this factory.
     * @param creationFormalArgs A list of tuples of formal arguments that can be used for creation.
     */
    protected AbstractFactory(Class<T> objectClass,
                              Args meta,
                              List<FormalArgs> creationFormalArgs) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(meta, "meta");
        Checks.isNotNullOrEmpty(creationFormalArgs, "creationFormalArgs");
        this.objectClass = objectClass;
        this.creationFormalArgs = Collections.unmodifiableList(creationFormalArgs);
        this.meta = meta;
    }

    protected AbstractFactory(Class<T> objectClass,
                              List<FormalArgs> creationFormalArgs) {
        this(objectClass, Args.NO_ARGS, creationFormalArgs);
    }

    /**
     * Creates a factory with an array of possible tuples for creation.
     *
     * @param objectClass The class of created objects.
     * @param meta The meta parameters associated to this factory.
     * @param creationFormalArgs An array of tuples of formal arguments that can be used for creation.
     */
    protected AbstractFactory(Class<T> objectClass,
                              Args meta,
                              FormalArgs... creationFormalArgs) {
        this(objectClass,
             meta,
             Arrays.asList(creationFormalArgs));
    }

    protected AbstractFactory(Class<T> objectClass,
                              FormalArgs... creationFormalArgs) {
        this(objectClass,
             Args.NO_ARGS,
             creationFormalArgs);
    }

    /**
     * Creates a factory that does not accept any argument for creation.
     *
     * @param meta The meta parameters associated to this factory.
     * @param objectClass The class of created objects.
     */
    protected AbstractFactory(Class<T> objectClass,
                              Args meta) {
        this(objectClass,
             meta,
             FormalArgs.DEFAULT_FARGS_LIST);
    }

    /**
     * Creates a factory that does not accept any argument for creation.
     *
     * @param objectClass The class of created objects.
     */
    protected AbstractFactory(Class<T> objectClass) {
        this(objectClass, Args.NO_ARGS);
    }

    @Override
    public final Class<T> getObjectClass() {
        return objectClass;
    }

    @Override
    public final Args getMeta() {
        return meta;
    }

    @Override
    public final List<FormalArgs> getCreationFormalArgsList() {
        return creationFormalArgs;
    }

    /**
     * Creates an object.
     * <p>
     * This is the creation method that must be implemented.
     *
     * @param args The effective arguments.
     * @param fargs The formal arguments that match the effective ones.
     * @return The create object.
     */
    protected abstract T create(Args args,
                                FormalArgs fargs);

    @Override
    public final T create(Args args) {
        final List<FormalArgs> clist = getCreationFormalArgsList();
        if (clist.isEmpty() || (clist.size() == 1 && clist.get(0).equals(FormalArgs.NO_FARGS))) {
            return create(args, FormalArgs.NO_FARGS);
        } else {
            for (final FormalArgs fargs : getCreationFormalArgsList()) {
                if (args.isLooselyCompliantWith(fargs)) {
                    return create(args, fargs);
                }
            }
            throw new IllegalArgumentException(args + " is not loosely compliant with one of " + getCreationFormalArgsList());
        }
    }

    @Override
    public String toString() {
        return "Factory<" + getObjectClass().getCanonicalName() + ">" + getMeta();
    }
}