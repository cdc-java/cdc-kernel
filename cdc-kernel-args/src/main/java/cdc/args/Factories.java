package cdc.args;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.debug.Printable;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.ComparatorUtils;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;
import cdc.util.lang.NotFoundException;

/**
 * Registry of factories.
 *
 * @author Damien Carbonne
 *
 */
public final class Factories {
    private static final Logger LOGGER = LogManager.getLogger(Factories.class);
    private static final Map<Class<?>, Factory<?>> MAP = new HashMap<>();
    private static final Set<Class<?>> AUTO_FAILURES = new HashSet<>();
    public static final Printable PRINTER = new Printer();

    private static final String CONVERTERS_NOT_SET =
            "Converter was not set. You should call Factories.setConverter(ArgsConversion::convert) for example.";

    static {
        Printables.register(Factories.class, PRINTER);
    }

    private static ArgsConverter converter = null;

    /**
     * Interface used to convert arguments.
     *
     * @author Damien Carbonne
     *
     */
    @FunctionalInterface
    public static interface ArgsConverter {
        /**
         * Converts arguments.
         *
         * @param args The arguments to convert.
         * @param fargsList A list of formal arguments tuples. The result should match one of them.
         * @return The conversion of {@code args} to Args that are compliant with one of {@code fargsList}.
         * @throws IllegalArgumentException When no conversion is possible.
         */
        public Args apply(Args args,
                          List<FormalArgs> fargsList);
    }

    private Factories() {
    }

    /**
     * Sets the arguments converter.
     * <p>
     * This is useful when passed arguments don't have the expected types by can be converted to.<br>
     * This is typically the case when arguments are loaded from a file as strings, and must be converted to
     * something else (Integer, Float, ...) to match factory expectations.
     *
     * @param converter The converter.
     */
    public static void setConverter(ArgsConverter converter) {
        Factories.converter = converter;
    }

    private static void warnMissingConverter() {
        LOGGER.error(CONVERTERS_NOT_SET);
    }

    private static Args convert(Args args,
                                Factory<?> factory) {
        if (converter == null) {
            warnMissingConverter();
            return args;
        } else {
            return converter.apply(args, factory.getCreationFormalArgsList());
        }
    }

    /**
     * Returns {@code true} if an argument needs conversion to be viewed as a string.
     *
     * @param arg The argument.
     * @return {@code true} if {@code arg} needs conversion to be viewed as a string.
     */
    private static boolean needsStringConversion(Arg arg) {
        return !arg.isCompliantWith(String.class);
    }

    /**
     * Returns {@code true} if arguments need conversion to be viewed as strings.
     *
     * @param args The arguments.
     * @return {@code true} if {@code args} needs conversion to be viewed as strings.
     */
    private static boolean needsStringConversion(Args args) {
        for (final Arg arg : args.getArgs()) {
            if (needsStringConversion(arg)) {
                return true;
            }
        }
        return false;
    }

    public static Arg convertToStringValues(Arg arg) {
        final boolean needsConversion = needsStringConversion(arg);
        if (needsConversion) {
            if (converter == null) {
                warnMissingConverter();
                throw new IllegalArgumentException(CONVERTERS_NOT_SET);
            } else {
                final FormalArgs fargs = FormalArgs.builder()
                                                   .add(arg.getName(), String.class, Necessity.OPTIONAL)
                                                   .build();
                final List<FormalArgs> fargsList = new ArrayList<>();
                fargsList.add(fargs);
                final Args args = converter.apply(new Args(arg), fargsList);
                return args.getArgs().get(0);
            }
        } else {
            return arg;
        }
    }

    /**
     * Converts arguments to strings.
     *
     * @param args The arguments.
     * @return Arguments with same names, but with all values converted to strings.
     */
    public static Args convertToStringValues(Args args) {
        final boolean needsConversion = needsStringConversion(args);
        if (needsConversion) {
            if (converter == null) {
                warnMissingConverter();
                throw new IllegalArgumentException(CONVERTERS_NOT_SET);
            } else {
                final FormalArgs.Builder fbuilder = FormalArgs.builder();
                for (final Arg arg : args.getArgs()) {
                    fbuilder.add(arg.getName(), String.class, Necessity.OPTIONAL);
                }
                final FormalArgs fargs = fbuilder.build();
                final List<FormalArgs> fargsList = new ArrayList<>();
                fargsList.add(fargs);
                return converter.apply(args, fargsList);
            }
        } else {
            return args;
        }
    }

    /**
     * Registers a factory.
     *
     * @param factory The factory.
     * @throws IllegalArgumentException when {@code factory} is invalid or another factory is already associated to the built class.
     */
    public static void register(Factory<?> factory) {
        LOGGER.debug("register({})", factory);
        Checks.isNotNull(factory, "factory");
        Checks.isNotNullOrEmpty(factory.getCreationFormalArgsList(), "factory.creationArgs");
        if (MAP.containsKey(factory.getObjectClass())) {
            if (MAP.get(factory.getObjectClass()) == factory) {
                LOGGER.debug("Ignored");
                return;
            } else {
                throw new IllegalArgumentException("A '" + factory.getObjectClass().getCanonicalName()
                        + "' factory is already registered");
            }
        }

        MAP.put(factory.getObjectClass(), factory);
    }

    /**
     * @return A set of classes for which a factory is registered.
     */
    public static Set<Class<?>> getClasses() {
        return MAP.keySet();
    }

    /**
     * Returns the factory associated to a class.
     * <p>
     * If not factory was registered for the desired class, we try:
     * <ul>
     * <li>To detect and register a declared one. It is searched as a Factory instance in the class.
     * <li>To create a new factory and register it. The is possible for singletons.
     * This does not permit to attach meta parameters to the created factory.
     * </ul>
     *
     * @param <T> The object type.
     * @param cls The class.
     * @param reaction The reaction to adopt when no factory is registered for {@code cls}.
     * @return The factory associated to {@code cls} or  {@code null}.
     * @throws NotFoundException When reaction is FAIL and no factory is associated to {@code cls}.
     */
    public static <T> Factory<T> getFactory(Class<T> cls,
                                            FailureReaction reaction) {
        Checks.isNotNull(cls, "cls");

        Factory<T> result = Introspection.uncheckedCast(MAP.get(cls));

        if (result == null && !AUTO_FAILURES.contains(cls)) {
            // Tries to find a factory
            result = detectFactory(cls);
            if (result == null) {
                // We did not detect a factory, so we try to create one
                // This may fail!
                try {
                    result = new AutoFactory<>(cls);
                    LOGGER.debug("Created an auto factory for {}", cls.getCanonicalName());
                    register(result);
                } catch (final Exception e) {
                    LOGGER.warn("Failed to create an auto factory for {}", cls.getCanonicalName(), e);
                    AUTO_FAILURES.add(cls);
                    result = null;
                }
            } else {
                LOGGER.debug("Auto detected a factory for {}", cls.getCanonicalName());
                register(result);
            }
        }

        return NotFoundException.onResult(result,
                                          "No '" + cls.getCanonicalName() + "' factory found",
                                          LOGGER,
                                          reaction,
                                          null);
    }

    public static <T> Factory<T> getFactory(Class<T> cls) {
        return getFactory(cls, FailureReaction.FAIL);
    }

    private static <T> boolean isMatchingFactory(Field field,
                                                 Class<T> objectClass) {
        try {
            return Modifier.isStatic(field.getModifiers())
                    && Factory.class.isAssignableFrom(field.getType())
                    && ((Factory<?>) (field.get(null))).getObjectClass().equals(objectClass);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            LOGGER.error("Failed to retrieve {} in {}", field, objectClass.getCanonicalName());
            return false;
        }
    }

    private static <T> boolean isCompliantFactory(Field field,
                                                  Class<T> objectClass) {
        try {
            return Modifier.isStatic(field.getModifiers())
                    && Factory.class.isAssignableFrom(field.getType())
                    && objectClass.isAssignableFrom(((Factory<?>) (field.get(null))).getObjectClass());
        } catch (IllegalArgumentException | IllegalAccessException e) {
            LOGGER.error("Failed to retrieve {} in {}", field, objectClass.getCanonicalName());
            return false;
        }
    }

    private static <T> Factory<T> detectFactory(Class<T> objectClass) {
        final Set<Field> matchingFields = Introspection.getFieldsMatching(objectClass,
                                                                          field -> isMatchingFactory(field, objectClass));
        if (matchingFields.size() == 1) {
            final Field field = matchingFields.iterator().next();
            try {
                final Factory<?> factory = (Factory<?>) field.get(null);
                LOGGER.debug("Found one matching factory in {}", objectClass.getCanonicalName());
                return Introspection.uncheckedCast(factory);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                LOGGER.error("Failed to retrieve {} in {}", field, objectClass.getCanonicalName());
                return null;
            }
        } else {
            if (matchingFields.size() > 1) {
                LOGGER.warn("Too many ({}) matching factories found in {}",
                            matchingFields.size(),
                            objectClass.getCanonicalName());
            } else {
                // Try to find a compliant factory (no matching factory found)
                final Set<Field> compliantFields = Introspection.getFieldsMatching(objectClass,
                                                                                   field -> isCompliantFactory(field, objectClass));
                if (compliantFields.size() == 1) {
                    final Field field = compliantFields.iterator().next();
                    try {
                        final Factory<?> factory = (Factory<?>) field.get(null);
                        LOGGER.debug("Found one compliant factory in {}", objectClass.getCanonicalName());
                        return Introspection.uncheckedCast(factory);
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        LOGGER.error("Failed to retrieve {} in {}", field, objectClass.getCanonicalName());
                        return null;
                    }
                } else {
                    if (compliantFields.size() > 1) {
                        LOGGER.warn("Too many ({}) compliant factories found in {}",
                                    compliantFields.size(),
                                    objectClass.getCanonicalName());
                    }
                }
            }
            LOGGER.warn("No factories found in {}", objectClass.getCanonicalName());
            return null;
        }
    }

    /**
     * Creates an object.
     *
     * @param cls The class of the object to create.
     * @param args The creation arguments.
     * @return The created object.
     * @throws NotFoundException When no adequate factory is found.
     * @throws IllegalArgumentException When {@code args} are are not compliant with factory expectations.
     */
    public static Object create(Class<?> cls,
                                Args args) {
        LOGGER.debug("create({}, {})", cls, args);
        Checks.isNotNull(cls, "cls");
        Checks.isNotNull(args, "args");
        final Factory<?> factory = getFactory(cls, FailureReaction.FAIL);
        return factory.create(convert(args, factory));
    }

    /**
     * Creates an object.
     *
     * @param cls The class of the object to create.
     * @return The created object.
     * @throws NotFoundException When no adequate factory is found.
     * @throws IllegalArgumentException When {@link Args#NO_ARGS} are are not compliant with factory expectations.
     */
    public static Object create(Class<?> cls) {
        return create(cls, Args.NO_ARGS);
    }

    /**
     * Creates an object.
     *
     * @param className The class name of the object to create.
     * @param args The creation arguments.
     * @return The created object.
     * @throws NotFoundException When no adequate factory is found.
     * @throws IllegalArgumentException When {@code args} are are not compliant with factory expectations.
     */
    public static Object create(String className,
                                Args args) {
        LOGGER.debug("create({}, {})", className, args);
        final Class<?> cls = Introspection.getClass(className, FailureReaction.FAIL);
        return create(cls, args);
    }

    /**
     * Creates an object.
     *
     * @param className The class name of the object to create.
     * @return The created object.
     * @throws NotFoundException When no adequate factory is found.
     * @throws IllegalArgumentException When {@link Args#NO_ARGS} are are not compliant with factory expectations.
     */
    public static Object create(String className) {
        return create(className, Args.NO_ARGS);
    }

    protected static class Printer implements Printable {
        @Override
        public void print(PrintStream out,
                          int level) {
            indent(out, level);
            out.println("Factories (" + Factories.getClasses().size() + ")");
            for (final Class<?> cls : CollectionUtils.toSortedList(Factories.getClasses(),
                                                                   ComparatorUtils.CLASS_CANONICAL_NAME_COMPARATOR)) {
                final Factory<?> factory = Factories.getFactory(cls);
                indent(out, level + 1);
                out.println(factory.getObjectClass().getCanonicalName());
                for (final FormalArgs fargs : factory.getCreationFormalArgsList()) {
                    indent(out, level + 2);
                    out.println("fargs: " + fargs);
                }
                indent(out, level + 2);
                out.println("meta: " + factory.getMeta());
            }
        }
    }
}