package cdc.args;

import cdc.util.lang.Checks;
import cdc.util.lang.Operators;

/**
 * Effective argument.
 * <p>
 * It is a (name, value) pair.
 *
 * @author Damien Carbonne
 *
 */
public final class Arg {
    private final String name;
    private final Object value;

    /**
     * Creates an effective argument.
     * <p>
     * No check is done on argument value.
     *
     * @param name The name. It must be valid.
     * @param value the value.
     * @throws IllegalArgumentException When {@code name} is invalid.
     */
    public Arg(String name,
               Object value) {
        FormalArg.checkName(name);
        this.name = name;
        this.value = value;
    }

    /**
     * Creates an effective argument that is compliant with a formal argument.
     *
     * @param <T> The formal argument type.
     * @param farg The formal argument.
     * @param value The value.
     * @throws IllegalArgumentException When {@code farg} is null or {@code value} is not compliant with {@code farg}.
     */
    public <T> Arg(FormalArg<T> farg,
                   T value) {
        Checks.isNotNull(farg, "farg");
        Checks.isTrue(value != null || farg.isOptional(), "null value is not compliant with {}", farg);

        this.name = farg.getName();
        this.value = value;
    }

    /**
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * Returns the value or a default value.
     *
     * @param def The default value.
     * @return The value or {@code def} if value is {@code null}.
     */
    public Object getValue(Object def) {
        return value == null ? def : value;
    }

    /**
     * Returns the value cast to a target class.
     *
     * @param <T> The target type.
     * @param cls The target class.
     * @return The cast of value to {@code cls}.
     */
    public <T> T getValue(Class<T> cls) {
        return cls.cast(value);
    }

    /**
     * Returns the value cast to a target class or a default value.
     *
     * @param <T> The target type.
     * @param cls The target class.
     * @param def The default value.
     * @return The value cats to {@code cls} or {@code def} if value is {@code null}.
     */
    public <T> T getValue(Class<T> cls,
                          T def) {
        return value == null ? def : cls.cast(value);
    }

    public boolean isCompliantWith(FormalArg<?> farg) {
        Checks.isNotNull(farg, "farg");
        return farg.matchesName(name)
                && (value == null ? farg.isOptional() : farg.getType().isInstance(value));
    }

    public boolean isCompliantWith(FormalArgs fargs) {
        Checks.isNotNull(fargs, "fargs");
        final FormalArg<?> farg = fargs.getArg(name);
        return farg != null && farg.isCompliantWith(value);
    }

    public boolean isCompliantWith(Class<?> cls) {
        Checks.isNotNull(cls, "cls");
        return value == null || cls.isInstance(value);
    }

    @Override
    public int hashCode() {
        return name.hashCode() + Operators.hashCode(value);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Arg)) {
            return false;
        }
        final Arg other = (Arg) object;
        return name.equals(other.name)
                && Operators.equals(value, other.value);
    }

    @Override
    public String toString() {
        return "[" + name + "=" + value + "]";
    }
}