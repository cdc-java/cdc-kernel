package cdc.args;

import java.util.List;

import cdc.util.lang.Introspection;

/**
 * Formal argument for lists.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class ListFormalArg<E> extends CompositeFormalArg<List<E>, E> {
    /**
     * Creates a list formal argument.
     *
     * @param name The argument name.
     * @param elementType The list element type.
     * @param necessity The argument necessity.
     */
    public ListFormalArg(String name,
                         Class<E> elementType,
                         Necessity necessity) {
        super(name,
              Introspection.uncheckedCast(List.class),
              elementType,
              necessity);
    }

    /**
     * Creates a MANDATORY list formal argument.
     *
     * @param name The argument name.
     * @param elementType The list element type.
     */
    public ListFormalArg(String name,
                         Class<E> elementType) {
        this(name, elementType, Necessity.MANDATORY);
    }
}