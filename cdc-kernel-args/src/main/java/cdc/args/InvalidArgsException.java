package cdc.args;

public class InvalidArgsException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public InvalidArgsException() {
        super();
    }

    public InvalidArgsException(String message) {
        super(message);
    }
}