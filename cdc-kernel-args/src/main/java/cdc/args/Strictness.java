package cdc.args;

/**
 * Strictness of a match.
 *
 * @author Damien Carbonne
 *
 */
public enum Strictness {
    STRICT,
    LOOSE
}