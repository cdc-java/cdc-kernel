package cdc.args;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.util.lang.Checks;

/**
 * Tuple of formal arguments.
 *
 * @author Damien Carbonne
 *
 */
public final class FormalArgs {
    private final FormalArg<?>[] args;

    /**
     * Empty tuple of formal arguments.
     */
    public static final FormalArgs NO_FARGS = new FormalArgs();

    public static final List<FormalArgs> DEFAULT_FARGS_LIST = List.of(FormalArgs.NO_FARGS);

    /**
     * Creates an empty formal argument tuple.
     */
    public FormalArgs() {
        this.args = new FormalArg[0];
    }

    /**
     * Creates a tuple of formal arguments from an array.
     *
     * @param args The formal arguments.
     * @throws IllegalArgumentException When {@code args} of of of its elements is {@code null},
     *             or when there are duplicate names.
     */
    public FormalArgs(FormalArg<?>... args) {
        Checks.isNotNull(args, "args");
        this.args = new FormalArg[args.length];
        for (int index = 0; index < args.length; index++) {
            this.args[index] = args[index];
        }
        check();
    }

    /**
     * Creates a tuple of formal arguments from a list.
     *
     * @param args The formal arguments.
     * @throws IllegalArgumentException When {@code args} of of of its elements is {@code null},
     *             or when there are duplicate names.
     */
    public FormalArgs(List<FormalArg<?>> args) {
        Checks.isNotNull(args, "args");
        this.args = new FormalArg[args.size()];
        for (int index = 0; index < args.size(); index++) {
            this.args[index] = args.get(index);
        }
        check();
    }

    /**
     * Creates a tuple of formal arguments by merging two tuples.
     *
     * @param fargs1 The first tuple of formal arguments.
     * @param fargs2 The second tuple of formal arguments.
     * @throws IllegalArgumentException When there are duplicate names.
     */
    public FormalArgs(FormalArgs fargs1,
                      FormalArgs fargs2) {
        Checks.isNotNull(fargs1, "fargs1");
        Checks.isNotNull(fargs2, "fargs2");

        args = new FormalArg[fargs1.args.length + fargs2.args.length];
        for (int index = 0; index < fargs1.args.length; index++) {
            args[index] = fargs1.args[index];
        }
        for (int index = 0; index < fargs2.args.length; index++) {
            args[fargs1.args.length + index] = fargs2.args[index];
        }
        check();
    }

    /**
     * Checks the validity of arguments.
     *
     * @throws IllegalArgumentException When an argument is {@code null},
     *             or there are duplicate names.
     */
    private void check() {
        final Set<String> names = new HashSet<>();
        for (final FormalArg<?> arg : args) {
            Checks.isNotNull(arg, "arg");
            final String name = arg.getName();
            if (names.contains(name)) {
                throw new IllegalArgumentException("Duplicate name: '" + name + "'");
            } else {
                names.add(name);
            }
        }
    }

    /**
     * @return {@code true} if this list is empty.
     */
    public boolean isEmpty() {
        return args.length == 0;
    }

    /**
     * @return The formal arguments as an array.
     */
    public FormalArg<?>[] getArgs() {
        return args;
    }

    /**
     * Returns {@code true} if this tuple of formal arguments contains
     * at least one argument whose necessity matches a necessity.
     *
     * @param necessity The necessity.
     * @return {@code true} if this list contains at least one argument whose necessity matches {@code necessity}.
     */
    public boolean hasArgs(Necessity necessity) {
        for (final FormalArg<?> arg : args) {
            if (arg.getNecessity() == necessity) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the arguments that match a necessity.
     *
     * @param necessity The necessity.
     * @return The arguments that match {@code necessity}.
     */
    public FormalArgs getArgs(Necessity necessity) {
        Checks.isNotNull(necessity, "necessity");
        final List<FormalArg<?>> tmp = new ArrayList<>();
        for (final FormalArg<?> arg : args) {
            if (arg.getNecessity() == necessity) {
                tmp.add(arg);
            }
        }
        if (tmp.isEmpty()) {
            return NO_FARGS;
        } else {
            return new FormalArgs(tmp);
        }
    }

    /**
     * @return The number of formal arguments.
     */
    public int size() {
        return args.length;
    }

    /**
     * Returns the argument that has a position.
     *
     * @param index The index
     * @return The argument at {@code index}.
     */
    public FormalArg<?> getArg(int index) {
        return args[index];
    }

    /**
     * Returns the index of an argument.
     *
     * @param name The argument name.
     * @return The index of the argument whose name is {@code name} or -1.
     */
    public int getArgIndex(String name) {
        for (int index = 0; index < args.length; index++) {
            if (args[index].matchesName(name)) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Returns the argument that has a name.
     *
     * @param name The argument name.
     * @return The argument whose name is {@code name} or {@code null}.
     */
    public FormalArg<?> getArg(String name) {
        final int index = getArgIndex(name);
        if (index >= 0) {
            return getArg(index);
        } else {
            return null;
        }
    }

    /**
     * Returns {@code true} when this tuple contains an argument that has a given name.
     *
     * @param name The name.
     * @return {@code true} when this tuple contains an argument named {@code name}.
     */
    public boolean hasArg(String name) {
        return getArg(name) != null;
    }

    public boolean hasArgMatchingName(FormalArg<?> farg) {
        Checks.isNotNull(farg, "farg");
        return hasArg(farg.getName());
    }

    /**
     * Returns {@code true} when this formal tuple can accept another formal tuple.
     * <p>
     * The result is positive when:
     * <ul>
     * <li>all mandatory args of this tuple are present and compliant in {@code other} (they may be optional or mandatory).
     * <li>all optional args of this tuple are either absent from {@code other} or are present in {@code other} and compliant.
     * </ul>
     *
     * @param other The other formal tuple.
     * @return {@code true} when this formal tuple can accept other formal tuple.
     */
    public boolean accepts(FormalArgs other) {
        Checks.isNotNull(other, "other");

        for (final FormalArg<?> arg : args) {
            final FormalArg<?> oarg = other.getArg(arg.getName());
            if (oarg == null) {
                if (arg.isMandatory()) {
                    // A mandatory arg must be present in other
                    return false;
                }
                // An optional arg can be absent from other
            } else {
                // A name matching arg has been found in other
                // Check its type compliance
                if (!arg.hasWeakerType(oarg.getType())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Merges 2 tuples of formal arguments.
     * <ul>
     * <li>If an argument name is in only one tuple, the argument it is added as defined.
     * <li>If an argument name is in both tuples, arguments are merged,
     * choosing MANDATORY if necessities are different, and the most specialized type.<br>
     * This may raise an exception if merge is impossible (no inheritance hierarchy between types).
     * </ul>
     *
     * @param fargs1 The first tuple.
     * @param fargs2 the second tuple.
     * @return The merge of {@code left} and {@code right}.
     * @throws IllegalArgumentException if there is a type conflict when merging 2 arguments.
     */
    public static FormalArgs merge(FormalArgs fargs1,
                                   FormalArgs fargs2) {
        Checks.isNotNull(fargs1, "left");
        Checks.isNotNull(fargs2, "right");

        if (fargs1.isEmpty()) {
            return fargs2;
        } else if (fargs2.isEmpty()) {
            return fargs1;
        } else {
            final List<String> names = new ArrayList<>();
            for (final FormalArg<?> farg : fargs1.args) {
                names.add(farg.getName());
            }
            for (final FormalArg<?> farg : fargs2.args) {
                if (!names.contains(farg.getName())) {
                    names.add(farg.getName());
                }
            }
            final List<FormalArg<?>> fargs = new ArrayList<>();
            for (final String name : names) {
                final FormalArg<?> l = fargs1.getArg(name);
                final FormalArg<?> r = fargs2.getArg(name);
                final FormalArg<?> farg = FormalArg.merge(l, r, Necessity.MANDATORY);
                fargs.add(farg);
            }
            return new FormalArgs(fargs);
        }
    }

    public static FormalArgs merge(FormalArgs... all) {
        FormalArgs result = FormalArgs.NO_FARGS;
        for (final FormalArgs fargs : all) {
            result = merge(result, fargs);
        }
        return result;
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (final FormalArg<?> arg : args) {
            result += arg.hashCode();
        }
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof FormalArgs)) {
            return false;
        }
        final FormalArgs o = (FormalArgs) other;
        return Arrays.equals(args, o.args);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("(");
        boolean first = true;
        for (final FormalArg<?> arg : args) {
            if (!first) {
                builder.append(", ");
            }
            builder.append(arg);
            first = false;
        }
        builder.append(")");

        return builder.toString();
    }

    /**
     * @return A new Builder instance.
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder of formal arguments.
     *
     * @author Damien Carbonne
     *
     */
    public static final class Builder {
        private final List<FormalArg<?>> args = new ArrayList<>();

        Builder() {
        }

        public Builder clear() {
            args.clear();
            return this;
        }

        public Builder add(FormalArg<?> farg) {
            args.add(farg);
            return this;
        }

        public Builder add(String name,
                           Class<?> type,
                           Necessity necessity) {
            args.add(new FormalArg<>(name, type, necessity));
            return this;
        }

        public FormalArgs build() {
            return new FormalArgs(args);
        }
    }
}