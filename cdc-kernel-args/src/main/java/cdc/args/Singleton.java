package cdc.args;

import java.util.List;

import cdc.util.lang.Checks;

/**
 * Implementation of Factory dedicated to singletons.
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of created objects.
 */
public class Singleton<T> implements Factory<T> {
    private final Class<T> objectClass;
    private final T object;
    private final Args meta;

    /**
     * Creates a Singleton factory.
     *
     * @param objectClass Class of created objects.
     * @param object The singleton object to use.
     *            It is this instance that will be returned each time a creation is asked.
     * @param meta The meta data associated to this factory.
     */
    public Singleton(Class<T> objectClass,
                     T object,
                     Args meta) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(meta, "meta");

        this.objectClass = objectClass;
        this.object = object;
        this.meta = meta;
    }

    /**
     * Creates a Singleton factory.
     * <p>
     * Class of created objects is the class of {@code object}.
     *
     * @param object The singleton object to use.
     *            It is this instance that will be returned each time a creation is asked.
     * @param meta The meta data associated to this factory.
     */
    public Singleton(T object,
                     Args meta) {
        Checks.isNotNull(object, "object");
        Checks.isNotNull(meta, "meta");

        @SuppressWarnings("unchecked")
        final Class<T> tmp = (Class<T>) object.getClass();
        this.objectClass = tmp;
        this.object = object;
        this.meta = meta;
    }

    @Override
    public Class<T> getObjectClass() {
        return objectClass;
    }

    @Override
    public Args getMeta() {
        return meta;
    }

    @Override
    public List<FormalArgs> getCreationFormalArgsList() {
        return FormalArgs.DEFAULT_FARGS_LIST;
    }

    @Override
    public T create(Args args) {
        return object;
    }
}