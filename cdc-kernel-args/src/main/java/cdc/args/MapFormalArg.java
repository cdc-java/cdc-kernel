package cdc.args;

import java.util.Map;

import cdc.util.lang.Introspection;

/**
 * Formal argument for maps.
 *
 * @author Damien Carbonne
 *
 * @param <K> The key type.
 * @param <V> The value type.
 */
public class MapFormalArg<K, V> extends FormalArg<Map<K, V>> {
    private final Class<K> keyType;
    private final Class<V> valueType;

    /**
     * Creates a map formal argument.
     *
     * @param name The argument name.
     * @param keyType The key type.
     * @param valueType The value type.
     * @param necessity The argument necessity.
     */
    public MapFormalArg(String name,
                        Class<K> keyType,
                        Class<V> valueType,
                        Necessity necessity) {
        super(name,
              Introspection.uncheckedCast(Map.class),
              necessity);
        this.keyType = keyType;
        this.valueType = valueType;
    }

    /**
     * Creates a MANDATORY map formal argument.
     *
     * @param name The argument name.
     * @param keyType The key type.
     * @param valueType The value type.
     */
    public MapFormalArg(String name,
                        Class<K> keyType,
                        Class<V> valueType) {
        this(name, keyType, valueType, Necessity.MANDATORY);
    }

    public final Class<K> getKeyType() {
        return keyType;
    }

    public final Class<V> getValueType() {
        return valueType;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 31 * (keyType.hashCode() + 31 * valueType.hashCode());
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof MapFormalArg)) {
            return false;
        }
        final MapFormalArg<?, ?> o = (MapFormalArg<?, ?>) other;
        return super.equals(other)
                && keyType.equals(o.keyType)
                && valueType.equals(o.valueType);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(getName());
        builder.append(" => ");
        builder.append(getType().getSimpleName());
        builder.append("<");
        builder.append(getKeyType().getSimpleName());
        builder.append(", ");
        builder.append(getValueType().getSimpleName());
        builder.append("> ");
        builder.append(getNecessity());
        builder.append("]");
        return builder.toString();
    }
}