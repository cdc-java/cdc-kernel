package cdc.args;

public class IllegalValueException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public IllegalValueException() {
        super();
    }

    public IllegalValueException(String message) {
        super(message);
    }
}