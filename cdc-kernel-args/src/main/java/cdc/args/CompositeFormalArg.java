package cdc.args;

/**
 * Base class of formal arguments whose type is composite.
 *
 * @author Damien Carbonne
 *
 * @param <C> The composite type.
 * @param <E> The element type.
 */
public class CompositeFormalArg<C, E> extends FormalArg<C> {
    private final Class<E> elementType;

    public CompositeFormalArg(String name,
                              Class<C> type,
                              Class<E> elementType,
                              Necessity necessity) {
        super(name, type, necessity);
        this.elementType = elementType;
    }

    public CompositeFormalArg(String name,
                              Class<C> type,
                              Class<E> elementType) {
        this(name, type, elementType, Necessity.MANDATORY);
    }

    /**
     * @return The class of collection elements.
     */
    public final Class<E> getElementType() {
        return elementType;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + 31 * elementType.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof CompositeFormalArg)) {
            return false;
        }
        final CompositeFormalArg<?, ?> o = (CompositeFormalArg<?, ?>) other;
        return super.equals(other)
                && elementType.equals(o.elementType);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(getName());
        builder.append(" => ");
        builder.append(getType().getSimpleName());
        builder.append("<");
        builder.append(getElementType().getSimpleName());
        builder.append("> ");
        builder.append(getNecessity());
        builder.append("]");
        return builder.toString();
    }
}