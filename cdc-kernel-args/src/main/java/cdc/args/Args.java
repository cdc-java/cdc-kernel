package cdc.args;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.Operators;

/**
 * 'Set' of effective arguments.
 * <p>
 * This class is immutable. {@link Builder} can be used to create instances of Args.
 * <p>
 * <b>Compliance rules</b>
 * <p>
 * There is compliance between Args {@code args} and FormalArgs {@code fargs} when:
 * <ul>
 * <li>In STRICT mode: each {@code arg} of {@code args} corresponds to one {@code farg} of {@code fargs} with a compliant
 * value.</li>
 * <li>In LOOSE mode: each {@code arg} of {@code args} 1) is absent of {@code fargs}, 2) or corresponds to one {@code farg} of
 * {@code fargs} with a compliant value.</li>
 * <li>In both nodes, each MANDATORY {@code farg} has a compliant value defined in {@code args}.</li>
 * </ul>
 * For example, with {@code fargs = [arg0: MANDATORY String, arg1: OPTIONAL Object]}, compliance will be:
 * <ul>
 * <li>{@code true} with: {@code args = [arg0: "Hello"]} in any mode.</li>
 * <li>{@code true} with: {@code args = [arg0: "Hello", arg1: null]} in any mode.</li>
 * <li>{@code true} with: {@code args = [arg0: "Hello", arg1: "World"]} in any mode.</li>
 * <li>{@code true} with: {@code args = [arg0: "Hello", arg2: null]} in LOOSE mode.</li>
 * <li>{@code false} with: {@code args = [arg0: "Hello", arg2: null]} in STRICT mode.</li>
 * <li>{@code false} with: {@code args = [arg0: null]} in any mode.</li>
 * </ul>
 *
 *
 * @author Damien Carbonne
 *
 */
public final class Args {
    private static final Logger LOGGER = LogManager.getLogger(Args.class);
    private final Map<String, Arg> map;

    /**
     * Empty arguments.
     */
    public static final Args NO_ARGS = new Args();

    /**
     * Creates an empty set.
     */
    public Args() {
        map = Collections.emptyMap();
    }

    /**
     * Creates a set.
     *
     * @param args The arguments.
     */
    public Args(Arg... args) {
        Checks.isNotNull(args, "args");
        final Map<String, Arg> tmp = new LinkedHashMap<>();
        for (final Arg arg : args) {
            Checks.isNotNull(arg, "args[?]");
            tmp.put(arg.getName(), arg);
        }
        this.map = Collections.unmodifiableMap(tmp);
    }

    /**
     * Creates a set.
     *
     * @param args The arguments.
     */
    public Args(List<Arg> args) {
        Checks.isNotNull(args, "args");
        final Map<String, Arg> tmp = new LinkedHashMap<>();
        for (final Arg arg : args) {
            Checks.isNotNull(arg, "args.get(?)");
            tmp.put(arg.getName(), arg);
        }
        this.map = Collections.unmodifiableMap(tmp);
    }

    /**
     * @return {@code true} when there are no arguments.
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * @return {@code true} if this tuple is empty or all its values are {@code null}.
     */
    public boolean isEmptyOrAllValuesAreNull() {
        if (map.isEmpty()) {
            return true;
        } else {
            for (final Arg arg : map.values()) {
                if (arg.getValue() != null) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * @return The number of arguments.
     */
    public int size() {
        return map.size();
    }

    /**
     * @return A list of argument names, in the order they were declared.
     */
    public List<String> getNames() {
        final List<String> names = new ArrayList<>();
        for (final String name : map.keySet()) {
            names.add(name);
        }
        return names;
    }

    public List<String> getSortedNames() {
        final List<String> names = getNames();
        Collections.sort(names);
        return names;
    }

    /**
     * Returns the argument that has a given name or {@code null}.
     *
     * @param name The name.
     * @return The argument named {@code name} or {@code null}
     */
    public Arg getArg(String name) {
        return map.get(name);
    }

    /**
     * @return A list of arguments, in the order they were declared.
     */
    public List<Arg> getArgs() {
        final List<Arg> tmp = new ArrayList<>();
        for (final String name : map.keySet()) {
            tmp.add(getArg(name));
        }
        return tmp;
    }

    /**
     * Returns {@code true} if this Args contains an argument with a particular name.
     *
     * @param name The name.
     * @return {@code true} if this args contains an argument named {@code name}.
     */
    public boolean hasArg(String name) {
        return map.containsKey(name);
    }

    /**
     * Returns {@code true} if this tuple contains an argument whose name matches the name of a formal argument.
     *
     * @param farg The formal argument.
     * @return {@code true} if this tuple contains an argument whose name matches the name of {@code farg}.
     */
    public boolean hasArgWithMatchingName(FormalArg<?> farg) {
        Checks.isNotNull(farg, "farg");
        return map.containsKey(farg.getName());
    }

    /**
     * Creates a new set from this one and another one.
     * <p>
     * Values in {@code other} take precedence on values of this set.
     *
     * @param other The other arguments.
     * @return A new set from this one and {@code other}.
     */
    public Args set(Args other) {
        Checks.isNotNull(other, "other");
        if (other.isEmpty()) {
            return this;
        } else if (isEmpty()) {
            return other;
        } else {
            return builder().args(this)
                            .args(other)
                            .build();
        }
    }

    /**
     * Returns the value (as Object) associated to a name or a default value.
     *
     * @param name Name of the retrieved value.
     * @param def The default value.
     * @return The value (as Object) associated to name or def.
     */
    public Object getValue(String name,
                           Object def) {
        Checks.isNotNull(name, "name");
        final Arg arg = map.get(name);
        return arg == null ? def : arg.getValue();
    }

    /**
     * Returns the value (as Object) associated to a name or null.
     * <p>
     * If no value is associated to name, {@code null} is returned.
     *
     * @param name Name of the retrieved value.
     * @return The value (as Object) associated to name or null.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public Object getValue(String name) {
        return getValue(name, (Object) null);
    }

    /**
     * Returns the value associated to a formal argument or a default value.
     *
     * @param <T> The formal argument type.
     * @param farg The formal argument.
     * @param def The default value.
     * @return The value corresponding to {@code farg} or {@code null}.
     * @throws IllegalArgumentException When {@code farg} is {@code null}.
     * @throws ClassCastException When the associated value can not be cast to expected class.
     */
    public <T> T getValue(FormalArg<T> farg,
                          T def) {
        Checks.isNotNull(farg, "farg");
        return farg.getWrappedType().cast(getValue(farg.getName(), def));
    }

    /**
     * Returns the value associated to a formal argument or {@code null}.
     *
     * @param <T> The formal argument type.
     * @param farg The formal argument.
     * @return The value associated to {@code farg}.
     * @throws IllegalArgumentException When {@code farg} is {@code null}.
     * @throws ClassCastException When the associated value can not be cast to expected class.
     */
    public <T> T getValue(FormalArg<T> farg) {
        Checks.isNotNull(farg, "farg");
        return getValue(farg, (T) null);
    }

    /**
     * Returns the value associated to a name, as a type, or a default value.
     * <p>
     * If the value associated to name can not be converted to type, an exception is
     * raised.
     *
     * @param <T> Return type.
     * @param name Name of the retrieved value.
     * @param cls Class of the retrieved value.
     * @param def The default value to return if no value is associated to name.
     * @return The value associated to {@code name} or {@code def}.
     * @throws IllegalArgumentException When {@code name} of {@code cls} is {@code null}.
     * @throws ClassCastException When the associated value can not be cast to {@code cls}.
     */
    public <T> T getValue(String name,
                          Class<T> cls,
                          T def) {
        Checks.isNotNull(name, "name");
        Checks.isNotNull(cls, "cls");
        return cls.cast(getValue(name, def));
    }

    /**
     * Returns the value associated to a name, as a type, or {@code null}.
     * <p>
     * If no value is associated to name, {@code null} is returned.<br>
     * If the value associated to name can not be converted to type, an exception is raised.
     *
     * @param <T> Return type.
     * @param name Name of the retrieved value.
     * @param cls Class of the retrieved value.
     * @return The value associated to {@code name} or {@code null}.
     * @throws IllegalArgumentException When {@code name} of {@code cls} is {@code null}.
     * @throws ClassCastException When the associated value can not be cast to {@code cls}.
     */
    public <T> T getValue(String name,
                          Class<T> cls) {
        return getValue(name, cls, null);
    }

    /**
     * Returns true when all args are compliant with formal arguments.
     * <p>
     * In STRICT compliance mode, all effective arguments must
     * be present in formal arguments.<br>
     * In LOOSE compliance mode, an effective argument can exist
     * for which there is no corresponding formal argument.<br>
     * Note that for compliance computation, this may be insufficient:
     * some MANDATORY arguments may be missing.
     *
     * @param fargs The formal arguments.
     * @param strictness The compliance strictness.
     * @return {@code true} when all arguments of {@code args} are compliant with {@code fargs}.
     */
    private boolean allArgsAreCompliant(FormalArgs fargs,
                                        Strictness strictness) {
        Checks.isNotNull(fargs, "fargs");
        Checks.isNotNull(strictness, "strictness");

        // Iterate on each effective argument
        for (final String name : getNames()) {
            final Object value = getValue(name);
            // Search the corresponding formal argument
            final FormalArg<?> farg = fargs.getArg(name);
            if (farg == null) {
                // No formal argument was found.
                // In STRICT mode, this is a non compliance.
                // In LOOSE mode, this can be ignored.
                if (strictness == Strictness.STRICT) {
                    LOGGER.debug("Unexpected effective arg name: {}", name);
                    return false;
                }
            } else if (!farg.isCompliantWith(value)) {
                // A corresponding formal argument was found, but there is a compliance problem
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Non compliant value: '{}' for: '{}', expected class: '{}'",
                                 Operators.toStringWithClass(value),
                                 name,
                                 farg.getType().getCanonicalName());
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Returns {@code true} when all mandatory formal arguments have a corresponding
     * effective argument.
     * <p>
     * <b>WARNING:</b> This does not check that arguments are compliant.
     *
     * @param fargs The formal arguments.
     * @return {@code true} when all mandatory arguments of {@code fargs} have a corresponding
     *         effective argument in {@code args}.
     */
    private boolean allMandatoryFormalArgsAreSet(FormalArgs fargs) {
        Checks.isNotNull(fargs, "fargs");

        // Iterate on all formal arguments.
        for (final FormalArg<?> farg : fargs.getArgs()) {
            // A mandatory formal argument is found, but no corresponding effective argument
            if (farg.isMandatory() && !hasArg(farg.getName())) {
                LOGGER.debug("Missing mandatory formal arg: {} to: {}", farg, this);
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true when this Args is compliant with formal arguments and strictness.
     *
     * @param fargs The formal arguments.
     * @param strictness The compliance strictness.
     * @return {@code true} if this Args is compliant with {@code fargs} and {@code strictness}.
     */
    public boolean isCompliantWith(FormalArgs fargs,
                                   Strictness strictness) {
        Checks.isNotNull(fargs, "fargs");

        return allArgsAreCompliant(fargs, strictness)
                && allMandatoryFormalArgsAreSet(fargs);
    }

    /**
     * Returns true when this Args is loosely compliant with formal arguments.
     *
     * @param fargs The formal arguments.
     * @return {@code true} if this Args is loosely compliant with {@code fargs}.
     */
    public boolean isLooselyCompliantWith(FormalArgs fargs) {
        return isCompliantWith(fargs, Strictness.LOOSE);
    }

    /**
     * Returns true when this Args is strictly compliant with formal arguments.
     *
     * @param fargs The formal arguments.
     * @return {@code true} if this Args is strictly compliant with {@code fargs}.
     */
    public boolean isStrictlyCompliantWith(FormalArgs fargs) {
        return isCompliantWith(fargs, Strictness.STRICT);
    }

    public boolean isConflictingWith(FormalArgs fargs) {
        return !allArgsAreCompliant(fargs, Strictness.LOOSE);
    }

    /**
     * Returns {@code true} if this arguments is compliant with at least one formal argument tuple.
     *
     * @param strictness The compliance strictness.
     * @param collection The collection of formal arguments tuples.
     * @return {@code true} if this arguments is compliant with at least one element of {@code collection}.
     */
    public boolean isCompliantWithOneOf(Strictness strictness,
                                        Collection<FormalArgs> collection) {
        Checks.isNotNull(strictness, "strictness");
        Checks.isNotNull(collection, "collection");

        for (final FormalArgs fargs : collection) {
            if (isCompliantWith(fargs, strictness)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns {@code true} if this arguments is loosely compliant with at least one formal argument tuple.
     *
     * @param collection The collection of formal arguments tuples.
     * @return {@code true} if this arguments is loosely compliant with at least one element of {@code collection}.
     */
    public boolean isLooselyCompliantWithOneOf(Collection<FormalArgs> collection) {
        Checks.isNotNull(collection, "collection");

        return isCompliantWithOneOf(Strictness.LOOSE, collection);
    }

    /**
     * Returns {@code true} if this arguments is strictly compliant with at least one formal argument tuple.
     *
     * @param collection The collection of formal arguments tuples.
     * @return {@code true} if this arguments is strictly compliant with at least one element of {@code collection}.
     */
    public boolean isStrictlyCompliantWithOneOf(Collection<FormalArgs> collection) {
        Checks.isNotNull(collection, "collection");

        return isCompliantWithOneOf(Strictness.STRICT, collection);
    }

    /**
     * Returns {@code true} if this arguments is compliant with at least one formal argument tuple.
     *
     * @param strictness The compliance strictness.
     * @param array The array of formal arguments tuples.
     * @return {@code true} if this arguments is compliant with at least one element of {@code array}.
     */
    public boolean isCompliantWithOneOf(Strictness strictness,
                                        FormalArgs... array) {
        Checks.isNotNull(strictness, "strictness");
        Checks.isNotNull(array, "array");

        for (final FormalArgs fargs : array) {
            if (isCompliantWith(fargs, strictness)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns {@code true} if this arguments is loosely compliant with at least one formal argument tuple.
     *
     * @param array The array of formal arguments tuples.
     * @return {@code true} if this arguments is loosely compliant with at least one element of {@code array}.
     */
    public boolean isLooselyCompliantWithOneOf(FormalArgs... array) {
        Checks.isNotNull(array, "array");

        return isCompliantWithOneOf(Strictness.LOOSE, array);
    }

    /**
     * Returns {@code true} if this arguments is strictly compliant with at least one formal argument tuple.
     *
     * @param array The array of formal arguments tuples.
     * @return {@code true} if this arguments is strictly compliant with at least one element of {@code array}.
     */
    public boolean isStrictlyCompliantWithOneOf(FormalArgs... array) {
        Checks.isNotNull(array, "array");

        return isCompliantWithOneOf(Strictness.STRICT, array);
    }

    /**
     * Checks compliance of this arguments with one of a collection of formal arguments.
     *
     * @param strictness The strictness.
     * @param collection The collection of formal arguments.
     * @throws InvalidArgsException If there is no compliance.
     */
    public void checkComplianceWithOneOf(Strictness strictness,
                                         Collection<FormalArgs> collection) {
        Checks.isNotNull(strictness, "strictness");
        Checks.isNotNull(collection, "collection");

        if (!isCompliantWithOneOf(strictness, collection)) {
            throw new InvalidArgsException(this + " is not " + strictness + " compliant with one of " + collection);
        }
    }

    /**
     * Checks loose compliance of this arguments with one of a collection of formal arguments.
     *
     * @param collection The collection of formal arguments.
     * @throws InvalidArgsException If there is no compliance.
     */
    public void checkLooseComplianceWithOneOf(Collection<FormalArgs> collection) {
        Checks.isNotNull(collection, "collection");

        checkComplianceWithOneOf(Strictness.LOOSE, collection);
    }

    /**
     * Checks strict compliance of this arguments with one of a collection of formal arguments.
     *
     * @param collection The collection of formal arguments.
     * @throws InvalidArgsException If there is no compliance.
     */
    public void checkStrictComplianceWithOneOf(Collection<FormalArgs> collection) {
        Checks.isNotNull(collection, "collection");

        checkComplianceWithOneOf(Strictness.STRICT, collection);
    }

    /**
     * Checks compliance of the arguments with formal arguments tuples.
     *
     * @param strictness The compliance strictness.
     * @param array The array of formal arguments tuples.
     * @throws InvalidArgsException When there is no compliance.
     */
    public void checkComplianceWithOneOf(Strictness strictness,
                                         FormalArgs... array) {
        Checks.isNotNull(strictness, "strictness");
        Checks.isNotNull(array, "array");

        if (!isCompliantWithOneOf(strictness, array)) {
            throw new InvalidArgsException(this + " is not " + strictness + " compliant with one of " + Arrays.toString(array));
        }
    }

    /**
     * Checks loose compliance of the arguments with formal arguments tuples.
     *
     * @param array The array of formal arguments tuples.
     * @throws IllegalArgumentException When there is no compliance.
     */
    public void checkLooseComplianceWithOneOf(FormalArgs... array) {
        Checks.isNotNull(array, "array");

        checkComplianceWithOneOf(Strictness.LOOSE, array);
    }

    /**
     * Checks strict compliance of the arguments with formal arguments tuples.
     *
     * @param array The array of formal arguments tuples.
     * @throws IllegalArgumentException When there is no compliance.
     */
    public void checkStrictComplianceWithOneOf(FormalArgs... array) {
        Checks.isNotNull(array, "array");

        checkComplianceWithOneOf(Strictness.STRICT, array);
    }

    /**
     * Returns {@code true} when an arguments is compliant with a formal arguments.
     *
     * @param args The arguments.
     * @param fargs The formal arguments.
     * @param strictness The strictness.
     * @return {@code true} when {@code args} is compliant with {@code fargs}.
     */
    public static boolean areCompliant(Args args,
                                       FormalArgs fargs,
                                       Strictness strictness) {
        Checks.isNotNull(args, "args");
        Checks.isNotNull(fargs, "fargs");
        Checks.isNotNull(strictness, "strictness");

        return args.isCompliantWith(fargs, strictness);
    }

    /**
     * Returns {@code true} when an arguments is loosely compliant with a formal arguments.
     *
     * @param args The arguments.
     * @param fargs The formal arguments.
     * @return {@code true} when {@code args} is loosely compliant with {@code fargs}.
     */
    public static boolean areLooselyCompliant(Args args,
                                              FormalArgs fargs) {
        Checks.isNotNull(args, "args");
        Checks.isNotNull(fargs, "fargs");

        return args.isLooselyCompliantWith(fargs);
    }

    /**
     * Returns {@code true} when an arguments is strictly compliant with a formal arguments.
     *
     * @param args The arguments.
     * @param fargs The formal arguments.
     * @return {@code true} when {@code args} is strictly compliant with {@code fargs}.
     */
    public static boolean areStrictlyCompliant(Args args,
                                               FormalArgs fargs) {
        Checks.isNotNull(args, "args");
        Checks.isNotNull(fargs, "fargs");

        return args.isStrictlyCompliantWith(fargs);
    }

    public static boolean areCompliant(Args args,
                                       Strictness strictness,
                                       Collection<FormalArgs> collection) {
        Checks.isNotNull(args, "args");

        return args.isCompliantWithOneOf(strictness, collection);
    }

    public static boolean areLooselyCompliant(Args args,
                                              Collection<FormalArgs> collection) {
        Checks.isNotNull(args, "args");

        return args.isLooselyCompliantWithOneOf(collection);
    }

    public static boolean areStrictlyCompliant(Args args,
                                               Collection<FormalArgs> collection) {
        Checks.isNotNull(args, "args");

        return args.isStrictlyCompliantWithOneOf(collection);
    }

    public static boolean areCompliant(Args args,
                                       Strictness strictness,
                                       FormalArgs... array) {
        Checks.isNotNull(args, "args");

        return args.isCompliantWithOneOf(strictness, array);
    }

    public static boolean areLooselyCompliant(Args args,
                                              FormalArgs... array) {
        Checks.isNotNull(args, "args");

        return args.isLooselyCompliantWithOneOf(array);
    }

    public static boolean areStrictlyCompliant(Args args,
                                               FormalArgs... array) {
        Checks.isNotNull(args, "args");

        return args.isStrictlyCompliantWithOneOf(array);
    }

    public static void checkCompliance(Args args,
                                       Strictness strictness,
                                       Collection<FormalArgs> collection) {
        Checks.isNotNull(args, "args");

        args.checkComplianceWithOneOf(strictness, collection);
    }

    public static void checkLooseCompliance(Args args,
                                            Collection<FormalArgs> collection) {
        Checks.isNotNull(args, "args");

        args.checkLooseComplianceWithOneOf(collection);
    }

    public static void checkStrictCompliance(Args args,
                                             Collection<FormalArgs> collection) {
        Checks.isNotNull(args, "args");

        args.checkStrictComplianceWithOneOf(collection);
    }

    public static void checkCompliance(Args args,
                                       Strictness strictness,
                                       FormalArgs... array) {
        Checks.isNotNull(args, "args");

        args.checkComplianceWithOneOf(strictness, array);
    }

    public static void checkLooseCompliance(Args args,
                                            FormalArgs... array) {
        Checks.isNotNull(args, "args");

        args.checkLooseComplianceWithOneOf(array);
    }

    public static void checkStrictCompliance(Args args,
                                             FormalArgs... array) {
        Checks.isNotNull(args, "args");

        args.checkStrictComplianceWithOneOf(array);
    }

    public static FormalArgs reduceNecessity(FormalArgs fargs,
                                             Args args) {
        Checks.isNotNull(fargs, "fargs");
        Checks.isNotNull(args, "args");
        if (args.isConflictingWith(fargs)) {
            throw new IllegalArgumentException("Conflict between " + fargs + " and " + args);
        } else if (!fargs.hasArgs(Necessity.MANDATORY) || args.isEmpty()) {
            return fargs;
        } else {
            final List<FormalArg<?>> tmp = new ArrayList<>();
            for (final FormalArg<?> farg : fargs.getArgs()) {
                if (farg.isOptional() || !args.hasArgWithMatchingName(farg)) {
                    tmp.add(farg);
                } else {
                    tmp.add(new FormalArg<>(farg.getName(), farg.getType(), Necessity.OPTIONAL));
                }
            }
            return new FormalArgs(tmp);
        }
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (final String name : getNames()) {
            result += name.hashCode();
            final Object value = getValue(name);
            if (value != null) {
                result += value.hashCode();
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Args)) {
            return false;
        }
        final Args other = (Args) object;
        return map.equals(other.map);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("(");
        boolean first = true;
        for (final Arg arg : map.values()) {
            if (!first) {
                builder.append(", ");
            }
            builder.append(arg.getName());
            builder.append(" => ");
            builder.append(arg.getValue());
            first = false;
        }
        builder.append(")");
        return builder.toString();
    }

    /**
     * @return A new Builder instance.
     */
    public static Builder builder() {
        return builder(null);
    }

    public static Builder builder(FormalArgs fargs) {
        return new Builder(fargs);
    }

    /**
     * Helper class used to create Args.
     *
     * @author Damien Carbonne
     *
     */
    public static final class Builder {
        private final FormalArgs fargs;
        private final Map<String, Arg> args = new LinkedHashMap<>();

        Builder(FormalArgs fargs) {
            this.fargs = fargs;
        }

        public Builder clear() {
            args.clear();
            return this;
        }

        public FormalArgs getFormalArgs() {
            return fargs;
        }

        public boolean hasArg(String name) {
            return args.containsKey(name);
        }

        public boolean hasArgMatchingName(FormalArg<?> farg) {
            return hasArg(farg.getName());
        }

        public Arg getArg(String name) {
            return args.get(name);
        }

        public Builder arg(Arg arg) {
            Checks.isNotNull(arg, "arg");
            if (fargs != null && !arg.isCompliantWith(fargs)) {
                throw new IllegalArgumentException("Arg " + arg + " is not compliant with " + fargs);
            }
            args.put(arg.getName(), arg);
            return this;
        }

        public Builder arg(String name,
                           Object value) {
            return arg(new Arg(name, value));
        }

        public final <T> Builder arg(FormalArg<T> farg,
                                     T value) {
            Checks.isNotNull(farg, "farg");
            if (farg.isOptional() && value == null) {
                return this;
            } else {
                return arg(new Arg(farg, value));
            }
        }

        public final Builder argRaw(FormalArg<?> farg,
                                    Object value) {
            Checks.isNotNull(farg, "farg");
            if (farg.isOptional() && value == null) {
                return this;
            } else if (farg.isCompliantWith(value)) {
                return arg(farg.getName(), value);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public Builder args(Args other) {
            Checks.isNotNull(other, "other");
            for (final Arg arg : other.getArgs()) {
                arg(arg);
            }
            return this;
        }

        public Args build() {
            final Arg[] tmp = new Arg[args.size()];
            int index = 0;
            for (final Arg arg : args.values()) {
                tmp[index] = arg;
                index++;
            }
            return new Args(tmp);
        }
    }
}