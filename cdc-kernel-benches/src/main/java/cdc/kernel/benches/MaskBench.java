package cdc.kernel.benches;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.enums.AbstractMask;
import cdc.enums.MaskSupport;
import cdc.enums.Nullable;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1,
        jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5,
        time = 100,
        timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5,
        time = 100,
        timeUnit = TimeUnit.MILLISECONDS)
public class MaskBench {

    private enum X {
        A,
        B,
        C
    }

    private static class XMask extends AbstractMask<XMask, X> {
        static final MaskSupport<XMask, X> NON_NULLABLE_SUPPORT = support(XMask.class,
                                                                          XMask::new,
                                                                          X.class,
                                                                          Nullable.FALSE);
        static final MaskSupport<XMask, X> NULLABLE_SUPPORT = support(XMask.class,
                                                                      XMask::new,
                                                                      X.class,
                                                                      Nullable.TRUE);

        private XMask(MaskSupport<XMask, X> support,
                      Set<X> values) {
            super(support, values);
        }
    }

    static final List<XMask> NON_NULLABLE_MASKS = new ArrayList<>();
    static final List<XMask> NULLABLE_MASKS = new ArrayList<>();

    static {
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create());
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.A));
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.B));
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.C));
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.A, X.B));
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.A, X.C));
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.B, X.C));
        NON_NULLABLE_MASKS.add(XMask.NON_NULLABLE_SUPPORT.create(X.A, X.B, X.C));

        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create());
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.A));
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.B));
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.C));
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.A, X.B));
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.A, X.C));
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.B, X.C));
        NULLABLE_MASKS.add(XMask.NULLABLE_SUPPORT.create(X.A, X.B, X.C));

    }

    @Param({ "0", "1", "2", "3", "4", "5", "6", "7" })
    public int xIndex;

    @Param({ "0", "1", "2", "3", "4", "5", "6", "7" })
    public int yIndex;

    @Benchmark
    public XMask benchNonNullableAnd() {
        return NON_NULLABLE_MASKS.get(xIndex).and(NON_NULLABLE_MASKS.get(yIndex));
    }

    @Benchmark
    public XMask benchNullableAnd() {
        return NULLABLE_MASKS.get(xIndex).and(NULLABLE_MASKS.get(yIndex));
    }

    @Benchmark
    public XMask benchNonNullableOr() {
        return NON_NULLABLE_MASKS.get(xIndex).or(NON_NULLABLE_MASKS.get(yIndex));
    }

    @Benchmark
    public XMask benchNullableOr() {
        return NULLABLE_MASKS.get(xIndex).or(NULLABLE_MASKS.get(yIndex));
    }

    @Benchmark
    public XMask benchNonNullableNot() {
        return NON_NULLABLE_MASKS.get(xIndex).not();
    }

    @Benchmark
    public XMask benchNullableNot() {
        return NULLABLE_MASKS.get(xIndex).not();
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(MaskBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", MaskBench.class, ".csv"))
                                    .forks(0)
                                    .build();
        new Runner(opt).run();
    }

}