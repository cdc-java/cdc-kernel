package cdc.informers;

import java.util.function.Function;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Base interface of informers related to identified objects.
 *
 * @author Damien Carbonne
 *
 * @param <O> The object type.
 * @param <I> The object identifier type.
 */
public interface IdentifiableInformer<O, I> extends Informer<O> {
    /**
     * @return The class of identifiers of objects supported by this informer.
     */
    public Class<I> getIdClass();

    /**
     * Returns the identifier of an object.
     *
     * @param object The object.
     * @return The identifier of {@code object} or {@code null}.
     */
    public I getId(O object);

    public static class Service {
        protected Service() {
        }

        public static Object getId(Object object,
                                   FailureReaction reaction) {
            final Function<IdentifiableInformer<Object, Object>, Function<?, Object>> extractor = i -> i::getId;
            return Informers.applyOnObject(Introspection.uncheckedCast(IdentifiableInformer.class),
                                           object,
                                           extractor,
                                           reaction);
        }

        public static Object getId(Object object) {
            return getId(object, FailureReaction.FAIL);
        }

        public static Class<?> getIdClass(Object object,
                                          FailureReaction reaction) {
            final Function<IdentifiableInformer<?, ?>, Class<?>> f = IdentifiableInformer::getIdClass;
            return Informers.applyOnInformer(IdentifiableInformer.class,
                                             object,
                                             f,
                                             reaction);
        }

        public static Class<?> getIdClass(Object object) {
            return getIdClass(object, FailureReaction.FAIL);
        }

        public static Class<?> getIdClass(Class<?> objectClass,
                                          FailureReaction reaction) {
            final Function<IdentifiableInformer<?, ?>, Class<?>> f = IdentifiableInformer::getIdClass;
            return Informers.applyOnInformer(IdentifiableInformer.class,
                                             objectClass,
                                             f,
                                             reaction);
        }

        public static Class<?> getIdClass(Class<?> objectClass) {
            return getIdClass(objectClass, FailureReaction.FAIL);
        }
    }
}