package cdc.informers;

import java.util.function.Function;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Interface of informers that are a registry of identifiable objects.
 *
 * @author Damien Carbonne
 *
 * @param <O> The object type.
 * @param <I> The object identifier type.
 */
public interface RegistryInformer<O, I> extends IdentifiableInformer<O, I> {
    /**
     * Returns the object associated to an identifier.
     *
     * @param id The identifier.
     * @return The object associated to {@code id}.
     */
    public O getObject(I id);

    /**
     * Returns {@code true} if an identifier has an associated registered object.
     *
     * @param id The identifier.
     * @return {@code true} if {@code id} is associated to a registered object.
     */
    public default boolean hasRegisteredObject(I id) {
        return getObject(id) != null;
    }

    public static class Service extends IdentifiableInformer.Service {
        protected Service() {
        }

        public static <O> O getObject(Class<O> objectClass,
                                      Object id,
                                      FailureReaction reaction) {
            final Function<RegistryInformer<O, Object>, Function<Object, O>> extractor = i -> i::getObject;
            return Informers.applyOnValue(Introspection.uncheckedCast(RegistryInformer.class),
                                          objectClass,
                                          id,
                                          extractor,
                                          reaction);
        }

        public static <O> O getObject(Class<O> objectClass,
                                      Object id) {
            return getObject(objectClass, id, FailureReaction.FAIL);
        }

        public static <O> boolean hasRegisteredObject(Class<O> objectClass,
                                                      Object id,
                                                      FailureReaction reaction) {
            final Function<RegistryInformer<O, Object>, Function<Object, Boolean>> extractor = i -> i::hasRegisteredObject;
            return Informers.applyOnValue(Introspection.uncheckedCast(RegistryInformer.class),
                                          objectClass,
                                          id,
                                          extractor,
                                          reaction);
        }

        public static <O> boolean hasRegisteredObject(Class<O> objectClass,
                                                      Object id) {
            return hasRegisteredObject(objectClass, id, FailureReaction.FAIL);
        }
    }
}