package cdc.informers;

public abstract class AbstractIdentifiableInformer<O, I> extends AbstractInformer<O> implements IdentifiableInformer<O, I> {
    private final Class<I> idClass;

    protected AbstractIdentifiableInformer(Class<O> objectClass,
                                           Class<I> idClass) {
        super(objectClass);
        this.idClass = idClass;
    }

    @Override
    public final Class<I> getIdClass() {
        return idClass;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "<" + getObjectClass().getSimpleName() + ", " + getIdClass().getSimpleName() + ">";
    }
}