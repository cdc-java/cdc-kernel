package cdc.informers;

import java.util.function.Function;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Base interface of informers that can convert an object to a string and the reverse operation.
 *
 * @author Damien Carbonne
 *
 * @param <O> The object type.
 */
public interface StringConversionInformer<O> extends Informer<O> {
    /**
     * Converts an object to a String.
     *
     * @param object The object.
     * @return The string representation of {@code object}.
     */
    public String toString(O object);

    /**
     * Converts a string to an object.
     *
     * @param s The string.
     * @return The conversion of {@code s} to an object.
     */
    public O fromString(String s);

    public static class Service {
        protected Service() {
        }

        public static String toString(Object object,
                                      FailureReaction reaction) {
            if (object == null) {
                return null;
            } else {
                final Function<StringConversionInformer<Object>, Function<?, String>> extractor = i -> i::toString;
                return Informers.applyOnObject(Introspection.uncheckedCast(StringConversionInformer.class),
                                               object,
                                               extractor,
                                               reaction);
            }
        }

        public static String toString(Object object) {
            return toString(object, FailureReaction.FAIL);
        }

        public static <O> O fromString(Class<O> objectClass,
                                       String s,
                                       FailureReaction reaction) {
            final Function<StringConversionInformer<O>, Function<String, O>> extractor = i -> i::fromString;
            return Informers.applyOnValue(Introspection.uncheckedCast(StringConversionInformer.class),
                                          objectClass,
                                          s,
                                          extractor,
                                          reaction);
        }

        public static <O> O fromString(Class<O> objectClass,
                                       String s) {
            return fromString(objectClass, s, FailureReaction.FAIL);
        }
    }
}