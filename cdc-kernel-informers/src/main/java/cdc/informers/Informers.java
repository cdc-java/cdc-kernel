package cdc.informers;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.debug.Printable;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;
import cdc.util.lang.NotFoundException;
import cdc.util.lang.ValueHolder;

/**
 * Registry of informers.
 *
 * @author Damien Carbonne
 *
 */
public final class Informers {
    private static final Logger LOGGER = LogManager.getLogger(Informers.class);
    public static final Printable PRINTER = new Printer();

    static {
        Printables.register(Informers.class, PRINTER);
    }

    /**
     * Set of registered informers.
     */
    private static final Set<Informer<?>> INFORMERS = new HashSet<>();

    /**
     * Map from informers to associated topics.
     */
    private static final Map<Informer<?>, Set<Class<? extends Informer<?>>>> INFORMERS_TO_TOPICS = new HashMap<>();

    /**
     * Set of known topics.
     */
    private static final Set<Class<? extends Informer<?>>> TOPICS = new HashSet<>();

    /**
     * Set of classes for which informers are registered.
     */
    private static final Set<Class<?>> OBJECT_CLASSES = new HashSet<>();

    /**
     * Data associated to classes.
     * <p>
     * It is a map from topics to informers (that implement the topic).
     */
    private static final Map<Class<?>, ObjectClassData> OBJECT_CLASS_TO_DATA = new HashMap<>();

    private static class ObjectClassData {
        ObjectClassData() {
        }

        final List<Informer<?>> informers = new ArrayList<>();
        final Map<Class<? extends Informer<?>>, Informer<?>> topicToInformer = new HashMap<>();
    }

    private Informers() {
    }

    /**
     * Registers an informer.
     *
     * @param informer The informer.
     * @throws IllegalArgumentException when {@code informer} is {@code null} or already registered.
     */
    public static void register(Informer<?> informer) {
        LOGGER.debug("register({})", informer);
        Checks.isNotNull(informer, "informer");
        if (INFORMERS.contains(informer)) {
            throw new IllegalArgumentException("Already registered");
        }

        INFORMERS.add(informer);
        OBJECT_CLASSES.add(informer.getObjectClass());

        // Compute all topics associated to this informer
        final Set<Class<? extends Informer<?>>> topics = new HashSet<>();
        Introspection.traverseAllInterfaces(informer.getClass(),
                                            cls -> {
                                                if (cls.isInterface() && Informer.class.isAssignableFrom(cls)) {
                                                    topics.add(Introspection.uncheckedCast(cls));
                                                }
                                            });
        INFORMERS_TO_TOPICS.put(informer, topics);

        TOPICS.addAll(topics);

        // Use this informer for the associated class and for all supported topics
        final ObjectClassData data =
                OBJECT_CLASS_TO_DATA.computeIfAbsent(informer.getObjectClass(), k -> new ObjectClassData());

        data.informers.add(informer);

        for (final Class<? extends Informer<?>> topic : topics) {
            if (data.topicToInformer.containsKey(topic)) {
                LOGGER.warn("Replace informer");
            }
            data.topicToInformer.put(topic, informer);
        }

    }

    /**
     * @return A Set of registered informers.
     */
    public static Set<Informer<?>> getInformers() {
        return INFORMERS;
    }

    /**
     * Returns A Set of topics supported by an informer.
     *
     * @param informer The informer.
     * @return A Set of topics supported by {@code informer}.
     */
    public static Set<Class<? extends Informer<?>>> getInformerTopics(Informer<?> informer) {
        return INFORMERS_TO_TOPICS.getOrDefault(informer, Collections.emptySet());
    }

    /**
     * @return A set of known topics.
     */
    public static Set<Class<? extends Informer<?>>> getTopics() {
        return TOPICS;
    }

    /**
     * @return A Set of object classes for which at least one informer has been registered.
     */
    public static Set<Class<?>> getObjectClasses() {
        return OBJECT_CLASSES;
    }

    /**
     * Returns the topics that are supported for an object class.
     * <p>
     * <b>WARNING:</b> This does not take {@code objectClass} inheritance into account.
     *
     * @param <O> The object type.
     * @param objectClass The object class.
     * @return A set of topics supported for {@code objectClass}.
     */
    public static <O> Set<Class<? extends Informer<O>>> getObjectClassTopics(Class<O> objectClass) {
        final ObjectClassData data = OBJECT_CLASS_TO_DATA.get(objectClass);
        return data == null ? Collections.emptySet() : Introspection.uncheckedCast(data.topicToInformer.keySet());
    }

    /**
     * A list of informers associated to an object class in the order they were declared.
     * <p>
     * <b>WARNING:</b> This does not take {@code objectClass} inheritance into account.
     *
     * @param <O> The object type.
     * @param objectClass The object class.
     * @return A list of informers associated to {@code objectClass} in the order they were declared.
     */
    public static <O> List<Informer<O>> getObjectClassInformers(Class<O> objectClass) {
        final ObjectClassData data = OBJECT_CLASS_TO_DATA.get(objectClass);
        return data == null ? Collections.emptyList() : Introspection.uncheckedCast(data.informers);
    }

    /**
     * Returns the informer related to a topic for an object class.
     * <p>
     * An exact matching is performed.
     *
     * @param <O> The object type.
     * @param <T> The topic type.
     * @param objectClass The object class.
     * @param topic The topic class.
     * @param reaction The reaction to adopt when no informer is found.
     * @return The informer related to {@code topic} for {@code objectClass} or {@code null}.
     * @throws NotFoundException when no informer is found and {@code reaction} is FAIL.
     */
    public static <O, T extends Informer<O>> T getInformer(Class<O> objectClass,
                                                           Class<T> topic,
                                                           FailureReaction reaction) {
        return Introspection.uncheckedCast(getInformerRaw(objectClass, topic, reaction));
    }

    /**
     * Returns the informer related to a topic for an object class.
     * <p>
     * An exact matching is performed.
     *
     * @param <T> The topic type.
     * @param objectClass The object class.
     * @param topic The topic class.
     * @param reaction The reaction to adopt when no informer is found.
     * @return The informer related to {@code topic} for {@code objectClass} or {@code null}.
     * @throws NotFoundException when no informer is found and {@code reaction} is FAIL.
     */
    public static <T extends Informer<?>> T getInformerRaw(Class<?> objectClass,
                                                           Class<T> topic,
                                                           FailureReaction reaction) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(topic, "topic");

        final ObjectClassData data = OBJECT_CLASS_TO_DATA.get(objectClass);

        final T result;
        if (data == null) {
            result = null;
        } else {
            result = Introspection.uncheckedCast(data.topicToInformer.get(topic));
        }
        return NotFoundException.onResult(result,
                                          "Could not find " + topic.getSimpleName() + " informer for " + objectClass.getCanonicalName(),
                                          LOGGER,
                                          reaction,
                                          null);
    }

    /**
     * Returns the best informer related to a topic for an object class.
     * <p>
     * If not matching is found for {@code objectClass}, a search is performed on ancestors.
     *
     * @param <O> The object type.
     * @param <T> The topic type.
     * @param objectClass The object class.
     * @param topic The topic class.
     * @param reaction The reaction to adopt when no informer is found.
     * @return The informer related to {@code topic} for {@code objectClass} or {@code null}.
     * @throws NotFoundException when no informer is found and {@code reaction} is FAIL.
     */
    public static <O, T extends Informer<? super O>> T getBestInformer(Class<O> objectClass,
                                                                       Class<T> topic,
                                                                       FailureReaction reaction) {
        return Introspection.uncheckedCast(getBestInformerRaw(objectClass, topic, reaction));
    }

    /**
     * Returns the best informer related to a topic for an object class.
     * <p>
     * If not matching is found for {@code objectClass}, a search is performed on ancestors.
     *
     * @param <T> The topic type.
     * @param objectClass The object class.
     * @param topic The topic class.
     * @param reaction The reaction to adopt when no informer is found.
     * @return The informer related to {@code topic} for {@code objectClass} or {@code null}.
     * @throws NotFoundException when no informer is found and {@code reaction} is FAIL.
     */
    public static <T extends Informer<?>> T getBestInformerRaw(Class<?> objectClass,
                                                               Class<T> topic,
                                                               FailureReaction reaction) {
        final ValueHolder<T> result = new ValueHolder<>();
        Introspection.traverseAllSuperClasses(objectClass,
                                              true,
                                              cls -> {
                                                  if (result.value == null) {
                                                      result.value = getInformerRaw(cls, topic, FailureReaction.DEFAULT);
                                                  }
                                              });
        return NotFoundException.onResult(result.value,
                                          "Could not find " + topic.getSimpleName() + " best informer for " + objectClass.getCanonicalName(),
                                          LOGGER,
                                          reaction,
                                          null);
    }

    /**
     * Utility function that can return a result using the best informer for an object on a topic.
     * <p>
     * If, using inheritance, we wrote this:<pre>{@code
     *    public interface Foo {
     *       public String foo();
     *
     *       public static String sfoo(Foo object) {
     *          return object == null ? null : object.foo();
     *       }
     *    }
     * }</pre>
     * <p>
     * It could be replaced by something that would look like this (<b>note:</b> this code does not compile and needs some adaptations.):<pre>{@code
     *    public interface Foo<O> extends Informer<O> {
     *       public String foo(O object);
     *
     *       public static String sfoo(Object object) {
     *         return Informers.apply(Foo.class, object, informer -> informer::foo, FailureReaction.DEFAULT);
     *       }
     *    }
     * }</pre>
     *
     * @param <T> The topic type
     * @param <R> The result type.
     * @param topic The topic of the searched informer.
     * @param object The object for which an informer is searched.
     * @param extractor A function that accepts an informer (of type {@code <T>}) as input
     *            and returns a function that accepts a value (of type {@code <O>})
     *            as input and returns a result (of type {@code <R>}).
     * @param reaction The reaction to adopt in case of failure.
     * @return The result obtained by applying the extracted function to {@code object}, or {@code null}.
     * @throws NotFoundException when no informer is found and {@code reaction} is FAIL.
     * @throws IllegalArgumentException When {@code topic} or {@code object} is {@code null}.
     */
    public static <T extends Informer<?>, R> R applyOnObject(Class<T> topic,
                                                             Object object,
                                                             Function<T, Function<?, R>> extractor,
                                                             FailureReaction reaction) {
        Checks.isNotNull(topic, "topic");
        Checks.isNotNull(object, "object");

        final T informer = getBestInformerRaw(object.getClass(), topic, reaction);
        if (informer == null) {
            // Reaction is already handled by getInformerRaw
            return null;
        } else {
            @SuppressWarnings("unchecked")
            final Function<Object, R> function = (Function<Object, R>) extractor.apply(informer);
            // object has a type that is compliant with informer.getObjectClass()
            return function.apply(object);
        }
    }

    /**
     * Utility function that ...
     *
     * @param <T> The topic type.
     * @param <V> The value type.
     * @param <R> The result type.
     * @param topic The topic.
     * @param objectClass The object class for which the best informer is searched.
     * @param value The value.
     * @param extractor A function that accepts a topic (of type {@code <T>}) as input
     *            and returns a function that accepts a value
     *            (of type {@code <V>}) as input and returns a
     *            result (of type {@code <R>}).
     * @param reaction The reaction to adopt in case of failure.
     * @return TODO
     */
    public static <T extends Informer<?>, V, R> R applyOnValue(Class<T> topic,
                                                               Class<?> objectClass,
                                                               V value,
                                                               Function<T, Function<V, R>> extractor,
                                                               FailureReaction reaction) {
        Checks.isNotNull(topic, "topic");
        Checks.isNotNull(objectClass, "objectClass");

        final T informer = getBestInformerRaw(objectClass, topic, reaction);
        if (informer == null) {
            // Reaction is already handled by getInformerRaw
            return null;
        } else {
            final Function<V, R> function = extractor.apply(informer);
            return function.apply(value);
        }
    }

    public static <T extends Informer<?>, R> R applyOnInformer(Class<T> topic,
                                                               Object object,
                                                               Function<?, R> function,
                                                               FailureReaction reaction) {
        Checks.isNotNull(topic, "topic");
        Checks.isNotNull(object, "object");

        return applyOnInformer(topic, object.getClass(), function, reaction);
    }

    public static <T extends Informer<?>, R> R applyOnInformer(Class<T> topic,
                                                               Class<?> objectClass,
                                                               Function<?, R> function,
                                                               FailureReaction reaction) {
        Checks.isNotNull(topic, "topic");
        Checks.isNotNull(objectClass, "objectClass");

        final T informer = getBestInformerRaw(objectClass, topic, reaction);
        if (informer == null) {
            // Reaction is already handled by getInformerRaw
            return null;
        } else {
            @SuppressWarnings("unchecked")
            final Function<Object, R> f = (Function<Object, R>) function;
            return f.apply(informer);
        }
    }

    protected static class Printer implements Printable {
        @Override
        public void print(PrintStream out,
                          int level) {
            indent(out, level);
            out.println("Informers (" + Informers.getInformers().size() + ")");
            for (final Informer<?> informer : getInformers()) {
                indent(out, level + 1);
                out.println(Informer.toString(informer) + " (topics: " + getInformerTopics(informer).size() + ")");
                for (final Class<? extends Informer<?>> topic : getInformerTopics(informer)) {
                    indent(out, level + 2);
                    out.println("topic: " + topic.getSimpleName());
                }
            }

            indent(out, level);
            out.println("Object classes (" + Informers.getObjectClasses().size() + ")");
            for (final Class<?> objectClass : Informers.getObjectClasses()) {
                indent(out, level + 1);
                out.println(objectClass.getSimpleName()
                        + " (informers: " + Informers.getObjectClassInformers(objectClass).size()
                        + " topics: " + Informers.getObjectClassTopics(objectClass).size() + ")");
                for (final Class<? extends Informer<?>> topic : Informers.getObjectClassTopics(objectClass)) {
                    indent(out, level + 2);
                    out.println("topic: " + topic.getSimpleName());
                }
                for (final Informer<?> informer : Informers.getObjectClassInformers(objectClass)) {
                    indent(out, level + 2);
                    out.println("informer: " + Informer.toString(informer));
                }
            }

            indent(out, level);
            out.println("Topics (" + Informers.getTopics().size() + ")");
            for (final Class<? extends Informer<?>> topic : Informers.getTopics()) {
                indent(out, level + 1);
                out.println(topic.getSimpleName());
            }
        }
    }
}