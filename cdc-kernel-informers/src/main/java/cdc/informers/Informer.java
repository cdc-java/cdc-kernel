package cdc.informers;

/**
 * Base informer interface.
 * <p>
 * An informer can inform on a topic.<br>
 * A topic is an informer class.
 *
 * @author Damien Carbonne
 *
 * @param <O> The object type.
 */
public interface Informer<O> {
    /**
     * @return The class of objects that are supported by this informer.
     */
    public Class<O> getObjectClass();

    public static String toString(Informer<?> informer) {
        return informer.getClass().getSimpleName() + "<" + informer.getObjectClass().getSimpleName() + ">";
    }
}