package cdc.informers;

public abstract class AbstractInformer<O> implements Informer<O> {
    private final Class<O> objectClass;

    protected AbstractInformer(Class<O> objectClass) {
        this.objectClass = objectClass;
    }

    @Override
    public final Class<O> getObjectClass() {
        return objectClass;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "<" + getObjectClass().getSimpleName() + ">";
    }
}