package cdc.informers;

import cdc.util.encoding.Encoder;
import cdc.util.encoding.Encoders;
import cdc.util.lang.FailureReaction;

public class EnumStringConversionInformer<E extends Enum<E>> extends AbstractIdentifiableInformer<E, E> implements StringConversionInformer<E> {
    private final Encoder<E, String> encoder;

    public EnumStringConversionInformer(Class<E> enumClass) {
        super(enumClass,
              enumClass);
        this.encoder = Encoders.nameEncoder(enumClass);
    }

    @Override
    public E getId(E object) {
        return object;
    }

    @Override
    public String toString(E id) {
        return id == null ? null : encoder.encode(id, FailureReaction.FAIL);
    }

    @Override
    public E fromString(String code) {
        return code == null ? null : encoder.decode(code, FailureReaction.FAIL);
    }
}