package cdc.enums;

import java.util.List;

import cdc.util.lang.Checks;

public interface ForestDynamicEnum<V extends ForestDynamicEnum<V>> extends DynamicEnum {
    /**
     * @return The parent of this enumeration value.
     */
    public V getParent();

    /**
     * @return The children of this enumeration value.
     */
    public List<V> getChildren();

    /**
     * @param name The name.
     * @return {@code true} if this enumeration value has a child named {@code name}.
     */
    public default boolean hasChildNamed(String name) {
        for (final V child : getChildren()) {
            if (name.equals(child.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param other The other enumeration value.
     * @return {@code true} if this enumeration value is over or equal to {@code other}.
     */
    public default boolean isOverOrEqual(V other) {
        Checks.isNotNull(other, "other");
        V index = other;
        while (index != null) {
            if (index == this) {
                return true;
            }
            index = index.getParent();
        }
        return false;
    }

    /**
     * @param other The other enumeration value.
     * @return {@code true} if this enumeration value is strictly over {@code other}.
     */
    public default boolean isStrictlyOver(V other) {
        Checks.isNotNull(other, "other");
        V index = other;
        while (index != null) {
            if (index == this) {
                return this != other;
            }
            index = index.getParent();
        }
        return false;
    }
}