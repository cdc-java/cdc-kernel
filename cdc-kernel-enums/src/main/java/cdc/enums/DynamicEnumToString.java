package cdc.enums;

import cdc.args.Args;
import cdc.args.Factory;
import cdc.converters.AbstractConverter;
import cdc.converters.Converter;

public class DynamicEnumToString extends AbstractConverter<DynamicEnum, String> {
    public static final DynamicEnumToString INSTANCE = new DynamicEnumToString();

    public static final Factory<DynamicEnumToString> FACTORY = Converter.singleton(INSTANCE);

    private DynamicEnumToString() {
        super(DynamicEnum.class,
              String.class);
    }

    @Override
    public String apply(DynamicEnum source) {
        return source == null ? null : source.getQName();
    }

    @Override
    public Args getParams() {
        return Args.NO_ARGS;
    }
}