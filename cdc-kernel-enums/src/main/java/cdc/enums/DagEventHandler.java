package cdc.enums;

@FunctionalInterface
public interface DagEventHandler {
    public void processEvent(DagEvent event);
}