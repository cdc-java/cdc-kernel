package cdc.enums;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.Converter;
import cdc.converters.defaults.AbstractSequenceConverter;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Converter of EnumMask to String.
 *
 * @author Damien Carbonne
 *
 * @param <M> The mask type.
 * @param <V> The value type.
 */
public class AbstractMaskToString<M extends Mask<M, V>, V> extends AbstractSequenceConverter<M, String> {
    private final Converter<? super V, String> converter;

    public static final FormalArg<Converter<?, String>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PREFIX,
                           SEPARATOR,
                           SUFFIX,
                           CONVERTER);

    @FunctionalInterface
    protected static interface Creator<C, S> {
        public C create(String prefix,
                        String separator,
                        String suffix,
                        Converter<? super S, String> converter);
    }

    protected static <C extends AbstractMaskToString<M, V>, M extends Mask<M, V>, V> Factory<C> factory(Class<C> converterClass,
                                                                                                        Class<M> maskClass,
                                                                                                        Class<V> valueClass,
                                                                                                        Creator<C, V> creator) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, maskClass)
                                          .arg(TARGET_CLASS, String.class)
                                          .build(),
                                      FPARAMS) {
            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                final String prefix = args.getValue(PREFIX, null);
                final String separator = args.getValue(SEPARATOR, null);
                final String suffix = args.getValue(SUFFIX, null);
                final Converter<? super V, String> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                Checks.isTrue(converter.isCompliantSourceClass(valueClass), "Non compliant converter");
                return creator.create(prefix, separator, suffix, converter);
            }
        };
    }

    protected AbstractMaskToString(Class<M> maskClass,
                                   String prefix,
                                   String separator,
                                   String suffix,
                                   Converter<? super V, String> converter) {
        super(maskClass,
              String.class,
              prefix,
              separator,
              suffix);
        this.converter = converter;
    }

    public static <M extends Mask<M, V>, V> AbstractMaskToString<M, V> create(Class<M> maskClass,
                                                                              String prefix,
                                                                              String separator,
                                                                              String suffix,
                                                                              Converter<? super V, String> converter) {
        return new AbstractMaskToString<>(maskClass, prefix, separator, suffix, converter);
    }

    public Converter<? super V, String> getConverter() {
        return converter;
    }

    @Override
    public String apply(M mask) {
        if (mask == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(getPrefix());
            boolean first = true;
            for (final V value : mask.getSupport().getType().getValues()) {
                if (mask.isSet(value)) {
                    if (!first) {
                        builder.append(getSeparator());
                    } else {
                        first = false;
                    }
                    builder.append(converter.apply(value));
                }
            }
            builder.append(getSuffix());
            return builder.toString();
        }
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PREFIX, getPrefix())
                   .arg(SEPARATOR, getSeparator())
                   .arg(SUFFIX, getSuffix())
                   .arg(CONVERTER, getConverter())
                   .build();
    }
}