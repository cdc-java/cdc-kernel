package cdc.enums;

import java.util.function.Function;

/**
 * Interface describing a synthesis mask.
 * <p>
 * It is a map from values to {@link SynthesisStatus}.
 *
 * @author Damien Carbonne
 *
 * @param <M> The synthesis mask type.
 * @param <V> the value type.
 */
public interface SynthesisMask<M extends SynthesisMask<M, V>, V> {

    /**
     * @return The associated support.
     */
    public SynthesisMaskSupport<M, V> getSupport();

    /**
     * @return The associated type.
     */
    public ListType<V> getType();

    /**
     * @return {@code true} if {@code null} is a valid mask value that can be characterized.
     */
    public boolean isNullable();

    /**
     * Sets the status of a value.
     *
     * @param value The value.
     * @param status The status.
     * @return The corresponding mask.
     */
    public M set(V value,
                 SynthesisStatus status);

    /**
     * Sets the status of all values.
     *
     * @param status The status.
     * @return The corresponding mask.
     */
    public M setAll(SynthesisStatus status);

    /**
     * Returns the status associated to a value.
     *
     * @param value The value.
     * @return The status of {@code value}.
     */
    public SynthesisStatus get(V value);

    public default boolean isLooselySet(V value) {
        final SynthesisStatus status = get(value);
        return status == SynthesisStatus.ALL || status == SynthesisStatus.PARTIAL;
    }

    public M merge(M other);

    public M merge(Mask<?, V> mask);

    public String toString(Function<SynthesisStatus, String> statusToString,
                           String separator);
}