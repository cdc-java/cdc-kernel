package cdc.enums;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;
import cdc.util.lang.Operators;

/**
 * EnumType implementation dedicated to booleans.
 *
 * @author Damien Carbonne
 *
 */
public final class BooleanEnumType implements EnumType<Boolean> {
    private static final Logger LOGGER = LogManager.getLogger(BooleanEnumType.class);
    private final List<Boolean> values;

    public static final BooleanEnumType INSTANCE = new BooleanEnumType();

    private BooleanEnumType() {
        this.values = List.of(Boolean.FALSE, Boolean.TRUE);
    }

    @Override
    public void addEventHandler(DagEventHandler handler) {
        // Ignore
    }

    @Override
    public void removeEventHandler(DagEventHandler handler) {
        // Ignore
    }

    @Override
    public boolean isSupported(DagFeature feature) {
        return false;
    }

    @Override
    public boolean isLocked() {
        return true;
    }

    @Override
    public Class<Boolean> getValueClass() {
        return Boolean.class;
    }

    @Override
    public List<Boolean> getValues() {
        return values;
    }

    @Override
    public List<Boolean> getRoots() {
        return getValues();
    }

    @Override
    public List<Boolean> getChildren(Boolean value) {
        return Collections.emptyList();
    }

    @Override
    public List<Boolean> getParents(Boolean value) {
        return Collections.emptyList();
    }

    @Override
    public String getName(Boolean value) {
        return value == null ? null : value.toString();
    }

    @Override
    public String getQName(Boolean value) {
        return getName(value);
    }

    @Override
    public boolean isValid(Boolean value) {
        return value != null;
    }

    @Override
    public Boolean valueOf(String qname,
                           FailureReaction reaction) {
        final Boolean tmp;
        if (qname == null) {
            tmp = null;
        } else if ("true".equals(qname)) {
            tmp = Boolean.TRUE;
        } else if ("false".equals(qname)) {
            tmp = Boolean.FALSE;
        } else {
            tmp = null;
        }
        return NotFoundException.onResult(tmp,
                                          EnumType.unknownQName(qname),
                                          LOGGER,
                                          reaction,
                                          null);
    }

    @Override
    public boolean areEqual(Boolean left,
                            Boolean right) {
        return Operators.equals(left, right);
    }

    @Override
    public boolean isStrictlyOver(Boolean left,
                                  Boolean right) {
        return false;
    }

    @Override
    public boolean equals(Object other) {
        // Only one instance
        return this == other;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}