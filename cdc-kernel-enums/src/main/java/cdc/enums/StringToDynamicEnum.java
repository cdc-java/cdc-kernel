package cdc.enums;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Converter from String to DynamicEnum.
 *
 * @author Damien Carbonne
 *
 * @param <V> The dynamic enum type.
 */
public class StringToDynamicEnum<V extends DynamicEnum> extends AbstractConverter<String, V> {
    private final EnumType<V> enumType;

    public static final FormalArg<EnumType<?>> ENUM_TYPE =
            new FormalArg<>("enumType", Introspection.uncheckedCast(EnumType.class), Necessity.MANDATORY);

    public static final FormalArgs FARGS = new FormalArgs(ENUM_TYPE);

    public static final Factory<StringToDynamicEnum<?>> FACTORY =
            new AbstractFactory<StringToDynamicEnum<?>>(Introspection.uncheckedCast(StringToDynamicEnum.class),
                                                        Args.builder()
                                                            .arg(SOURCE_CLASS, String.class)
                                                            .arg(TARGET_CLASS, DynamicEnum.class)
                                                            .build(),
                                                        FARGS) {
                @Override
                protected StringToDynamicEnum<?> create(Args args,
                                                        FormalArgs fargs) {
                    final EnumType<?> enumType = args.getValue(ENUM_TYPE);
                    return StringToDynamicEnum.create(Introspection.uncheckedCast(enumType));
                }
            };

    protected StringToDynamicEnum(EnumType<V> enumType) {
        super(String.class,
              enumType.getValueClass());
        this.enumType = enumType;
    }

    public static <V extends DynamicEnum> StringToDynamicEnum<V> create(EnumType<V> enumType) {
        return new StringToDynamicEnum<>(enumType);
    }

    public EnumType<V> getEnumType() {
        return enumType;
    }

    @Override
    public V apply(String source) {
        return source == null ? null : enumType.valueOf(source, FailureReaction.FAIL);
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(ENUM_TYPE, enumType)
                   .build();
    }
}