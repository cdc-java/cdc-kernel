package cdc.enums;

import java.util.Comparator;

/**
 * Base interface of dynamic enumerations.
 *
 * @author Damien Carbonne
 *
 */
public interface DynamicEnum {

    /**
     * Comparator that uses names.
     */
    public static final Comparator<DynamicEnum> NAME_COMPARATOR = (o1,
                                                                   o2) -> o1.getName().compareTo(o2.getName());

    /**
     * Comparator that uses qualified names.
     */
    public static final Comparator<DynamicEnum> QNAME_COMPARATOR = (o1,
                                                                    o2) -> o1.getQName().compareTo(o2.getQName());

    /**
     * @return The enumeration name.
     */
    public String getName();

    /**
     * @return The enumeration qualified name.
     */
    public String getQName();
}