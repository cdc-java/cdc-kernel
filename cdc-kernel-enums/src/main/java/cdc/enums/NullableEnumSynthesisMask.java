package cdc.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.util.lang.Introspection;

/**
 * Specialization of {@link AbstractSynthesisMask} to nullable enum values.
 * <p>
 * <b>WARNING:</b> The class can not be specialized.
 *
 * @author Damien Carbonne
 *
 * @param <E> The enum type.
 */
public final class NullableEnumSynthesisMask<E extends Enum<E>> extends AbstractSynthesisMask<NullableEnumSynthesisMask<E>, E> {
    private static final Map<Class<? extends Enum<?>>, SynthesisMaskSupport<?, ?>> MAP = new HashMap<>();

    private NullableEnumSynthesisMask(SynthesisMaskSupport<NullableEnumSynthesisMask<E>, E> support,
                                      Map<E, SynthesisStatus> map) {
        super(support,
              map);
    }

    private static <E extends Enum<E>> SynthesisMaskSupport<NullableEnumSynthesisMask<E>, E> newSupport(Class<E> enumClass) {
        final Class<NullableEnumSynthesisMask<E>> maskClass = Introspection.uncheckedCast(NullableEnumSynthesisMask.class);
        final Creator<NullableEnumSynthesisMask<E>, E> creator = NullableEnumSynthesisMask::new;
        return support(maskClass,
                       creator,
                       enumClass,
                       Nullable.TRUE);
    }

    /**
     * Retrieves or creates the support associated to a class.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The support associated to {@code enumClass}.
     */
    public static <E extends Enum<E>> SynthesisMaskSupport<NullableEnumSynthesisMask<E>, E> support(Class<E> enumClass) {
        final SynthesisMaskSupport<?, ?> tmp;
        tmp = MAP.computeIfAbsent(enumClass, k -> newSupport(enumClass));

        @SuppressWarnings("unchecked")
        final SynthesisMaskSupport<NullableEnumSynthesisMask<E>, E> result = (SynthesisMaskSupport<NullableEnumSynthesisMask<E>, E>) tmp;
        return result;
    }

    /**
     * @return A set of all classes for which a support has been created.
     */
    public static Set<Class<? extends Enum<?>>> getEnumClasses() {
        return MAP.keySet();
    }
}