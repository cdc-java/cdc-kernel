package cdc.enums;

import java.util.function.Predicate;

import cdc.util.lang.Checks;

/**
 * Base class for dynamic enum support.
 *
 * @author Damien Carbonne
 *
 * @param <V> The dynamic enum type.
 */
public abstract class AbstractDynamicEnumSupport<V> extends AbstractBaseDagSupport<V> implements DynamicEnumSupport<V> {
    private boolean locked = false;
    protected final Predicate<String> nameValidator;
    public static final Predicate<String> DEFAULT_NAME_VALIDATOR = s -> s != null && !s.isEmpty();

    protected AbstractDynamicEnumSupport(Class<V> cls,
                                         Predicate<String> nameValidator,
                                         DagFeature... features) {
        super(cls, features);
        Checks.isNotNull(nameValidator, "nameValidator");

        this.nameValidator = nameValidator;
    }

    @Override
    public final boolean isLocked() {
        return locked;
    }

    @Override
    public void lock() {
        if (!locked) {
            this.locked = true;
            fire(null, DagEventType.LOCKED);
        }
    }

    protected final void checkNameIsValid(String name) {
        if (!nameValidator.test(name)) {
            throw new IllegalArgumentException("Invalid name '" + name + "'");
        }
    }

    protected final void checkIsUnlocked() {
        if (isLocked()) {
            throw new IllegalStateException(getValueClass().getCanonicalName() + " is locked");
        }
    }

    protected final void checkHasNoSiblingNamed(V value,
                                                String name) {
        if (hasSiblingNamed(value, name)) {
            throw new IllegalArgumentException("A sibling named '" + name + "' exists");
        }
    }
}