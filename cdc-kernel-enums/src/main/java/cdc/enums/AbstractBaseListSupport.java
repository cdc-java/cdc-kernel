package cdc.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

public abstract class AbstractBaseListSupport<V> implements ListType<V> {
    protected final Logger logger;
    private final Set<DagFeature> features;
    private final Class<V> cls;
    private final List<DagEventHandler> handlers = new ArrayList<>();

    protected AbstractBaseListSupport(Class<V> cls,
                                      DagFeature... features) {
        Checks.isNotNull(cls, "cls");

        this.logger = LogManager.getLogger(getClass().getCanonicalName() + "<" + cls.getCanonicalName() + ">");
        this.cls = cls;
        this.features = EnumSet.of(DagFeature.LOCKING, features);
        this.features.add(DagFeature.CREATION);
    }

    @Override
    public final void addEventHandler(DagEventHandler handler) {
        if (handler != null && !handlers.contains(handler)) {
            handlers.add(handler);
        }
    }

    @Override
    public final void removeEventHandler(DagEventHandler handler) {
        handlers.remove(handler);
    }

    protected final void fire(V value,
                              DagEventType eventType) {
        if (!handlers.isEmpty()) {
            final DagEvent event = new DagEvent(this, value, eventType);
            for (final DagEventHandler handler : handlers) {
                handler.processEvent(event);
            }
        }
    }

    @Override
    public final boolean isSupported(DagFeature feature) {
        Checks.isNotNull(feature, "feature");
        return features.contains(feature);
    }

    @Override
    public final Class<V> getValueClass() {
        return cls;
    }

    /**
     * Checks that a value is valid.
     *
     * @param value The value.
     * @throws IllegalArgumentException When {@code value} is not valid.
     */
    protected final void checkIsValid(V value) {
        if (value == null || !isValid(value)) {
            throw new IllegalArgumentException("Invalid value: " + value);
        }
        if (!isContained(value)) {
            throw new IllegalArgumentException("Unrelated value: " + value);
        }
    }

    /**
     * Checks that a feature is supported.
     *
     * @param feature The feature.
     * @throws IllegalArgumentException When {@code feature} is not supported.
     */
    protected final void checkIsSupported(DagFeature feature) {
        if (!isSupported(feature)) {
            throw new UnsupportedOperationException("Unsupported feature: " + feature);
        }
    }

    /**
     * Returns {@code true} if a value is contained by this support.
     *
     * @param value The value.
     * @return {@code true} if {@code value} is contained by this support.
     */
    protected abstract boolean isContained(V value);
}