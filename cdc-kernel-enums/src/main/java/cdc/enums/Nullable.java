package cdc.enums;

/**
 * Enumeration for nullable.
 * <p>
 * Used to avid confusion with boolean parameters.
 *
 * @author Damien Carbonne
 *
 */
public enum Nullable {
    TRUE,
    FALSE;
    public boolean isTrue() {
        return this == TRUE;
    }

    public boolean isFalse() {
        return this == FALSE;
    }

    public static Nullable fromBoolean(boolean value) {
        return value ? TRUE : FALSE;
    }
}