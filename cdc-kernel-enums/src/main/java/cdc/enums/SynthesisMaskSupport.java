package cdc.enums;

import java.util.Map;

public interface SynthesisMaskSupport<M extends SynthesisMask<M, V>, V> {
    /**
     * @return The class of the synthesis mask.
     */
    public Class<M> getSynthesisMaskClass();

    /**
     * @return The value list type.
     */
    public ListType<V> getType();

    /**
     * @return {@code true} if {@code null} is a valid synthesis mask value.
     */
    public boolean isNullable();

    /**
     * @return A new synthesis mask with all values set to {@link SynthesisStatus#UNDEFINED}.
     */
    public M create();

    /**
     * Creates a synthesis mask for a map.
     *
     * @param map The map giving statuses.
     * @return A synthesis mask matching {@code map}.
     */
    public M create(Map<V, SynthesisStatus> map);

    /**
     * Creates a synthesis mask with values set to the same status.
     *
     * @param status The status.
     * @return A synthesis mask with all values set to {@code status}.
     */
    public M create(SynthesisStatus status);

    /**
     * Creates a synthesis mask from a standard mask.
     * <p>
     * All set values in {@code mask} are mapped to {@link SynthesisStatus#ALL}.<br>
     * All clear values in {@code mask} are mapped to {@link SynthesisStatus#NONE}.
     *
     * @param mask The mask.
     * @return The synthesis mask corresponding to {@code mask}.
     */
    public M create(Mask<?, V> mask);
}