package cdc.enums;

/**
 * Features that can be supported by an implementation.
 *
 * @author Damien Carbonne
 */
public enum DagFeature {
    /**
     * Does the implementation support locking?
     */
    LOCKING,

    /**
     * Does the implementation support value removal?
     */
    REMOVAL,

    /**
     * Does the implementation support value creation?
     */
    CREATION,

    /**
     * Does the implementation support changing content (name, ...)?
     */
    CONTENT_CHANGE,

    /**
     * Does the implementation support changing value parenting?
     */
    REPARENTING
}