package cdc.enums;

public enum DagEventType {
    /**
     * An item was created.
     */
    CREATED,

    /**
     * An item was removed.
     */
    REMOVED,

    /**
     * An item had its content changed.
     */
    CONTENT_CHANGED,

    /**
     * An item has its relationships changed.
     */
    REPARENTED,

    /**
     * Global changes (creation, removal, ...) happened.
     */
    GLOBAL,

    /**
     * The underlying container was locked.
     */
    LOCKED
}