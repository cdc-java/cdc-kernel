package cdc.enums;

import java.util.Set;
import java.util.function.Function;

/**
 * Interface describing a mask based on DagType.
 *
 * @author Damien Carbonne
 *
 * @param <M> The mask type.
 * @param <V> The value type.
 */
public interface Mask<M extends Mask<M, V>, V> {

    /**
     * @return The enum type.
     */
    public MaskSupport<M, V> getSupport();

    /**
     * @return {@code true} if this mask is a nullable mask.
     */
    public boolean isNullable();

    /**
     * @return {@code true} if this mask is empty.
     */
    public boolean isEmpty();

    /**
     *
     * @return {@code true} if this mask is full.
     *         The result may change if underlying enum type is dynamic.
     */
    public boolean isFull();

    /**
     * @return The values that are set.
     */
    public Set<V> getValues();

    /**
     * Returns {@code true} if a value is contained.
     *
     * @param value The value.
     * @return {@code true} if {@code value} is contained in this mask.
     */
    public boolean isSet(V value);

    /**
     * Returns {@code true} if a value is not contained.
     *
     * @param value The value.
     * @return {@code true} if {@code value} is not contained in this mask.
     */
    public boolean isClear(V value);

    /**
     * Sets or clears a value.
     *
     * @param value The value.
     * @param enabled If {@code true} sets the value, otherwise clears it.
     * @return A new mask with {@code value} set or cleared.
     */
    public M set(V value,
                 boolean enabled);

    /**
     * Returns a new mask augmented with a value.
     * <p>
     * If {@code value} is already set, returns this mask.
     *
     * @param value The value.
     * @return A new mask augmented with {@code value}.
     */
    public M set(V value);

    /**
     * Returns a new mask reduced with a value.
     * <p>
     * If {@code value} is already cleared, returns this mask.
     *
     * @param value The value.
     * @return A new mask reduced with {@code value}.
     */
    public M clear(V value);

    /**
     * Returns an empty or full mask.
     * <p>
     * The full mask may change if underlying enum type is dynamic.
     *
     * @param enabled If {@code true}, creates a full mask. An empty mask otherwise.
     * @return An empty or full mask.
     */
    public M setAll(boolean enabled);

    /**
     * @return An empty mask.
     */
    public M empty();

    /**
     * @return A full mask at the time of call.
     *         Result may change if underlying enum type is dynamic.
     */
    public M full();

    /**
     * Returns the intersection of this mask with another one.
     *
     * @param other The other mask.
     * @return The intersection of this mask with {@code other}.
     */
    public M and(M other);

    public M and(V value);

    @SuppressWarnings("unchecked")
    public M and(V... values);

    /**
     * Returns the union of this mask with another one.
     *
     * @param other The other mask.
     * @return The union of this mask with {@code other}.
     */
    public M or(M other);

    public M or(V value);

    @SuppressWarnings("unchecked")
    public M or(V... values);

    /**
     * Returns the complement of this mask (at the time of calling this method).
     *
     * @return The complement of this mask (at the time of calling this method).
     */
    public M not();

    /**
     * Returns {@code true} if all values of this mask are contained in another mask.
     *
     * @param other The other mask.
     * @return {@code true} if all values of this mask are contained in {@code other}.
     */
    public boolean contains(M other);

    @SuppressWarnings("unchecked")
    public boolean contains(V... values);

    /**
     * Returns a string representation of this mask.
     *
     * @param valueToString The function used to converter values (including {@code null}) to string.
     * @param separator The separator to use between values.
     * @return A string representation of this mask.
     */
    public String toString(Function<? super V, String> valueToString,
                           String separator);

}