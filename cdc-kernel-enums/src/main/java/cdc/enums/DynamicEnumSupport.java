package cdc.enums;

/**
 * Base interface of services offered by dynamic enumerations.
 * <p>
 * This adds modification (creation removal, ...) methods to EnumType.
 *
 * @author Damien Carbonne
 *
 * @param <V> The enum value type.
 */
public interface DynamicEnumSupport<V> extends EnumType<V> {
    /**
     * Definitely locks modifications.
     */
    public void lock();

    /**
     * Returns the value corresponding to a name.
     * <p>
     * If none exists, creates a new value and returns it.
     *
     * @param qname The qualified name.
     * @return The value corresponding to {@code qname}, possibly creating it.
     * @throws IllegalStateException When locked and value corresponding to {@code qname} does not exist.
     * @throws IllegalArgumentException When locked and value corresponding to {@code qname} does not exist and {@code qname} is invalid.
     */
    public V findOrCreate(String qname);

    /**
     * Removes a value.
     *
     * @param value The value to remove.
     * @throws IllegalStateException When locked.
     * @throws UnsupportedOperationException When removal is not supported.
     * @throws IllegalArgumentException When {@code value} is invalid.
     */
    public void remove(V value);

    /**
     * Changes a value name.
     *
     * @param value The value.
     * @param name The new local name.
     * @throws IllegalStateException When locked.
     * @throws UnsupportedOperationException When renaming is not supported.
     * @throws IllegalArgumentException When {@code value} or {@code name} is invalid.
     */
    public void setName(V value,
                        String name);
}