package cdc.enums;

import java.util.List;

/**
 * Interface describing a DAG type.
 *
 * @author Damien Carbonne
 *
 * @param <V> The value type.
 */
public interface DagType<V> extends ListType<V> {

    /**
     * @return A list of root values.
     */
    public List<V> getRoots();

    /**
     * Returns the children of a value.
     *
     * @param value The value.
     * @return The children of {@code value}
     */
    public List<V> getChildren(V value);

    /**
     * Returns the parents of a value.
     *
     * @param value The value.
     * @return The parents of {@code value}
     */
    public List<V> getParents(V value);

    /**
     * Returns {@code true} if a value is a root: it has no parents.
     *
     * @param value The value.
     * @return {@code true} if {@code value} is a root.
     */
    public default boolean isRoot(V value) {
        return getParents(value).isEmpty();
    }

    /**
     * Returns {@code true} when one value is a strict superset of another value.
     *
     * @param left The left value (potential superset).
     * @param right The right value.
     * @return {@code true} when {@code left} is a superset of {@code right}.
     */
    public boolean isStrictlyOver(V left,
                                  V right);

    /**
     * Returns {@code true} when one value is a superset of, or equal to, another value.
     *
     * @param left The left value (potential superset).
     * @param right The right value.
     * @return {@code true} when {@code left} is a superset of, or equal to, {@code right}.
     */
    public default boolean isOverOrEqual(V left,
                                         V right) {
        return areEqual(left, right) || isStrictlyOver(left, right);
    }

    /**
     * Returns {@code true} when one value is a strict subset of another value.
     *
     * @param left The left value (potential subset).
     * @param right The right value.
     * @return {@code true} when {@code left} is a subset of {@code right}.
     */
    public default boolean isStrictlyUnder(V left,
                                           V right) {
        return isStrictlyOver(right, left);
    }

    /**
     * Returns {@code true} when one value is a subset of, or equal to, another value.
     *
     * @param left The left value (potential superset).
     * @param right The right value.
     * @return {@code true} when {@code left} is a subset of, or equal to, {@code right}.
     */
    public default boolean isUnderOrEqual(V left,
                                          V right) {
        return areEqual(left, right) || isStrictlyUnder(left, right);
    }
}