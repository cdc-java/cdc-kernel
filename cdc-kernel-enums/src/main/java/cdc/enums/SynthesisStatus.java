package cdc.enums;

import cdc.util.encoding.Encoded;
import cdc.util.encoding.Encoder;
import cdc.util.encoding.Encoders;

/**
 * Enumeration of synthesis statuses.
 * <p>
 * This can be used to characterize data coming from different sources.
 *
 * @author Damien Carbonne
 *
 */
public enum SynthesisStatus implements Encoded<Character> {
    /**
     * No source is available.
     */
    UNDEFINED('U'),

    /**
     * All sources agree to say that something is false.
     */
    NONE('N'),

    /**
     * Some source agree to say that something is true, others that it is false.
     */
    PARTIAL('P'),

    /**
     * All sources agree to say that something is true.
     */
    ALL('A');

    private final Character code;

    private SynthesisStatus(char code) {
        this.code = code;
    }

    public static final Encoder<SynthesisStatus, Character> ENCODER =
            Encoders.encoder(SynthesisStatus.class, Character.class);

    @Override
    public Character getCode() {
        return code;
    }

    /**
     * Merges this status with another one.
     * <p>
     * The logic is:
     * <table >
     * <caption></caption>
     * <tr>
     * <td></td>
     * <td>U</td>
     * <td>N</td>
     * <td>P</td>
     * <td>F</td>
     * </tr>
     * <tr>
     * <td>U</td>
     * <td>U</td>
     * <td>N</td>
     * <td>P</td>
     * <td>F</td>
     * </tr>
     * <tr>
     * <td>N</td>
     * <td>N</td>
     * <td>N</td>
     * <td>P</td>
     * <td>P</td>
     * </tr>
     * <tr>
     * <td>P</td>
     * <td>P</td>
     * <td>P</td>
     * <td>P</td>
     * <td>P</td>
     * </tr>
     * <tr>
     * <td>F</td>
     * <td>F</td>
     * <td>P</td>
     * <td>P</td>
     * <td>F</td>
     * </tr>
     * </table>
     *
     * @param other The other synthesis status.
     * @return The merge of this status with {@code other}.
     */
    public SynthesisStatus merge(SynthesisStatus other) {
        if (this == UNDEFINED) {
            return other;
        } else if (other == UNDEFINED) {
            return this;
        } else if (this == other) {
            return this;
        } else {
            return PARTIAL;
        }
    }
}