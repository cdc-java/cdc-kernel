package cdc.enums;

import java.util.Set;

/**
 * Mask of non nullable booleans.
 *
 * @author Damien Carbonne
 *
 */
public final class BooleanMask extends AbstractMask<BooleanMask, Boolean> {
    public static final MaskSupport<BooleanMask, Boolean> SUPPORT = support(BooleanMask.class,
                                                                            BooleanMask::new,
                                                                            BooleanEnumType.INSTANCE,
                                                                            Nullable.FALSE);

    private BooleanMask(MaskSupport<BooleanMask, Boolean> support,
                        Set<Boolean> values) {
        super(support, values);
    }

    public static final BooleanMask EMPTY = SUPPORT.empty();
    public static final BooleanMask FULL = SUPPORT.full();
}