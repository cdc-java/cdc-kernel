package cdc.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.util.lang.Introspection;

/**
 * Specialization of AbstractEnumMask dedicated to standard enums.
 * <p>
 * This class must not (and can not) be specialized to work correctly.<br>
 * If specialization is needed, use AbstractEnumMask directly.
 *
 * @author Damien Carbonne
 *
 * @param <E> The enum type.
 */
public final class NullableEnumMask<E extends Enum<E>> extends AbstractMask<NullableEnumMask<E>, E> {
    private static final Map<Class<? extends Enum<?>>, MaskSupport<?, ?>> MAP = new HashMap<>();

    private NullableEnumMask(MaskSupport<NullableEnumMask<E>, E> support,
                             Set<E> values) {
        super(support,
              values);
    }

    /**
     * Creates a Support implementation for a standard enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return A Support implementation.
     */
    public static <E extends Enum<E>> MaskSupport<NullableEnumMask<E>, E> newSupport(Class<E> enumClass) {
        final Class<NullableEnumMask<E>> maskClass = Introspection.uncheckedCast(NullableEnumMask.class);
        final Creator<NullableEnumMask<E>, E> creator = NullableEnumMask::new;
        return support(maskClass,
                       creator,
                       enumClass,
                       Nullable.TRUE);
    }

    /**
     * Returns the support associated to an enum class.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The support for {@code NullableEnumMask<E>}.
     */
    public static <E extends Enum<E>> MaskSupport<NullableEnumMask<E>, E> support(Class<E> enumClass) {
        final MaskSupport<?, ?> tmp;
        tmp = MAP.computeIfAbsent(enumClass, k -> newSupport(enumClass));

        @SuppressWarnings("unchecked")
        final MaskSupport<NullableEnumMask<E>, E> result = (MaskSupport<NullableEnumMask<E>, E>) tmp;
        return result;
    }

    /**
     * @return A set of enums for which a support has been created.
     */
    public static Set<Class<? extends Enum<?>>> getEnumClasses() {
        return MAP.keySet();
    }

    /**
     * Returns the empty mask corresponding to an enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The empty {@code NullableEnumMask<E>}.
     */
    public static <E extends Enum<E>> NullableEnumMask<E> empty(Class<E> enumClass) {
        return support(enumClass).empty();
    }

    /**
     * Returns the full mask corresponding to an enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The full {@code NullableEnumMask<E>}.
     */
    public static <E extends Enum<E>> NullableEnumMask<E> full(Class<E> enumClass) {
        return support(enumClass).full();
    }
}