package cdc.enums;

import java.util.List;

import cdc.args.Args;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.Converter;
import cdc.converters.defaults.AbstractStringToSequence;
import cdc.util.lang.Introspection;

/**
 * Converter from a string to a mask.
 *
 * @author Damien Carbonne
 *
 * @param <M> The mask type.
 * @param <V> The value type.
 */
public class StringToMask<M extends Mask<M, V>, V> extends AbstractStringToSequence<M> {
    private final MaskSupport<M, V> support;
    private final Converter<String, ? extends V> converter;

    public static final FormalArg<MaskSupport<?, ?>> SUPPORT =
            new FormalArg<>("support", Introspection.uncheckedCast(MaskSupport.class), Necessity.MANDATORY);
    public static final FormalArg<Converter<String, ?>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PREFIX,
                           SEPARATOR,
                           SUFFIX,
                           TRIM,
                           SUPPORT,
                           CONVERTER);

    public StringToMask(MaskSupport<M, V> maskSupport,
                        String prefix,
                        String separator,
                        String suffix,
                        Converter<String, ? extends V> converter,
                        boolean trim) {
        super(maskSupport.getMaskClass(), prefix, separator, suffix, trim);
        this.support = maskSupport;
        this.converter = converter;
    }

    public final MaskSupport<M, V> getMaskSupport() {
        return support;
    }

    public final Converter<String, ? extends V> getConverter() {
        return converter;
    }

    @Override
    public M apply(String s) {
        final List<V> list = toList(s, converter);
        return support.create(list);
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PREFIX, getPrefix())
                   .arg(SEPARATOR, getSeparator())
                   .arg(SUFFIX, getSuffix())
                   .arg(TRIM, getTrim())
                   .arg(SUPPORT, getMaskSupport())
                   .arg(CONVERTER, getConverter())
                   .build();
    }
}