package cdc.enums;

import java.util.ArrayList;
import java.util.List;

import cdc.args.Args;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.Converter;
import cdc.converters.defaults.AbstractSequenceToString;
import cdc.util.lang.Introspection;

/**
 * Converter from a mask to a string.
 *
 * @author Damien Carbonne
 *
 * @param <M> The mask type.
 * @param <V> The value type.
 */
public class MaskToString<M extends Mask<M, V>, V> extends AbstractSequenceToString<M> {
    private final MaskSupport<M, V> support;
    private final Converter<? super V, String> converter;

    public static final FormalArg<MaskSupport<?, ?>> SUPPORT =
            new FormalArg<>("support", Introspection.uncheckedCast(MaskSupport.class), Necessity.MANDATORY);
    public static final FormalArg<Converter<?, String>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PREFIX,
                           SEPARATOR,
                           SUFFIX,
                           SUPPORT,
                           CONVERTER);

    public MaskToString(MaskSupport<M, V> maskSupport,
                        String prefix,
                        String separator,
                        String suffix,
                        Converter<? super V, String> converter) {
        super(maskSupport.getMaskClass(), prefix, separator, suffix);
        this.support = maskSupport;
        this.converter = converter;
    }

    public final MaskSupport<M, V> getMaskSupport() {
        return support;
    }

    public final Converter<? super V, String> getConverter() {
        return converter;
    }

    @Override
    public String apply(M mask) {
        // Create a sorted list in order of declarations of values in the supporting type.
        final List<V> list = new ArrayList<>();
        for (final V value : support.getType().getValues()) {
            if (mask.getValues().contains(value)) {
                list.add(value);
            }
        }
        return toString(converter, list);
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PREFIX, getPrefix())
                   .arg(SEPARATOR, getSeparator())
                   .arg(SUFFIX, getSuffix())
                   .arg(SUPPORT, getMaskSupport())
                   .arg(CONVERTER, getConverter())
                   .build();
    }
}