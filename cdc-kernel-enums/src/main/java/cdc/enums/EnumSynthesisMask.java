package cdc.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.util.lang.Introspection;

/**
 * Specialization of {@link AbstractSynthesisMask} to non nullable enum values.
 * <p>
 * <b>WARNING:</b> The class can not be specialized.
 *
 * @author Damien Carbonne
 *
 * @param <E> The enum type.
 */
public final class EnumSynthesisMask<E extends Enum<E>> extends AbstractSynthesisMask<EnumSynthesisMask<E>, E> {
    private static final Map<Class<? extends Enum<?>>, SynthesisMaskSupport<?, ?>> MAP = new HashMap<>();

    private EnumSynthesisMask(SynthesisMaskSupport<EnumSynthesisMask<E>, E> support,
                              Map<E, SynthesisStatus> map) {
        super(support,
              map);
    }

    private static <E extends Enum<E>> SynthesisMaskSupport<EnumSynthesisMask<E>, E> newSupport(Class<E> enumClass) {
        final Class<EnumSynthesisMask<E>> maskClass = Introspection.uncheckedCast(EnumSynthesisMask.class);
        final Creator<EnumSynthesisMask<E>, E> creator = EnumSynthesisMask::new;
        return support(maskClass,
                       creator,
                       enumClass,
                       Nullable.FALSE);
    }

    /**
     * Retrieves or creates the support associated to a class.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The support associated to {@code enumClass}.
     */
    public static <E extends Enum<E>> SynthesisMaskSupport<EnumSynthesisMask<E>, E> support(Class<E> enumClass) {
        final SynthesisMaskSupport<?, ?> tmp;
        tmp = MAP.computeIfAbsent(enumClass, k -> newSupport(enumClass));

        @SuppressWarnings("unchecked")
        final SynthesisMaskSupport<EnumSynthesisMask<E>, E> result = (SynthesisMaskSupport<EnumSynthesisMask<E>, E>) tmp;
        return result;
    }

    /**
     * @return A set of all classes for which a support has been created.
     */
    public static Set<Class<? extends Enum<?>>> getEnumClasses() {
        return MAP.keySet();
    }
}