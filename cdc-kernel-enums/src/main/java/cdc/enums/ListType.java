package cdc.enums;

import java.util.List;

/**
 * Interface describing a List type.
 *
 * @author Damien Carbonne
 *
 * @param <V> The value type.
 */
public interface ListType<V> {

    /**
     * Adds an event handler.
     *
     * @param handler The event handler.
     */
    public void addEventHandler(DagEventHandler handler);

    /**
     * Removes an event handler.
     *
     * @param handler The event handler.
     */
    public void removeEventHandler(DagEventHandler handler);

    /**
     * Returns {@code true} when a feature is supported.
     * <p>
     * Locking may make a feature not available even if it is supported.
     *
     * @param feature The feature.
     * @return {@code true} when {@code feature} is supported.
     */
    public boolean isSupported(DagFeature feature);

    /**
     * Returns {@code true} when a feature can be used.
     * <p>
     * It must be supported and the type must not be locked.
     *
     * @param feature the feature.
     * @return {@code true} when {@code feature} can be used.
     */
    public default boolean isUsable(DagFeature feature) {
        return !isLocked() && isSupported(feature);
    }

    /**
     * Returns {@code true} when modifications are disabled.
     * <p>
     * A static implementation is always locked.<br>
     * After locking a dynamic implementation, it is impossible to unlock it.
     *
     * @return {@code true} when modifications (creation, removal, ...) are disabled.
     */
    public boolean isLocked();

    /**
     * @return The enum value class.
     */
    public Class<V> getValueClass();

    /**
     * @return A list of values.
     */
    public List<V> getValues();

    /**
     * @return The number of values.
     */
    public default int size() {
        return getValues().size();
    }

    /**
     * Returns {@code true} when a value is valid.
     * <p>
     * Depending on the enum implementation, a value may be valid at a time and invalid at another time.<br>
     * Standard enums values are always valid.<br>
     * A {@code null} value is invalid.
     *
     * @param value The value.
     * @return {@code true} if {@code value} is valid.
     */
    public boolean isValid(V value);

    /**
     * Returns {@code true} if 2 values are equal.
     *
     * @param left The left value.
     * @param right The right value.
     * @return {@code true} if {@code left} and {@code right} are equal.
     */
    public boolean areEqual(V left,
                            V right);
}