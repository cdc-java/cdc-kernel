package cdc.enums;

import java.util.Set;

/**
 * Mask of nullable booleans.
 *
 * @author Damien Carbonne
 *
 */
public final class NullableBooleanMask extends AbstractMask<NullableBooleanMask, Boolean> {
    public static final MaskSupport<NullableBooleanMask, Boolean> SUPPORT = support(NullableBooleanMask.class,
                                                                                    NullableBooleanMask::new,
                                                                                    BooleanEnumType.INSTANCE,
                                                                                    Nullable.TRUE);

    private NullableBooleanMask(MaskSupport<NullableBooleanMask, Boolean> support,
                                Set<Boolean> values) {
        super(support, values);
    }

    public static final NullableBooleanMask EMPTY = SUPPORT.empty();
    public static final NullableBooleanMask FULL = SUPPORT.full();
}