package cdc.enums;

import java.util.Set;
import java.util.function.Predicate;

/**
 * Interface describing a mask type.
 *
 * @author Damien Carbonne
 *
 * @param <M> The mask type.
 * @param <V> The value type.
 */
public interface MaskSupport<M extends Mask<M, V>, V> {
    /**
     * @return The mask class.
     */
    public Class<M> getMaskClass();

    /**
     * @return The associated List type.
     */
    public ListType<V> getType();

    /**
     * @return {@code true} if {@code null} is a valid mask value.
     */
    public boolean isNullable();

    /**
     * @return The empty mask.
     */
    public M empty();

    /**
     * @return The full mask (at the time of call).
     */
    public M full();

    /**
     * @return The empty mask.
     */
    public M create();

    /**
     * Creates a mask with one value.
     *
     * @param value The value.
     * @return The mask that contains {@code value}.
     */
    public M create(V value);

    /**
     * Creates a mask from an array.
     *
     * @param values The values (possibly including {@code null}).
     * @return The created mask.
     */
    @SuppressWarnings("unchecked")
    public M create(V... values);

    /**
     * Creates a mask from an iterable.
     *
     * @param values The values (possibly including {@code null}).
     * @return The created mask.
     */
    public M create(Iterable<V> values);

    /**
     * Creates a mask from a predicate.
     *
     * @param predicate A predicate of values to include.
     * @return The created mask.
     */
    public M create(Predicate<V> predicate);

    /**
     * Creates a full or empty mask.
     * <p>
     * <b>WARNING:</b> Full is meaningful at the time of calling this constructor.
     * It may become false if values change after this call.
     *
     * @param enabled If {@code true}, creates a full mask. An empty mask otherwise.
     * @return The created mask.
     */
    public M create(boolean enabled);

    /**
     * Creates a Set that is compliant with implementation.
     * <p>
     * This allows using the best implementation.
     *
     * @param size The initial set size.
     * @return A new Set.
     */
    public Set<V> newSet(int size);
}