package cdc.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.util.lang.Introspection;

/**
 * Specialization of AbstractEnumMask dedicated to non nullable standard enums masks.
 * <p>
 * This class must not (and can not) be specialized to work correctly.<br>
 * If specialization is needed, use AbstractEnumMask directly.
 *
 * @author Damien Carbonne
 *
 * @param <E> The enum type.
 */
public final class EnumMask<E extends Enum<E>> extends AbstractMask<EnumMask<E>, E> {
    private static final Map<Class<? extends Enum<?>>, MaskSupport<?, ?>> MAP = new HashMap<>();

    private EnumMask(MaskSupport<EnumMask<E>, E> support,
                     Set<E> values) {
        super(support,
              values);
    }

    /**
     * Creates a Support implementation for a standard enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return A Support implementation.
     */
    private static <E extends Enum<E>> MaskSupport<EnumMask<E>, E> newSupport(Class<E> enumClass) {
        final Class<EnumMask<E>> maskClass = Introspection.uncheckedCast(EnumMask.class);
        final Creator<EnumMask<E>, E> creator = EnumMask::new;
        return support(maskClass,
                       creator,
                       enumClass,
                       Nullable.FALSE);
    }

    /**
     * Returns the support associated to an enum class.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The support for {@code EnumMask<E>}.
     */
    public static <E extends Enum<E>> MaskSupport<EnumMask<E>, E> support(Class<E> enumClass) {
        final MaskSupport<?, ?> tmp;
        tmp = MAP.computeIfAbsent(enumClass, k -> newSupport(enumClass));

        @SuppressWarnings("unchecked")
        final MaskSupport<EnumMask<E>, E> result = (MaskSupport<EnumMask<E>, E>) tmp;
        return result;
    }

    /**
     * @return A set of enums for which a support has been created.
     */
    public static Set<Class<? extends Enum<?>>> getEnumClasses() {
        return MAP.keySet();
    }

    /**
     * Returns the empty mask corresponding to an enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The empty {@code EnumMask<E>}.
     */
    public static <E extends Enum<E>> EnumMask<E> empty(Class<E> enumClass) {
        return support(enumClass).empty();
    }

    /**
     * Returns the full mask corresponding to an enum.
     *
     * @param <E> The enum type.
     * @param enumClass The enum class.
     * @return The full {@code EnumMask<E>}.
     */
    public static <E extends Enum<E>> EnumMask<E> full(Class<E> enumClass) {
        return support(enumClass).full();
    }
}