package cdc.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;

/**
 * Factory of EnumType implementations dedicated to enums.
 *
 * @author Damien Carbonne
 *
 */
public final class EnumTypes {
    protected static final Logger LOGGER = LogManager.getLogger(EnumTypes.class);
    private static final Map<Class<? extends Enum<?>>, StandardEnumType> MAP = new HashMap<>();

    private EnumTypes() {
    }

    public static <E extends Enum<E>> EnumType<E> getEnumType(Class<E> enumClass) {
        @SuppressWarnings("unchecked")
        final StandardEnumType tmp = MAP.computeIfAbsent(enumClass, k -> new StandardEnumType((Class<Enum<?>>) k));
        @SuppressWarnings("unchecked")
        final EnumType<E> result = (EnumType<E>) tmp;
        return result;
    }

    public static Set<Class<? extends Enum<?>>> getEnumClasses() {
        return MAP.keySet();
    }

    private static class StandardEnumType implements EnumType<Enum<?>> {
        private final Class<Enum<?>> valueClass;
        private final List<Enum<?>> values;
        private final Map<String, Enum<?>> nameToValue;

        public StandardEnumType(Class<Enum<?>> valueClass) {
            Checks.isNotNull(valueClass, "valueClass");
            this.valueClass = valueClass;
            this.values = Collections.unmodifiableList(Arrays.asList(valueClass.getEnumConstants()));
            this.nameToValue = new HashMap<>();
            for (final Enum<?> value : values) {
                nameToValue.put(value.name(), value);
            }
        }

        @Override
        public void addEventHandler(DagEventHandler handler) {
            // Ignore
        }

        @Override
        public void removeEventHandler(DagEventHandler handler) {
            // Ignore
        }

        @Override
        public boolean isSupported(DagFeature feature) {
            return false;
        }

        @Override
        public boolean isLocked() {
            return true;
        }

        @Override
        public Class<Enum<?>> getValueClass() {
            return valueClass;
        }

        @Override
        public List<Enum<?>> getValues() {
            return values;
        }

        @Override
        public List<Enum<?>> getRoots() {
            return getValues();
        }

        @Override
        public List<Enum<?>> getChildren(Enum<?> value) {
            return Collections.emptyList();
        }

        @Override
        public List<Enum<?>> getParents(Enum<?> value) {
            return Collections.emptyList();
        }

        @Override
        public String getName(Enum<?> value) {
            return value == null ? null : value.name();
        }

        @Override
        public String getQName(Enum<?> value) {
            return getName(value);
        }

        @Override
        public boolean isValid(Enum<?> value) {
            return value != null;
        }

        @Override
        public Enum<?> valueOf(String qname,
                               FailureReaction reaction) {
            return NotFoundException.onResult(nameToValue.get(qname),
                                              EnumType.unknownQName(qname),
                                              LOGGER,
                                              reaction,
                                              null);
        }

        @Override
        public boolean areEqual(Enum<?> left,
                                Enum<?> right) {
            return left == right;
        }

        @Override
        public boolean isStrictlyOver(Enum<?> left,
                                      Enum<?> right) {
            return false;
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof StandardEnumType)) {
                return false;
            }
            final StandardEnumType o = (StandardEnumType) other;
            return valueClass.equals(o.valueClass);
        }

        @Override
        public int hashCode() {
            return valueClass.hashCode();
        }
    }
}