package cdc.enums;

public class DagEvent {
    private final ListType<?> type;
    private final Object value;
    private final DagEventType eventType;

    public DagEvent(ListType<?> type,
                    Object value,
                    DagEventType eventType) {
        this.type = type;
        this.value = value;
        this.eventType = eventType;
    }

    public ListType<?> getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public DagEventType getEventType() {
        return eventType;
    }

    @Override
    public String toString() {
        return "[" + getType().getValueClass().getCanonicalName() + ", " + getEventType() + ", " + getValue() + "]";
    }
}