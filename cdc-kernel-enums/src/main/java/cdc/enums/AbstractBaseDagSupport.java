package cdc.enums;

import java.util.function.Consumer;

public abstract class AbstractBaseDagSupport<V> extends AbstractBaseListSupport<V> implements DagType<V> {
    protected AbstractBaseDagSupport(Class<V> cls,
                                     DagFeature... features) {
        super(cls, features);
    }

    protected final void iterateUnder(V value,
                                      Consumer<V> consumer) {
        consumer.accept(value);
        for (final V child : getChildren(value)) {
            iterateUnder(child, consumer);
        }
    }

    protected final void iterateOver(V value,
                                     Consumer<V> consumer) {
        consumer.accept(value);
        for (final V parent : getParents(value)) {
            iterateOver(parent, consumer);
        }
    }

    protected final void checkIsNotOverOrEqual(V value,
                                               V other) {
        if (other != null) {
            iterateOver(other,
                        v -> {
                            if (v == value) {
                                throw new IllegalArgumentException(value + " is over or equal to " + other);
                            }
                        });
        }
    }
}