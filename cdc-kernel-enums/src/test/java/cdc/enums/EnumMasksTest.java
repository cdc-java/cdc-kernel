package cdc.enums;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.util.function.Evaluation;

class EnumMasksTest {
    private static class EvaluationMask extends AbstractMask<EvaluationMask, Evaluation> {
        static final MaskSupport<EvaluationMask, Evaluation> SUPPORT = support(EvaluationMask.class,
                                                                               EvaluationMask::new,
                                                                               Evaluation.class,
                                                                               Nullable.FALSE);

        private EvaluationMask(MaskSupport<EvaluationMask, Evaluation> support,
                               Set<Evaluation> values) {
            super(support, values);
        }
    }

    private static class NullableEvaluationMask extends AbstractMask<NullableEvaluationMask, Evaluation> {
        static final MaskSupport<NullableEvaluationMask, Evaluation> SUPPORT = support(NullableEvaluationMask.class,
                                                                                       NullableEvaluationMask::new,
                                                                                       Evaluation.class,
                                                                                       Nullable.TRUE);

        private NullableEvaluationMask(MaskSupport<NullableEvaluationMask, Evaluation> support,
                                       Set<Evaluation> values) {
            super(support, values);
        }
    }

    @Test
    void testEmptyMask() {
        final EvaluationMask maskF = EvaluationMask.SUPPORT.empty();
        assertTrue(maskF.isEmpty());
        assertEquals(maskF.empty(), maskF);
        final NullableEvaluationMask maskT = NullableEvaluationMask.SUPPORT.empty();
        assertTrue(maskT.isEmpty());
        assertEquals(maskT.empty(), maskT);
    }

    @Test
    void testFullMask() {
        final EvaluationMask maskF = EvaluationMask.SUPPORT.full();
        assertTrue(maskF.isFull());
        assertEquals(maskF.full(), maskF);
        final NullableEvaluationMask maskT = NullableEvaluationMask.SUPPORT.full();
        assertTrue(maskT.isFull());
        assertEquals(maskT.full(), maskT);
    }

    @Test
    void testConstructorBoolean() {
        final EvaluationMask maskTrue = EvaluationMask.SUPPORT.create(true);
        for (final Evaluation value : Evaluation.values()) {
            assertTrue(maskTrue.isSet(value));
        }
        assertEquals(EnumTypes.getEnumType(Evaluation.class), maskTrue.getSupport().getType());

        final EvaluationMask maskFalse = EvaluationMask.SUPPORT.create(false);
        for (final Evaluation value : Evaluation.values()) {
            assertFalse(maskFalse.isSet(value));
        }
    }

    @Test
    void testConstructorVarArg() {
        final EvaluationMask mask = EvaluationMask.SUPPORT.create();
        for (final Evaluation value : Evaluation.values()) {
            assertFalse(mask.isSet(value));
        }
    }

    @Test
    void testConstructorPredicate() {
        final EvaluationMask mask = EvaluationMask.SUPPORT.create(e -> true);
        for (final Evaluation value : Evaluation.values()) {
            assertTrue(mask.isSet(value));
        }
    }

    @Test
    void testConstructorIterable() {
        final Set<Evaluation> values = new HashSet<>();
        values.add(Evaluation.CONTINUE);
        values.add(null);
        final NullableEvaluationMask mask = NullableEvaluationMask.SUPPORT.create(values);
        for (final Evaluation value : Evaluation.values()) {
            assertEquals(values.contains(value), mask.isSet(value));
        }
        assertEquals(values, mask.getValues());
    }

    @Test
    void testSet() {
        final EvaluationMask mask0 = EvaluationMask.SUPPORT.create();
        assertEquals(mask0, mask0);
        assertNotEquals(mask0, "Hello");

        final EvaluationMask mask1 = mask0.set(Evaluation.CONTINUE);
        assertTrue(mask1.isSet(Evaluation.CONTINUE));
        assertFalse(mask1.isSet(Evaluation.PRUNE));
        assertNotEquals(mask0, mask1);

        final EvaluationMask mask2 = mask1.set(Evaluation.PRUNE);
        assertTrue(mask2.isSet(Evaluation.CONTINUE));
        assertTrue(mask2.isSet(Evaluation.PRUNE));

        final EvaluationMask mask3 = mask2.set(Evaluation.PRUNE);
        assertTrue(mask3.isSet(Evaluation.CONTINUE));
        assertTrue(mask3.isSet(Evaluation.PRUNE));
    }

    @Test
    void testClear() {
        final EvaluationMask mask0 = EvaluationMask.SUPPORT.create(true);
        final EvaluationMask mask1 = mask0.clear(Evaluation.CONTINUE);
        assertFalse(mask1.isSet(Evaluation.CONTINUE));
        assertTrue(mask1.isSet(Evaluation.PRUNE));

        final EvaluationMask mask2 = mask1.clear(Evaluation.CONTINUE);
        assertFalse(mask2.isSet(Evaluation.CONTINUE));
        assertTrue(mask2.isSet(Evaluation.PRUNE));

        assertEquals(mask1, mask2);
    }

    @Test
    void testAnd() {
        final EvaluationMask all = EvaluationMask.SUPPORT.create(true);
        final EvaluationMask none = EvaluationMask.SUPPORT.create(false);
        assertEquals(all, all.and(all));
        assertEquals(none, all.and(none));
        assertEquals(none, none.and(all));
        assertEquals(none, none.and(none));
    }

    @Test
    void testOr() {
        final EvaluationMask all = EvaluationMask.SUPPORT.create(true);
        final EvaluationMask none = EvaluationMask.SUPPORT.create(false);
        assertEquals(all, all.or(all));
        assertEquals(all, all.or(none));
        assertEquals(all, none.or(all));
        assertEquals(none, none.or(none));
    }

    @Test
    void testNot() {
        final NullableEvaluationMask fullT = NullableEvaluationMask.SUPPORT.create(true);
        final NullableEvaluationMask emptyT = NullableEvaluationMask.SUPPORT.create(false);
        assertEquals(emptyT, fullT.not());
        assertEquals(fullT, emptyT.not());

        final EvaluationMask fullF = EvaluationMask.SUPPORT.create(true);
        final EvaluationMask emptyF = EvaluationMask.SUPPORT.create(false);
        assertEquals(emptyF, fullF.not());
        assertEquals(fullF, emptyF.not());
    }

    @Test
    void testToString() {
        final NullableEvaluationMask fullT = NullableEvaluationMask.SUPPORT.create(true);
        assertEquals("CONTINUE|PRUNE|null", fullT.toString());
        final EvaluationMask fullF = EvaluationMask.SUPPORT.create(true);
        assertEquals("CONTINUE|PRUNE", fullF.toString());
    }

    @Test
    void testContains() {
        final NullableEvaluationMask emptyT = NullableEvaluationMask.SUPPORT.create(false);
        final NullableEvaluationMask fullT = NullableEvaluationMask.SUPPORT.create(true);
        assertTrue(fullT.contains(emptyT));
    }
}