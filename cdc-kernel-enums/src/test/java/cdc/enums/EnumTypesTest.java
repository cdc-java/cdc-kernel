package cdc.enums;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cdc.util.function.Evaluation;
import cdc.util.lang.FailureReaction;

public class EnumTypesTest {
    @Test
    public void test() {
        final EnumType<Evaluation> type1 = EnumTypes.getEnumType(Evaluation.class);
        assertEquals(type1, type1);
        assertNotEquals(type1, "Hello");
        assertEquals(Evaluation.class, type1.getValueClass());
        assertTrue(type1.isValid(Evaluation.CONTINUE));
        assertFalse(type1.isValid(null));
        assertEquals(Arrays.asList(Evaluation.CONTINUE, Evaluation.PRUNE), type1.getValues());
        final EnumType<FailureReaction> type2 = EnumTypes.getEnumType(FailureReaction.class);
        assertNotEquals(type1, type2);

        for (final Evaluation x : type1.getValues()) {
            assertTrue(type1.areEqual(x, x));
        }

        for (final Evaluation x : type1.getValues()) {
            for (final Evaluation y : type1.getValues()) {
                assertEquals(x == y, type1.areEqual(x, y));
                assertEquals(x == y, type1.isOverOrEqual(x, y));
            }
        }
    }
}