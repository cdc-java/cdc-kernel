package cdc.enums;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cdc.util.lang.NotFoundException;

class AbstractListDynamicEnumTest {
    private static final Logger LOGGER = LogManager.getLogger(AbstractListDynamicEnumTest.class);

    private static final String A = "A";
    private static final String B = "B";
    private static final String D = "D";

    AbstractListDynamicEnumTest() {
    }

    static class Enum1 extends AbstractListDynamicEnum<Enum1> {
        static final Support<Enum1> SUPPORT = support(Enum1.class,
                                                      Enum1::new);

        protected Enum1(String name) {
            super(name);
        }
    }

    static class Enum2 extends AbstractListDynamicEnum<Enum2> {
        static final Support<Enum2> SUPPORT = support(Enum2.class,
                                                      Enum2::new,
                                                      DagFeature.REMOVAL,
                                                      DagFeature.CONTENT_CHANGE);

        protected Enum2(String name) {
            super(name);
        }
    }

    static class Enum3 extends AbstractListDynamicEnum<Enum3> {
        static final Support<Enum3> SUPPORT1 = support(Enum3.class,
                                                       Enum3::new,
                                                       DagFeature.REMOVAL,
                                                       DagFeature.CONTENT_CHANGE);
        static final Support<Enum3> SUPPORT2 = support(Enum3.class,
                                                       Enum3::new,
                                                       DagFeature.REMOVAL,
                                                       DagFeature.CONTENT_CHANGE);

        protected Enum3(String name) {
            super(name);
        }
    }

    @BeforeAll
    static void init() {
        Enum1.SUPPORT.addEventHandler(e -> LOGGER.debug(e));
        Enum2.SUPPORT.addEventHandler(e -> LOGGER.debug(e));
        Enum1.SUPPORT.findOrCreate(A);
        Enum1.SUPPORT.findOrCreate(B);
        Enum2.SUPPORT.findOrCreate(A);
        Enum2.SUPPORT.findOrCreate(B);
        Enum3.SUPPORT1.findOrCreate(A);
        Enum3.SUPPORT2.findOrCreate(A);
        Enum3.SUPPORT2.findOrCreate(B);
    }

    @Test
    void testValid() {
        assertEquals(A, Enum1.SUPPORT.valueOf(A).getName());
        assertEquals(B, Enum1.SUPPORT.valueOf(B).getName());
    }

    void testInvalidValueOf() {
        assertThrows(NotFoundException.class, () -> {
            Enum1.SUPPORT.valueOf("C");
        });
    }

    @Test
    void testEquals() {
        assertEquals(Enum1.SUPPORT.valueOf(A), Enum1.SUPPORT.valueOf(A));
        assertEquals(Enum1.SUPPORT.valueOf(B), Enum1.SUPPORT.valueOf(B));
        assertNotEquals(Enum1.SUPPORT.valueOf(A), Enum1.SUPPORT.valueOf(B));
    }

    @Test
    void testRemove() {
        assertEquals(2, Enum2.SUPPORT.getValues().size());
        final Enum2 value = Enum2.SUPPORT.findOrCreate(D);
        assertEquals(3, Enum2.SUPPORT.getValues().size());
        Enum2.SUPPORT.remove(value);
        assertEquals(2, Enum2.SUPPORT.getValues().size());
        assertFalse(Enum2.SUPPORT.isValid(value));
    }

    @Test
    void testRename() {
        final Enum2 value = Enum2.SUPPORT.valueOf(A);
        Enum2.SUPPORT.setName(value, "a");
        assertEquals("a", value.getName());
    }

    @Test
    void testMultiSupport() {
        final Enum3 va1 = Enum3.SUPPORT1.valueOf(A);
        final Enum3 va2 = Enum3.SUPPORT2.valueOf(A);
        assertNotEquals(va1, va2);
        assertEquals(1, Enum3.SUPPORT1.getValues().size());
        assertEquals(2, Enum3.SUPPORT2.getValues().size());
    }
}