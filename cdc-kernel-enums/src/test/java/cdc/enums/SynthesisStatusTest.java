package cdc.enums;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SynthesisStatusTest {
    @Test
    public void testMerge() {
        assertEquals(SynthesisStatus.UNDEFINED, SynthesisStatus.UNDEFINED.merge(SynthesisStatus.UNDEFINED));
        assertEquals(SynthesisStatus.NONE, SynthesisStatus.UNDEFINED.merge(SynthesisStatus.NONE));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.UNDEFINED.merge(SynthesisStatus.PARTIAL));
        assertEquals(SynthesisStatus.ALL, SynthesisStatus.UNDEFINED.merge(SynthesisStatus.ALL));

        assertEquals(SynthesisStatus.NONE, SynthesisStatus.NONE.merge(SynthesisStatus.UNDEFINED));
        assertEquals(SynthesisStatus.NONE, SynthesisStatus.NONE.merge(SynthesisStatus.NONE));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.NONE.merge(SynthesisStatus.PARTIAL));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.NONE.merge(SynthesisStatus.ALL));

        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.PARTIAL.merge(SynthesisStatus.UNDEFINED));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.PARTIAL.merge(SynthesisStatus.NONE));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.PARTIAL.merge(SynthesisStatus.PARTIAL));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.PARTIAL.merge(SynthesisStatus.ALL));

        assertEquals(SynthesisStatus.ALL, SynthesisStatus.ALL.merge(SynthesisStatus.UNDEFINED));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.ALL.merge(SynthesisStatus.NONE));
        assertEquals(SynthesisStatus.PARTIAL, SynthesisStatus.ALL.merge(SynthesisStatus.PARTIAL));
        assertEquals(SynthesisStatus.ALL, SynthesisStatus.ALL.merge(SynthesisStatus.ALL));
    }
}