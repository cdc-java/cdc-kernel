package cdc.enums;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cdc.util.lang.NotFoundException;

class AbstractForestDynamicEnumTest {
    private static final Logger LOGGER = LogManager.getLogger(AbstractForestDynamicEnumTest.class);

    private static final String A = "A";
    private static final String AA = "A/A";
    private static final String AB = "A/B";
    private static final String B = "B";
    private static final String BA = "B/A";
    private static final String BB = "B/B";

    AbstractForestDynamicEnumTest() {
    }

    @BeforeAll
    static void init() {
        Enum1.SUPPORT.addEventHandler(e -> LOGGER.debug(e));
        Enum2.SUPPORT.addEventHandler(e -> LOGGER.debug(e));
        Enum1.SUPPORT.findOrCreate(AA);
        Enum1.SUPPORT.findOrCreate(AB);
        Enum1.SUPPORT.findOrCreate(BA);
        Enum1.SUPPORT.findOrCreate(BB);
        Enum2.SUPPORT.findOrCreate(A);
        Enum2.SUPPORT.findOrCreate(B);
    }

    static class Enum1 extends AbstractForestDynamicEnum<Enum1> {
        static final Support<Enum1> SUPPORT = support(Enum1.class,
                                                      Enum1::new,
                                                      DagFeature.REMOVAL,
                                                      DagFeature.CONTENT_CHANGE);

        protected Enum1(Enum1 parent,
                        String name) {
            super(parent, name);
        }
    }

    static class Enum2 extends AbstractForestDynamicEnum<Enum2> {
        static final Support<Enum2> SUPPORT = support(Enum2.class, Enum2::new);

        protected Enum2(Enum2 parent,
                        String name) {
            super(parent, name);
        }
    }

    @Test
    void testValid() {
        assertEquals(A, Enum1.SUPPORT.valueOf(A).getName());
        assertEquals(B, Enum1.SUPPORT.valueOf(B).getName());
        assertEquals(AA, Enum1.SUPPORT.valueOf(AA).getQName());
        assertEquals(AB, Enum1.SUPPORT.valueOf(AB).getQName());
    }

    void testInvalidValueOf() {
        assertThrows(NotFoundException.class, () -> {
            Enum1.SUPPORT.valueOf("C");
        });
    }

    @Test
    void testEquals() {
        assertEquals(Enum1.SUPPORT.valueOf(A), Enum1.SUPPORT.valueOf(A));
        assertEquals(Enum1.SUPPORT.valueOf(AA), Enum1.SUPPORT.valueOf(AA));
        assertNotEquals(Enum1.SUPPORT.valueOf(A), Enum1.SUPPORT.valueOf(B));
    }

    @Test
    void testRemove() {
        assertEquals(6, Enum1.SUPPORT.getValues().size());
        Enum1.SUPPORT.findOrCreate("C/A");
        Enum1.SUPPORT.findOrCreate("C/B");
        Enum1.SUPPORT.findOrCreate("C/C");
        assertEquals(10, Enum1.SUPPORT.getValues().size());
        assertEquals(3, Enum1.SUPPORT.getRoots().size());
        Enum1.SUPPORT.remove(Enum1.SUPPORT.findOrCreate("C/C"));
        assertEquals(9, Enum1.SUPPORT.getValues().size());
        assertEquals(3, Enum1.SUPPORT.getRoots().size());
        Enum1.SUPPORT.remove(Enum1.SUPPORT.findOrCreate("C"));
        assertEquals(6, Enum1.SUPPORT.getValues().size());
        assertEquals(2, Enum1.SUPPORT.getRoots().size());
    }

    @Test
    void testRename() {
        final Enum1 a = Enum1.SUPPORT.findOrCreate(A);
        final Enum1 aa = Enum1.SUPPORT.findOrCreate(AA);
        final Enum1 ab = Enum1.SUPPORT.findOrCreate(AB);
        Enum1.SUPPORT.setName(a, "a");
        assertEquals("a", a.getName());
        assertEquals("A", aa.getName());
        assertEquals("B", ab.getName());
        assertEquals("a/A", aa.getQName());
        assertEquals("a/B", ab.getQName());
    }
}