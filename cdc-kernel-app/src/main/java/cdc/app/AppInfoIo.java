package cdc.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;

import cdc.io.json.JsonpUtils;

public final class AppInfoIo {
    private AppInfoIo() {
    }

    private static final String TIMESTAMP = "timestamp";
    private static final String MAIN_ARGS = "mainArguments";
    private static final String VM_ARGS = "vmArguments";
    private static final String APP_PROPS = "appProperties";
    private static final String ENV_VARS = "envVariables";
    private static final String SYS_PROPS = "sysProperties";
    private static final String STACK_TRACE = "stackTrace";

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE_TIME;
    private static final DateTimeFormatter DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss.SSSSSS");

    private static LocalDateTime toLocalDateTime(Instant instant) {
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    private static String toString(Instant instant) {
        return FORMATTER.format(toLocalDateTime(instant));
    }

    private static void write(JsonGenerator generator,
                              String name,
                              String value) {
        if (value == null) {
            generator.writeNull(name);
        } else {
            generator.write(name, value);
        }
    }

    /**
     * Generate a File usable for exception traces.
     *
     * @param dir The directory that will contain the file.
     * @param instant The Instant to use to generate file name.
     * @return A new File placed into {@code dir}, whose name
     *         contains {@code instant}, and with a json extension.
     */
    public static File generateJsonExceptionFile(File dir,
                                                 Instant instant) {
        final String s = DATE_TIME.format(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
        return new File(dir, s + "-exception.json");
    }

    public static File generateJsonExceptionFile(File dir) {
        return generateJsonExceptionFile(dir, Instant.now());
    }

    public static void saveAsJson(AppInfo info,
                                  File file) throws IOException {
        final JsonGeneratorFactory factory = Json.createGeneratorFactory(JsonpUtils.PRETTY_CONFIG);
        try (final Writer writer = new BufferedWriter(new FileWriter(file));
                final JsonGenerator generator = factory.createGenerator(writer)) {
            generator.writeStartObject();

            generator.write(TIMESTAMP, toString(info.getTimestamp()));

            generator.writeStartArray(MAIN_ARGS);
            info.getMainArgs()
                .stream()
                .forEach(generator::write);
            generator.writeEnd();

            generator.writeStartArray(VM_ARGS);
            info.getVMArgs()
                .stream()
                .forEach(generator::write);
            generator.writeEnd();

            generator.writeStartObject(APP_PROPS);
            info.getAppPropsNames()
                .stream()
                .sorted()
                .forEach(name -> write(generator, name, info.getAppPropValue(name)));
            generator.writeEnd();

            generator.writeStartObject(ENV_VARS);
            info.getEnvVarsNames()
                .stream()
                .sorted()
                .forEach(name -> write(generator, name, info.getEnvVarValue(name)));
            generator.writeEnd();

            generator.writeStartObject(SYS_PROPS);
            info.getSysPropsNames()
                .stream()
                .sorted()
                .forEach(name -> write(generator, name, info.getSysPropValue(name)));
            generator.writeEnd();

            if (info.hasStackTrace()) {
                generator.writeStartArray(STACK_TRACE);
                info.getStackTrace()
                    .stream()
                    .forEach(generator::write);
                generator.writeEnd();
            }

            generator.writeEnd();
            generator.flush();
        }
    }
}