package cdc.app;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.util.debug.Printable;
import cdc.util.lang.ImplementationException;

public final class AppInfo implements Printable {
    // TODO with Java 9, process info can be retrieved
    private static final List<String> MAIN_ARGS = new ArrayList<>();
    private final Instant timestamp;
    private final List<String> mainArgs;
    private final List<String> vmArgs;
    private final Map<String, String> appProps;
    private final Map<String, String> env;
    private final Map<String, String> sysProps;
    private final List<String> stackTrace;

    private AppInfo(Instant timestamp,
                    List<String> mainArgs,
                    List<String> vmArgs,
                    Map<String, String> appProps,
                    Map<String, String> env,
                    Map<String, String> sysProps,
                    List<String> stacktrace) {
        this.timestamp = timestamp == null
                ? Instant.now()
                : timestamp;

        if (mainArgs == null) {
            this.mainArgs = new ArrayList<>(MAIN_ARGS);
        } else {
            this.mainArgs = mainArgs;
        }

        if (vmArgs == null) {
            this.vmArgs = new ArrayList<>();
            final RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
            this.vmArgs.addAll(bean.getInputArguments());
        } else {
            this.vmArgs = vmArgs;
        }

        this.appProps = appProps;

        if (env == null) {
            this.env = new HashMap<>(System.getenv());
        } else {
            this.env = env;
        }

        if (sysProps == null) {
            final RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
            this.sysProps = new HashMap<>(bean.getSystemProperties());
        } else {
            this.sysProps = sysProps;
        }

        this.stackTrace = stacktrace;
    }

    public static void setMainArgs(String[] args) {
        MAIN_ARGS.clear();
        Collections.addAll(MAIN_ARGS, args);
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public List<String> getMainArgs() {
        return mainArgs;
    }

    public Set<String> getEnvVarsNames() {
        return env.keySet();
    }

    public String getEnvVarValue(String name) {
        return env.get(name);
    }

    public Set<String> getSysPropsNames() {
        return sysProps.keySet();
    }

    public String getSysPropValue(String name) {
        return sysProps.get(name);
    }

    public Set<String> getAppPropsNames() {
        return appProps.keySet();
    }

    public String getAppPropValue(String name) {
        return appProps.get(name);
    }

    public List<String> getVMArgs() {
        return vmArgs;
    }

    public boolean hasStackTrace() {
        return !stackTrace.isEmpty();
    }

    public List<String> getStackTrace() {
        return stackTrace;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.print("Timestamp: ");
        out.print(getTimestamp());
        out.println();

        indent(out, level);
        out.println("Main arguments");
        getMainArgs().stream().forEach(arg -> {
            indent(out, level + 1);
            out.print(arg);
            out.println();
        });

        indent(out, level);
        out.println("VM arguments");
        getVMArgs().stream().forEach(arg -> {
            indent(out, level + 1);
            out.print(arg);
            out.println();
        });

        indent(out, level);
        out.println("Application properties");
        getAppPropsNames().stream().sorted().forEach(key -> {
            indent(out, level + 1);
            out.print(key);
            out.print(": ");
            out.print(getAppPropValue(key));
            out.println();
        });

        indent(out, level);
        out.println("Environment variables");
        getEnvVarsNames().stream().sorted().forEach(key -> {
            indent(out, level + 1);
            out.print(key);
            out.print(": ");
            out.print(getEnvVarValue(key));
            out.println();
        });

        indent(out, level);
        out.println("System properties");
        getSysPropsNames().stream().sorted().forEach(key -> {
            indent(out, level + 1);
            out.print(key);
            out.print(": ");
            out.print(getSysPropValue(key));
            out.println();
        });

        if (hasStackTrace()) {
            indent(out, level);
            out.println("Stack trace");
            getStackTrace().stream().forEach(line -> {
                indent(out, level + 1);
                out.print(line);
                out.println();
            });
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Instant timestamp;
        private List<String> mainargs;
        private List<String> vmargs;
        private final Map<String, String> appProps = new HashMap<>();
        private Map<String, String> env;
        private Map<String, String> sysprops;
        private final List<String> stacktrace = new ArrayList<>();

        private Builder() {
        }

        public Builder timestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder mainArgs(List<String> args) {
            this.mainargs = new ArrayList<>(args);
            return this;
        }

        public Builder vmArgs(List<String> args) {
            this.vmargs = new ArrayList<>(args);
            return this;
        }

        public Builder appProp(String name,
                               String value) {
            this.appProps.put(name, value);
            return this;
        }

        public Builder env(Map<String, String> env) {
            this.env = new HashMap<>(env);
            return this;
        }

        public Builder sysProps(Map<String, String> sysprops) {
            this.sysprops = new HashMap<>(sysprops);
            return this;
        }

        public Builder stacktrace(Throwable exception) {
            this.stacktrace.clear();

            final ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            try (final PrintWriter writer = new PrintWriter(buffer)) {
                exception.printStackTrace(writer);
                writer.flush();

                try (final BufferedReader reader =
                        new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer.toByteArray())))) {
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        this.stacktrace.add(line);
                    }
                } catch (final IOException e) {
                    throw new ImplementationException("Failed to parse stacktrace", e);
                }
                return this;
            }
        }

        public Builder stacktrace(List<String> stacktrace) {
            this.stacktrace.clear();
            this.stacktrace.addAll(stacktrace);
            return this;
        }

        public AppInfo build() {
            return new AppInfo(timestamp,
                               mainargs,
                               vmargs,
                               appProps,
                               env,
                               sysprops,
                               stacktrace);
        }
    }
}