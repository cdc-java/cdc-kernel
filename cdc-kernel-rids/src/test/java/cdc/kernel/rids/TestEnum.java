package cdc.kernel.rids;

import cdc.informers.EnumStringConversionInformer;
import cdc.informers.Informers;
import cdc.util.encoding.Encoders;

enum TestEnum {
    A,
    B,
    C;

    static {
        Encoders.CLASS_CODE.put(TestEnum.class, "TestEnum");
        Informers.register(new EnumStringConversionInformer<>(TestEnum.class));
    }

    public static void elaborate() {
        // Ignore
    }
}