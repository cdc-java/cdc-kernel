package cdc.kernel.rids;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class RawIdProxyTest {
    @Test
    void testConstructor() {
        final RawIdProxy ridp1 = new RawIdProxy("TestEnum", "A");
        assertEquals("TestEnum", ridp1.getObjectClassCode());
        assertEquals("A", ridp1.getIdCode());
        assertEquals("TestEnum:A", ridp1.getCode());
        assertEquals(ridp1, ridp1);
        assertEquals(TestEnum.class, ridp1.getObjectClass());

        final RawIdProxy ridp2 = new RawIdProxy("TestEnum:A");
        assertEquals("TestEnum", ridp2.getObjectClassCode());
        assertEquals("A", ridp2.getIdCode());
        assertEquals("TestEnum:A", ridp2.getCode());
        assertEquals(TestEnum.A, ridp2.getId());
        assertEquals(ridp2, ridp2);
        assertEquals(ridp1, ridp2);
        assertEquals(ridp2, ridp1);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new RawIdProxy("Hello");
                     });

        final RawId rid = new RawId(TestEnum.class, TestEnum.A);
        final RawIdProxy ridp3 = new RawIdProxy(rid);
        assertEquals("TestEnum", ridp3.getObjectClassCode());
        assertEquals("A", ridp3.getIdCode());
        assertEquals("TestEnum:A", ridp3.getCode());
    }

    @Test
    void testToString() {
        final RawIdProxy ridp1 = new RawIdProxy("TestEnum", "A");
        assertEquals("TestEnum:A", ridp1.toString());
    }

    @Test
    void testEquals() {
        final RawIdProxy ridp1 = new RawIdProxy("TestEnum", "A");
        final RawIdProxy ridp2 = new RawIdProxy("TestEnum", "B");
        final RawIdProxy ridp3 = new RawIdProxy("String", "A");
        final RawIdProxy ridp4 = new RawIdProxy("String", "A");
        assertEquals(ridp1, ridp1);
        assertNotEquals(ridp1, ridp2);
        assertNotEquals(ridp1, ridp3);
        assertNotEquals(ridp1, "Hello");
        assertEquals(ridp3, ridp4);
    }

    @Test
    void testCompareTo() {
        final RawIdProxy ridp1 = new RawIdProxy("TestEnum", "A");
        assertSame(ridp1.compareTo(ridp1), 0);
        final RawIdProxy ridp2 = new RawIdProxy("TestEnum", "B");
        assertTrue(ridp1.compareTo(ridp2) < 0);
        assertTrue(ridp2.compareTo(ridp1) > 0);
        final RawIdProxy ridp3 = new RawIdProxy("String", "A");
        assertTrue(ridp1.compareTo(ridp3) > 0);
        assertTrue(ridp3.compareTo(ridp1) < 0);
    }

    @Test
    void testCheckers() {
        TestEnum.elaborate();
        assertTrue(RawIdProxy.isValidObjectClassCode("X"));
        assertFalse(RawIdProxy.isValidObjectClassCode("X:A"));
        assertFalse(RawIdProxy.isValidObjectClassCode(":"));
        assertFalse(RawIdProxy.isValidObjectClassCode(""));
        assertFalse(RawIdProxy.isValidObjectClassCode(null));

        assertTrue(RawIdProxy.isValidDecodableObjectClassCode("TestEnum"));
        assertFalse(RawIdProxy.isValidDecodableObjectClassCode("X"));
        assertFalse(RawIdProxy.isValidDecodableObjectClassCode(""));
        assertFalse(RawIdProxy.isValidDecodableObjectClassCode(null));
        assertFalse(RawIdProxy.isValidDecodableObjectClassCode("X:"));

        assertFalse(RawIdProxy.isValidEncodableObjectClass(null));
        assertFalse(RawIdProxy.isValidEncodableObjectClass(String.class));
        assertTrue(RawIdProxy.isValidEncodableObjectClass(TestEnum.class));

        assertTrue(RawIdProxy.isValidIdCode("1"));
        assertFalse(RawIdProxy.isValidIdCode(""));
        assertFalse(RawIdProxy.isValidIdCode(null));
    }

    @Test
    void testMisc() {
        assertEquals("A", RawIdProxy.encodeId(TestEnum.class, TestEnum.A));
        assertEquals(null, RawIdProxy.encodeId(TestEnum.class, null));
        assertEquals(TestEnum.A, RawIdProxy.decodeId(TestEnum.class, "A"));
        assertEquals(null, RawIdProxy.decodeId(TestEnum.class, null));
        assertEquals("TestEnum", RawIdProxy.encodeObjectClass(TestEnum.class));
        assertEquals(null, RawIdProxy.encodeObjectClass(null));
        assertEquals(TestEnum.class, RawIdProxy.decodeObjectClass("TestEnum"));
        assertEquals(null, RawIdProxy.decodeObjectClass(null));
    }

    @Test
    void testConverters() {
        final RawIdProxy ridp1 = new RawIdProxy("TestEnum", "A");
        final RawId rid1 = new RawId(TestEnum.class, TestEnum.A);
        assertEquals(rid1, RawIdProxy.toRawId(ridp1));
        assertEquals(rid1, ridp1.toRawId());
        assertEquals(null, RawIdProxy.toRawId(null));
        assertEquals(ridp1, RawIdProxy.fromRawId(rid1));
        assertEquals(null, RawIdProxy.fromRawId(null));
    }
}