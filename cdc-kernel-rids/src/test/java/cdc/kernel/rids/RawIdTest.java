package cdc.kernel.rids;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class RawIdTest {
    @Test
    void testNullId() {
        final RawId rid1 = new RawId(Object.class, null);
        final RawId rid2 = new RawId(Object.class, null);
        assertEquals(null, rid1.getId());
        assertEquals(Object.class, rid1.getObjectClass());
        assertEquals(rid1, rid1);
        assertEquals(rid1, rid2);
        assertEquals(0, rid1.compareTo(rid2));

        final RawId rid3 = new RawId(String.class, null);
        assertNotEquals(rid1, rid3);
        assertNotSame(rid1.compareTo(rid3), 0);
    }

    @Test
    void testCompare() {
        final RawId rid1 = new RawId(Object.class, null);
        final RawId rid2 = new RawId(Object.class, 0);
        final RawId rid3 = new RawId(Object.class, 1);
        final RawId rid4 = new RawId(Integer.class, 10);
        final RawId rid5 = new RawId(String.class, "Hello");
        final RawId rid6 = new RawId(Object.class, new Object());
        final RawId rid7 = new RawId(Object.class, new Object());
        assertEquals(1, rid3.getId());
        assertEquals(1, rid3.getId(Integer.class));
        assertSame(0, rid1.compareTo(rid1));
        assertTrue(rid1.compareTo(rid2) < 0);
        assertTrue(rid2.compareTo(rid1) > 0);
        assertTrue(rid2.compareTo(rid3) < 0);
        assertTrue(rid3.compareTo(rid2) > 0);
        assertTrue(rid4.compareTo(rid3) < 0);
        assertTrue(rid4.compareTo(rid5) < 0);
        assertNotSame(0, rid6.compareTo(rid7));
    }

    @Test
    void testToString() {
        final RawId rid1 = new RawId(Object.class, null);
        assertEquals("[java.lang.Object:null]", rid1.toString());
        final RawId rid2 = new RawId(Integer.class, 0);
        assertEquals("[java.lang.Integer:0]", rid2.toString());
    }

    @Test
    void testConverters() {
        final RawIdProxy ridp1 = new RawIdProxy("TestEnum", "A");
        final RawId rid1 = new RawId(TestEnum.class, TestEnum.A);
        assertEquals(ridp1, RawId.toRawIdProxy(rid1));
        assertEquals(null, RawId.toRawIdProxy(null));
        assertEquals(rid1, RawId.fromRawIdProxy(ridp1));
        assertEquals(null, RawId.fromRawIdProxy(null));
    }

    @Test
    void testMisc() {
        final RawId rid1 = new RawId(TestEnum.class, TestEnum.A);
        assertEquals("TestEnum", rid1.getObjectClassCode());
        assertEquals("A", rid1.getIdCode());
    }
}