package cdc.kernel.rids;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

/**
 * RawId (raw identifier) is used to identify an object in a generic way.
 * <p>
 * It is a kind of global identifier that carries semantic.<br>
 * It is an immutable {@code (class, id)} pair, where {@code class}
 * is the class of the identified object and {@code id} its identifier.<br>
 * RawId is intended for in-memory use.<br>
 * The sister class {@link RawIdProxy} is intended for persistence.
 * <p>
 * <b>WARNING:</b> Creating a RawId does not means that the corresponding RawIdProxy can be created.<br>
 * Appropriate class and id converters must exist.
 *
 * @author Damien Carbonne
 *
 */
public final class RawId implements Comparable<RawId> {
    private static final Logger LOGGER = LogManager.getLogger(RawId.class);
    private final Class<?> objectClass;
    private final Object id;

    /**
     * Creates a RawId from an object class and identifier.
     * <p>
     * <b>WARNING:</b> Compatibility between {@code objectClass} and {@code id} is not checked.<br>
     * <b>WARNING:</b> Conversion to a RawIdProxy is not guaranteed.
     *
     * @param objectClass The object class.
     * @param id The identifier.
     * @throws IllegalArgumentException When {@code objectClass} is {@code null}.
     */
    public RawId(Class<?> objectClass,
                 Object id) {
        Checks.isNotNull(objectClass, "objectClass");
        this.objectClass = objectClass;
        this.id = id;
        if (!RawIdProxy.isValidEncodableObjectClass(objectClass)) {
            LOGGER.warn("{} can not be encoded", objectClass);
        }
    }

    /**
     * Creates a RawId from its RawIdProxy.
     *
     * @param ridp The RawIdProxy.
     */
    public RawId(RawIdProxy ridp) {
        this(ridp.getObjectClass(),
             ridp.getId());
    }

    public static RawId create(Class<?> objectClass,
                               Object id) {
        return new RawId(objectClass, id);
    }

    /**
     * @return The object class.
     */
    public Class<?> getObjectClass() {
        return objectClass;
    }

    /**
     * @return The String encoding of object class.
     */
    public String getObjectClassCode() {
        return RawIdProxy.encodeObjectClass(objectClass);
    }

    /**
     * @return The object identifier (possibly {@code null}).
     */
    public Object getId() {
        return id;
    }

    /**
     * Returns the object identifier converted to a type.
     *
     * @param <T> The conversion type.
     * @param type The conversion class.
     * @return The object identifier converted as {@code type}.
     */
    public <T> T getId(Class<T> type) {
        return type.cast(id);
    }

    /**
     * @return The String encoding of the id.
     */
    public String getIdCode() {
        return RawIdProxy.encodeId(objectClass, id);
    }

    private String safeEncodeId() {
        try {
            final String s = getIdCode();
            return s == null ? Objects.toString(id) : s;
        } catch (final Exception e) {
            return Objects.toString(id);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(objectClass,
                            id);
    }

    public static RawIdProxy toRawIdProxy(RawId rid) {
        return rid == null ? null : new RawIdProxy(rid);
    }

    public static RawId fromRawIdProxy(RawIdProxy ridp) {
        return ridp == null ? null : ridp.toRawId();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RawId)) {
            return false;
        }
        final RawId o = (RawId) object;
        return objectClass.equals(o.objectClass)
                && Objects.equals(id, o.id);
    }

    @Override
    public int compareTo(RawId o) {
        // Start by comparing object classes
        final int objectClassCmp = objectClass.getCanonicalName().compareTo(o.objectClass.getCanonicalName());
        if (objectClassCmp == 0) {
            // Object classes are the same, compare ids
            if (id == o.id) {
                return 0;
            } else if (id == null) {
                return -1;
            } else if (o.id == null) {
                return 1;
            } else if (id.getClass() == o.id.getClass()
                    && Comparable.class.isAssignableFrom(id.getClass())) {
                // Both ids have the same comparable class
                @SuppressWarnings("unchecked")
                final Comparable<Object> cid = (Comparable<Object>) id;
                return cid.compareTo(o.id);
            } else {
                // Fallback to general case
                final String eid = safeEncodeId();
                final String eoid = o.safeEncodeId();
                return eid.compareTo(eoid);
            }
        } else {
            // Object classes are different
            return objectClassCmp;
        }
    }

    @Override
    public String toString() {
        return "[" + objectClass.getCanonicalName() + ":" + id + "]";
    }
}