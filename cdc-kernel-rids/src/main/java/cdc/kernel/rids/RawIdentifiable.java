package cdc.kernel.rids;

import java.util.Comparator;

import cdc.util.lang.Operators;

/**
 * Interface implemented by objects that have a raw identifier.
 * <p>
 * Having a raw identifier means that the object has a normal identifier.
 *
 * @author Damien Carbonne
 *
 */
public interface RawIdentifiable {
    /**
     * Comparator of RawIdentifiable based on their raw identifier.
     */
    public static final Comparator<RawIdentifiable> RAW_ID_COMPARATOR =
            (o1,
             o2) -> Operators.compare(getRawId(o1), getRawId(o2));

    /**
     * @return The object raw identifier.
     */
    public RawId getRawId();

    /**
     * @return The object identifier.<br>
     *         <b>Note:</b> This should be overridden with a more precise type when possible.
     */
    public default Object getId() {
        return getRawId().getId();
    }

    /**
     * Returns the raw identifier of a raw identifiable.
     *
     * @param x the raw identifiable.
     * @return The raw identifier of {@code x} or {@code null} if {@code x} is {@code null}.
     */
    public static RawId getRawId(RawIdentifiable x) {
        return x == null ? null : x.getRawId();
    }
}