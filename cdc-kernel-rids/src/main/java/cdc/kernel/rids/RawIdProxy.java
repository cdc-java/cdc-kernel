package cdc.kernel.rids;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.informers.IdentifiableInformer;
import cdc.informers.StringConversionInformer;
import cdc.util.encoding.Encoded;
import cdc.util.encoding.Encoders;
import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

/**
 * RawIdProxy similar to {@link RawId} but is intended for persistence.
 * <p>
 * Is is a {@code (object class code, id code)} pair where {@code object class code}
 * is a string representation of a class and {@code id code} the string
 * representation of identifier.
 * <p>
 * <b>WARNING:</b> Creating a RawIdProxy does not means that the corresponding RawId can be created.<br>
 * Appropriate class and id converters must exist.
 *
 * @author Damien Carbonne
 *
 */
public final class RawIdProxy implements Comparable<RawIdProxy>, Encoded<String> {
    private static final Logger LOGGER = LogManager.getLogger(RawIdProxy.class);
    public static final char SEPARATOR = ':';
    private final String code;

    /**
     * Creates a RawIdProxy.
     *
     * @param objectClassCode The object class code.
     * @param idCode The id code.
     * @throws IllegalArgumentException When {@code objectClassCode} or {@code idCode} are invalid.
     */
    public RawIdProxy(String objectClassCode,
                      String idCode) {
        check(objectClassCode, idCode);
        this.code = objectClassCode + SEPARATOR + idCode;
    }

    public RawIdProxy(String code) {
        Checks.isNotNull(code, "code");
        final int sep = code.indexOf(SEPARATOR);
        if (sep > 0) {
            final String objectClassCode = code.substring(0, sep);
            final String idCode = code.substring(sep + 1);
            check(objectClassCode, idCode);
            this.code = code;
        } else {
            throw new IllegalArgumentException("RawIdProxy(" + code + ") failed, missing + " + SEPARATOR);
        }
    }

    public RawIdProxy(RawId rid) {
        this(Encoders.CLASS_CODE.encode(rid.getObjectClass(), FailureReaction.FAIL),
             encodeId(rid.getObjectClass(), rid.getId()));
    }

    private static void check(String objectClassCode,
                              String idCode) {
        Checks.isTrue(isValidObjectClassCode(objectClassCode), "Invalid object class code '{}'", objectClassCode);
        Checks.isTrue(isValidIdCode(idCode), "Invalid id code '{}'", idCode);
        if (!isValidDecodableObjectClassCode(objectClassCode)) {
            LOGGER.warn("'{}' can not be decoded", objectClassCode);
        }
    }

    public static boolean isValidEncodableObjectClass(Class<?> objectClass) {
        return objectClass != null
                && Encoders.CLASS_CODE.canEncode(objectClass);
    }

    public static boolean isValidObjectClassCode(String objectClassCode) {
        return objectClassCode != null
                && !objectClassCode.isEmpty()
                && objectClassCode.indexOf(SEPARATOR) < 0;
    }

    public static boolean isValidDecodableObjectClassCode(String objectClassCode) {
        return objectClassCode != null
                && objectClassCode.indexOf(SEPARATOR) < 0
                && Encoders.CLASS_CODE.canDecode(objectClassCode);
    }

    public static boolean isValidIdCode(String idCode) {
        return idCode != null
                && idCode.length() > 0;
    }

    public static boolean areValidCreationArgs(String objectClassCode,
                                               String idCode) {
        return isValidDecodableObjectClassCode(objectClassCode)
                && isValidIdCode(idCode);
    }

    public String getObjectClassCode() {
        final int sep = code.indexOf(SEPARATOR);
        return code.substring(0, sep);
    }

    public Class<?> getObjectClass() {
        return decodeObjectClass(getObjectClassCode());
    }

    public String getIdCode() {
        final int sep = code.indexOf(SEPARATOR);
        return code.substring(sep + 1);
    }

    public static String encodeObjectClass(Class<?> objectClass) {
        return objectClass == null ? null : Encoders.CLASS_CODE.encode(objectClass, FailureReaction.FAIL);
    }

    public static Class<?> decodeObjectClass(String objectClassCode) {
        return objectClassCode == null ? null : Encoders.CLASS_CODE.decode(objectClassCode, FailureReaction.FAIL);
    }

    public static Object decodeId(Class<?> objectClass,
                                  String idCode) {
        return StringConversionInformer.Service.fromString(IdentifiableInformer.Service.getIdClass(objectClass), idCode);
    }

    public static String encodeId(Class<?> objectClass,
                                  Object id) {
        return StringConversionInformer.Service.toString(id, FailureReaction.FAIL);
    }

    public Object getId() {
        return decodeId(getObjectClass(), getIdCode());
    }

    public RawId toRawId() {
        final Class<?> objectClass = getObjectClass();
        final Object id = decodeId(objectClass, getIdCode());
        final String idCode2 = encodeId(objectClass, id);
        if (!getIdCode().equals(idCode2)) {
            LOGGER.warn("Encoding mismatch [{}] [{}]", getIdCode(), idCode2);
        }
        return new RawId(objectClass, id);
    }

    public static RawId toRawId(RawIdProxy ridp) {
        return ridp == null ? null : new RawId(ridp);
    }

    public static RawIdProxy fromRawId(RawId rid) {
        return rid == null ? null : new RawIdProxy(rid);
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RawIdProxy)) {
            return false;
        }
        final RawIdProxy o = (RawIdProxy) object;
        return code.equals(o.code);
    }

    @Override
    public int compareTo(RawIdProxy o) {
        final int cmp = getObjectClassCode().compareTo(o.getObjectClassCode());
        if (cmp == 0) {
            return getIdCode().compareTo(o.getIdCode());
        } else {
            return cmp;
        }
    }

    @Override
    public String toString() {
        return getCode();
    }
}