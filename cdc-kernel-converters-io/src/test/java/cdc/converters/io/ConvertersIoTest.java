package cdc.converters.io;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import cdc.converters.ArgsConversion;
import cdc.util.lang.FailureReaction;

class ConvertersIoTest {

    @Test
    void test0() throws IOException {
        cdc.args.Factories.setConverter(ArgsConversion::convert);
        final ConvertersIo.StAXLoader loader = new ConvertersIo.StAXLoader(FailureReaction.FAIL);

        loader.load(new File("src/test/resources/converters-sample.xml"));

        assertTrue(true);

        // try (final XmlWriter writer = new XmlWriter("target/test0.xml")) {
        // writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        // writer.beginDocument();
        // writer.beginElement(ConvertersIo.CONVERTERS);
        // for (final String name : Converters.getNames().stream().sorted().collect(Collectors.toList())) {
        // ConvertersIo.write(writer, Converters.getConverter(name));
        // }
        // ConvertersIo.write(writer, DateToString.DASH_DD_MM_YYYY);
        // writer.endElement();
        // writer.endDocument();
        // }
    }
}