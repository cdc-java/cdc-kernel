package cdc.converters.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Args;
import cdc.args.Factories;
import cdc.args.io.ArgsIo;
import cdc.converters.AndThenConverter;
import cdc.converters.ArgsConversion;
import cdc.converters.Converter;
import cdc.converters.ConverterAdapter;
import cdc.converters.Converters;
import cdc.converters.OrElseConverter;
import cdc.converters.RawRefConverter;
import cdc.io.data.Element;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

public final class ConvertersIo {
    static final Logger LOGGER = LogManager.getLogger(ConvertersIo.class);
    private static final String AND = "and";
    private static final String CLASS = "class";
    private static final String CONVERTER = "converter";
    public static final String CONVERTERS = "converters";
    private static final String DEF = "def";
    private static final String DEFAULT = "default";
    private static final String OR = "or";
    private static final String NAME = "name";
    private static final String REF = "ref";

    private ConvertersIo() {
    }

    public static void write(XmlWriter writer,
                             Converter<?, ?> converter) throws IOException {
        if (converter instanceof RawRefConverter) {
            writer.beginElement(REF);
            writer.addAttribute(NAME, ((RawRefConverter) converter).getName());
            writer.endElement();
        } else if (converter instanceof AndThenConverter) {
            writer.beginElement(AND);
            write(writer, ((AndThenConverter<?, ?, ?>) converter).getBefore());
            write(writer, ((AndThenConverter<?, ?, ?>) converter).getAfter());
            writer.endElement();
        } else if (converter instanceof OrElseConverter) {
            writer.beginElement(OR);
            for (final Converter<?, ?> child : ((OrElseConverter<?, ?>) converter).getConverters()) {
                write(writer, child);
            }
            writer.endElement();
        } else if (converter instanceof ConverterAdapter) {
            write(writer, ((ConverterAdapter<?, ?>) converter).getDelegate());
        } else {
            writer.beginElement(DEF);
            writer.addAttribute(CLASS, converter.getClass().getCanonicalName());
            ArgsIo.ARG_NAMING.write(writer, ArgsConversion.convertToStringValues(converter.getParams()), false);
            writer.endElement();
        }
    }

    public static Element toElement(Converter<?, ?> converter) {
        if (converter instanceof RawRefConverter) {
            final Element element = new Element(REF);
            element.addAttribute(NAME, ((RawRefConverter) converter).getName());
            return element;
        } else if (converter instanceof AndThenConverter) {
            final Element element = new Element(AND);
            element.addChild(toElement(((AndThenConverter<?, ?, ?>) converter).getBefore()));
            element.addChild(toElement(((AndThenConverter<?, ?, ?>) converter).getAfter()));
            return element;
        } else if (converter instanceof OrElseConverter) {
            final Element element = new Element(OR);
            for (final Converter<?, ?> child : ((OrElseConverter<?, ?>) converter).getConverters()) {
                element.addChild(toElement(child));
            }
            return element;
        } else if (converter instanceof ConverterAdapter) {
            return toElement(((ConverterAdapter<?, ?>) converter).getDelegate());
        } else {
            final Element element = new Element(DEF);
            element.addAttribute(CLASS, converter.getClass().getCanonicalName());
            final Element args = ArgsIo.ARG_NAMING.toElement(ArgsConversion.convertToStringValues(converter.getParams()), false);
            if (args != null) {
                element.addChild(args);
            }
            return element;
        }
    }

    public static final class DataLoader extends AbstractResourceLoader<Void> {
        private final ArgsIo.DataLoader argsLoader;

        public DataLoader(FailureReaction reaction) {
            super(reaction);
            Converters.elaborate();
            argsLoader = new ArgsIo.DataLoader(reaction);
        }

        @Override
        protected Void loadRoot(Element root) {
            loadAndRegisterNamedConverters(root);
            return null;
        }

        public void loadAndRegisterNamedConverters(Element element) {
            LOGGER.debug("loadAndRegisterNamedConverters({})", element);
            if (CONVERTERS.equals(element.getName())) {
                for (final Element child : element.getChildren(Element.class)) {
                    if (CONVERTER.equals(child.getName())) {
                        loadAndRegisterNamedConverter(child);
                    } else {
                        unexpectedElement(child);
                    }
                }
            } else {
                unexpectedElement(element, CONVERTERS);
            }
        }

        private void loadAndRegisterNamedConverter(Element element) {
            LOGGER.debug("loadAndRegisterNamedConverter({})", element);
            if (CONVERTER.equals(element.getName())) {
                if (element.getChildrenCount(Element.class) == 1) {
                    final Converter<?, ?> converter = loadConverter(element.getChild(Element.class));
                    if (converter != null) {
                        final String name = element.getAttributeValue(NAME);
                        if (Converters.hasConverter(name)) {
                            onError("A converter named '" + name + "' is already registered");
                        } else {
                            final boolean isDefault = element.getAttributeAsBoolean(DEFAULT, false);
                            Converters.register(converter, name, isDefault, FailureReaction.FAIL);
                        }
                    }
                } else {
                    onError("Exactly one child expected under " + element);
                }
            } else {
                unexpectedElement(element, CONVERTER);
            }
        }

        public Converter<?, ?> loadAnonymousConverter(Element element) {
            if (element.getName().equals(CONVERTER)) {
                if (element.getChildrenCount(Element.class) == 1) {
                    return loadConverter(element.getChildAt(Element.class, 0));
                } else {
                    return onError("Exactly one child expected under " + element, null);
                }
            } else {
                return unexpectedElement(element, (Converter<?, ?>) null, CONVERTER);
            }
        }

        public Converter<?, ?> loadConverter(Element element) {
            LOGGER.debug("loadConverter({})", element);
            try {
                switch (element.getName()) {
                case AND:
                    if (element.getChildrenCount(Element.class) == 0) {
                        return onError("At least one child expected under " + AND, null);
                    } else {
                        return andThen(loadChildrenConverters(element));
                    }

                case OR:
                    if (element.getChildrenCount(Element.class) == 0) {
                        return onError("At least one child expected under " + OR, null);
                    } else {
                        return orElse(loadChildrenConverters(element));
                    }

                case DEF:
                    final Args args = argsLoader.loadOptionalChildArgs(element);
                    final String className = element.getAttributeValue(CLASS);
                    return (Converter<?, ?>) Factories.create(className, args);

                case REF:
                    final String name = element.getAttributeValue(NAME);
                    return new RawRefConverter(name);

                default:
                    unexpectedElement(element, DEF, REF, AND);
                    return null;
                }
            } catch (final RuntimeException e) {
                getLogger().error("loadConverter({}) failed", element);
                throw e;
            }
        }

        private static Converter<?, ?> andThen(List<Converter<?, ?>> converters) {
            LOGGER.debug("compose(...)");
            if (converters.isEmpty()) {
                return null;
            } else if (converters.size() == 1) {
                return converters.get(0);
            } else if (converters.size() == 2) {
                return converters.get(0).andThenRaw(converters.get(1));
            } else {
                return converters.get(0).andThenRaw(andThen(converters.subList(1, converters.size())));
            }
        }

        private static Converter<?, ?> orElse(List<Converter<?, ?>> converters) {
            LOGGER.debug("first(...)");
            if (converters.isEmpty()) {
                return null;
            } else {
                final Converter<?, ?>[] others =
                        converters.subList(1, converters.size())
                                  .toArray(new Converter<?, ?>[converters.size() - 1]);
                return converters.get(0).orElseRaw(others);
            }
        }

        private List<Converter<?, ?>> loadChildrenConverters(Element element) {
            LOGGER.debug("loadChildrenConverters({})", element);
            final List<Converter<?, ?>> result = new ArrayList<>();
            for (final Element child : element.getElements()) {
                result.add(loadConverter(child));
            }
            return result;
        }
    }

    public static class StAXLoader extends AbstractStAXLoader<Void> {
        public StAXLoader(FailureReaction reaction) {
            super((reader,
                   systemId) -> new Parser(reader, systemId, reaction));
        }

        private static class Parser extends AbstractStAXParser<Void> {
            private final ArgsIo.ArgsParser argsParser;

            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction) {
                super(reader, systemId, reaction);
                argsParser = new ArgsIo.ArgsParser(reader, systemId, reaction);
            }

            @Override
            protected Void parse() throws XMLStreamException {
                expectStartDocument("parse(");
                nextTag();

                if (isStartElement(CONVERTERS)) {
                    parseConverters();
                    expectEndDocument("parse()");
                    return null;
                } else {
                    throw unexpectedEvent();
                }
            }

            private void parseConverters() throws XMLStreamException {
                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(CONVERTER)) {
                        parseAndRegisterNamedConverter();
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
                next();
            }

            private void parseAndRegisterNamedConverter() throws XMLStreamException {
                final String name = getAttributeValue(NAME, null);
                final boolean isDefault = getAttributeAsBoolean(DEFAULT, false);
                nextTag();
                if (isStartElement(AND) || isStartElement(OR) || isStartElement(DEF) || isStartElement(REF)) {
                    final Converter<?, ?> converter = parseConverter();
                    nextTag();

                    if (Converters.hasConverter(name)) {
                        onError("A converter named '" + name + "' is already registered");
                    } else {
                        Converters.register(converter, name, isDefault, FailureReaction.FAIL);
                    }
                } else {
                    throw unexpectedEvent();
                }
            }

            private Converter<?, ?> parseConverter() throws XMLStreamException {
                Converter<?, ?> result = null;
                if (isStartElement(AND)) {
                    nextTag();
                    result = andThen(parseChildrenConverters());
                } else if (isStartElement(OR)) {
                    nextTag();
                    result = orElse(parseChildrenConverters());
                } else if (isStartElement(DEF)) {
                    final String className = getAttributeValue(CLASS, null);
                    nextTag();
                    final Args args;
                    if (isStartElement(ArgsIo.ARGS)) {
                        LOGGER.debug("Has Args");
                        args = argsParser.parseArgs();
                        nextTag();
                    } else {
                        LOGGER.debug("Has NO Args");
                        args = Args.NO_ARGS;
                    }
                    result = (Converter<?, ?>) Factories.create(className, args);
                } else if (isStartElement(REF)) {
                    final String name = getAttributeValue(NAME, null);
                    result = new RawRefConverter(name);
                    nextTag();
                } else {
                    throw unexpectedEvent();
                }

                return result;
            }

            private static Converter<?, ?> andThen(List<Converter<?, ?>> converters) {
                if (converters.isEmpty()) {
                    throw new IllegalArgumentException();
                } else if (converters.size() == 1) {
                    return converters.get(0);
                } else if (converters.size() == 2) {
                    return converters.get(0).andThenRaw(converters.get(1));
                } else {
                    return converters.get(0).andThenRaw(andThen(converters.subList(1, converters.size())));
                }
            }

            private static Converter<?, ?> orElse(List<Converter<?, ?>> converters) {
                if (converters.isEmpty()) {
                    throw new IllegalArgumentException();
                } else {
                    final Converter<?, ?>[] others =
                            converters.subList(1, converters.size())
                                      .toArray(new Converter<?, ?>[converters.size() - 1]);
                    return converters.get(0).orElseRaw(others);
                }
            }

            private List<Converter<?, ?>> parseChildrenConverters() throws XMLStreamException {
                final List<Converter<?, ?>> result = new ArrayList<>();
                while (isStartElement(AND) || isStartElement(DEF) || isStartElement(REF)) {
                    result.add(parseConverter());
                    nextTag();
                }
                return result;
            }
        }
    }
}