package cdc.args.io;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.args.Arg;
import cdc.args.Args;
import cdc.args.Factories;
import cdc.args.Factory;
import cdc.io.data.Element;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * XML IO of arguments.
 *
 * @author Damien Carbonne
 *
 */
public final class ArgsIo {
    public static final String ARG = "arg";
    public static final String ARGS = "args";
    public static final String CLASS = "class";
    public static final String FACTORIES = "factories";
    public static final String FACTORY = "factory";
    public static final String INSTANCE = "instance";
    public static final String NAME = "name";
    public static final String PARAM = "param";
    public static final String PARAMS = "params";
    public static final String VALUE = "value";

    public static final ArgsIo ARG_NAMING = new ArgsIo(Naming.ARG);
    public static final ArgsIo PARAM_NAMING = new ArgsIo(Naming.PARAM);

    private final Naming naming;

    public enum Naming {
        ARG,
        PARAM
    }

    public ArgsIo(Naming naming) {
        this.naming = naming;
    }

    private String argName() {
        return naming == Naming.ARG ? ARG : PARAM;
    }

    private String argsName() {
        return naming == Naming.ARG ? ARGS : PARAMS;
    }

    public void write(XmlWriter writer,
                      Args args,
                      boolean convert) throws IOException {
        if (!args.isEmpty()) {
            writer.beginElement(argsName());
            for (final Arg arg : args.getArgs()) {
                write(writer, arg, convert);
            }
            writer.endElement();
        }
    }

    public void write(XmlWriter writer,
                      Arg arg,
                      boolean convert) throws IOException {
        if (arg.getValue() == null) {
            writer.beginElement(argName());
            writer.addAttribute(NAME, arg.getName());
            writer.endElement();
        } else {
            final Arg tmp = convert ? Factories.convertToStringValues(arg) : arg;
            writer.beginElement(argName());
            writer.addAttribute(NAME, tmp.getName());
            if (tmp.getValue() != null) {
                writer.addAttribute(VALUE, tmp.getValue());
            }
            writer.endElement();
        }
    }

    public Element toElement(Args args,
                             boolean convert) {
        if (!args.isEmpty()) {
            final Element element = new Element(argsName());
            for (final Arg arg : args.getArgs()) {
                element.addChild(toElement(arg, convert));
            }
            return element;
        } else {
            return null;
        }
    }

    public Element toElement(Arg arg,
                             boolean convert) {
        final Element element = new Element(argName());
        element.addAttribute(NAME, arg.getName());
        if (arg.getValue() != null) {
            final Arg tmp = convert ? Factories.convertToStringValues(arg) : arg;
            if (tmp.getValue() != null) {
                element.addAttribute(VALUE, tmp.getValue());
            }
        }
        return element;
    }

    public static class DataLoader extends AbstractResourceLoader<Void> {
        public DataLoader(FailureReaction reaction) {
            super(reaction);
        }

        @Override
        protected Void loadRoot(Element root) {
            loadAndRegisterFactories(root);
            return null;
        }

        public void loadAndRegisterFactories(Element element) {
            if (element.getName().equals(FACTORIES)) {
                for (final Element child : element.getElements()) {
                    if (child.getName().equals(FACTORY)) {
                        loadAndRegisterFactory(child);
                    } else {
                        unexpectedElement(child, FACTORY);
                    }
                }
            } else {
                unexpectedElement(element, FACTORIES);
            }
        }

        public void loadAndRegisterFactory(Element element) {
            final Factory<?> factory = loadFactory(element);
            if (factory != null) {
                if (Factories.getClasses().contains(factory.getObjectClass())) {
                    onError("A '" + factory.getObjectClass().getCanonicalName() + "' factory is already registered");
                } else {
                    Factories.register(factory);
                }
            }
        }

        public Factory<?> loadFactory(Element element) {
            if (element.getName().equals(FACTORY)) {
                final String className = element.getAttributeValue(CLASS, null);
                final String instanceName = element.getAttributeValue(INSTANCE, null);
                if ((className == null && instanceName == null) || (className != null && instanceName != null)) {
                    return onError("one of " + CLASS + " or " + INSTANCE + " attribute must be set", null);
                } else if (className != null) {
                    @SuppressWarnings("unchecked")
                    final Class<Factory<?>> cls = (Class<Factory<?>>) Introspection.getClass(className, getReaction());
                    if (cls != null) {
                        try {
                            return cls.getDeclaredConstructor().newInstance();

                        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                            return onError("Failed to instantiate '" + className + "'", null);
                        }
                    } else {
                        return null;
                    }
                } else {
                    // TODO
                    return onError("instance support not yet implemented", null);
                }
            } else {
                return unexpectedElement(element, (Factory<?>) null, FACTORY);
            }
        }

        public Args loadArgs(Element element) {
            if (element.getName().equals(ARGS) || element.getName().equals(PARAMS)) {
                final Args.Builder builder = Args.builder();
                for (final Element child : element.getElements()) {
                    if (child.getName().equals(ARG) || child.getName().equals(PARAM)) {
                        final String argName = child.getAttributeValue(NAME, null);
                        final String argValue = child.getAttributeValue(VALUE, null);
                        builder.arg(argName, argValue);
                    } else {
                        unexpectedElement(child, ARG, PARAM);
                    }
                }
                return builder.build();
            } else {
                return unexpectedElement(element, Args.NO_ARGS, ARGS, PARAMS);
            }
        }

        public static Args toArgsLoose(Element element) {
            final Args.Builder builder = Args.builder();
            for (final Element child : element.getElements()) {
                if (child.getName().equals(ARG) || child.getName().equals(PARAM)) {
                    final String argName = child.getAttributeValue(NAME, null);
                    final String argValue = child.getAttributeValue(VALUE, null);
                    builder.arg(argName, argValue);
                }
            }
            return builder.build();
        }

        public Args loadOptionalChildArgs(Element parent) {
            if (parent.hasChildren(Element.class, Element.named(ArgsIo.ARGS))) {
                return loadArgs(parent.getChildAt(Element.class, Element.named(ArgsIo.ARGS), 0));
            } else if (parent.hasChildren(Element.class, Element.named(ArgsIo.PARAMS))) {
                return loadArgs(parent.getChildAt(Element.class, Element.named(ArgsIo.PARAMS), 0));
            } else {
                return Args.NO_ARGS;
            }
        }
    }

    public static class ArgsParser extends AbstractStAXParser<Void> {
        public ArgsParser(XMLStreamReader reader,
                          String systemId,
                          FailureReaction reaction) {
            super(reader, systemId, reaction);
        }

        @Override
        protected Void parse() {
            throw new UnsupportedOperationException();
        }

        public Args parseArgs() throws XMLStreamException {
            expectStartElement("parseArgs()", ARGS, PARAMS);
            final Args.Builder builder = Args.builder();
            nextTag();
            while (isStartElement(ARG) || isStartElement(PARAM)) {
                final String argName = getAttributeValue(NAME, null);
                final String argValue = getAttributeValue(VALUE, null);
                builder.arg(argName, argValue);
                nextTag();
                expectEndElement("parseArgs()", ARG, PARAM);
                nextTag();
            }
            expectEndElement("parseArgs()", ARGS, PARAMS);
            return builder.build();
        }
    }

    public static class StAXLoader extends AbstractStAXLoader<Void> {
        public StAXLoader(FailureReaction reaction) {
            super((reader,
                   systemId) -> new Parser(reader, systemId, reaction));
        }

        private static class Parser extends AbstractStAXParser<Void> {
            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction) {
                super(reader, systemId, reaction);
            }

            @Override
            protected Void parse() throws XMLStreamException {
                nextTag();

                if (isStartElement(FACTORIES)) {
                    parseFactories();
                    next();
                    expectEndDocument("parse()");
                    return null;
                } else {
                    throw unexpectedEvent();
                }
            }

            private void parseFactories() throws XMLStreamException {
                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(FACTORY)) {
                        parseAndRegisterFactory();
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
            }

            private void parseAndRegisterFactory() throws XMLStreamException {
                final Factory<?> factory = parseFactory();
                if (factory != null) {
                    if (Factories.getClasses().contains(factory.getObjectClass())) {
                        onError("A '" + factory.getObjectClass().getCanonicalName() + "' factory is already registered");
                    } else {
                        Factories.register(factory);
                    }
                }
            }

            private Factory<?> parseFactory() throws XMLStreamException {
                // expectStartElement("parseFactory()", FACTORY);

                final String className = getAttributeValue(CLASS, null);
                final String instanceName = getAttributeValue(INSTANCE, null);

                Factory<?> factory = null;

                if ((className == null && instanceName == null) || (className != null && instanceName != null)) {
                    factory = onError("one of " + CLASS + " or " + INSTANCE + " attribute must be set", null);
                } else if (className != null) {
                    @SuppressWarnings("unchecked")
                    final Class<Factory<?>> cls = (Class<Factory<?>>) Introspection.getClass(className, getReaction());
                    if (cls != null) {
                        try {
                            factory = cls.getDeclaredConstructor().newInstance();

                        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                            factory = onError("Failed to instantiate '" + className + "'", null);
                        }
                    } else {
                        // Ignore
                    }
                } else {
                    // TODO
                    factory = onError("instance support not yet implemented", null);
                }

                nextTag();
                expectEndElement("parseFactory()", FACTORY);
                return factory;
            }
        }
    }
}