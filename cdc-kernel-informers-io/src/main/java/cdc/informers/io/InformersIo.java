package cdc.informers.io;

import cdc.args.Factories;
import cdc.informers.Informer;
import cdc.informers.Informers;
import cdc.io.data.Element;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.util.lang.FailureReaction;

public final class InformersIo {
    private static final String CLASS = "class";
    private static final String INFORMER = "informer";
    private static final String INFORMERS = "informers";

    private InformersIo() {
    }

    public static final class DataLoader extends AbstractResourceLoader<Void> {
        public DataLoader(FailureReaction reaction) {
            super(reaction);
        }

        @Override
        protected Void loadRoot(Element root) {
            loadAndRegisterInformers(root);
            return null;
        }

        private void loadAndRegisterInformers(Element element) {
            if (INFORMERS.equals(element.getName())) {
                for (final Element child : element.getChildren(Element.class)) {
                    if (INFORMER.equals(child.getName())) {
                        loadAndRegisterInformer(child);
                    } else {
                        unexpectedElement(child);
                    }
                }
            } else {
                unexpectedElement(element, INFORMERS);
            }
        }

        private void loadAndRegisterInformer(Element element) {
            if (INFORMER.equals(element.getName())) {
                final String informerClassName = element.getAttributeValue(CLASS, null);
                final Informer<?> informer = (Informer<?>) Factories.create(informerClassName);
                Informers.register(informer);
            } else {
                unexpectedElement(element, INFORMER);
            }
        }
    }
}