package cdc.converters;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.args.Args;
import cdc.args.Factory;
import cdc.converters.defaults.AbstractNumberToString;
import cdc.converters.defaults.AbstractSequenceConverter;
import cdc.converters.defaults.AbstractStringToNumber;
import cdc.converters.defaults.ArrayToString;
import cdc.converters.defaults.BigDecimalToString;
import cdc.converters.defaults.BigIntegerToString;
import cdc.converters.defaults.BooleanArrayToString;
import cdc.converters.defaults.BooleanToString;
import cdc.converters.defaults.ByteArrayToString;
import cdc.converters.defaults.ByteToString;
import cdc.converters.defaults.ClassToString;
import cdc.converters.defaults.DateToString;
import cdc.converters.defaults.DoubleArrayToString;
import cdc.converters.defaults.DoubleToString;
import cdc.converters.defaults.EnumToString;
import cdc.converters.defaults.FloatArrayToString;
import cdc.converters.defaults.FloatToString;
import cdc.converters.defaults.Identity;
import cdc.converters.defaults.IntegerArrayToString;
import cdc.converters.defaults.IntegerToString;
import cdc.converters.defaults.LocalDateTimeToString;
import cdc.converters.defaults.LocalDateToString;
import cdc.converters.defaults.LocalTimeToString;
import cdc.converters.defaults.LocaleToString;
import cdc.converters.defaults.LongArrayToString;
import cdc.converters.defaults.LongToString;
import cdc.converters.defaults.ObjectToString;
import cdc.converters.defaults.ShortArrayToString;
import cdc.converters.defaults.ShortToString;
import cdc.converters.defaults.StringToArray;
import cdc.converters.defaults.StringToBigDecimal;
import cdc.converters.defaults.StringToBigInteger;
import cdc.converters.defaults.StringToBoolean;
import cdc.converters.defaults.StringToBooleanArray;
import cdc.converters.defaults.StringToByte;
import cdc.converters.defaults.StringToByteArray;
import cdc.converters.defaults.StringToClass;
import cdc.converters.defaults.StringToDate;
import cdc.converters.defaults.StringToDouble;
import cdc.converters.defaults.StringToDoubleArray;
import cdc.converters.defaults.StringToEnum;
import cdc.converters.defaults.StringToFloat;
import cdc.converters.defaults.StringToFloatArray;
import cdc.converters.defaults.StringToInteger;
import cdc.converters.defaults.StringToIntegerArray;
import cdc.converters.defaults.StringToLocalDate;
import cdc.converters.defaults.StringToLocalDateTime;
import cdc.converters.defaults.StringToLocalTime;
import cdc.converters.defaults.StringToLocale;
import cdc.converters.defaults.StringToLong;
import cdc.converters.defaults.StringToLongArray;
import cdc.converters.defaults.StringToShort;
import cdc.converters.defaults.StringToShortArray;

class DefaultConvertersTest {
    private static final Logger LOGGER = LogManager.getLogger(DefaultConvertersTest.class);
    private static final char WSS = '\u202f'; // '\u00a0'; // The non-breaking space has changed between Java 11 and Java 17

    private static String encode(String s) {
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < s.length(); index++) {
            if (index > 0) {
                builder.append(' ');
            }
            builder.append((int) s.charAt(index));
        }
        return builder.toString();
    }

    private static <S> void checkToString(Factory<? extends Converter<S, String>> factory,
                                          String expected,
                                          S source,
                                          String pattern,
                                          Locale locale) {
        LOGGER.debug("source: {} pattern: {} expected: {}", source, pattern, expected);
        final Args args =
                Args.builder()
                    .arg(AbstractNumberToString.PATTERN, pattern)
                    .arg(AbstractNumberToString.LOCALE, locale)
                    .build();
        final Converter<S, String> converter = factory.create(args);
        final String result = converter.apply(source);
        if (!Objects.equals(expected, result)) {
            LOGGER.error("Expected: <{}> {}", expected, encode(expected));
            LOGGER.error("Actual  : <{}> {}", result, encode(result));
        }
        assertEquals(expected, result);
    }

    @SafeVarargs
    private static <S> void checkArrayToString(Factory<? extends Converter<S[], String>> factory,
                                               Converter<?, String> elementConverter,
                                               String expected,
                                               S... elements) {
        final Args args =
                Args.builder(ArrayToString.FPARAMS)
                    .arg(AbstractSequenceConverter.PREFIX, "[")
                    .arg(AbstractSequenceConverter.SEPARATOR, ", ")
                    .arg(AbstractSequenceConverter.SUFFIX, "]")
                    .arg(ArrayToString.CONVERTER, elementConverter)
                    .build();
        final Converter<S[], String> converter = factory.create(args);
        final String result = converter.apply(elements);
        assertEquals(expected, result);
    }

    @SafeVarargs
    private static <T> void checkStringToArray(Factory<? extends Converter<String, T[]>> factory,
                                               Converter<String, ? extends T> elementConverter,
                                               String s,
                                               T... expected) {
        final Args args =
                Args.builder(StringToArray.FPARAMS)
                    .arg(AbstractSequenceConverter.PREFIX, "[")
                    .arg(AbstractSequenceConverter.SEPARATOR, ", ")
                    .arg(AbstractSequenceConverter.SUFFIX, "]")
                    .arg(StringToArray.CONVERTER, elementConverter)
                    .build();
        final Converter<String, T[]> converter = factory.create(args);
        final T[] result = converter.apply(s);
        assertArrayEquals(expected, result);
    }

    private static <S> void checkToString(Factory<? extends Converter<S, String>> factory,
                                          String expected,
                                          S source) {
        checkToString(factory, expected, source, null, null);
    }

    private static <S> void checkFromString(Factory<? extends Converter<String, S>> factory,
                                            S expected,
                                            String source,
                                            String pattern,
                                            Locale locale) {
        LOGGER.debug("source: {} pattern: {} locale: '{}' expected: {}", source, pattern, locale, expected);
        final Args args =
                Args.builder()
                    .arg(AbstractNumberToString.PATTERN, pattern)
                    .arg(AbstractNumberToString.LOCALE, locale)
                    .build();
        final Converter<String, S> converter = factory.create(args);
        final S result = converter.apply(source);
        assertEquals(expected, result);
    }

    private static <S> void checkFromString(Factory<? extends Converter<String, S>> factory,
                                            S expected,
                                            String source) {
        checkFromString(factory, expected, source, null, null);
    }

    @Test
    void testBooleanToString() {
        assertEquals(Boolean.class, BooleanToString.INSTANCE.getSourceClass());
        assertEquals(null, BooleanToString.INSTANCE.apply(null));
        assertEquals("false", BooleanToString.INSTANCE.apply(false));
        assertEquals("true", BooleanToString.INSTANCE.apply(true));

        checkToString(BooleanToString.FACTORY, null, null);
    }

    @Test
    void testBooleanArrayToString() {
        checkArrayToString(BooleanArrayToString.FACTORY,
                           BooleanToString.INSTANCE,
                           "[]");
        checkArrayToString(BooleanArrayToString.FACTORY,
                           BooleanToString.INSTANCE,
                           "[true]",
                           true);
        checkArrayToString(BooleanArrayToString.FACTORY,
                           BooleanToString.INSTANCE,
                           "[true, false]",
                           true,
                           false);
    }

    @Test
    void testStringToBoolean() {
        assertEquals(Boolean.class, StringToBoolean.INSTANCE.getTargetClass());
        assertEquals(null, StringToBoolean.INSTANCE.apply(null));
        assertEquals(false, StringToBoolean.INSTANCE.apply("false"));
        assertEquals(true, StringToBoolean.INSTANCE.apply("true"));
        checkFromString(StringToBoolean.FACTORY, null, null);
    }

    @Test
    void testStringToBooleanArray() {
        checkStringToArray(StringToBooleanArray.FACTORY,
                           StringToBoolean.INSTANCE,
                           "[]");
        checkStringToArray(StringToBooleanArray.FACTORY,
                           StringToBoolean.INSTANCE,
                           "[true]",
                           true);
        checkStringToArray(StringToBooleanArray.FACTORY,
                           StringToBoolean.INSTANCE,
                           "[true, false]",
                           true,
                           false);
    }

    @Test
    void testDoubleToString() {
        assertEquals(Double.class, DoubleToString.INSTANCE.getSourceClass());
        checkToString(DoubleToString.FACTORY, null, null);
        checkToString(DoubleToString.FACTORY, "0.0", 0.0);
        checkToString(DoubleToString.FACTORY, "0,00", 0.0, "0.00", Locale.FRANCE);
        checkToString(DoubleToString.FACTORY, "0,00", 0.0e3, "0.00", Locale.FRANCE);
        checkToString(DoubleToString.FACTORY, "0.00", 0.0, "0.00", Locale.ENGLISH);
    }

    @Test
    void testDoubleArrayToString() {
        checkArrayToString(DoubleArrayToString.FACTORY,
                           DoubleToString.INSTANCE,
                           "[]");
        checkArrayToString(DoubleArrayToString.FACTORY,
                           DoubleToString.INSTANCE,
                           "[2.0]",
                           2.0);
        checkArrayToString(DoubleArrayToString.FACTORY,
                           DoubleToString.INSTANCE,
                           "[2.0, 3.0]",
                           2.0,
                           3.0);
    }

    @Test
    void testStringToDouble() {
        checkFromString(StringToDouble.FACTORY, null, null);
        checkFromString(StringToDouble.FACTORY, 0.0, "0.0");
        checkFromString(StringToDouble.FACTORY, 0.0, "0.0", null, Locale.ENGLISH);
        checkFromString(StringToDouble.FACTORY, 0.1, "0.1");
        checkFromString(StringToDouble.FACTORY, 0.1e0, "0.1");
        checkFromString(StringToDouble.FACTORY, 0.1, "0.1", "0.0", Locale.ENGLISH);
        checkFromString(StringToDouble.FACTORY, 0.1e0, "0.1", "0.0", Locale.ENGLISH);
        checkFromString(StringToDouble.FACTORY, 0.1, "0,1", "0.0", Locale.FRANCE);
        checkFromString(StringToDouble.FACTORY, 0.1e0, "0,1", "0.0", Locale.FRANCE);
    }

    @Test
    void testStringToDoubleArray() {
        checkStringToArray(StringToDoubleArray.FACTORY,
                           StringToDouble.INSTANCE,
                           "[]");
        checkStringToArray(StringToDoubleArray.FACTORY,
                           StringToDouble.INSTANCE,
                           "[2.0]",
                           2.0);
        checkStringToArray(StringToDoubleArray.FACTORY,
                           StringToDouble.INSTANCE,
                           "[2.0, 3.0]",
                           2.0,
                           3.0);
    }

    @Test
    void testBigDecimalToString() {
        assertEquals(BigDecimal.class, BigDecimalToString.INSTANCE.getSourceClass());
        checkToString(BigDecimalToString.FACTORY, null, null);
        checkToString(BigDecimalToString.FACTORY, "0.0", new BigDecimal("0.0"));
        checkToString(BigDecimalToString.FACTORY, "0,00", new BigDecimal("0.0"), "0.00", Locale.FRANCE);
        checkToString(BigDecimalToString.FACTORY, "0,00", new BigDecimal("0.0e3"), "0.00", Locale.FRANCE);
        checkToString(BigDecimalToString.FACTORY, "0.00", new BigDecimal("0.0"), "0.00", Locale.ENGLISH);
    }

    @Test
    void testStringToBigDecimal() {
        checkFromString(StringToBigDecimal.FACTORY, null, null);
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0"), "0.0");
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0"), "0.0", null, Locale.ENGLISH);
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0.1"), "0.1");
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0.1e0"), "0.1");
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0.1"), "0.1", "0.0", Locale.ENGLISH);
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0.1e0"), "0.1", "0.0", Locale.ENGLISH);
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0.1"), "0,1", "0.0", Locale.FRANCE);
        checkFromString(StringToBigDecimal.FACTORY, new BigDecimal("0.1e0"), "0,1", "0.0", Locale.FRANCE);
    }

    @Test
    void testFloatToString() {
        assertEquals(Float.class, FloatToString.INSTANCE.getSourceClass());
        checkToString(FloatToString.FACTORY, null, null);
        checkToString(FloatToString.FACTORY, "10.01", 10.01f);
        checkToString(FloatToString.FACTORY, "123456.0", 123456.0f);
        checkToString(FloatToString.FACTORY, "123456.0", 123456.0000f);
        checkToString(FloatToString.FACTORY, "0.0", 0.0f);

        checkToString(FloatToString.FACTORY, "0,00", 0.0f, "0.00", Locale.FRANCE);
        checkToString(FloatToString.FACTORY, "0,00E0", 0.0f, "0.00E0", Locale.FRANCE);
        checkToString(FloatToString.FACTORY, "1,00E1", 10.0f, "0.00E0", Locale.FRANCE);
        checkToString(FloatToString.FACTORY, "1,00E01", 10.0f, "0.00E00", Locale.FRANCE);
        checkToString(FloatToString.FACTORY, "1,00E-3", 1.0e-3f, "0.00E0", Locale.FRANCE);
        checkToString(FloatToString.FACTORY, "1,00E-03", 1.0e-3f, "0.00E00", Locale.FRANCE);
        checkToString(FloatToString.FACTORY, "0.00", 0.0f, "0.00", Locale.ENGLISH);
    }

    @Test
    void testFloatArrayToString() {
        checkArrayToString(FloatArrayToString.FACTORY,
                           FloatToString.INSTANCE,
                           "[]");
        checkArrayToString(FloatArrayToString.FACTORY,
                           FloatToString.INSTANCE,
                           "[2.0]",
                           2.0F);
        checkArrayToString(FloatArrayToString.FACTORY,
                           FloatToString.INSTANCE,
                           "[2.0, 3.0]",
                           2.0F,
                           3.0F);
    }

    @Test
    void testStringToFloat() throws ParseException {
        final NumberFormat f = NumberFormat.getInstance(Locale.ENGLISH);
        final Object o = f.parse("0.0");
        LOGGER.debug("o: {} {}", o, o.getClass().getSimpleName());

        checkFromString(StringToFloat.FACTORY, null, null);
        checkFromString(StringToFloat.FACTORY, 0.0f, "0.0", null, Locale.ENGLISH);
        checkFromString(StringToFloat.FACTORY, 0.1f, "0.1");
        checkFromString(StringToFloat.FACTORY, 0.1f, "0.1", "0.0", Locale.ENGLISH);
        checkFromString(StringToFloat.FACTORY, 0.1f, "0,1", "0.0", Locale.FRANCE);
    }

    @Test
    void testStringToFloatArray() {
        checkStringToArray(StringToFloatArray.FACTORY,
                           StringToFloat.INSTANCE,
                           "[]");
        checkStringToArray(StringToFloatArray.FACTORY,
                           StringToFloat.INSTANCE,
                           "[2.0]",
                           2.0F);
        checkStringToArray(StringToFloatArray.FACTORY,
                           StringToFloat.INSTANCE,
                           "[2.0, 3.0]",
                           2.0F,
                           3.0F);
    }

    @Test
    void testLongToString() {
        assertEquals(Long.class, LongToString.INSTANCE.getSourceClass());
        checkToString(LongToString.FACTORY, null, null);
        checkToString(LongToString.FACTORY, "0", 0L);
        checkToString(LongToString.FACTORY, "123456789", 123456789L);

        checkToString(LongToString.FACTORY, "000", 0L, "000", Locale.FRANCE);
        checkToString(LongToString.FACTORY, "000", 0L, "000", Locale.ENGLISH);
        checkToString(LongToString.FACTORY, "1000000", 1000000L, "000", Locale.ENGLISH);
        checkToString(LongToString.FACTORY, "1,000,000", 1000000L, "000,000", Locale.ENGLISH);
        checkToString(LongToString.FACTORY, "001" + WSS + "000" + WSS + "000", 1000000L, "000000,000", Locale.FRANCE);
    }

    @Test
    void testLongArrayToString() {
        checkArrayToString(LongArrayToString.FACTORY,
                           LongToString.INSTANCE,
                           "[]");
        checkArrayToString(LongArrayToString.FACTORY,
                           LongToString.INSTANCE,
                           "[10]",
                           10L);
        checkArrayToString(LongArrayToString.FACTORY,
                           LongToString.INSTANCE,
                           "[10, 20]",
                           10L,
                           20L);
    }

    @Test
    void testStringToLong() {
        checkFromString(StringToLong.FACTORY, null, null);
        checkFromString(StringToLong.FACTORY, 0L, "0");
        checkFromString(StringToLong.FACTORY, 1000000L, "1000000");
        checkFromString(StringToLong.FACTORY, 1000000L, "1000000", "0", Locale.FRANCE);
        checkFromString(StringToLong.FACTORY, 1000000L, "001000000", "000000000", Locale.FRANCE);
        final Args params = Args.builder()
                                .arg(AbstractStringToNumber.PATTERN, null)
                                .arg(AbstractStringToNumber.LOCALE, null)
                                .build();
        assertEquals(params, StringToLong.INSTANCE.getParams());
    }

    @Test
    void testStringToLongArray() {
        checkStringToArray(StringToLongArray.FACTORY,
                           StringToLong.INSTANCE,
                           "[]");
        checkStringToArray(StringToLongArray.FACTORY,
                           StringToLong.INSTANCE,
                           "[10]",
                           10L);
        checkStringToArray(StringToLongArray.FACTORY,
                           StringToLong.INSTANCE,
                           "[10, 20]",
                           10L,
                           20L);
    }

    @Test
    void testIntegerToString() {
        assertEquals(Integer.class, IntegerToString.INSTANCE.getSourceClass());
        checkToString(IntegerToString.FACTORY, null, null);
        checkToString(IntegerToString.FACTORY, "0", 0);
        checkToString(IntegerToString.FACTORY, "123456789", 123456789);

        checkToString(IntegerToString.FACTORY, "000", 0, "000", Locale.FRANCE);
        checkToString(IntegerToString.FACTORY, "000", 0, "000", Locale.ENGLISH);
    }

    @Test
    void testIntegerArrayToString() {
        checkArrayToString(IntegerArrayToString.FACTORY,
                           IntegerToString.INSTANCE,
                           "[]");
        checkArrayToString(IntegerArrayToString.FACTORY,
                           IntegerToString.INSTANCE,
                           "[10]",
                           10);
        checkArrayToString(IntegerArrayToString.FACTORY,
                           IntegerToString.INSTANCE,
                           "[10, 20]",
                           10,
                           20);
    }

    @Test
    void testStringToInteger() {
        checkFromString(StringToInteger.FACTORY, null, null);
        checkFromString(StringToInteger.FACTORY, 0, "0");
        final Args params = Args.builder()
                                .arg(AbstractStringToNumber.PATTERN, null)
                                .arg(AbstractStringToNumber.LOCALE, null)
                                .build();
        assertEquals(params, StringToInteger.INSTANCE.getParams());
    }

    @Test
    void testStringToIntegerArray() {
        checkStringToArray(StringToIntegerArray.FACTORY,
                           StringToInteger.INSTANCE,
                           "[]");
        checkStringToArray(StringToIntegerArray.FACTORY,
                           StringToInteger.INSTANCE,
                           "[10]",
                           10);
        checkStringToArray(StringToIntegerArray.FACTORY,
                           StringToInteger.INSTANCE,
                           "[10, 20]",
                           10,
                           20);
    }

    @Test
    void testBigIntegerToString() {
        assertEquals(BigInteger.class, BigIntegerToString.INSTANCE.getSourceClass());
        checkToString(BigIntegerToString.FACTORY, null, null);
        checkToString(BigIntegerToString.FACTORY, "0", new BigInteger("0"));
        checkToString(BigIntegerToString.FACTORY, "123456789", new BigInteger("123456789"));

        checkToString(BigIntegerToString.FACTORY, "000", new BigInteger("0"), "000", Locale.FRANCE);
        checkToString(BigIntegerToString.FACTORY, "000", new BigInteger("0"), "000", Locale.ENGLISH);
    }

    @Test
    void testStringToBigInteger() {
        checkFromString(StringToBigInteger.FACTORY, null, null);
        checkFromString(StringToBigInteger.FACTORY, new BigInteger("0"), "0");
        final Args params = Args.builder()
                                .arg(AbstractStringToNumber.PATTERN, null)
                                .arg(AbstractStringToNumber.LOCALE, null)
                                .build();
        assertEquals(params, StringToBigInteger.INSTANCE.getParams());
    }

    @Test
    void testShortToString() {
        assertEquals(Short.class, ShortToString.INSTANCE.getSourceClass());
        checkToString(ShortToString.FACTORY, null, null);
        checkToString(ShortToString.FACTORY, "0", (short) 0);
        checkToString(ShortToString.FACTORY, "12345", (short) 12345);

        checkToString(ShortToString.FACTORY, "000", (short) 0, "000", Locale.FRANCE);
        checkToString(ShortToString.FACTORY, "000", (short) 0, "000", Locale.ENGLISH);
    }

    @Test
    void testShortArrayToString() {
        checkArrayToString(ShortArrayToString.FACTORY,
                           ShortToString.INSTANCE,
                           "[]");
        checkArrayToString(ShortArrayToString.FACTORY,
                           ShortToString.INSTANCE,
                           "[10]",
                           (short) 10);
        checkArrayToString(ShortArrayToString.FACTORY,
                           ShortToString.INSTANCE,
                           "[10, 20]",
                           (short) 10,
                           (short) 20);
    }

    @Test
    void testStringToShort() {
        checkFromString(StringToShort.FACTORY, null, null);
        checkFromString(StringToShort.FACTORY, (short) 0, "0");
        final Args params = Args.builder()
                                .arg(AbstractStringToNumber.PATTERN, null)
                                .arg(AbstractStringToNumber.LOCALE, null)
                                .build();
        assertEquals(params, StringToShort.INSTANCE.getParams());
    }

    @Test
    void testStringToShortArray() {
        checkStringToArray(StringToShortArray.FACTORY,
                           StringToShort.INSTANCE,
                           "[]");
        checkStringToArray(StringToShortArray.FACTORY,
                           StringToShort.INSTANCE,
                           "[10]",
                           (short) 10);
        checkStringToArray(StringToShortArray.FACTORY,
                           StringToShort.INSTANCE,
                           "[10, 20]",
                           (short) 10,
                           (short) 20);
    }

    @Test
    void testByteToString() {
        assertEquals(Byte.class, ByteToString.INSTANCE.getSourceClass());
        checkToString(ByteToString.FACTORY, null, null);
        checkToString(ByteToString.FACTORY, "0", (byte) 0);
        checkToString(ByteToString.FACTORY, "123", (byte) 123);
        checkToString(ByteToString.FACTORY, "-123", (byte) -123);

        checkToString(ByteToString.FACTORY, "000", (byte) 0, "000", Locale.FRANCE);
        checkToString(ByteToString.FACTORY, "000", (byte) 0, "000", Locale.ENGLISH);
    }

    @Test
    void testByteArrayToString() {
        checkArrayToString(ByteArrayToString.FACTORY,
                           ByteToString.INSTANCE,
                           "[]");
        checkArrayToString(ByteArrayToString.FACTORY,
                           ByteToString.INSTANCE,
                           "[10]",
                           (byte) 10);
        checkArrayToString(ByteArrayToString.FACTORY,
                           ByteToString.INSTANCE,
                           "[10, 20]",
                           (byte) 10,
                           (byte) 20);
    }

    @Test
    void testStringToByte() {
        checkFromString(StringToByte.FACTORY, null, null);
        checkFromString(StringToByte.FACTORY, (byte) 0, "0");
        final Args params = Args.builder()
                                .arg(AbstractStringToNumber.PATTERN, null)
                                .arg(AbstractStringToNumber.LOCALE, null)
                                .build();
        assertEquals(params, StringToByte.INSTANCE.getParams());
    }

    @Test
    void testStringToByteArray() {
        checkStringToArray(StringToByteArray.FACTORY,
                           StringToByte.INSTANCE,
                           "[]");
        checkStringToArray(StringToByteArray.FACTORY,
                           StringToByte.INSTANCE,
                           "[10]",
                           (byte) 10);
        checkStringToArray(StringToByteArray.FACTORY,
                           StringToByte.INSTANCE,
                           "[10, 20]",
                           (byte) 10,
                           (byte) 20);
    }

    @Test
    void testLocaleToString() {
        assertEquals(Locale.class, LocaleToString.INSTANCE.getSourceClass());
        checkToString(LocaleToString.FACTORY, null, null);
        checkToString(LocaleToString.FACTORY, "fr-FR", Locale.FRANCE);
    }

    @Test
    void testStringToLocale() {
        checkFromString(StringToLocale.FACTORY, null, null);
        checkFromString(StringToLocale.FACTORY, Locale.FRANCE, "fr-FR");
        assertEquals(Args.NO_ARGS, StringToLocale.INSTANCE.getParams());
    }

    @Test
    void testClassToString() {
        assertEquals(Class.class, ClassToString.INSTANCE.getSourceClass());
        checkToString(ClassToString.FACTORY, null, null);
        checkToString(ClassToString.FACTORY, "java.lang.String", String.class);
    }

    @Test
    void testStringToClass() {
        assertEquals(Class.class, StringToClass.INSTANCE.getTargetClass());
        assertEquals(null, StringToClass.INSTANCE.apply(null));
        assertEquals(Long.class, StringToClass.INSTANCE.apply("Long"));
        assertEquals(Integer.class, StringToClass.INSTANCE.apply("Integer"));
        assertEquals(Short.class, StringToClass.INSTANCE.apply("Short"));
        assertEquals(Byte.class, StringToClass.INSTANCE.apply("Byte"));
        assertEquals(Double.class, StringToClass.INSTANCE.apply("Double"));
        assertEquals(Float.class, StringToClass.INSTANCE.apply("Float"));
        assertEquals(Character.class, StringToClass.INSTANCE.apply("Character"));
        assertEquals(Boolean.class, StringToClass.INSTANCE.apply("Boolean"));

        assertEquals(String.class, StringToClass.INSTANCE.apply("String"));

        checkFromString(StringToClass.FACTORY, null, null);
        checkFromString(StringToClass.FACTORY, String.class, "String");
        checkFromString(StringToClass.FACTORY, String.class, "java.lang.String");
    }

    @Test
    void testIdentity() {
        assertEquals(null, Identity.INSTANCE.apply(null));
        assertEquals("Hello", Identity.INSTANCE.apply("Hello"));
        assertEquals("Hello", Identity.FACTORY.create().apply("Hello"));
        assertEquals(Args.NO_ARGS, Identity.INSTANCE.getParams());
    }

    private enum TestEnum {
        A,
        B,
        C
    }

    @Test
    void testEnumToString() {
        assertEquals(Enum.class, EnumToString.INSTANCE.getSourceClass());
        assertEquals(null, EnumToString.INSTANCE.apply(null));
        assertEquals("A", EnumToString.INSTANCE.apply(TestEnum.A));

        checkToString(EnumToString.FACTORY, null, null);
        checkToString(EnumToString.FACTORY, "A", TestEnum.A);
    }

    @Test
    void testStringToEnum() {
        final StringToEnum<TestEnum> converter = StringToEnum.create(TestEnum.class);
        assertEquals(TestEnum.class, converter.getTargetClass());
        assertEquals(null, converter.apply(null));
        assertEquals(TestEnum.A, converter.apply("A"));

        final StringToEnum<?> converter2 =
                StringToEnum.FACTORY.create(Args.builder().arg(StringToEnum.CLASS, TestEnum.class).build());
        assertEquals(TestEnum.A, converter2.apply("A"));
    }

    @Test
    void testObjectToString() {
        checkToString(ObjectToString.FACTORY, null, null);
        checkToString(ObjectToString.FACTORY, "Hello", "Hello");
        checkToString(ObjectToString.FACTORY, "10", 10);
    }

    @Test
    void testObjectToStringFormat() {
        final ObjectToString converter =
                ObjectToString.FACTORY.create(Args.builder().arg(ObjectToString.FORMAT, "AAA%sBBB").build());
        assertEquals("AAAnullBBB", converter.apply(null));
        assertEquals("AAAHelloBBB", converter.apply("Hello"));
    }

    @Test
    void testDateToString() {
        final Calendar cal = Calendar.getInstance();
        // Month is 0-based in calendar
        cal.set(2000, 06, 15, 16, 20, 30);
        final Date date = cal.getTime();
        assertEquals("2000-07-15", DateToString.DASH_YYYY_MM_DD.apply(date));
        assertEquals("15-07-2000", DateToString.DASH_DD_MM_YYYY.apply(date));
        assertEquals("16:20:30", DateToString.HH_MM_SS.apply(date));
    }

    @Test
    void testStringToDate() {
        final Calendar cal0 = Calendar.getInstance();
        cal0.clear();
        // Month is 0-based in calendar
        cal0.set(2000, 06, 15, 00, 00, 00);
        final Date date0 = cal0.getTime();

        final Calendar cal1 = Calendar.getInstance();
        cal1.clear();
        // Month is 0-based in calendar
        cal1.set(1970, 00, 01, 16, 20, 30);
        final Date date1 = cal1.getTime();

        final Calendar cal2 = Calendar.getInstance();
        cal2.clear();
        // Month is 0-based in calendar
        cal2.set(2000, 06, 15, 12, 25, 50);
        final Date date2 = cal2.getTime();

        final Calendar cal3 = Calendar.getInstance();
        cal3.clear();
        // Month is 0-based in calendar
        cal3.set(2000, 06, 15, 12, 25);
        final Date date3 = cal3.getTime();

        final Calendar cal4 = Calendar.getInstance();
        cal4.clear();
        // Month is 0-based in calendar
        cal4.set(1970, 00, 01, 16, 20);
        final Date date4 = cal4.getTime();

        assertEquals(date0, StringToDate.DASH_YYYY_MM_DD.apply("2000-07-15"));
        assertEquals(date0, StringToDate.DASH_DD_MM_YYYY.apply("15-07-2000"));
        assertEquals(date1, StringToDate.HH_MM_SS.apply("16:20:30"));
        assertEquals(date0, StringToDate.AUTO.apply("2000-07-15"));
        assertEquals(date0, StringToDate.AUTO.apply("15-07-2000"));
        assertEquals(date1, StringToDate.AUTO.apply("16:20:30.000"));
        assertEquals(date1, StringToDate.AUTO.apply("16:20:30"));
        assertEquals(date2, StringToDate.AUTO.apply("2000-07-15 12:25:50.000"));
        assertEquals(date2, StringToDate.AUTO.apply("2000-07-15 12:25:50"));
        assertEquals(date2, StringToDate.AUTO.apply("2000/07/15 12:25:50.000"));
        assertEquals(date2, StringToDate.AUTO.apply("2000/07/15 12:25:50"));
        assertEquals(date2, StringToDate.AUTO.apply("15-07-2000 12:25:50.000"));
        assertEquals(date2, StringToDate.AUTO.apply("15-07-2000 12:25:50"));
        assertEquals(date2, StringToDate.AUTO.apply("15/07/2000 12:25:50.000"));
        assertEquals(date2, StringToDate.AUTO.apply("15/07/2000 12:25:50"));
        assertEquals(date3, StringToDate.AUTO.apply("2000-07-15 12:25"));
        assertEquals(date3, StringToDate.AUTO.apply("2000/07/15 12:25"));
        assertEquals(date3, StringToDate.AUTO.apply("15-07-2000 12:25"));
        assertEquals(date3, StringToDate.AUTO.apply("15/07/2000 12:25"));
        assertEquals(date4, StringToDate.AUTO.apply("16:20"));

        // assertThrows(IllegalArgumentException.class,
        // () -> {
        // StringToDate.DASH_DD_MM_YYYY.apply("15-7-20");
        // });
    }

    @Test
    void testLocalDateToString() {
        final LocalDate x = LocalDate.of(2018, 01, 23);
        assertEquals("2018-01-23", LocalDateToString.DASH_YYYY_MM_DD.apply(x));
        assertEquals("23-01-2018", LocalDateToString.DASH_DD_MM_YYYY.apply(x));
        assertEquals("20180123", LocalDateToString.BASIC_ISO_DATE.apply(x));
        assertEquals("2018-01-23", LocalDateToString.ISO_LOCAL_DATE.apply(x));
        assertEquals("2018-01-23", LocalDateToString.ISO_DATE.apply(x));
    }

    @Test
    void testStringToLocalDate() {
        final LocalDate x = LocalDate.of(2018, 01, 23);
        assertEquals(x, StringToLocalDate.DASH_YYYY_MM_DD.apply("2018-01-23"));
        assertEquals(x, StringToLocalDate.DASH_DD_MM_YYYY.apply("23-01-2018"));
        assertEquals(x, StringToLocalDate.BASIC_ISO_DATE.apply("20180123"));
        assertEquals(x, StringToLocalDate.ISO_LOCAL_DATE.apply("2018-01-23"));
        assertEquals(x, StringToLocalDate.ISO_DATE.apply("2018-01-23"));

        final LocalDate dt0 = LocalDate.of(2000, 07, 15);
        assertEquals(dt0, StringToLocalDate.AUTO.apply("2000-07-15"));
        assertEquals(dt0, StringToLocalDate.AUTO.apply("2000/07/15"));
        assertEquals(dt0, StringToLocalDate.AUTO.apply("15-07-2000"));
        assertEquals(dt0, StringToLocalDate.AUTO.apply("15/07/2000"));
    }

    @Test
    void testLocalTimeToString() {
        final LocalTime x = LocalTime.of(14, 25, 58);
        assertEquals("14:25:58", LocalTimeToString.HH_MM_SS.apply(x));
        assertEquals("14:25:58", LocalTimeToString.ISO_LOCAL_TIME.apply(x));
        assertEquals("14:25:58", LocalTimeToString.ISO_TIME.apply(x));
    }

    @Test
    void testStringToLocalTime() {
        final LocalTime x = LocalTime.of(14, 25, 58);
        assertEquals(x, StringToLocalTime.HH_MM_SS.apply("14:25:58"));
        assertEquals(x, StringToLocalTime.ISO_LOCAL_TIME.apply("14:25:58"));
        assertEquals(x, StringToLocalTime.ISO_TIME.apply("14:25:58"));

        final LocalTime dt0 = LocalTime.of(12, 25, 50);
        assertEquals(dt0, StringToLocalTime.AUTO.apply("12:25:50.000"));
        assertEquals(dt0, StringToLocalTime.AUTO.apply("12:25:50"));

        final LocalTime dt1 = LocalTime.of(12, 25);
        assertEquals(dt1, StringToLocalTime.AUTO.apply("12:25"));
    }

    @Test
    void testLocalDateTimeToString() {
        final LocalDateTime x = LocalDateTime.of(2018, 01, 23, 14, 25, 58);
        assertEquals("2018-01-23T14:25:58", LocalDateTimeToString.ISO_DATE_TIME.apply(x));
    }

    @Test
    void testStringToLocalDateTime() {
        final LocalDateTime x = LocalDateTime.of(2018, 01, 23, 14, 25, 58);
        assertEquals(x, StringToLocalDateTime.ISO_DATE_TIME.apply("2018-01-23T14:25:58"));

        final LocalDateTime dt0 = LocalDateTime.of(2000, 07, 15, 12, 25, 50);
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("2000-07-15 12:25:50.000"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("2000-07-15 12:25:50"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("2000/07/15 12:25:50.000"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("2000/07/15 12:25:50"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("15-07-2000 12:25:50.000"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("15-07-2000 12:25:50"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("15/07/2000 12:25:50.000"));
        assertEquals(dt0, StringToLocalDateTime.AUTO.apply("15/07/2000 12:25:50"));

        final LocalDateTime dt1 = LocalDateTime.of(2000, 07, 15, 12, 25);
        assertEquals(dt1, StringToLocalDateTime.AUTO.apply("2000-07-15 12:25"));
        assertEquals(dt1, StringToLocalDateTime.AUTO.apply("2000/07/15 12:25"));
        assertEquals(dt1, StringToLocalDateTime.AUTO.apply("15-07-2000 12:25"));
        assertEquals(dt1, StringToLocalDateTime.AUTO.apply("15/07/2000 12:25"));
    }
}