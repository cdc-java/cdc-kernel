package cdc.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.function.IterableUtils;

class ConvertersTest {
    private static final Logger LOGGER = LogManager.getLogger(ConvertersTest.class);

    @Test
    void test() {
        for (final String name : IterableUtils.toSortedList(Converters.getNames())) {
            final Converter<?, ?> converter = Converters.getConverter(name);
            LOGGER.debug((Converters.isSpecialConverter(converter, SpecialConverterKind.DEFAULT) ? "* " : "  ") + converter + " "
                    + name + " " + converter.getParams());
        }

        assertTrue(true);
        // TODO
    }

    @Test
    void testConvertRaw() {
        assertEquals(null, Converters.convertRaw((Object) null, Object.class));
        assertEquals("Hello", Converters.convertRaw((Object) "Hello", String.class));
        assertEquals(true, Converters.convertRaw(true, Boolean.class));

        final List<String> in = new ArrayList<>();
        in.add("Hello");
        in.add("World");

        assertEquals(in, Converters.convertRaw(in, List.class));
    }
}