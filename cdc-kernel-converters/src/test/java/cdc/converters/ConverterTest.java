package cdc.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.converters.defaults.FloatToString;
import cdc.converters.defaults.StringToFloat;

class ConverterTest {

    @Test
    void testAndThen() {
        final Converter<Float, Float> converter = FloatToString.INSTANCE.andThen(StringToFloat.INSTANCE);
        assertEquals(1.0f, (float) converter.apply(1.0f));
        assertEquals(Float.class, converter.getSourceClass());
        assertEquals(Float.class, converter.getTargetClass());
        assertTrue(converter.areCompliantSourceAndTargetClasses(Float.class, Float.class));
        assertFalse(converter.isCompliantSourceClass(null));
        assertFalse(converter.isCompliantTargetClass(null));
        assertFalse(converter.isCompliantSourceClass(String.class));
        assertFalse(converter.isCompliantTargetClass(String.class));
    }

    @Test
    void testCompose() {
        final Converter<Float, Float> converter = StringToFloat.INSTANCE.compose(FloatToString.INSTANCE);
        assertEquals(1.0f, (float) converter.apply(1.0f));
        assertEquals(Float.class, converter.getSourceClass());
        assertEquals(Float.class, converter.getTargetClass());
        assertTrue(converter.areCompliantSourceAndTargetClasses(Float.class, Float.class));
    }
}