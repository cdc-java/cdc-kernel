package cdc.converters;

public class ConversionException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;

    public ConversionException() {
        super();
    }

    public ConversionException(String message) {
        super(message);
    }

    public ConversionException(String message,
                               Throwable cause) {
        super(message, cause);
    }
}