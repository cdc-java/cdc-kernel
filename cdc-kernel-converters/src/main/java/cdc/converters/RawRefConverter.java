package cdc.converters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Args;
import cdc.util.lang.FailureReaction;
import cdc.util.refs.ResolutionException;
import cdc.util.refs.ResolutionStatus;

public final class RawRefConverter implements Converter<Object, Object> {
    private static final Logger LOGGER = LogManager.getLogger(RawRefConverter.class);
    private final String name;
    private ResolutionStatus status = ResolutionStatus.PENDING;
    private Converter<Object, Object> delegate = null;

    public RawRefConverter(String name) {
        this.name = name;
    }

    /**
     * @return The delegate name.
     */
    public String getName() {
        return name;
    }

    /**
     * Resolves the delegate.
     * <p>
     * The delegate is searched only the first time this method is called.
     *
     * @param reaction The reaction to adopt if resolution fails.
     */
    public void resolve(FailureReaction reaction) {
        if (status == ResolutionStatus.PENDING) {
            @SuppressWarnings("unchecked")
            final Converter<Object, Object> tmp = (Converter<Object, Object>) Converters.getConverter(name, FailureReaction.FAIL);
            this.delegate = tmp;
            if (this.delegate == null) {
                status = ResolutionStatus.FAILURE;
            } else {
                status = ResolutionStatus.SUCCESS;
            }
        }
        if (status == ResolutionStatus.FAILURE) {
            if (reaction == FailureReaction.FAIL) {
                throw new ResolutionException("Failed to resolve " + name);
            } else if (reaction == FailureReaction.WARN) {
                LOGGER.warn("Failed to resolve {}", name);
            }
        }
    }

    public ResolutionStatus getResolutionStatus() {
        return status;
    }

    /**
     * @return The associated delegate, possibly {@code null} if resolution is not yet done or failed.
     */
    public Converter<?, ?> getDelegate() {
        return delegate;
    }

    @Override
    public Object apply(Object value) {
        resolve(FailureReaction.FAIL);
        return delegate.applyRaw(value);
    }

    @Override
    public Class<Object> getSourceClass() {
        resolve(FailureReaction.FAIL);
        return delegate.getSourceClass();
    }

    @Override
    public Class<Object> getTargetClass() {
        resolve(FailureReaction.FAIL);
        return delegate.getTargetClass();
    }

    @Override
    public Args getParams() {
        return Args.NO_ARGS;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        builder.append(getClass().getSimpleName());
        builder.append(" '");
        builder.append(getName());
        builder.append("' ");
        builder.append(getResolutionStatus());
        if (getResolutionStatus() == ResolutionStatus.SUCCESS) {
            builder.append(" ");
            builder.append(getDelegate());
        }
        builder.append(']');
        return builder.toString();
    }
}