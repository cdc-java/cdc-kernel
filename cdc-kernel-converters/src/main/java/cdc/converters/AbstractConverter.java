package cdc.converters;

import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Base implementation of typed converter.
 *
 * @author Damien Carbonne
 *
 * @param <S> Source type.
 * @param <T> Target type.
 */
public abstract class AbstractConverter<S, T> implements Converter<S, T> {
    private final Class<S> sourceClass;
    private final Class<T> targetClass;

    public AbstractConverter(Class<S> sourceClass,
                             Class<T> targetClass) {
        Checks.isNotNull(sourceClass, "sourceClass");
        Checks.isNotNull(targetClass, "targetClass");
        this.sourceClass = sourceClass;
        this.targetClass = targetClass;
    }

    @Override
    public final Class<S> getSourceClass() {
        return sourceClass;
    }

    @Override
    public final Class<S> getWrappedSourceClass() {
        return Introspection.wrap(getSourceClass());
    }

    @Override
    public final Class<T> getTargetClass() {
        return targetClass;
    }

    @Override
    public final Class<T> getWrappedTargetClass() {
        return Introspection.wrap(getTargetClass());
    }

    @Override
    public String toString() {
        return "[" + getClass().getSimpleName()
                + " " + getSourceClass().getSimpleName() + "->" + getTargetClass().getSimpleName()
                + getParams() + "]";
    }
}