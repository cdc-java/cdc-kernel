package cdc.converters;

/**
 * Enumeration of special converter kinds.
 *
 * @author Damien Carbonne
 *
 */
public enum SpecialConverterKind {
    /**
     * The converter has been declared as a default converter for a (source class, target class) pair.
     */
    DEFAULT,

    /**
     * The converter is either a default converter or single for a (source class, target class) pair.
     */
    DEFAULT_OR_SINGLE
}