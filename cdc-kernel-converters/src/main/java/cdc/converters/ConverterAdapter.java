package cdc.converters;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Args;
import cdc.util.debug.Debug;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Class used to create a typed converter from a raw converter.
 *
 * @author Damien Carbonne
 *
 * @param <S> The source type.
 * @param <T> The target type.
 */
public final class ConverterAdapter<S, T> extends AbstractConverter<S, T> {
    private static final Logger LOGGER = LogManager.getLogger(ConverterAdapter.class);

    private final Converter<?, ?> delegate;

    public ConverterAdapter(Class<S> sourceClass,
                            Class<T> targetClass,
                            Converter<?, ?> delegate) {
        super(sourceClass, targetClass);
        LOGGER.info("<init>({}, {}, {})", sourceClass.getCanonicalName(), targetClass.getCanonicalName(), delegate);
        Debug.printStackTrace(LOGGER, Level.DEBUG);
        Checks.isNotNull(delegate, "delegate");
        this.delegate = delegate;
        if (!delegate.isCompliantSourceClass(sourceClass)) {
            throw new IllegalArgumentException(sourceClass.getCanonicalName() + " is not compliant with delegate source class of "
                    + delegate);
        }

        // If target classes are not related, cast will always fail.
        // Otherwise, it may fail
        if (Introspection.mostGeneralizedWithWrap(targetClass, delegate.getTargetClass()) == null) {
            throw new IllegalArgumentException(targetClass.getCanonicalName() + " and target class of delegate " + delegate
                    + " are not related");
        }
    }

    @Override
    public T apply(S source) {
        return getTargetClass().cast(delegate.applyRaw(source));
    }

    @Override
    public Args getParams() {
        return Args.NO_ARGS;
    }

    public Converter<?, ?> getDelegate() {
        return delegate;
    }

    @Override
    public String toString() {
        return "[" + getClass().getSimpleName()
                + " " + getSourceClass().getSimpleName() + "->" + getTargetClass().getSimpleName()
                + " " + getDelegate() + "]";
    }
}