package cdc.converters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.args.Args;

/**
 * Converter that returns the result of the first successful converter.
 *
 * @author Damien Carbonne
 *
 * @param <S> The source/input type.
 * @param <T> The target/result type.
 */
public class OrElseConverter<S, T> implements Converter<S, T> {
    private final Converter<S, T> first;
    private final List<Converter<? super S, ? extends T>> all = new ArrayList<>();

    @SafeVarargs
    public OrElseConverter(Converter<S, T> first,
                           Converter<? super S, ? extends T>... others) {
        this.first = first;
        this.all.add(first);
        Collections.addAll(this.all, others);
    }

    @Override
    public T apply(S arg) {
        for (final Converter<? super S, ? extends T> converter : all) {
            try {
                return converter.apply(arg);
            } catch (final RuntimeException e) {
                // Ignore
            }
        }
        throw new ConversionException("Can not convert '" + arg + "' using any of available converters");
    }

    @Override
    public Class<S> getSourceClass() {
        return first.getSourceClass();
    }

    @Override
    public Class<T> getTargetClass() {
        return first.getTargetClass();
    }

    @Override
    public Args getParams() {
        return Args.NO_ARGS;
    }

    public int getSize() {
        return all.size();
    }

    public Converter<? super S, ? extends T> getConverter(int index) {
        return all.get(index);
    }

    public List<Converter<? super S, ? extends T>> getConverters() {
        return all;
    }

    @Override
    public String toString() {
        return "[" + getClass().getSimpleName() + " " + all + "]";
    }
}