package cdc.converters;

import cdc.args.Args;

/**
 * Base implementation of converters that can be configured.
 *
 * @author Damien Carbonne
 *
 * @param <S> The source type.
 * @param <T> The target type.
 */
public abstract class AbstractNoArgsConverter<S, T> extends AbstractConverter<S, T> {
    public AbstractNoArgsConverter(Class<S> sourceClass,
                                   Class<T> targetClass) {
        super(sourceClass, targetClass);
    }

    @Override
    public final Args getParams() {
        return Args.NO_ARGS;
    }
}