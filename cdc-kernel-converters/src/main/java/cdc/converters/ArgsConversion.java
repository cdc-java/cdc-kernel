package cdc.converters;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Arg;
import cdc.args.Args;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.util.lang.Checks;

/**
 * Utilities to convert Arguments.
 * <p>
 * This can be useful when an Argument value is defined as a String, and the expected value has another type.
 *
 * @author Damien Carbonne
 */
public final class ArgsConversion {
    private static final Logger LOGGER = LogManager.getLogger(ArgsConversion.class);

    private ArgsConversion() {
    }

    /**
     * Converts Arguments to expected types using default converters.
     * <p>
     * Result may miss some mandatory formal args.
     *
     * @param args The arguments.
     * @param fargs The description of expected argument types.
     *            One matching (name) argument must exist for each argument of {@code args}.
     * @return The converted arguments.
     * @throws ConversionException When {@code fargs} does not contain matching arguments,
     *             or no converter was found
     *             or conversion error was raised.
     */
    public static Args convert(Args args,
                               FormalArgs fargs) {
        LOGGER.debug("convert({}, {})", args, fargs);
        Checks.isNotNull(args, "args");
        Checks.isNotNull(fargs, "fargs");

        final Args.Builder builder = Args.builder();
        for (final Arg arg : args.getArgs()) {
            final FormalArg<?> farg = fargs.getArg(arg.getName());
            if (farg == null) {
                throw new ConversionException("No formal argument was found for " + arg + " in " + fargs);
            } else {
                final Object value = Converters.convertRaw(arg.getValue(), farg.getType());
                builder.arg(arg.getName(), value);
            }
        }
        return builder.build();
    }

    /**
     * Converts Arguments to the first strictly compliant solution.
     *
     * @param args The arguments to convert.
     * @param fargsList A list of formal arguments tuples. The result should match one of them.
     * @return The conversion of {@code args} to Arguments that are compliant with one of {@code fargsList}.
     * @throws ConversionException When conversion is impossible.
     */
    public static Args convert(Args args,
                               List<FormalArgs> fargsList) {
        LOGGER.debug("convert({}, {})", args, fargsList);
        Checks.isNotNull(args, "args");
        Checks.isNotNullOrEmpty(fargsList, "fargs");
        for (final FormalArgs f : fargsList) {
            try {
                final Args result = convert(args, f);
                if (result.isStrictlyCompliantWith(f)) {
                    return result;
                } else {
                    LOGGER.debug("No strict compliance of: {} with: {}", result, f);
                }
            } catch (final ConversionException e) {
                // Ignore
                LOGGER.debug("Converting: {} with: {} failed, {}", args, f, e.getMessage());
            }
        }
        throw new ConversionException("Can not convert " + args + " to one of " + fargsList);
    }

    /**
     * Converts Arguments using default string converters.
     *
     * @param args The arguments to convert.
     * @return The conversion of {@code args} to an equivalent Arguments composed of strings.
     * @throws ConversionException When conversion is impossible.
     */
    public static Args convertToStringValues(Args args) {
        LOGGER.debug("convertToStrings({})", args);
        Checks.isNotNull(args, "args");
        final Args.Builder builder = Args.builder();
        for (final Arg arg : args.getArgs()) {
            final String value = Converters.convert(arg.getValue(), String.class);
            builder.arg(arg.getName(), value);
        }
        return builder.build();
    }
}