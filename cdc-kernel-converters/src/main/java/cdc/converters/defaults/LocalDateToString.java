package cdc.converters.defaults;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.Factory;

public final class LocalDateToString extends AbstractTemporalToString<LocalDate> {
    /**
     * 'yyyyMMdd'
     */
    public static final LocalDateToString BASIC_ISO_DATE = create(DateTimeFormatter.BASIC_ISO_DATE);

    /**
     * 'yyyy-MM-dd' or 'yyyy-MM-dd+HH:mm'
     */
    public static final LocalDateToString ISO_DATE = create(DateTimeFormatter.ISO_DATE);

    /**
     * 'yyyy-MM-dd'
     */
    public static final LocalDateToString ISO_LOCAL_DATE = create(DateTimeFormatter.ISO_LOCAL_DATE);

    /**
     * 'yyyy-MM-dd'
     */
    public static final LocalDateToString DASH_YYYY_MM_DD = create("yyyy-MM-dd", null);

    /**
     * 'yyyy/MM/dd'
     */
    public static final LocalDateToString SLASH_YYYY_MM_DD = create("yyyy/MM/dd", null);

    /**
     * 'dd-MM-yyyy'
     */
    public static final LocalDateToString DASH_DD_MM_YYYY = create("dd-MM-yyyy", null);

    /**
     * 'dd/MM/yyyy'
     */
    public static final LocalDateToString SLASH_DD_MM_YYYY = create("dd/MM/yyyy", null);

    public static final Factory<LocalDateToString> FACTORY = factory(LocalDateToString.class,
                                                                     LocalDate.class,
                                                                     LocalDateToString::create,
                                                                     LocalDateToString::create);

    private LocalDateToString(DateTimeFormatter formatter,
                              String pattern,
                              Locale locale) {
        super(LocalDate.class,
              formatter,
              pattern,
              locale);
    }

    public static LocalDateToString create(DateTimeFormatter formatter) {
        return new LocalDateToString(formatter, null, null);
    }

    public static LocalDateToString create(String pattern,
                                           Locale locale) {
        final DateTimeFormatter formatter = locale == null ? DateTimeFormatter.ofPattern(pattern) : DateTimeFormatter.ofPattern(pattern, locale);
        return new LocalDateToString(formatter, pattern, locale);
    }
}