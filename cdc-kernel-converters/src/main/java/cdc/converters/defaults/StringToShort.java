package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class StringToShort extends AbstractStringToNumber<Short> {
    public static final StringToShort INSTANCE = new StringToShort(null, null);

    public static final Factory<StringToShort> FACTORY = factory(StringToShort.class,
                                                                 Short.class,
                                                                 StringToShort::create,
                                                                 NumberConversionSupport.DEFAULT_INTEGER_PATTERN);

    public static StringToShort create(String pattern,
                                       Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToShort(pattern, locale);
        }
    }

    private StringToShort(String pattern,
                          Locale locale) {
        super(Short.class,
              pattern,
              locale,
              Number::shortValue);
    }
}