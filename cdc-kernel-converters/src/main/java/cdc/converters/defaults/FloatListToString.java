package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class FloatListToString extends ListToString<Float> {
    public static final Factory<FloatListToString> FACTORY = factory(FloatListToString.class,
                                                                     Float.class,
                                                                     FloatListToString::create);

    public FloatListToString(String prefix,
                             String separator,
                             String suffix,
                             Converter<? super Float, String> converter) {
        super(Float.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static FloatListToString create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<? super Float, String> converter) {
        return new FloatListToString(prefix, separator, suffix, converter);
    }
}