package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToFloatList extends StringToList<Float> {
    public static final Factory<StringToFloatList> FACTORY = factory(StringToFloatList.class,
                                                                     Float.class,
                                                                     StringToFloatList::create);

    public StringToFloatList(String prefix,
                             String separator,
                             String suffix,
                             Converter<String, ? extends Float> converter,
                             boolean trim) {
        super(Float.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToFloatList create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<String, ? extends Float> converter,
                                           boolean trim) {
        return new StringToFloatList(prefix, separator, suffix, converter, trim);
    }
}