package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;

public final class StringToLocale extends AbstractNoArgsConverter<String, Locale> {
    public static final StringToLocale INSTANCE = new StringToLocale();
    public static final Factory<StringToLocale> FACTORY = Converter.singleton(INSTANCE);

    private StringToLocale() {
        super(String.class,
              Locale.class);
    }

    @Override
    public Locale apply(String source) {
        return source == null || source.isEmpty()
                ? null
                : Locale.forLanguageTag(source);
    }
}