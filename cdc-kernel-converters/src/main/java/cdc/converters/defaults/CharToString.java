package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;

public final class CharToString extends AbstractNoArgsConverter<Character, String> {
    public static final CharToString INSTANCE = new CharToString();
    public static final Factory<CharToString> FACTORY = Converter.singleton(INSTANCE);

    private CharToString() {
        super(Character.class,
              String.class);
    }

    @Override
    public String apply(Character source) {
        return source == null ? null : source.toString();
    }
}