package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class ByteArrayToString extends ArrayToString<Byte> {
    public static final Factory<ByteArrayToString> FACTORY = factory(ByteArrayToString.class,
                                                                     Byte.class,
                                                                     ByteArrayToString::create);

    public ByteArrayToString(String prefix,
                             String separator,
                             String suffix,
                             Converter<? super Byte, String> converter) {
        super(Byte.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static ByteArrayToString create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<? super Byte, String> converter) {
        return new ByteArrayToString(prefix, separator, suffix, converter);
    }
}