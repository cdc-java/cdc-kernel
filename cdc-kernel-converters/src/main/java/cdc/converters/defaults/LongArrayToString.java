package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class LongArrayToString extends ArrayToString<Long> {
    public static final Factory<LongArrayToString> FACTORY = factory(LongArrayToString.class,
                                                                     Long.class,
                                                                     LongArrayToString::create);

    public LongArrayToString(String prefix,
                             String separator,
                             String suffix,
                             Converter<? super Long, String> converter) {
        super(Long.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static LongArrayToString create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<? super Long, String> converter) {
        return new LongArrayToString(prefix, separator, suffix, converter);
    }
}