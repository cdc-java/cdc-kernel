package cdc.converters.defaults;

import java.util.List;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.Converter;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Converter from a string to a list of elements.
 *
 * @author Damien Carbonne
 *
 * @param <T> The element type.
 */
public class StringToList<T> extends AbstractStringToSequence<List<T>> {
    private final Converter<String, ? extends T> converter;
    private final Class<T> elementClass;

    public static final FormalArg<Converter<String, ?>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PREFIX,
                           SEPARATOR,
                           SUFFIX,
                           TRIM,
                           CONVERTER);

    public static final Factory<StringToList<?>> FACTORY =
            new AbstractFactory<StringToList<?>>(Introspection.uncheckedCast(StringToList.class),
                                                 Args.builder()
                                                     .arg(SOURCE_CLASS, String.class)
                                                     .arg(TARGET_CLASS, Object[].class)
                                                     .build(),
                                                 FPARAMS) {
                @Override
                protected StringToList<?> create(Args args,
                                                 FormalArgs fargs) {
                    final String prefix = args.getValue(PREFIX, null);
                    final String separator = args.getValue(SEPARATOR, null);
                    final String suffix = args.getValue(SUFFIX, null);
                    final boolean trim = args.getValue(TRIM, false);
                    final Converter<String, Object> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                    final Class<Object> elementClass = converter.getWrappedTargetClass();
                    return StringToList.create(elementClass, prefix, separator, suffix, converter, trim);
                }
            };

    @FunctionalInterface
    protected static interface Creator<C, T> {
        public C create(String prefix,
                        String separator,
                        String suffix,
                        Converter<String, ? extends T> converter,
                        boolean trim);
    }

    protected static <C extends StringToList<T>, T> Factory<C> factory(Class<C> converterClass,
                                                                       Class<T> elementClass,
                                                                       Creator<C, T> creator) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, String.class)
                                          .arg(TARGET_CLASS, Introspection.getArrayClass(elementClass))
                                          .build(),
                                      FPARAMS) {

            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                final String prefix = args.getValue(PREFIX, null);
                final String separator = args.getValue(SEPARATOR, null);
                final String suffix = args.getValue(SUFFIX, null);
                final Converter<String, ? extends T> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                final boolean trim = args.getValue(TRIM, false);
                Checks.isTrue(converter.isCompliantTargetClass(elementClass), "Non compliant converter");
                return creator.create(prefix, separator, suffix, converter, trim);
            }
        };
    }

    public StringToList(Class<T> elementClass,
                        String prefix,
                        String separator,
                        String suffix,
                        Converter<String, ? extends T> converter,
                        boolean trim) {
        super(Introspection.uncheckedCast(List.class),
              prefix,
              separator,
              suffix,
              trim);
        Checks.isNotNull(converter, "converter");
        this.converter = converter;
        this.elementClass = elementClass;
    }

    public static <T> StringToList<T> create(Class<T> elementClass,
                                             String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<String, ? extends T> converter,
                                             boolean trim) {
        return new StringToList<>(elementClass, prefix, separator, suffix, converter, trim);
    }

    public final Converter<String, ? extends T> getConverter() {
        return converter;
    }

    public final Class<T> getElementClass() {
        return elementClass;
    }

    @Override
    public List<T> apply(String s) {
        return toList(s, converter);
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PREFIX, getPrefix())
                   .arg(SEPARATOR, getSeparator())
                   .arg(SUFFIX, getSuffix())
                   .arg(TRIM, getTrim())
                   .arg(CONVERTER, getConverter())
                   .build();
    }
}