package cdc.converters.defaults;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;

public final class DateConversionSupport {
    public static final FormalArg<DateFormat> FORMAT =
            new FormalArg<>("format", DateFormat.class, Necessity.MANDATORY);
    public static final FormalArgs FPARAMS_FORMAT =
            new FormalArgs(FORMAT);

    public static final FormalArg<DateTimeFormatter> FORMATTER =
            new FormalArg<>("formatter", DateTimeFormatter.class, Necessity.MANDATORY);
    public static final FormalArgs FPARAMS_FORMATTER =
            new FormalArgs(FORMAT);

    public static final FormalArg<String> PATTERN =
            new FormalArg<>("pattern", String.class, Necessity.MANDATORY);
    public static final FormalArg<Locale> LOCALE =
            new FormalArg<>("locale", Locale.class, Necessity.OPTIONAL);
    public static final FormalArgs FPARAMS_PATTERN_LOCALE =
            new FormalArgs(PATTERN,
                           LOCALE);

    private DateConversionSupport() {
    }
}