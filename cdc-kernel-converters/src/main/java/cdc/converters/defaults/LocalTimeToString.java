package cdc.converters.defaults;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.Factory;

public final class LocalTimeToString extends AbstractTemporalToString<LocalTime> {
    /**
     * 'HH:mm:ss.nnnnnnnnn'
     */
    public static final LocalTimeToString ISO_LOCAL_TIME = create(DateTimeFormatter.ISO_LOCAL_TIME);

    /**
     * 'HH:mm:ss'
     */
    public static final LocalTimeToString ISO_TIME = create(DateTimeFormatter.ISO_TIME);

    /**
     * 'HH:mm:ss'
     */
    public static final LocalTimeToString HH_MM_SS = create("HH:mm:ss", null);

    public static final Factory<LocalTimeToString> FACTORY = factory(LocalTimeToString.class,
                                                                     LocalTime.class,
                                                                     LocalTimeToString::create,
                                                                     LocalTimeToString::create);

    private LocalTimeToString(DateTimeFormatter formatter,
                              String pattern,
                              Locale locale) {
        super(LocalTime.class,
              formatter,
              pattern,
              locale);
    }

    public static LocalTimeToString create(DateTimeFormatter formatter) {
        return new LocalTimeToString(formatter, null, null);
    }

    public static LocalTimeToString create(String pattern,
                                           Locale locale) {
        final DateTimeFormatter formatter = locale == null ? DateTimeFormatter.ofPattern(pattern) : DateTimeFormatter.ofPattern(pattern, locale);
        return new LocalTimeToString(formatter, pattern, locale);
    }
}