package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class ShortToString extends AbstractNumberToString<Short> {
    public static final ShortToString INSTANCE = new ShortToString(null, null);

    public static final Factory<ShortToString> FACTORY = factory(ShortToString.class,
                                                                 Short.class,
                                                                 ShortToString::create);

    public static ShortToString create(String pattern,
                                       Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new ShortToString(pattern, locale);
        }
    }

    private ShortToString(String pattern,
                          Locale locale) {
        super(Short.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_INTEGER_FORMAT);
    }
}