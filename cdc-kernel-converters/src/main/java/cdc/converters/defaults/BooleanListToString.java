package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class BooleanListToString extends ListToString<Boolean> {
    public static final Factory<BooleanListToString> FACTORY = factory(BooleanListToString.class,
                                                                       Boolean.class,
                                                                       BooleanListToString::create);

    public BooleanListToString(String prefix,
                               String separator,
                               String suffix,
                               Converter<? super Boolean, String> converter) {
        super(Boolean.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static BooleanListToString create(String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<? super Boolean, String> converter) {
        return new BooleanListToString(prefix, separator, suffix, converter);
    }
}