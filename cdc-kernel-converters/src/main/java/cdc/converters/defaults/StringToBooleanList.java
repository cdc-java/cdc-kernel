package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToBooleanList extends StringToList<Boolean> {
    public static final Factory<StringToBooleanList> FACTORY = factory(StringToBooleanList.class,
                                                                       Boolean.class,
                                                                       StringToBooleanList::create);

    public StringToBooleanList(String prefix,
                               String separator,
                               String suffix,
                               Converter<String, ? extends Boolean> converter,
                               boolean trim) {
        super(Boolean.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToBooleanList create(String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<String, ? extends Boolean> converter,
                                             boolean trim) {
        return new StringToBooleanList(prefix, separator, suffix, converter, trim);
    }
}