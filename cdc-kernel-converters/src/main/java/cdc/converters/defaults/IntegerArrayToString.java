package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class IntegerArrayToString extends ArrayToString<Integer> {
    public static final Factory<IntegerArrayToString> FACTORY = factory(IntegerArrayToString.class,
                                                                        Integer.class,
                                                                        IntegerArrayToString::create);

    public IntegerArrayToString(String prefix,
                                String separator,
                                String suffix,
                                Converter<? super Integer, String> converter) {
        super(Integer.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static IntegerArrayToString create(String prefix,
                                              String separator,
                                              String suffix,
                                              Converter<? super Integer, String> converter) {
        return new IntegerArrayToString(prefix, separator, suffix, converter);
    }
}