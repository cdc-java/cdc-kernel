package cdc.converters.defaults;

import java.math.BigInteger;
import java.util.Locale;

import cdc.args.Factory;

public final class StringToBigInteger extends AbstractStringToNumber<BigInteger> {
    public static final StringToBigInteger INSTANCE = new StringToBigInteger(null, null);

    public static final Factory<StringToBigInteger> FACTORY = factory(StringToBigInteger.class,
                                                                      BigInteger.class,
                                                                      StringToBigInteger::create,
                                                                      NumberConversionSupport.DEFAULT_INTEGER_PATTERN);

    public static StringToBigInteger create(String pattern,
                                            Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToBigInteger(pattern, locale);
        }
    }

    private static BigInteger toBigInteger(Number number) {
        if (number == null) {
            return null;
        } else if (number instanceof BigInteger) {
            return (BigInteger) number;
        } else {
            return new BigInteger(number.toString());
        }
    }

    private StringToBigInteger(String pattern,
                               Locale locale) {
        super(BigInteger.class,
              pattern,
              locale,
              StringToBigInteger::toBigInteger);
    }
}