package cdc.converters.defaults;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Function;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.AbstractConverter;
import cdc.util.lang.Checks;

public abstract class AbstractTemporalToString<S extends TemporalAccessor> extends AbstractConverter<S, String> {
    private final DateTimeFormatter formatter;
    private final String pattern;
    private final Locale locale;

    public static final FormalArg<DateTimeFormatter> FORMATTER = DateConversionSupport.FORMATTER;
    public static final FormalArgs FPARAMS1 = DateConversionSupport.FPARAMS_FORMATTER;

    public static final FormalArg<String> PATTERN = DateConversionSupport.PATTERN;
    public static final FormalArg<Locale> LOCALE = DateConversionSupport.LOCALE;
    public static final FormalArgs FPARAMS2 = DateConversionSupport.FPARAMS_PATTERN_LOCALE;

    protected static <X extends AbstractTemporalToString<S>, S extends TemporalAccessor> Factory<X> factory(Class<X> converterClass,
                                                                                                            Class<S> sourceClass,
                                                                                                            Function<DateTimeFormatter, X> builder1,
                                                                                                            BiFunction<String, Locale, X> builder2) {
        return new AbstractFactory<X>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, sourceClass)
                                          .arg(TARGET_CLASS, String.class)
                                          .build(),
                                      FPARAMS2,
                                      FPARAMS1) {
            @Override
            protected X create(Args args,
                               FormalArgs fargs) {
                if (fargs == FPARAMS1) {
                    final DateTimeFormatter vformatter = args.getValue(FORMATTER);
                    return builder1.apply(vformatter);
                } else {
                    final String vpattern = args.getValue(PATTERN);
                    final Locale vlocale = args.getValue(LOCALE, null);
                    return builder2.apply(vpattern, vlocale);
                }
            }
        };
    }

    protected AbstractTemporalToString(Class<S> sourceClass,
                                       DateTimeFormatter formatter,
                                       String pattern,
                                       Locale locale) {
        super(sourceClass,
              String.class);
        Checks.isNotNull(formatter, "formatter");
        this.formatter = formatter;
        this.pattern = pattern;
        this.locale = locale;
    }

    public final DateTimeFormatter getFormatter() {
        return formatter;
    }

    public final String getPattern() {
        return pattern;
    }

    public final Locale getLocale() {
        return locale;
    }

    @Override
    public final String apply(S source) {
        return source == null ? null : formatter.format(source);
    }

    @Override
    public final Args getParams() {
        if (pattern == null) {
            return Args.builder(FPARAMS1)
                       .arg(FORMATTER, getFormatter())
                       .build();
        } else {
            return Args.builder(FPARAMS2)
                       .arg(PATTERN, getPattern())
                       .arg(LOCALE, getLocale())
                       .build();
        }
    }
}