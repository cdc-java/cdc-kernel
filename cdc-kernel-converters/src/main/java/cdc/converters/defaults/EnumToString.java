package cdc.converters.defaults;

import cdc.args.Args;
import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.util.lang.Introspection;
import cdc.util.strings.StringConversion;

/**
 * Class that can convert any enum type to a string.
 * <p>
 * It is possible to specialize this class for a specific enum type.<br>
 * This is not necessary as this class can convert any enum to a string.
 *
 * @author Damien Carbonne
 * @param <E> The enum type.
 *
 */
public class EnumToString<E extends Enum<?>> extends AbstractNoArgsConverter<E, String> {
    public static final EnumToString<Enum<?>> INSTANCE = new EnumToString<>(Introspection.uncheckedCast(Enum.class));
    public static final Factory<EnumToString<Enum<?>>> FACTORY = Factory.singleton(INSTANCE,
                                                                                   Args.builder()
                                                                                       .arg(SOURCE_CLASS, Enum.class)
                                                                                       .arg(TARGET_CLASS, String.class)
                                                                                       .build());

    protected EnumToString(Class<E> targetClass) {
        super(targetClass,
              String.class);
    }

    @Override
    public String apply(E source) {
        return StringConversion.asString(source);
    }
}