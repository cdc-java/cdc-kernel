package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class FloatToString extends AbstractNumberToString<Float> {
    public static final FloatToString INSTANCE = new FloatToString(null, null);

    public static final Factory<FloatToString> FACTORY = factory(FloatToString.class,
                                                                 Float.class,
                                                                 FloatToString::create);

    public static FloatToString create(String pattern,
                                       Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new FloatToString(pattern, locale);
        }
    }

    private FloatToString(String pattern,
                          Locale locale) {
        super(Float.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_REAL_FORMAT);
    }
}