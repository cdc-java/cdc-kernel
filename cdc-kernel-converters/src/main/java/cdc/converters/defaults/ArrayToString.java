package cdc.converters.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.Converter;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Converter from an array of elements to a string.
 * <p>
 * It has the following form:<br>
 * {@code (prefix) element separator element separator ... element (suffix).}
 * where {@code prefix} and {@code suffix} are optional.
 *
 * @author Damien Carbonne
 *
 * @param <S> The element type.
 */
public class ArrayToString<S> extends AbstractSequenceToString<S[]> {
    private final Converter<? super S, String> converter;
    private final Class<S> elementClass;

    public static final FormalArg<Converter<?, String>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PREFIX,
                           SEPARATOR,
                           SUFFIX,
                           CONVERTER);

    public static final Factory<ArrayToString<?>> FACTORY =
            new AbstractFactory<ArrayToString<?>>(Introspection.uncheckedCast(ArrayToString.class),
                                                  Args.builder()
                                                      .arg(SOURCE_CLASS, Object[].class)
                                                      .arg(TARGET_CLASS, String.class)
                                                      .build(),
                                                  FPARAMS) {
                @Override
                protected ArrayToString<?> create(Args args,
                                                  FormalArgs fargs) {
                    final String prefix = args.getValue(PREFIX, null);
                    final String separator = args.getValue(SEPARATOR, null);
                    final String suffix = args.getValue(SUFFIX, null);
                    final Converter<Object, String> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                    final Class<Object> elementClass = converter.getWrappedSourceClass();
                    return ArrayToString.create(elementClass, prefix, separator, suffix, converter);
                }
            };

    @FunctionalInterface
    protected static interface Creator<C, S> {
        public C create(String prefix,
                        String separator,
                        String suffix,
                        Converter<? super S, String> converter);
    }

    protected static <C extends ArrayToString<S>, S> Factory<C> factory(Class<C> converterClass,
                                                                        Class<S> elementClass,
                                                                        Creator<C, S> creator) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, Introspection.getArrayClass(elementClass))
                                          .arg(TARGET_CLASS, String.class)
                                          .build(),
                                      FPARAMS) {
            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                final String prefix = args.getValue(PREFIX, null);
                final String separator = args.getValue(SEPARATOR, null);
                final String suffix = args.getValue(SUFFIX, null);
                final Converter<? super S, String> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                Checks.isTrue(converter.isCompliantSourceClass(elementClass), "Non compliant converter");
                return creator.create(prefix, separator, suffix, converter);
            }
        };
    }

    public ArrayToString(Class<S> elementClass,
                         String prefix,
                         String separator,
                         String suffix,
                         Converter<? super S, String> converter) {
        super(Introspection.getArrayClass(elementClass),
              prefix,
              separator,
              suffix);
        Checks.isNotNull(converter, "converter");
        this.converter = converter;
        this.elementClass = elementClass;
    }

    public static <S> ArrayToString<S> create(Class<S> elementClass,
                                              String prefix,
                                              String separator,
                                              String suffix,
                                              Converter<? super S, String> converter) {
        return new ArrayToString<>(elementClass, prefix, separator, suffix, converter);
    }

    public final Converter<? super S, String> getConverter() {
        return converter;
    }

    public final Class<S> getElementClass() {
        return elementClass;
    }

    @Override
    public String apply(S[] array) {
        return toString(converter, array);
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PREFIX, getPrefix())
                   .arg(SEPARATOR, getSeparator())
                   .arg(SUFFIX, getSuffix())
                   .arg(CONVERTER, getConverter())
                   .build();
    }
}