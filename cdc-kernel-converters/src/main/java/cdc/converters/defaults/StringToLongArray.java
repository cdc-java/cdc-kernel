package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToLongArray extends StringToArray<Long> {
    public static final Factory<StringToLongArray> FACTORY = factory(StringToLongArray.class,
                                                                     Long.class,
                                                                     StringToLongArray::create);

    public StringToLongArray(String prefix,
                             String separator,
                             String suffix,
                             Converter<String, ? extends Long> converter,
                             boolean trim) {
        super(Long.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToLongArray create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<String, ? extends Long> converter,
                                           boolean trim) {
        return new StringToLongArray(prefix, separator, suffix, converter, trim);
    }
}