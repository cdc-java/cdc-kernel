package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class LongToString extends AbstractNumberToString<Long> {
    public static final LongToString INSTANCE = new LongToString(null, null);

    public static final Factory<LongToString> FACTORY = factory(LongToString.class,
                                                                Long.class,
                                                                LongToString::create);

    public static LongToString create(String pattern,
                                      Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new LongToString(pattern, locale);
        }
    }

    private LongToString(String pattern,
                         Locale locale) {
        super(Long.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_INTEGER_FORMAT);
    }
}