package cdc.converters.defaults;

import java.io.File;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;

public final class FileToString extends AbstractNoArgsConverter<File, String> {
    public static final FileToString INSTANCE = new FileToString();
    public static final Factory<FileToString> FACTORY = Converter.singleton(INSTANCE);

    private FileToString() {
        super(File.class,
              String.class);
    }

    @Override
    public String apply(File source) {
        // TODO '\' handling ?
        return source == null ? null : source.getPath();
    }
}