package cdc.converters.defaults;

import java.math.BigDecimal;
import java.util.Locale;

import cdc.args.Factory;

public final class BigDecimalToString extends AbstractNumberToString<BigDecimal> {
    public static final BigDecimalToString INSTANCE = new BigDecimalToString(null, null);

    public static final Factory<BigDecimalToString> FACTORY = factory(BigDecimalToString.class,
                                                                      BigDecimal.class,
                                                                      BigDecimalToString::create);

    public static BigDecimalToString create(String pattern,
                                            Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new BigDecimalToString(pattern, locale);
        }
    }

    private BigDecimalToString(String pattern,
                               Locale locale) {
        super(BigDecimal.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_REAL_FORMAT);
    }
}