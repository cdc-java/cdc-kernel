package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;
import cdc.util.lang.Introspection;

public final class StringToClass extends AbstractNoArgsConverter<String, Class<?>> {
    public static final StringToClass INSTANCE = new StringToClass();
    public static final Factory<StringToClass> FACTORY = Converter.singleton(INSTANCE);

    private StringToClass() {
        super(String.class,
              Introspection.uncheckedCast(Class.class));
    }

    @Override
    public Class<?> apply(String source) {
        return Introspection.getClass(source);
    }
}