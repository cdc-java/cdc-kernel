package cdc.converters.defaults;

import java.util.List;
import java.util.function.Function;

public abstract class AbstractSequenceToString<S> extends AbstractSequenceConverter<S, String> {
    public AbstractSequenceToString(Class<S> sourceClass,
                                    String prefix,
                                    String separator,
                                    String suffix) {
        super(sourceClass, String.class, prefix, separator, suffix);
    }

    @SuppressWarnings("unchecked")
    protected <V> String toString(Function<? super V, String> converter,
                                  V... array) {
        if (array == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(getPrefix());
            boolean first = true;
            for (final V x : array) {
                if (!first) {
                    builder.append(getSeparator());
                }
                builder.append(converter.apply(x));
                first = false;
            }
            builder.append(getSuffix());

            return builder.toString();
        }
    }

    protected <V> String toString(Function<? super V, String> converter,
                                  List<V> list) {
        if (list == null) {
            return null;
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append(getPrefix());
            boolean first = true;
            for (final V x : list) {
                if (!first) {
                    builder.append(getSeparator());
                }
                builder.append(converter.apply(x));
                first = false;
            }
            builder.append(getSuffix());

            return builder.toString();
        }
    }
}