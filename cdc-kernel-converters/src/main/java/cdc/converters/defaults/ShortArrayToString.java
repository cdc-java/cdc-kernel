package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class ShortArrayToString extends ArrayToString<Short> {
    public static final Factory<ShortArrayToString> FACTORY = factory(ShortArrayToString.class,
                                                                      Short.class,
                                                                      ShortArrayToString::create);

    public ShortArrayToString(String prefix,
                              String separator,
                              String suffix,
                              Converter<? super Short, String> converter) {
        super(Short.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static ShortArrayToString create(String prefix,
                                            String separator,
                                            String suffix,
                                            Converter<? super Short, String> converter) {
        return new ShortArrayToString(prefix, separator, suffix, converter);
    }
}