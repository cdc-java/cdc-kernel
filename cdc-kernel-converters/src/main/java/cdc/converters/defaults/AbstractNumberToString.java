package cdc.converters.defaults;

import java.text.NumberFormat;
import java.util.Locale;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.AbstractConverter;

public abstract class AbstractNumberToString<S extends Number> extends AbstractConverter<S, String> {
    public static final FormalArg<String> PATTERN = NumberConversionSupport.PATTERN;
    public static final FormalArg<Locale> LOCALE = NumberConversionSupport.LOCALE;

    public static final FormalArgs FPARAMS =
            new FormalArgs(PATTERN,
                           LOCALE);

    private final String pattern;
    private final Locale locale;
    private final String defaultFormat;

    @FunctionalInterface
    protected static interface Creator<C> {
        public C create(String pattern,
                        Locale locale);
    }

    protected static <C extends AbstractNumberToString<S>, S extends Number> Factory<C> factory(Class<C> converterClass,
                                                                                                Class<S> numberClass,
                                                                                                Creator<C> creator) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, numberClass)
                                          .arg(TARGET_CLASS, String.class)
                                          .build(),
                                      FPARAMS) {
            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                final String pattern = args.getValue(PATTERN);
                final Locale locale = args.getValue(LOCALE);
                return creator.create(pattern, locale);
            }
        };
    }

    protected AbstractNumberToString(Class<S> sourceClass,
                                     String pattern,
                                     Locale locale,
                                     String defaultFormat) {
        super(sourceClass,
              String.class);
        this.pattern = pattern;
        this.locale = locale;
        this.defaultFormat = defaultFormat;
    }

    @Override
    public final String apply(S source) {
        if (source == null) {
            return null;
        } else {
            if (pattern == null) {
                if (locale == null) {
                    return source.toString();
                } else {
                    return String.format(locale, defaultFormat, source);
                }
            } else {
                final NumberFormat format = NumberConversionSupport.getFormat(pattern, locale);
                return format.format(source);
            }
        }
    }

    @Override
    public Args getParams() {
        return Args.builder(FPARAMS)
                   .arg(PATTERN, getPattern())
                   .arg(LOCALE, getLocale())
                   .build();
    }

    public final String getPattern() {
        return pattern;
    }

    public final Locale getLocale() {
        return locale;
    }

    public final String getDefaultFormat() {
        return defaultFormat;
    }
}