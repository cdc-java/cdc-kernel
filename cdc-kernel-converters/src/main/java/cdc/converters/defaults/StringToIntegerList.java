package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToIntegerList extends StringToList<Integer> {
    public static final Factory<StringToIntegerList> FACTORY = factory(StringToIntegerList.class,
                                                                       Integer.class,
                                                                       StringToIntegerList::create);

    public StringToIntegerList(String prefix,
                               String separator,
                               String suffix,
                               Converter<String, ? extends Integer> converter,
                               boolean trim) {
        super(Integer.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToIntegerList create(String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<String, ? extends Integer> converter,
                                             boolean trim) {
        return new StringToIntegerList(prefix, separator, suffix, converter, trim);
    }
}