package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class StringToLong extends AbstractStringToNumber<Long> {
    public static final StringToLong INSTANCE = new StringToLong(null, null);

    public static final Factory<StringToLong> FACTORY = factory(StringToLong.class,
                                                                Long.class,
                                                                StringToLong::create,
                                                                NumberConversionSupport.DEFAULT_INTEGER_PATTERN);

    public static StringToLong create(String pattern,
                                      Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToLong(pattern, locale);
        }
    }

    private StringToLong(String pattern,
                         Locale locale) {
        super(Long.class,
              pattern,
              locale,
              Number::longValue);
    }
}