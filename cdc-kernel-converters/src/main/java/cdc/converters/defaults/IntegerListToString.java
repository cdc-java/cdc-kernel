package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class IntegerListToString extends ListToString<Integer> {
    public static final Factory<IntegerListToString> FACTORY = factory(IntegerListToString.class,
                                                                       Integer.class,
                                                                       IntegerListToString::create);

    public IntegerListToString(String prefix,
                               String separator,
                               String suffix,
                               Converter<? super Integer, String> converter) {
        super(Integer.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static IntegerListToString create(String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<? super Integer, String> converter) {
        return new IntegerListToString(prefix, separator, suffix, converter);
    }
}