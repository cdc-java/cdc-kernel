package cdc.converters.defaults;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import cdc.args.FormalArg;
import cdc.args.Necessity;

/**
 * Base class used to convert a string to a sequence.
 * <p>
 * A sequence is converted to: {@code (prefix) item separator item separator ... (suffix)}<br>
 * If trim is set, then each item string representation is trimmed before being converted to item.<br>
 * prefix and suffix are optional.<br>
 * separator must be mandatory for this class to be useful.
 *
 * @author Damien Carbonne
 *
 * @param <T> The sequence type.
 */
public abstract class AbstractStringToSequence<T> extends AbstractSequenceConverter<String, T> {
    private final boolean trim;

    public static final FormalArg<Boolean> TRIM = new FormalArg<>("trim", Boolean.class, Necessity.OPTIONAL);

    public AbstractStringToSequence(Class<T> targetClass,
                                    String prefix,
                                    String separator,
                                    String suffix,
                                    boolean trim) {
        super(String.class, targetClass, prefix, separator, suffix);
        this.trim = trim;
    }

    public final boolean getTrim() {
        return trim;
    }

    /**
     * Converts the string to a list of items.
     * <p>
     * This works when separator is mandatory.
     *
     * @param <V> The item type.
     * @param s The string.
     * @param converter The converter from string to {@code V}.
     * @return A list of items after interpreting {@code s} with prefix, suffix, separator and trim.
     */
    protected <V> List<V> toList(String s,
                                 Function<String, ? extends V> converter) {
        final int start = getPrefix().length();
        final int end = getSuffix().length();
        // remove prefix and suffix
        final String content = s.substring(start, s.length() - end);

        final List<V> list = new ArrayList<>();
        int from = 0;
        while (from < content.length()) {
            final int to = content.indexOf(getSeparator(), from);
            final String item;
            if (to < 0) {
                item = content.substring(from);
                from = content.length();
            } else {
                item = content.substring(from, to);
                from = to + getSeparator().length();
            }
            if (getTrim()) {
                list.add(converter.apply(item.trim()));
            } else {
                list.add(converter.apply(item));
            }
        }
        return list;
    }
}