package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToFloatArray extends StringToArray<Float> {
    public static final Factory<StringToFloatArray> FACTORY = factory(StringToFloatArray.class,
                                                                      Float.class,
                                                                      StringToFloatArray::create);

    public StringToFloatArray(String prefix,
                              String separator,
                              String suffix,
                              Converter<String, ? extends Float> converter,
                              boolean trim) {
        super(Float.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToFloatArray create(String prefix,
                                            String separator,
                                            String suffix,
                                            Converter<String, ? extends Float> converter,
                                            boolean trim) {
        return new StringToFloatArray(prefix, separator, suffix, converter, trim);
    }
}