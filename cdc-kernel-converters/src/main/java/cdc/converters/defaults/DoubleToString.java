package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class DoubleToString extends AbstractNumberToString<Double> {
    public static final DoubleToString INSTANCE = new DoubleToString(null, null);

    public static final Factory<DoubleToString> FACTORY = factory(DoubleToString.class,
                                                                  Double.class,
                                                                  DoubleToString::create);

    public static DoubleToString create(String pattern,
                                        Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new DoubleToString(pattern, locale);
        }
    }

    private DoubleToString(String pattern,
                           Locale locale) {
        super(Double.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_REAL_FORMAT);
    }
}