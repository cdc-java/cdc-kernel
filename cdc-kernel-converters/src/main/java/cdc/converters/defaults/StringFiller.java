package cdc.converters.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;
import cdc.util.strings.StringUtils;

/**
 * Converter that takes a string and replaces all characters by a fixed letter.
 * <p>
 * Spaces can be preserved.
 *
 * @author Damien Carbonne
 */
public final class StringFiller extends AbstractConverter<String, String> {
    private final char fillChar;
    private final boolean preserveSpaces;
    public static final FormalArg<Character> FILL_CHAR =
            new FormalArg<>("fillChar", char.class, Necessity.MANDATORY);
    public static final FormalArg<Boolean> PRESERVE_SPACES =
            new FormalArg<>("preserveSpaces", Boolean.class, Necessity.OPTIONAL);

    public static final FormalArgs FPARAMS =
            new FormalArgs(FILL_CHAR,
                           PRESERVE_SPACES);

    public static final Factory<StringFiller> FACTORY =
            new AbstractFactory<StringFiller>(StringFiller.class,
                                              Args.builder()
                                                  .arg(SOURCE_CLASS, String.class)
                                                  .arg(TARGET_CLASS, String.class)
                                                  .build(),
                                              FPARAMS) {
                @Override
                protected StringFiller create(Args args,
                                              FormalArgs fargs) {
                    return StringFiller.create(args.getValue(FILL_CHAR),
                                               args.getValue(PRESERVE_SPACES, false));
                }
            };

    private StringFiller(char fillChar,
                         boolean preserveSpaces) {
        super(String.class,
              String.class);
        this.fillChar = fillChar;
        this.preserveSpaces = preserveSpaces;
    }

    public static StringFiller create(char fillChar,
                                      boolean preserveSpaces) {
        return new StringFiller(fillChar, preserveSpaces);
    }

    public char getFillChar() {
        return fillChar;
    }

    public boolean getPreserveSpaces() {
        return preserveSpaces;
    }

    @Override
    public String apply(String source) {
        if (source == null) {
            return null;
        } else {
            return StringUtils.fillWithCharacter(source, preserveSpaces, fillChar);
        }
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(FILL_CHAR, getFillChar())
                   .arg(PRESERVE_SPACES, getPreserveSpaces())
                   .build();
    }
}