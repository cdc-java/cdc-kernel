package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;
import cdc.util.strings.StringConversion;

public final class BooleanToString extends AbstractNoArgsConverter<Boolean, String> {
    public static final BooleanToString INSTANCE = new BooleanToString();
    public static final Factory<BooleanToString> FACTORY = Converter.singleton(INSTANCE);

    private BooleanToString() {
        super(Boolean.class,
              String.class);
    }

    @Override
    public String apply(Boolean source) {
        return StringConversion.asString(source);
    }
}