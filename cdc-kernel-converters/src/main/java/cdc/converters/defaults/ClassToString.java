package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;
import cdc.util.lang.Introspection;

public final class ClassToString extends AbstractNoArgsConverter<Class<?>, String> {
    public static final ClassToString INSTANCE = new ClassToString();
    public static final Factory<ClassToString> FACTORY = Converter.singleton(INSTANCE);

    private ClassToString() {
        super(Introspection.uncheckedCast(Class.class),
              String.class);
    }

    @Override
    public String apply(Class<?> source) {
        return source == null ? null : source.getCanonicalName();
    }
}