package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class ByteListToString extends ListToString<Byte> {
    public static final Factory<ByteListToString> FACTORY = factory(ByteListToString.class,
                                                                    Byte.class,
                                                                    ByteListToString::create);

    public ByteListToString(String prefix,
                            String separator,
                            String suffix,
                            Converter<? super Byte, String> converter) {
        super(Byte.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static ByteListToString create(String prefix,
                                          String separator,
                                          String suffix,
                                          Converter<? super Byte, String> converter) {
        return new ByteListToString(prefix, separator, suffix, converter);
    }
}