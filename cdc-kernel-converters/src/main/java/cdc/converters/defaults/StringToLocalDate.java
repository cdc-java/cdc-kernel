package cdc.converters.defaults;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.Factory;

public final class StringToLocalDate extends AbstractStringToTemporal<LocalDate> {
    private static final Default[] DEFAULTS = {
            new Default("yyyy-MM-dd"),
            new Default("yyyy/MM/dd"),
            new Default("dd-MM-yyyy"),
            new Default("dd/MM/yyyy")
    };

    public static final StringToLocalDate AUTO = new StringToLocalDate();

    /**
     * 'yyyyMMdd'
     */
    public static final StringToLocalDate BASIC_ISO_DATE = create(DateTimeFormatter.BASIC_ISO_DATE);

    /**
     * 'yyyy-MM-dd' or 'yyyy-MM-dd+HH:mm'
     */
    public static final StringToLocalDate ISO_DATE = create(DateTimeFormatter.ISO_DATE);

    /**
     * 'yyyy-MM-dd'
     */
    public static final StringToLocalDate ISO_LOCAL_DATE = create(DateTimeFormatter.ISO_LOCAL_DATE);

    /**
     * 'yyyy-MM-dd'
     */
    public static final StringToLocalDate DASH_YYYY_MM_DD = create("yyyy-MM-dd", null);

    /**
     * 'yyyy/MM/dd'
     */
    public static final StringToLocalDate SLASH_YYYY_MM_DD = create("yyyy/MM/dd", null);

    /**
     * 'dd-MM-yyyy'
     */
    public static final StringToLocalDate DASH_DD_MM_YYYY = create("dd-MM-yyyy", null);

    /**
     * 'dd/MM/yyyy'
     */
    public static final StringToLocalDate SLASH_DD_MM_YYYY = create("dd/MM/yyyy", null);

    public static final Factory<StringToLocalDate> FACTORY = factory(StringToLocalDate.class,
                                                                     LocalDate.class,
                                                                     null, // TODO
                                                                     StringToLocalDate::create,
                                                                     StringToLocalDate::create);

    private StringToLocalDate() {
        super(LocalDate.class);
    }

    private StringToLocalDate(DateTimeFormatter formatter,
                              String pattern,
                              Locale locale) {
        super(LocalDate.class,
              formatter,
              pattern,
              locale);
    }

    public static StringToLocalDate create(DateTimeFormatter formatter) {
        return new StringToLocalDate(formatter, null, null);
    }

    public static StringToLocalDate create(String pattern,
                                           Locale locale) {
        final DateTimeFormatter formatter = locale == null
                ? DateTimeFormatter.ofPattern(pattern)
                : DateTimeFormatter.ofPattern(pattern, locale);
        return new StringToLocalDate(formatter, pattern, locale);
    }

    @Override
    public final LocalDate apply(String source) {
        return apply(source, LocalDate::parse, DEFAULTS);
    }
}