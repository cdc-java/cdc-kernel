package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;

public final class Identity extends AbstractNoArgsConverter<Object, Object> {
    public static final Identity INSTANCE = new Identity();
    public static final Factory<Identity> FACTORY = Converter.singleton(INSTANCE);

    private Identity() {
        super(Object.class,
              Object.class);
    }

    @Override
    public Object apply(Object source) {
        return source;
    }
}