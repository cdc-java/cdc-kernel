package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;

public final class LocaleToString extends AbstractNoArgsConverter<Locale, String> {
    public static final LocaleToString INSTANCE = new LocaleToString();
    public static final Factory<LocaleToString> FACTORY = Converter.singleton(INSTANCE);

    private LocaleToString() {
        super(Locale.class,
              String.class);
    }

    @Override
    public String apply(Locale source) {
        return source == null ? null : source.toLanguageTag();
    }
}