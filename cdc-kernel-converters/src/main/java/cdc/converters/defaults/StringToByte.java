package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class StringToByte extends AbstractStringToNumber<Byte> {
    public static final StringToByte INSTANCE = new StringToByte(null, null);

    public static final Factory<StringToByte> FACTORY = factory(StringToByte.class,
                                                                Byte.class,
                                                                StringToByte::create,
                                                                NumberConversionSupport.DEFAULT_INTEGER_PATTERN);

    public static StringToByte create(String pattern,
                                      Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToByte(pattern, locale);
        }
    }

    private StringToByte(String pattern,
                         Locale locale) {
        super(Byte.class,
              pattern,
              locale,
              Number::byteValue);
    }
}