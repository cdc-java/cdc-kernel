package cdc.converters.defaults;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.Factory;

public final class LocalDateTimeToString extends AbstractTemporalToString<LocalDateTime> {
    /**
     * 'yyyy-MM-ddTHH:mm:ss'
     */
    public static final LocalDateTimeToString ISO_DATE_TIME = create(DateTimeFormatter.ISO_DATE_TIME);

    public static final Factory<LocalDateTimeToString> FACTORY = factory(LocalDateTimeToString.class,
                                                                         LocalDateTime.class,
                                                                         LocalDateTimeToString::create,
                                                                         LocalDateTimeToString::create);

    private LocalDateTimeToString(DateTimeFormatter formatter,
                                  String pattern,
                                  Locale locale) {
        super(LocalDateTime.class,
              formatter,
              pattern,
              locale);
    }

    public static LocalDateTimeToString create(DateTimeFormatter formatter) {
        return new LocalDateTimeToString(formatter, null, null);
    }

    public static LocalDateTimeToString create(String pattern,
                                               Locale locale) {
        final DateTimeFormatter formatter = locale == null ? DateTimeFormatter.ofPattern(pattern) : DateTimeFormatter.ofPattern(pattern, locale);
        return new LocalDateTimeToString(formatter, pattern, locale);
    }
}