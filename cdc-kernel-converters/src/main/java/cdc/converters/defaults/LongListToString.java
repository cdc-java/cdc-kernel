package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class LongListToString extends ListToString<Long> {
    public static final Factory<LongListToString> FACTORY = factory(LongListToString.class,
                                                                    Long.class,
                                                                    LongListToString::create);

    public LongListToString(String prefix,
                            String separator,
                            String suffix,
                            Converter<? super Long, String> converter) {
        super(Long.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static LongListToString create(String prefix,
                                          String separator,
                                          String suffix,
                                          Converter<? super Long, String> converter) {
        return new LongListToString(prefix, separator, suffix, converter);
    }
}