package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToIntegerArray extends StringToArray<Integer> {
    public static final Factory<StringToIntegerArray> FACTORY = factory(StringToIntegerArray.class,
                                                                        Integer.class,
                                                                        StringToIntegerArray::create);

    public StringToIntegerArray(String prefix,
                                String separator,
                                String suffix,
                                Converter<String, ? extends Integer> converter,
                                boolean trim) {
        super(Integer.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToIntegerArray create(String prefix,
                                              String separator,
                                              String suffix,
                                              Converter<String, ? extends Integer> converter,
                                              boolean trim) {
        return new StringToIntegerArray(prefix, separator, suffix, converter, trim);
    }
}