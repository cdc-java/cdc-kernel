package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class StringToFloat extends AbstractStringToNumber<Float> {
    public static final StringToFloat INSTANCE = new StringToFloat(null, null);

    public static final Factory<StringToFloat> FACTORY = factory(StringToFloat.class,
                                                                 Float.class,
                                                                 StringToFloat::create,
                                                                 NumberConversionSupport.DEFAULT_REAL_PATTERN);

    public static StringToFloat create(String pattern,
                                       Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToFloat(pattern, locale);
        }
    }

    private StringToFloat(String pattern,
                          Locale locale) {
        super(Float.class,
              pattern,
              locale,
              Number::floatValue);
    }
}