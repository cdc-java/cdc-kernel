package cdc.converters.defaults;

import java.math.BigDecimal;
import java.util.Locale;

import cdc.args.Factory;

public final class StringToBigDecimal extends AbstractStringToNumber<BigDecimal> {
    public static final StringToBigDecimal INSTANCE = new StringToBigDecimal(null, null);

    public static final Factory<StringToBigDecimal> FACTORY = factory(StringToBigDecimal.class,
                                                                      BigDecimal.class,
                                                                      StringToBigDecimal::create,
                                                                      NumberConversionSupport.DEFAULT_REAL_PATTERN);

    public static StringToBigDecimal create(String pattern,
                                            Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToBigDecimal(pattern, locale);
        }
    }

    private static BigDecimal toBigDecimal(Number number) {
        if (number == null) {
            return null;
        } else if (number instanceof BigDecimal) {
            return (BigDecimal) number;
        } else {
            return new BigDecimal(number.toString());
        }
    }

    private StringToBigDecimal(String pattern,
                               Locale locale) {
        super(BigDecimal.class,
              pattern,
              locale,
              StringToBigDecimal::toBigDecimal);
    }
}