package cdc.converters.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;

/**
 * Converter from object to String using Object.toString() or String.format() method.
 *
 * @author Damien Carbonne
 *
 */
public final class ObjectToString extends AbstractConverter<Object, String> {
    public static final ObjectToString INSTANCE = new ObjectToString(null);

    private final String format;
    public static final FormalArg<String> FORMAT =
            new FormalArg<>("format", String.class, Necessity.OPTIONAL);

    public static final FormalArgs FPARAMS =
            new FormalArgs(FORMAT);

    public static final Factory<ObjectToString> FACTORY =
            new AbstractFactory<ObjectToString>(ObjectToString.class,
                                                Args.builder()
                                                    .arg(SOURCE_CLASS, Object.class)
                                                    .arg(TARGET_CLASS, String.class)
                                                    .build(),
                                                FPARAMS) {
                @Override
                protected ObjectToString create(Args args,
                                                FormalArgs fargs) {
                    return ObjectToString.create(args.getValue(FORMAT, null));
                }
            };

    private ObjectToString(String format) {
        super(Object.class,
              String.class);
        this.format = format;
    }

    public static ObjectToString create(String format) {
        return new ObjectToString(format);
    }

    public String getFormat() {
        return format;
    }

    @Override
    public String apply(Object source) {
        if (format == null) {
            return source == null ? null : source.toString();
        } else {
            return String.format(format, source);
        }
    }

    @Override
    public Args getParams() {
        return Args.builder(FPARAMS)
                   .arg(FORMAT, getFormat())
                   .build();
    }
}