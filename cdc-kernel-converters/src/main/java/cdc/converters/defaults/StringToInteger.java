package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class StringToInteger extends AbstractStringToNumber<Integer> {
    public static final StringToInteger INSTANCE = new StringToInteger(null, null);

    public static final Factory<StringToInteger> FACTORY = factory(StringToInteger.class,
                                                                   Integer.class,
                                                                   StringToInteger::create,
                                                                   NumberConversionSupport.DEFAULT_INTEGER_PATTERN);

    public static StringToInteger create(String pattern,
                                         Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToInteger(pattern, locale);
        }
    }

    private StringToInteger(String pattern,
                            Locale locale) {
        super(Integer.class,
              pattern,
              locale,
              Number::intValue);
    }
}