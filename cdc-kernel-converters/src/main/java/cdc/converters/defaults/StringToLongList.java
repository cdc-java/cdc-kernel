package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToLongList extends StringToList<Long> {
    public static final Factory<StringToLongList> FACTORY = factory(StringToLongList.class,
                                                                    Long.class,
                                                                    StringToLongList::create);

    public StringToLongList(String prefix,
                            String separator,
                            String suffix,
                            Converter<String, ? extends Long> converter,
                            boolean trim) {
        super(Long.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToLongList create(String prefix,
                                          String separator,
                                          String suffix,
                                          Converter<String, ? extends Long> converter,
                                          boolean trim) {
        return new StringToLongList(prefix, separator, suffix, converter, trim);
    }
}