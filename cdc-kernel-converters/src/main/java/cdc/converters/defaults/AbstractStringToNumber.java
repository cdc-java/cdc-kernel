package cdc.converters.defaults;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.function.Function;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.AbstractConverter;
import cdc.converters.ConversionException;

public class AbstractStringToNumber<T extends Number> extends AbstractConverter<String, T> {
    public static final FormalArg<String> PATTERN = NumberConversionSupport.PATTERN;
    public static final FormalArg<Locale> LOCALE = NumberConversionSupport.LOCALE;

    public static final FormalArgs FPARAMS =
            new FormalArgs(PATTERN,
                           LOCALE);

    private final String pattern;
    private final Locale locale;
    private final NumberFormat format;
    private final Function<Number, T> extractor;

    @FunctionalInterface
    protected static interface Creator<C> {
        public C create(String pattern,
                        Locale locale);
    }

    protected static <C extends AbstractStringToNumber<T>, T extends Number> Factory<C> factory(Class<C> converterClass,
                                                                                                Class<T> numberClass,
                                                                                                Creator<C> creator,
                                                                                                String defaultPattern) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, String.class)
                                          .arg(TARGET_CLASS, numberClass)
                                          .build(),
                                      FPARAMS) {
            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                final String pattern = args.getValue(PATTERN, defaultPattern);
                final Locale locale = args.getValue(LOCALE);
                return creator.create(pattern, locale);
            }
        };
    }

    protected AbstractStringToNumber(Class<T> targetClass,
                                     String pattern,
                                     Locale locale,
                                     Function<Number, T> extractor) {
        super(String.class,
              targetClass);
        this.pattern = pattern;
        this.locale = locale;
        this.extractor = extractor;
        this.format = NumberConversionSupport.getFormat(pattern, locale);
    }

    @Override
    public final T apply(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        } else {
            try {
                final Number number = format.parse(source);
                return extractor.apply(number);
            } catch (final ParseException e) {
                throw new ConversionException("Parse error: Can not convert '" + source + "' to "
                        + getTargetClass().getSimpleName(), e);
            }
        }
    }

    @Override
    public Args getParams() {
        return Args.builder(FPARAMS)
                   .arg(PATTERN, getPattern())
                   .arg(LOCALE, getLocale())
                   .build();
    }

    public final String getPattern() {
        return pattern;
    }

    public final Locale getLocale() {
        return locale;
    }
}