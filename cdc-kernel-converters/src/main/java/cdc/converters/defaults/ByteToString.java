package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class ByteToString extends AbstractNumberToString<Byte> {
    public static final ByteToString INSTANCE = new ByteToString(null, null);

    public static final Factory<ByteToString> FACTORY = factory(ByteToString.class,
                                                                Byte.class,
                                                                ByteToString::create);

    public static ByteToString create(String pattern,
                                      Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new ByteToString(pattern, locale);
        }
    }

    private ByteToString(String pattern,
                         Locale locale) {
        super(Byte.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_INTEGER_FORMAT);
    }
}