package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class FloatArrayToString extends ArrayToString<Float> {
    public static final Factory<FloatArrayToString> FACTORY = factory(FloatArrayToString.class,
                                                                      Float.class,
                                                                      FloatArrayToString::create);

    public FloatArrayToString(String prefix,
                              String separator,
                              String suffix,
                              Converter<? super Float, String> converter) {
        super(Float.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static FloatArrayToString create(String prefix,
                                            String separator,
                                            String suffix,
                                            Converter<? super Float, String> converter) {
        return new FloatArrayToString(prefix, separator, suffix, converter);
    }
}