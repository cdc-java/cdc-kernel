package cdc.converters.defaults;

import java.math.BigInteger;
import java.util.Locale;

import cdc.args.Factory;

public final class BigIntegerToString extends AbstractNumberToString<BigInteger> {
    public static final BigIntegerToString INSTANCE = new BigIntegerToString(null, null);

    public static final Factory<BigIntegerToString> FACTORY = factory(BigIntegerToString.class,
                                                                      BigInteger.class,
                                                                      BigIntegerToString::create);

    public static BigIntegerToString create(String pattern,
                                            Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new BigIntegerToString(pattern, locale);
        }
    }

    private BigIntegerToString(String pattern,
                               Locale locale) {
        super(BigInteger.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_INTEGER_FORMAT);
    }
}