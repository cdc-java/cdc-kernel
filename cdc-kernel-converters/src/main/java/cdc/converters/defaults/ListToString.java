package cdc.converters.defaults;

import java.util.List;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.Converter;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Converter from a list of elements to a string.
 * <p>
 * It has the following form:<br>
 * {@code (prefix) element separator element separator ... element (suffix).}
 * where {@code prefix} and {@code suffix} are optional.
 *
 * @author Damien Carbonne
 *
 * @param <S> The element type.
 */
public class ListToString<S> extends AbstractSequenceToString<List<S>> {
    private final Converter<? super S, String> converter;
    private final Class<S> elementClass;

    public static final FormalArg<Converter<?, String>> CONVERTER =
            new FormalArg<>("converter", Introspection.uncheckedCast(Converter.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PREFIX,
                           SEPARATOR,
                           SUFFIX,
                           CONVERTER);

    public static final Factory<ListToString<?>> FACTORY =
            new AbstractFactory<ListToString<?>>(Introspection.uncheckedCast(ListToString.class),
                                                 Args.builder()
                                                     .arg(SOURCE_CLASS, Object[].class)
                                                     .arg(TARGET_CLASS, String.class)
                                                     .build(),
                                                 FPARAMS) {
                @Override
                protected ListToString<?> create(Args args,
                                                 FormalArgs fargs) {
                    final String prefix = args.getValue(PREFIX, null);
                    final String separator = args.getValue(SEPARATOR, null);
                    final String suffix = args.getValue(SUFFIX, null);
                    final Converter<Object, String> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                    final Class<Object> elementClass = converter.getWrappedSourceClass();
                    return ListToString.create(elementClass, prefix, separator, suffix, converter);
                }
            };

    @FunctionalInterface
    protected static interface Creator<C, S> {
        public C create(String prefix,
                        String separator,
                        String suffix,
                        Converter<? super S, String> converter);
    }

    protected static <C extends ListToString<S>, S> Factory<C> factory(Class<C> converterClass,
                                                                       Class<S> elementClass,
                                                                       Creator<C, S> creator) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, Introspection.getArrayClass(elementClass))
                                          .arg(TARGET_CLASS, String.class)
                                          .build(),
                                      FPARAMS) {
            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                final String prefix = args.getValue(PREFIX, null);
                final String separator = args.getValue(SEPARATOR, null);
                final String suffix = args.getValue(SUFFIX, null);
                final Converter<? super S, String> converter = Introspection.uncheckedCast(args.getValue(CONVERTER));
                Checks.isTrue(converter.isCompliantSourceClass(elementClass), "Non compliant converter");
                return creator.create(prefix, separator, suffix, converter);
            }
        };
    }

    public ListToString(Class<S> elementClass,
                        String prefix,
                        String separator,
                        String suffix,
                        Converter<? super S, String> converter) {
        super(Introspection.uncheckedCast(List.class),
              prefix,
              separator,
              suffix);
        Checks.isNotNull(converter, "converter");
        this.converter = converter;
        this.elementClass = elementClass;
    }

    public static <S> ListToString<S> create(Class<S> elementClass,
                                             String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<? super S, String> converter) {
        return new ListToString<>(elementClass, prefix, separator, suffix, converter);
    }

    public final Converter<? super S, String> getConverter() {
        return converter;
    }

    public final Class<S> getElementClass() {
        return elementClass;
    }

    @Override
    public String apply(List<S> list) {
        return toString(converter, list);
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PREFIX, getPrefix())
                   .arg(SEPARATOR, getSeparator())
                   .arg(SUFFIX, getSuffix())
                   .arg(CONVERTER, getConverter())
                   .build();
    }
}