package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.ConversionException;
import cdc.converters.Converter;

public final class StringToChar extends AbstractNoArgsConverter<String, Character> {
    public static final StringToChar INSTANCE = new StringToChar();
    public static final Factory<StringToChar> FACTORY = Converter.singleton(INSTANCE);

    private StringToChar() {
        super(String.class,
              Character.class);
    }

    @Override
    public Character apply(String source) {
        if (source == null) {
            return null;
        } else if (source.length() == 1) {
            return source.charAt(0);
        } else {
            throw new ConversionException("Can not convert '" + source + "' to char");
        }
    }
}