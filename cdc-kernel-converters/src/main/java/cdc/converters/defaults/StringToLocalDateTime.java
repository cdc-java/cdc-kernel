package cdc.converters.defaults;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.Factory;

public final class StringToLocalDateTime extends AbstractStringToTemporal<LocalDateTime> {
    private static final Default[] DEFAULTS = {
            new Default("yyyy-MM-dd HH:mm:ss.SSS"),
            new Default("yyyy/MM/dd HH:mm:ss.SSS"),
            new Default("dd-MM-yyyy HH:mm:ss.SSS"),
            new Default("dd/MM/yyyy HH:mm:ss.SSS"),
            new Default("yyyy-MM-dd HH:mm:ss"),
            new Default("yyyy/MM/dd HH:mm:ss"),
            new Default("dd-MM-yyyy HH:mm:ss"),
            new Default("dd/MM/yyyy HH:mm:ss"),
            new Default("yyyy-MM-dd HH:mm"),
            new Default("yyyy/MM/dd HH:mm"),
            new Default("dd-MM-yyyy HH:mm"),
            new Default("dd/MM/yyyy HH:mm")
    };

    public static final StringToLocalDateTime AUTO = new StringToLocalDateTime();

    /**
     * 'yyyy-MM-ddTHH:mm:ss'
     */
    public static final StringToLocalDateTime ISO_DATE_TIME = create(DateTimeFormatter.ISO_DATE_TIME);

    public static final Factory<StringToLocalDateTime> FACTORY = factory(StringToLocalDateTime.class,
                                                                         LocalDateTime.class,
                                                                         AUTO,
                                                                         StringToLocalDateTime::create,
                                                                         StringToLocalDateTime::create);

    private StringToLocalDateTime() {
        super(LocalDateTime.class);
    }

    private StringToLocalDateTime(DateTimeFormatter formatter,
                                  String pattern,
                                  Locale locale) {
        super(LocalDateTime.class,
              formatter,
              pattern,
              locale);
    }

    public static StringToLocalDateTime create(DateTimeFormatter formatter) {
        return new StringToLocalDateTime(formatter, null, null);
    }

    public static StringToLocalDateTime create(String pattern,
                                               Locale locale) {
        final DateTimeFormatter formatter = locale == null
                ? DateTimeFormatter.ofPattern(pattern)
                : DateTimeFormatter.ofPattern(pattern, locale);
        return new StringToLocalDateTime(formatter, pattern, locale);
    }

    @Override
    public final LocalDateTime apply(String source) {
        return apply(source, LocalDateTime::parse, DEFAULTS);
    }
}