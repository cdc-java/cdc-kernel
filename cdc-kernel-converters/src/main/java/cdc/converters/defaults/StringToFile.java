package cdc.converters.defaults;

import java.io.File;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;

public final class StringToFile extends AbstractNoArgsConverter<String, File> {
    public static final StringToFile INSTANCE = new StringToFile();
    public static final Factory<StringToFile> FACTORY = Converter.singleton(INSTANCE);

    private StringToFile() {
        super(String.class,
              File.class);
    }

    @Override
    public File apply(String source) {
        return source == null || source.isEmpty()
                ? null
                : new File(source);
    }
}