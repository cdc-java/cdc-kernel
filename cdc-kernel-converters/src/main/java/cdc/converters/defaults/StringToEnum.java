package cdc.converters.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;
import cdc.util.lang.Introspection;
import cdc.util.strings.StringConversion;

/**
 * Class that can convert a string to any enum type.
 * <p>
 * It can be specialized if necessary.
 *
 * @author Damien Carbonne
 *
 * @param <E> The enum type.
 */
public class StringToEnum<E extends Enum<E>> extends AbstractConverter<String, E> {
    public static final FormalArg<Class<? extends Enum<?>>> CLASS =
            new FormalArg<>("class", Introspection.uncheckedCast(Class.class), Necessity.MANDATORY);

    public static final FormalArgs FPARAMS =
            new FormalArgs(CLASS);

    public static final Factory<StringToEnum<?>> FACTORY =
            new AbstractFactory<StringToEnum<?>>(Introspection.uncheckedCast(StringToEnum.class),
                                                 Args.builder()
                                                     .arg(SOURCE_CLASS, String.class)
                                                     .arg(TARGET_CLASS, Enum.class)
                                                     .build(),
                                                 FPARAMS) {
                @SuppressWarnings({ "rawtypes", "unchecked" })
                @Override
                protected StringToEnum<?> create(Args args,
                                                 FormalArgs fargs) {
                    final Class<? extends Enum<?>> cls = Introspection.uncheckedCast(args.getValue(CLASS));
                    return new StringToEnum(cls);
                }
            };

    public static <E extends Enum<E>> StringToEnum<E> create(Class<E> cls) {
        return new StringToEnum<>(cls);
    }

    protected StringToEnum(Class<E> targetClass) {
        super(String.class,
              targetClass);
    }

    @Override
    public E apply(String source) {
        return StringConversion.asOptionalEnum(source, getTargetClass());
    }

    @Override
    public final Args getParams() {
        return Args.builder(FPARAMS)
                   .arg(CLASS, getTargetClass())
                   .build();
    }
}