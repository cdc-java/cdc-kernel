package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;

public final class ToUpperCase extends AbstractConverter<String, String> {
    private final Locale locale;
    public static final FormalArg<Locale> LOCALE =
            new FormalArg<>("locale", Locale.class, Necessity.OPTIONAL);

    public static final FormalArgs FPARAMS =
            new FormalArgs(LOCALE);

    public static final ToUpperCase INSTANCE = new ToUpperCase();

    public static final Factory<ToUpperCase> FACTORY =
            new AbstractFactory<ToUpperCase>(ToUpperCase.class,
                                             Args.builder()
                                                 .arg(SOURCE_CLASS, String.class)
                                                 .arg(TARGET_CLASS, String.class)
                                                 .build(),
                                             FPARAMS) {
                @Override
                protected ToUpperCase create(Args args,
                                             FormalArgs fargs) {
                    return ToUpperCase.create(args.getValue(LOCALE, null));
                }
            };

    private ToUpperCase(Locale locale) {
        super(String.class,
              String.class);
        this.locale = locale;
    }

    private ToUpperCase() {
        this(null);
    }

    public static ToUpperCase create(Locale locale) {
        return locale == null ? INSTANCE : new ToUpperCase(locale);
    }

    public Locale getLocale() {
        return locale;
    }

    @Override
    public String apply(String source) {
        if (source == null) {
            return null;
        } else {
            return locale == null ? source.toUpperCase() : source.toUpperCase(locale);
        }
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(LOCALE, getLocale())
                   .build();
    }
}