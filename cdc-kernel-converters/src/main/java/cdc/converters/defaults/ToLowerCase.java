package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;

public final class ToLowerCase extends AbstractConverter<String, String> {
    private final Locale locale;
    public static final FormalArg<Locale> LOCALE =
            new FormalArg<>("locale", Locale.class, Necessity.OPTIONAL);

    public static final FormalArgs FPARAMS =
            new FormalArgs(LOCALE);

    public static final ToLowerCase INSTANCE = new ToLowerCase();

    public static final Factory<ToLowerCase> FACTORY =
            new AbstractFactory<ToLowerCase>(ToLowerCase.class,
                                             Args.builder()
                                                 .arg(SOURCE_CLASS, String.class)
                                                 .arg(TARGET_CLASS, String.class)
                                                 .build(),
                                             FPARAMS) {
                @Override
                protected ToLowerCase create(Args args,
                                             FormalArgs fargs) {
                    return ToLowerCase.create(args.getValue(LOCALE, null));
                }
            };

    private ToLowerCase(Locale locale) {
        super(String.class,
              String.class);
        this.locale = locale;
    }

    private ToLowerCase() {
        this(null);
    }

    public static ToLowerCase create(Locale locale) {
        return locale == null ? INSTANCE : new ToLowerCase(locale);
    }

    public Locale getLocale() {
        return locale;
    }

    @Override
    public String apply(String source) {
        if (source == null) {
            return null;
        } else {
            return locale == null ? source.toLowerCase() : source.toLowerCase(locale);
        }
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(LOCALE, getLocale())
                   .build();
    }
}