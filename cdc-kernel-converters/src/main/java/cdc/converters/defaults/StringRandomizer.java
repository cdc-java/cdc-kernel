package cdc.converters.defaults;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;
import cdc.util.strings.StringUtils;

/**
 * Converter that takes a string and replaces all characters by random letters or digits.
 * <p>
 * Spaces can be preserved.
 *
 * @author Damien Carbonne
 */
public final class StringRandomizer extends AbstractConverter<String, String> {
    private final boolean preserveSpaces;
    public static final FormalArg<Boolean> PRESERVE_SPACES =
            new FormalArg<>("preserveSpaces", Boolean.class, Necessity.OPTIONAL);

    public static final FormalArgs FPARAMS =
            new FormalArgs(PRESERVE_SPACES);

    public static final Factory<StringRandomizer> FACTORY =
            new AbstractFactory<StringRandomizer>(StringRandomizer.class,
                                                  Args.builder()
                                                      .arg(SOURCE_CLASS, String.class)
                                                      .arg(TARGET_CLASS, String.class)
                                                      .build(),
                                                  FPARAMS) {
                @Override
                protected StringRandomizer create(Args args,
                                                  FormalArgs fargs) {
                    return StringRandomizer.create(args.getValue(PRESERVE_SPACES, false));
                }
            };

    private StringRandomizer(boolean preserveSpaces) {
        super(String.class,
              String.class);
        this.preserveSpaces = preserveSpaces;
    }

    public static StringRandomizer create(boolean preserveSpaces) {
        return new StringRandomizer(preserveSpaces);
    }

    public boolean getPreserveSpaces() {
        return preserveSpaces;
    }

    @Override
    public String apply(String source) {
        if (source == null) {
            return null;
        } else {
            return StringUtils.scrambleWithLettersOrDigits(source, preserveSpaces);
        }
    }

    @Override
    public Args getParams() {
        return Args.builder()
                   .arg(PRESERVE_SPACES, getPreserveSpaces())
                   .build();
    }
}