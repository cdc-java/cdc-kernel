package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToByteArray extends StringToArray<Byte> {
    public static final Factory<StringToByteArray> FACTORY = factory(StringToByteArray.class,
                                                                     Byte.class,
                                                                     StringToByteArray::create);

    public StringToByteArray(String prefix,
                             String separator,
                             String suffix,
                             Converter<String, ? extends Byte> converter,
                             boolean trim) {
        super(Byte.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToByteArray create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<String, ? extends Byte> converter,
                                           boolean trim) {
        return new StringToByteArray(prefix, separator, suffix, converter, trim);
    }
}