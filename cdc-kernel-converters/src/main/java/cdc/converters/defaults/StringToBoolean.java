package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;
import cdc.util.strings.StringConversion;

public final class StringToBoolean extends AbstractNoArgsConverter<String, Boolean> {
    public static final StringToBoolean INSTANCE = new StringToBoolean();
    public static final Factory<StringToBoolean> FACTORY = Converter.singleton(INSTANCE);

    private StringToBoolean() {
        super(String.class,
              Boolean.class);
    }

    @Override
    public Boolean apply(String source) {
        return StringConversion.asOptionalBoolean(source);
    }
}