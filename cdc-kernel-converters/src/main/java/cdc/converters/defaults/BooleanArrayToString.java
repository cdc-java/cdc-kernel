package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class BooleanArrayToString extends ArrayToString<Boolean> {
    public static final Factory<BooleanArrayToString> FACTORY = factory(BooleanArrayToString.class,
                                                                        Boolean.class,
                                                                        BooleanArrayToString::create);

    public BooleanArrayToString(String prefix,
                                String separator,
                                String suffix,
                                Converter<? super Boolean, String> converter) {
        super(Boolean.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static BooleanArrayToString create(String prefix,
                                              String separator,
                                              String suffix,
                                              Converter<? super Boolean, String> converter) {
        return new BooleanArrayToString(prefix, separator, suffix, converter);
    }
}