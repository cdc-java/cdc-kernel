package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class ShortListToString extends ListToString<Short> {
    public static final Factory<ShortListToString> FACTORY = factory(ShortListToString.class,
                                                                     Short.class,
                                                                     ShortListToString::create);

    public ShortListToString(String prefix,
                             String separator,
                             String suffix,
                             Converter<? super Short, String> converter) {
        super(Short.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static ShortListToString create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<? super Short, String> converter) {
        return new ShortListToString(prefix, separator, suffix, converter);
    }
}