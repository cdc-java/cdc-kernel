package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.AbstractNoArgsConverter;
import cdc.converters.Converter;
import cdc.converters.Converters;
import cdc.util.lang.Introspection;

/**
 * Converters that returns the converter that is registered with a name.
 *
 * @author Damien Carbonne
 *
 */
public final class StringToConverter extends AbstractNoArgsConverter<String, Converter<?, ?>> {
    public static final StringToConverter INSTANCE = new StringToConverter();
    public static final Factory<StringToConverter> FACTORY = Converter.singleton(INSTANCE);

    private StringToConverter() {
        super(String.class,
              Introspection.uncheckedCast(Converter.class));
    }

    @Override
    public Converter<?, ?> apply(String source) {
        return source == null ? null : Converters.getConverter(source);
    }
}