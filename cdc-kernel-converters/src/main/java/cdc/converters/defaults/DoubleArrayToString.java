package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class DoubleArrayToString extends ArrayToString<Double> {
    public static final Factory<DoubleArrayToString> FACTORY = factory(DoubleArrayToString.class,
                                                                       Double.class,
                                                                       DoubleArrayToString::create);

    public DoubleArrayToString(String prefix,
                               String separator,
                               String suffix,
                               Converter<? super Double, String> converter) {
        super(Double.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static DoubleArrayToString create(String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<? super Double, String> converter) {
        return new DoubleArrayToString(prefix, separator, suffix, converter);
    }
}