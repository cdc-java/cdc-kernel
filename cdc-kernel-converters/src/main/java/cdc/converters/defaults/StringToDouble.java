package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class StringToDouble extends AbstractStringToNumber<Double> {
    public static final StringToDouble INSTANCE = new StringToDouble(null, null);

    public static final Factory<StringToDouble> FACTORY = factory(StringToDouble.class,
                                                                  Double.class,
                                                                  StringToDouble::create,
                                                                  NumberConversionSupport.DEFAULT_REAL_PATTERN);

    public static StringToDouble create(String pattern,
                                        Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new StringToDouble(pattern, locale);
        }
    }

    private StringToDouble(String pattern,
                           Locale locale) {
        super(Double.class,
              pattern,
              locale,
              Number::doubleValue);
    }
}