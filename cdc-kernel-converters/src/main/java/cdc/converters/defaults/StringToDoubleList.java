package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToDoubleList extends StringToList<Double> {
    public static final Factory<StringToDoubleList> FACTORY = factory(StringToDoubleList.class,
                                                                      Double.class,
                                                                      StringToDoubleList::create);

    public StringToDoubleList(String prefix,
                              String separator,
                              String suffix,
                              Converter<String, ? extends Double> converter,
                              boolean trim) {
        super(Double.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToDoubleList create(String prefix,
                                            String separator,
                                            String suffix,
                                            Converter<String, ? extends Double> converter,
                                            boolean trim) {
        return new StringToDoubleList(prefix, separator, suffix, converter, trim);
    }
}