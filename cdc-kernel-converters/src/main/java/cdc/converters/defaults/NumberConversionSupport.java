package cdc.converters.defaults;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.FormalArg;
import cdc.args.Necessity;

public final class NumberConversionSupport {
    private static final Logger LOGGER = LogManager.getLogger(NumberConversionSupport.class);

    static final String DEFAULT_REAL_FORMAT = "%f";
    static final String DEFAULT_INTEGER_FORMAT = "%d";
    static final String DEFAULT_REAL_PATTERN = "0.0";
    static final String DEFAULT_INTEGER_PATTERN = "0";

    public static final FormalArg<String> PATTERN =
            new FormalArg<>("pattern", String.class, Necessity.OPTIONAL);

    public static final FormalArg<Locale> LOCALE =
            new FormalArg<>("locale", Locale.class, Necessity.OPTIONAL);

    private NumberConversionSupport() {
    }

    public static NumberFormat getFormat(String pattern,
                                         Locale locale) {
        // TODO add a cache
        final NumberFormat format = locale == null
                ? NumberFormat.getNumberInstance(Locale.ENGLISH)
                : NumberFormat.getNumberInstance(locale);
        if (pattern != null) {
            if (format instanceof DecimalFormat) {
                ((DecimalFormat) format).applyPattern(pattern);
            } else {
                LOGGER.warn("Can not retrieve a DecimalFormat");
            }
        }
        return format;
    }
}