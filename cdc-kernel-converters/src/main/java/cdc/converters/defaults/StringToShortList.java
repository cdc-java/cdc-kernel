package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToShortList extends StringToList<Short> {
    public static final Factory<StringToShortList> FACTORY = factory(StringToShortList.class,
                                                                     Short.class,
                                                                     StringToShortList::create);

    public StringToShortList(String prefix,
                             String separator,
                             String suffix,
                             Converter<String, ? extends Short> converter,
                             boolean trim) {
        super(Short.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToShortList create(String prefix,
                                           String separator,
                                           String suffix,
                                           Converter<String, ? extends Short> converter,
                                           boolean trim) {
        return new StringToShortList(prefix, separator, suffix, converter, trim);
    }
}