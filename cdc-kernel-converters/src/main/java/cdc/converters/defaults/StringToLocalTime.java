package cdc.converters.defaults;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import cdc.args.Factory;

public final class StringToLocalTime extends AbstractStringToTemporal<LocalTime> {
    private static final Default[] DEFAULTS = {
            new Default("HH:mm:ss.SSS"),
            new Default("HH:mm:ss"),
            new Default("HH:mm")
    };

    public static final StringToLocalTime AUTO = new StringToLocalTime();

    /**
     * 'HH:mm:ss.nnnnnnnnn'
     */
    public static final StringToLocalTime ISO_LOCAL_TIME = create(DateTimeFormatter.ISO_LOCAL_TIME);

    /**
     * 'HH:mm:ss'
     */
    public static final StringToLocalTime ISO_TIME = create(DateTimeFormatter.ISO_TIME);

    /**
     * 'HH:mm:ss'
     */
    public static final StringToLocalTime HH_MM_SS = create("HH:mm:ss", null);

    public static final Factory<StringToLocalTime> FACTORY = factory(StringToLocalTime.class,
                                                                     LocalTime.class,
                                                                     null, // TODO
                                                                     StringToLocalTime::create,
                                                                     StringToLocalTime::create);

    private StringToLocalTime() {
        super(LocalTime.class);
    }

    private StringToLocalTime(DateTimeFormatter formatter,
                              String pattern,
                              Locale locale) {
        super(LocalTime.class,
              formatter,
              pattern,
              locale);
    }

    public static StringToLocalTime create(DateTimeFormatter formatter) {
        return new StringToLocalTime(formatter, null, null);
    }

    public static StringToLocalTime create(String pattern,
                                           Locale locale) {
        final DateTimeFormatter formatter = locale == null
                ? DateTimeFormatter.ofPattern(pattern)
                : DateTimeFormatter.ofPattern(pattern, locale);
        return new StringToLocalTime(formatter, pattern, locale);
    }

    @Override
    public final LocalTime apply(String source) {
        return apply(source, LocalTime::parse, DEFAULTS);
    }
}