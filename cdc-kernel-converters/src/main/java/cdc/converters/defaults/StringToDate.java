package cdc.converters.defaults;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.AbstractConverter;
import cdc.converters.ConversionException;
import cdc.util.lang.Checks;

/**
 * String to Date converter.
 * <p>
 * A String can be converted to Date using:
 * <ul>
 * <li>Nothing: different default formats are tested.
 * <li>A DateFormat.
 * <li>A pattern and an optional Locale.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class StringToDate extends AbstractConverter<String, Date> {
    private final DateFormat format;
    private final String pattern;
    private final Locale locale;

    private static final Default[] DEFAULTS = {
            new Default("yyyy-MM-dd HH:mm:ss.SSS", "[0-9]{4}[-][0-9]{2}[-][0-9]{2}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}"),
            new Default("yyyy/MM/dd HH:mm:ss.SSS", "[0-9]{4}[/][0-9]{2}[/][0-9]{2}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}"),
            new Default("dd-MM-yyyy HH:mm:ss.SSS", "[0-9]{2}[-][0-9]{2}[-][0-9]{4}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}"),
            new Default("dd/MM/yyyy HH:mm:ss.SSS", "[0-9]{2}[/][0-9]{2}[/][0-9]{4}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}"),
            new Default("yyyy-MM-dd HH:mm:ss", "[0-9]{4}[-][0-9]{2}[-][0-9]{2}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}"),
            new Default("yyyy/MM/dd HH:mm:ss", "[0-9]{4}[/][0-9]{2}[/][0-9]{2}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}"),
            new Default("dd-MM-yyyy HH:mm:ss", "[0-9]{2}[-][0-9]{2}[-][0-9]{4}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}"),
            new Default("dd/MM/yyyy HH:mm:ss", "[0-9]{2}[/][0-9]{2}[/][0-9]{4}[ ][0-9]{2}[:][0-9]{2}[:][0-9]{2}"),
            new Default("yyyy-MM-dd HH:mm", "[0-9]{4}[-][0-9]{2}[-][0-9]{2}[ ][0-9]{2}[:][0-9]{2}"),
            new Default("yyyy/MM/dd HH:mm", "[0-9]{4}[/][0-9]{2}[/][0-9]{2}[ ][0-9]{2}[:][0-9]{2}"),
            new Default("dd-MM-yyyy HH:mm", "[0-9]{2}[-][0-9]{2}[-][0-9]{4}[ ][0-9]{2}[:][0-9]{2}"),
            new Default("dd/MM/yyyy HH:mm", "[0-9]{2}[/][0-9]{2}[/][0-9]{4}[ ][0-9]{2}[:][0-9]{2}"),
            new Default("yyyy-MM-dd", "[0-9]{4}[-][0-9]{2}[-][0-9]{2}"),
            new Default("yyyy/MM/dd", "[0-9]{4}[/][0-9]{2}[/][0-9]{2}"),
            new Default("dd-MM-yyyy", "[0-9]{2}[-][0-9]{2}[-][0-9]{4}"),
            new Default("dd/MM/yyyy", "[0-9]{2}[/][0-9]{2}[/][0-9]{4}"),
            new Default("HH:mm:ss.SSS", "[0-9]{2}[:][0-9]{2}[:][0-9]{2}[.][0-9]{3}"),
            new Default("HH:mm:ss", "[0-9]{2}[:][0-9]{2}[:][0-9]{2}"),
            new Default("HH:mm", "[0-9]{2}[:][0-9]{2}")
    };

    public static final StringToDate AUTO = new StringToDate();
    public static final StringToDate DASH_YYYY_MM_DD = create("yyyy-MM-dd", null);
    public static final StringToDate SLASH_YYYY_MM_DD = create("yyyy/MM/dd", null);
    public static final StringToDate DASH_DD_MM_YYYY = create("dd-MM-yyyy", null);
    public static final StringToDate SLASH_DD_MM_YYYY = create("dd/MM/yyyy", null);
    public static final StringToDate HH_MM_SS = create("HH:mm:ss", null);
    public static final StringToDate DASH_YYYY_MM_DD_HH_MM_SS = create("yyyy-MM-dd HH:mm:ss", null);
    public static final StringToDate SLASH_YYYY_MM_DD_HH_MM_SS = create("yyyy/MM/dd HH:mm:ss", null);
    public static final StringToDate DASH_DD_MM_YYYY_HH_MM_SS = create("dd-MM-yyyy HH:mm:ss", null);
    public static final StringToDate SLASH_DD_MM_YYYY_HH_MM_SS = create("dd/MM/yyyy HH:mm:ss", null);

    public static final FormalArg<DateFormat> FORMAT = DateConversionSupport.FORMAT;
    public static final FormalArgs FPARAMS_FORMAT = DateConversionSupport.FPARAMS_FORMAT;

    public static final FormalArg<String> PATTERN = DateConversionSupport.PATTERN;
    public static final FormalArg<Locale> LOCALE = DateConversionSupport.LOCALE;
    public static final FormalArgs FPARAMS_PATTERN_LOCALE = DateConversionSupport.FPARAMS_PATTERN_LOCALE;

    public static final Factory<StringToDate> FACTORY =
            new AbstractFactory<StringToDate>(StringToDate.class,
                                              Args.builder()
                                                  .arg(SOURCE_CLASS, String.class)
                                                  .arg(TARGET_CLASS, Date.class)
                                                  .build(),
                                              FormalArgs.NO_FARGS,
                                              FPARAMS_PATTERN_LOCALE,
                                              FPARAMS_FORMAT) {
                @Override
                protected StringToDate create(Args args,
                                              FormalArgs fargs) {
                    if (fargs == FPARAMS_FORMAT) {
                        final DateFormat vformat = args.getValue(FORMAT);
                        return StringToDate.create(vformat);
                    } else if (fargs == FPARAMS_PATTERN_LOCALE) {
                        final String vpattern = args.getValue(PATTERN);
                        final Locale vlocale = args.getValue(LOCALE, null);
                        return StringToDate.create(vpattern, vlocale);
                    } else {
                        return AUTO;
                    }
                }
            };

    private StringToDate() {
        super(String.class,
              Date.class);
        this.format = null;
        this.pattern = null;
        this.locale = null;
    }

    private StringToDate(DateFormat format,
                         String pattern,
                         Locale locale) {
        super(String.class,
              Date.class);
        Checks.isNotNull(format, "format");
        this.format = format;
        this.pattern = pattern;
        this.locale = locale;
    }

    public static StringToDate create(DateFormat format) {
        return new StringToDate(format, null, null);
    }

    public static StringToDate create(String pattern,
                                      Locale locale) {
        final SimpleDateFormat format = locale == null
                ? new SimpleDateFormat(pattern)
                : new SimpleDateFormat(pattern, locale);
        format.setLenient(false);
        return new StringToDate(format, pattern, locale);
    }

    /**
     * @return The optional format.
     */
    public DateFormat getFormat() {
        return format;
    }

    /**
     * @return The optional pattern.
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * @return The optional Locale.
     */
    public Locale getLocale() {
        return locale;
    }

    private static Date convert(String source,
                                DateFormat format) {
        try {
            return format.parse(source);
        } catch (final ParseException e) {
            throw new ConversionException("Failed to parse '" + source + "' using " + format, e);
        }
    }

    @Override
    public Date apply(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        } else if (format == null) {
            for (final Default def : DEFAULTS) {
                if (def.pattern.matcher(source).find()) {
                    return convert(source, def.format);
                }
            }
            throw new ConversionException("Failed to parse '" + source + "' using default formats");
        } else {
            return convert(source, format);
        }
    }

    @Override
    public Args getParams() {
        if (format == null) {
            return Args.NO_ARGS;
        } else if (pattern == null) {
            return Args.builder(FPARAMS_FORMAT)
                       .arg(FORMAT, getFormat())
                       .build();
        } else {
            return Args.builder(FPARAMS_PATTERN_LOCALE)
                       .arg(PATTERN, getPattern())
                       .arg(LOCALE, getLocale())
                       .build();
        }
    }

    private static class Default {
        final SimpleDateFormat format;
        final Pattern pattern;

        public Default(String pattern,
                       String regex) {
            this.format = new SimpleDateFormat(pattern);
            this.pattern = Pattern.compile(regex);
        }
    }
}