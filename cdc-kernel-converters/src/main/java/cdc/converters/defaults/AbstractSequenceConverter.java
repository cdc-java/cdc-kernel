package cdc.converters.defaults;

import cdc.args.FormalArg;
import cdc.args.Necessity;
import cdc.converters.AbstractConverter;

/**
 * Base class used to convert a sequence to / from string.
 * <p>
 * A sequence is converted to: {@code (prefix) item separator item separator ... (suffix)}
 *
 * @author Damien Carbonne
 *
 * @param <S> The source type.
 * @param <T> The target type.
 */
public abstract class AbstractSequenceConverter<S, T> extends AbstractConverter<S, T> {
    private final String prefix;
    private final String separator;
    private final String suffix;

    public static final FormalArg<String> PREFIX = new FormalArg<>("prefix", String.class, Necessity.OPTIONAL);
    public static final FormalArg<String> SEPARATOR = new FormalArg<>("separator", String.class, Necessity.OPTIONAL);
    public static final FormalArg<String> SUFFIX = new FormalArg<>("suffix", String.class, Necessity.OPTIONAL);

    public AbstractSequenceConverter(Class<S> sourceClass,
                                     Class<T> targetClass,
                                     String prefix,
                                     String separator,
                                     String suffix) {
        super(sourceClass,
              targetClass);

        this.prefix = prefix == null ? "" : prefix;
        this.separator = separator == null || separator.isEmpty() ? " " : separator;
        this.suffix = suffix == null ? "" : suffix;
    }

    public final String getPrefix() {
        return prefix;
    }

    public final String getSeparator() {
        return separator;
    }

    public final String getSuffix() {
        return suffix;
    }
}