package cdc.converters.defaults;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.AbstractConverter;
import cdc.util.lang.Checks;

/**
 * Date to String converter.
 * <p>
 * A Date can be converted to String using:
 * <ul>
 * <li>A DateFormat.
 * <li>A pattern and an optional Locale.
 * </ul>
 *
 * @author Damien Carbonne
 *
 */
public final class DateToString extends AbstractConverter<Date, String> {
    private final DateFormat format;
    private final String pattern;
    private final Locale locale;

    /**
     * DateToString instance using "yyyy-MM-dd" pattern.
     */
    public static final DateToString DASH_YYYY_MM_DD = create("yyyy-MM-dd", null);

    /**
     * DateToString instance using "yyyy/MM/dd" pattern.
     */
    public static final DateToString SLASH_YYYY_MM_DD = create("yyyy/MM/dd", null);

    /**
     * DateToString instance using "dd-MM-yyyy" pattern.
     */
    public static final DateToString DASH_DD_MM_YYYY = create("dd-MM-yyyy", null);

    /**
     * DateToString instance using "dd/MM/yyyy" pattern.
     */
    public static final DateToString SLASH_DD_MM_YYYY = create("dd/MM/yyyy", null);

    /**
     * DateToString instance using "HH:mm:ss" pattern.
     */
    public static final DateToString HH_MM_SS = create("HH:mm:ss", null);

    /**
     * DateToString instance using "yyyy-MM-dd HH:mm:ss" pattern.
     */
    public static final DateToString DASH_YYYY_MM_DD_HH_MM_SS = create("yyyy-MM-dd HH:mm:ss", null);

    public static final DateToString DASH_DD_MM_YYYY_HH_MM_SS = create("dd-MM-yyyy HH:mm:ss", null);

    /**
     * DateToString instance using "yyyy/MM/dd HH:mm:ss" pattern.
     */
    public static final DateToString SLASH_YYYY_MM_DD_HH_MM_SS = create("yyyy/MM/dd HH:mm:ss", null);

    public static final DateToString SLASH_DD_MM_YYYY_HH_MM_SS = create("dd/MM/yyyy HH:mm:ss", null);

    public static final FormalArg<DateFormat> FORMAT = DateConversionSupport.FORMAT;
    public static final FormalArgs FPARAMS_FORMAT = DateConversionSupport.FPARAMS_FORMAT;

    public static final FormalArg<String> PATTERN = DateConversionSupport.PATTERN;
    public static final FormalArg<Locale> LOCALE = DateConversionSupport.LOCALE;
    public static final FormalArgs FPARAMS_PATTERN_LOCALE = DateConversionSupport.FPARAMS_PATTERN_LOCALE;

    public static final Factory<DateToString> FACTORY =
            new AbstractFactory<DateToString>(DateToString.class,
                                              Args.builder()
                                                  .arg(SOURCE_CLASS, Date.class)
                                                  .arg(TARGET_CLASS, String.class)
                                                  .build(),
                                              FPARAMS_PATTERN_LOCALE,
                                              FPARAMS_FORMAT) {
                @Override
                protected DateToString create(Args args,
                                              FormalArgs fargs) {
                    if (fargs == FPARAMS_FORMAT) {
                        final DateFormat vformat = args.getValue(FORMAT);
                        return DateToString.create(vformat);
                    } else {
                        final String vpattern = args.getValue(PATTERN);
                        final Locale vlocale = args.getValue(LOCALE, null);
                        return DateToString.create(vpattern, vlocale);
                    }
                }
            };

    private DateToString(DateFormat format,
                         String pattern,
                         Locale locale) {
        super(Date.class,
              String.class);
        Checks.isNotNull(format, "format");
        this.format = format;
        this.pattern = pattern;
        this.locale = locale;
    }

    /**
     * Creates a DateToString converter based on a format.
     *
     * @param format The format.
     * @return A new instance of DataToString that converts Dates using {@code format}.
     */
    public static DateToString create(DateFormat format) {
        return new DateToString(format, null, null);
    }

    /**
     * Creates a DateToString converter based on a pattern and locale.
     *
     * @param pattern The pattern
     * @param locale The optional locale.
     * @return A new instance of DataToString that converts Dates using {@code pattern} and {@code locale}.
     */
    public static DateToString create(String pattern,
                                      Locale locale) {
        final SimpleDateFormat format = locale == null
                ? new SimpleDateFormat(pattern)
                : new SimpleDateFormat(pattern, locale);
        format.setLenient(false);
        return new DateToString(format, pattern, locale);
    }

    /**
     * @return The format.
     */
    public DateFormat getFormat() {
        return format;
    }

    /**
     * @return The optional pattern.
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * @return The optional locale.
     */
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String apply(Date source) {
        return source == null
                ? null
                : format.format(source);
    }

    @Override
    public Args getParams() {
        if (pattern == null) {
            return Args.builder(FPARAMS_FORMAT)
                       .arg(FORMAT, getFormat())
                       .build();
        } else {
            return Args.builder(FPARAMS_PATTERN_LOCALE)
                       .arg(PATTERN, getPattern())
                       .arg(LOCALE, getLocale())
                       .build();
        }
    }
}