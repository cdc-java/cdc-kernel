package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class DoubleListToString extends ListToString<Double> {
    public static final Factory<DoubleListToString> FACTORY = factory(DoubleListToString.class,
                                                                      Double.class,
                                                                      DoubleListToString::create);

    public DoubleListToString(String prefix,
                              String separator,
                              String suffix,
                              Converter<? super Double, String> converter) {
        super(Double.class,
              prefix,
              separator,
              suffix,
              converter);
    }

    public static DoubleListToString create(String prefix,
                                            String separator,
                                            String suffix,
                                            Converter<? super Double, String> converter) {
        return new DoubleListToString(prefix, separator, suffix, converter);
    }
}