package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToBooleanArray extends StringToArray<Boolean> {
    public static final Factory<StringToBooleanArray> FACTORY = factory(StringToBooleanArray.class,
                                                                        Boolean.class,
                                                                        StringToBooleanArray::create);

    public StringToBooleanArray(String prefix,
                                String separator,
                                String suffix,
                                Converter<String, ? extends Boolean> converter,
                                boolean trim) {
        super(Boolean.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToBooleanArray create(String prefix,
                                              String separator,
                                              String suffix,
                                              Converter<String, ? extends Boolean> converter,
                                              boolean trim) {
        return new StringToBooleanArray(prefix, separator, suffix, converter, trim);
    }
}