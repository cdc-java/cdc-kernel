package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToDoubleArray extends StringToArray<Double> {
    public static final Factory<StringToDoubleArray> FACTORY = factory(StringToDoubleArray.class,
                                                                       Double.class,
                                                                       StringToDoubleArray::create);

    public StringToDoubleArray(String prefix,
                               String separator,
                               String suffix,
                               Converter<String, ? extends Double> converter,
                               boolean trim) {
        super(Double.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToDoubleArray create(String prefix,
                                             String separator,
                                             String suffix,
                                             Converter<String, ? extends Double> converter,
                                             boolean trim) {
        return new StringToDoubleArray(prefix, separator, suffix, converter, trim);
    }
}