package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToByteList extends StringToList<Byte> {
    public static final Factory<StringToByteList> FACTORY = factory(StringToByteList.class,
                                                                    Byte.class,
                                                                    StringToByteList::create);

    public StringToByteList(String prefix,
                            String separator,
                            String suffix,
                            Converter<String, ? extends Byte> converter,
                            boolean trim) {
        super(Byte.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToByteList create(String prefix,
                                          String separator,
                                          String suffix,
                                          Converter<String, ? extends Byte> converter,
                                          boolean trim) {
        return new StringToByteList(prefix, separator, suffix, converter, trim);
    }
}