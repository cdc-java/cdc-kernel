package cdc.converters.defaults;

import java.time.DateTimeException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Function;

import cdc.args.AbstractFactory;
import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.AbstractConverter;
import cdc.converters.ConversionException;
import cdc.util.lang.Checks;

/**
 * Base class used to create String to Temporal converters.
 *
 * @author Damien Carbonne
 *
 * @param <T> The temporal type.
 */
public abstract class AbstractStringToTemporal<T extends TemporalAccessor> extends AbstractConverter<String, T> {
    private final DateTimeFormatter formatter;
    private final String pattern;
    private final Locale locale;

    public static final FormalArg<DateTimeFormatter> FORMATTER = DateConversionSupport.FORMATTER;
    public static final FormalArgs FPARAMS_FORMATTER = DateConversionSupport.FPARAMS_FORMATTER;

    public static final FormalArg<String> PATTERN = DateConversionSupport.PATTERN;
    public static final FormalArg<Locale> LOCALE = DateConversionSupport.LOCALE;
    public static final FormalArgs FPARAMS_PATTERN_LOCALE = DateConversionSupport.FPARAMS_PATTERN_LOCALE;

    /**
     * Function used to create a String to Temporal converter Factory.
     *
     * @param <C> The converter type.
     * @param <T> The temporal type.
     * @param converterClass The converter class.
     * @param temporalClass The temporal class.
     * @param autoConverter The auto converter.
     * @param builderFormatter The creation function that uses a DateTimeFormatter.
     * @param builderPatternLocale The creation function that uses a pattern and a locale.
     * @return A converter Factory.
     */
    protected static <C extends AbstractStringToTemporal<T>, T extends TemporalAccessor> Factory<C> factory(Class<C> converterClass,
                                                                                                            Class<T> temporalClass,
                                                                                                            C autoConverter,
                                                                                                            Function<DateTimeFormatter, C> builderFormatter,
                                                                                                            BiFunction<String, Locale, C> builderPatternLocale) {
        return new AbstractFactory<C>(converterClass,
                                      Args.builder()
                                          .arg(SOURCE_CLASS, String.class)
                                          .arg(TARGET_CLASS, temporalClass)
                                          .build(),
                                      FormalArgs.NO_FARGS,
                                      FPARAMS_PATTERN_LOCALE,
                                      FPARAMS_FORMATTER) {
            @Override
            protected C create(Args args,
                               FormalArgs fargs) {
                if (fargs == FPARAMS_FORMATTER) {
                    final DateTimeFormatter vformatter = args.getValue(FORMATTER);
                    return builderFormatter.apply(vformatter);
                } else if (fargs == FPARAMS_PATTERN_LOCALE) {
                    final String vpattern = args.getValue(PATTERN);
                    final Locale vlocale = args.getValue(LOCALE, null);
                    return builderPatternLocale.apply(vpattern, vlocale);
                } else {
                    return autoConverter;
                }
            }
        };
    }

    protected AbstractStringToTemporal(Class<T> targetClass) {
        super(String.class,
              targetClass);
        this.formatter = null;
        this.pattern = null;
        this.locale = null;
    }

    protected AbstractStringToTemporal(Class<T> targetClass,
                                       DateTimeFormatter formatter,
                                       String pattern,
                                       Locale locale) {
        super(String.class,
              targetClass);
        Checks.isNotNull(formatter, "formatter");
        this.formatter = formatter;
        this.pattern = pattern;
        this.locale = locale;
    }

    public final DateTimeFormatter getFormatter() {
        return formatter;
    }

    public final String getPattern() {
        return pattern;
    }

    public final Locale getLocale() {
        return locale;
    }

    @Override
    public final Args getParams() {
        if (formatter == null) {
            return Args.NO_ARGS;
        } else if (pattern == null) {
            return Args.builder(FPARAMS_FORMATTER)
                       .arg(FORMATTER, getFormatter())
                       .build();
        } else {
            return Args.builder(FPARAMS_PATTERN_LOCALE)
                       .arg(PATTERN, getPattern())
                       .arg(LOCALE, getLocale())
                       .build();
        }
    }

    protected static class Default {
        final String pattern;
        final DateTimeFormatter formatter;
        // final Pattern pattern;

        public Default(String pattern
        /* String regex */) {
            this.pattern = pattern;
            this.formatter = DateTimeFormatter.ofPattern(pattern);
            // this.pattern = Pattern.compile(regex);
        }
    }

    protected T apply(String source,
                      BiFunction<String, DateTimeFormatter, T> parser,
                      Default[] defaults) {
        if (source == null || source.isEmpty()) {
            return null;
        } else if (formatter == null) {
            for (final Default def : defaults) {
                try {
                    return parser.apply(source, def.formatter);
                } catch (final DateTimeException e) {
                    // Ignore
                }
            }
            throw new ConversionException("Failed to parse '" + source + "' using default formats");
        } else {
            return parser.apply(source, getFormatter());
        }
    }
}