package cdc.converters.defaults;

import java.util.Locale;

import cdc.args.Factory;

public final class IntegerToString extends AbstractNumberToString<Integer> {
    public static final IntegerToString INSTANCE = new IntegerToString(null, null);

    public static final Factory<IntegerToString> FACTORY = factory(IntegerToString.class,
                                                                   Integer.class,
                                                                   IntegerToString::create);

    public static IntegerToString create(String pattern,
                                         Locale locale) {
        if (pattern == null && locale == null) {
            return INSTANCE;
        } else {
            return new IntegerToString(pattern, locale);
        }
    }

    private IntegerToString(String pattern,
                            Locale locale) {
        super(Integer.class,
              pattern,
              locale,
              NumberConversionSupport.DEFAULT_INTEGER_FORMAT);
    }
}