package cdc.converters.defaults;

import cdc.args.Factory;
import cdc.converters.Converter;

public class StringToShortArray extends StringToArray<Short> {
    public static final Factory<StringToShortArray> FACTORY = factory(StringToShortArray.class,
                                                                      Short.class,
                                                                      StringToShortArray::create);

    public StringToShortArray(String prefix,
                              String separator,
                              String suffix,
                              Converter<String, ? extends Short> converter,
                              boolean trim) {
        super(Short.class,
              prefix,
              separator,
              suffix,
              converter,
              trim);
    }

    public static StringToShortArray create(String prefix,
                                            String separator,
                                            String suffix,
                                            Converter<String, ? extends Short> converter,
                                            boolean trim) {
        return new StringToShortArray(prefix, separator, suffix, converter, trim);
    }
}