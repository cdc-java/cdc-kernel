package cdc.converters;

import java.lang.reflect.Array;
import java.util.function.Function;

import cdc.args.Args;
import cdc.args.Factory;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.args.Necessity;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Base interface of typed converters.
 * <p>
 * A Converter is a {@link Function} with additional properties:
 * <ul>
 * <li>The class of source/input type.
 * <li>The class of target/result type.
 * <li>Some configuration parameters.
 * </ul>
 *
 * @author Damien Carbonne
 *
 * @param <S> The source/input type.
 * @param <T> The target/result type.
 */
public interface Converter<S, T> extends Function<S, T> {
    public static final FormalArg<Class<?>> SOURCE_CLASS =
            new FormalArg<>("sourceClass", Introspection.uncheckedCast(Class.class), Necessity.MANDATORY);
    public static final FormalArg<Class<?>> TARGET_CLASS =
            new FormalArg<>("targetClass", Introspection.uncheckedCast(Class.class), Necessity.MANDATORY);
    public static final FormalArgs META_FARGS =
            new FormalArgs(SOURCE_CLASS,
                           TARGET_CLASS);

    /**
     * @return The class of source objects.
     */
    public Class<S> getSourceClass();

    /**
     * @return The wrapped type of source class.<br>
     *         If source class is the class of a primitive type,
     *         returns the class of corresponding boxing type.
     */
    public default Class<S> getWrappedSourceClass() {
        return Introspection.wrap(getSourceClass());
    }

    /**
     * @return The class of target objects.
     */
    public Class<T> getTargetClass();

    /**
     * @return The wrapped type of target class.
     *         If target class is the class of a primitive type,
     *         returns the class of corresponding boxing type.
     */
    public default Class<T> getWrappedTargetClass() {
        return Introspection.wrap(getTargetClass());
    }

    /**
     * @return The parameters used to configure this converter.
     */
    public Args getParams();

    /**
     * Applies this converter to a raw value.
     *
     * @param source The source value.
     * @return The converted value.
     * @throws ClassCastException When {@code source} is not {@code null} and can not be cast to source class.
     */
    public default T applyRaw(Object source) {
        final S tmp = getSourceClass().cast(source);
        return apply(tmp);
    }

    /**
     * Returns {@code true} when a class is a compliant source class.
     * <p>
     * {@code cls} must be descendant of source class.
     *
     * @param cls The class.
     * @return {@code true} when {@code cls} is a compliant source class.
     */
    public default boolean isCompliantSourceClass(Class<?> cls) {
        return cls != null && getWrappedSourceClass().isAssignableFrom(Introspection.wrap(cls));
    }

    /**
     * Returns {@code true} when a class is a compliant target class.
     * <p>
     * {@code cls} must be ancestor of target class.
     *
     * @param cls The class.
     * @return {@code true} when {@code cls} is a compliant target class.
     */
    public default boolean isCompliantTargetClass(Class<?> cls) {
        return cls != null && Introspection.wrap(cls).isAssignableFrom(getWrappedTargetClass());
    }

    /**
     * Returns {@code true} when a source and target classes are compliant.
     *
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return {@code true} when {@code source} and {@code target} classes are compliant.
     */
    public default boolean areCompliantSourceAndTargetClasses(Class<?> sourceClass,
                                                              Class<?> targetClass) {
        return isCompliantSourceClass(sourceClass)
                && isCompliantTargetClass(targetClass);
    }

    /**
     * Returns {@code true} when a class matches source class.
     * <p>
     * {@code cls} and source class are the same (after wrapping).
     *
     * @param cls The class.
     * @return {@code true} when {@code cls} matches source class.
     */
    public default boolean isMatchingSourceClass(Class<?> cls) {
        return cls != null && getWrappedSourceClass().equals(Introspection.wrap(cls));
    }

    /**
     * Returns {@code true} when a class matches target class.
     * <p>
     * {@code cls} and target class are the same (after wrapping).
     *
     * @param cls The class.
     * @return {@code true} when {@code cls} matches target class.
     */
    public default boolean isMatchingTargetClass(Class<?> cls) {
        return cls != null && Introspection.wrap(cls).equals(getWrappedTargetClass());
    }

    /**
     * Returns {@code true} when a source and target classes are matching.
     *
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return {@code true} when {@code source} and {@code target} classes are matching.
     */
    public default boolean areMatchingSourceAndTargetClasses(Class<?> sourceClass,
                                                             Class<?> targetClass) {
        return isMatchingSourceClass(sourceClass)
                && isMatchingTargetClass(targetClass);
    }

    /**
     * Returns the composition of this converter applied after another one.
     *
     * @param <R> The source type of the other converter.
     * @param before The other converter, applied before this one.
     * @return The composition of this converter applied after {@code before}.
     */
    default <R> Converter<R, T> compose(Converter<R, ? extends S> before) {
        Checks.isNotNull(before, "before");
        return new AndThenConverter<>(before, this);
    }

    /**
     * Returns the composition if this converter applied before another one.
     *
     * @param <U> The target type of the other converter.
     * @param after The other converter, applied after this one.
     * @return The composition of this converter applied before {@code after}.
     */
    default <U> Converter<S, U> andThen(Converter<? super T, U> after) {
        Checks.isNotNull(after, "after");
        return new AndThenConverter<>(this, after);
    }

    /**
     * Returns a converter that first tries to convert using this converter,
     * and falls back to other ones in case of failure.
     * <p>
     * The result of the first successful converter is used.
     * If no converter succeeds, an exception is thrown.
     *
     * @param others The fallback converters, used if this one fails.
     * @return A converter that first tries to convert using this converter,
     *         and falls back to {@code others} in case of failure.
     */
    @SuppressWarnings("unchecked")
    default Converter<S, T> orElse(Converter<? super S, ? extends T>... others) {
        return new OrElseConverter<>(this, others);
    }

    /**
     * Returns an adaptation of this converter to other source and target classes.
     *
     * @param <R> The result source type.
     * @param <U> The result target type.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return An adaptation of this converter to {@code sourceClass} and {@code targetClass}.
     * @throws IllegalArgumentException When no adaptation is possible.
     */
    public default <R, U> Converter<R, U> cast(Class<R> sourceClass,
                                               Class<U> targetClass) {
        return adapt(sourceClass, targetClass, this);
    }

    /**
     * Returns the composition of this converter applied after another one.
     *
     * @param <R> The source type of the other converter.
     * @param before The other converter, applied before this one.
     * @return The composition of this converter applied after {@code before}.
     * @throws IllegalArgumentException When {@code before} is {@code null} or no adaptation of {@code before} is possible.
     */
    public default <R> Converter<R, T> composeRaw(Converter<R, ?> before) {
        Checks.isNotNull(before, "before");
        return compose(before.cast(before.getSourceClass(), Converter.this.getSourceClass()));
    }

    /**
     * Returns the composition if this converter applied before another one.
     *
     * @param <U> The target type of the other converter.
     * @param after The other converter, applied after this one.
     * @return The composition of this converter applied before {@code after}.
     * @throws IllegalArgumentException When {@code after} is {@code null} or no adaptation of {@code after} is possible.
     */
    public default <U> Converter<S, U> andThenRaw(Converter<?, U> after) {
        Checks.isNotNull(after, "after");
        return andThen(after.cast(Converter.this.getTargetClass(), after.getTargetClass()));
    }

    public default Converter<S, T> orElseRaw(Converter<?, ?>... others) {
        @SuppressWarnings("unchecked")
        final Converter<? super S, ? extends T>[] array =
                (Converter<? super S, ? extends T>[]) Array.newInstance(Converter.class, others.length);
        for (int index = 0; index < others.length; index++) {
            array[index] = others[index].adapt(getSourceClass(), getTargetClass());
        }
        return orElse(array);
    }

    /**
     * Returns an adaptation of this converter to a specific source and target classes.
     *
     * @param <R> The source type.
     * @param <U> The target type.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return An adaptation of this converter that matches {@code sourceClass} and {@code targetClass}.
     * @throws IllegalArgumentException When no adaptation is possible.
     */
    public default <R, U> Converter<R, U> adapt(Class<R> sourceClass,
                                                Class<U> targetClass) {
        return adapt(sourceClass, targetClass, this);
    }

    /**
     * Returns an adaptation of a converter to specific source and target classes.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @param delegate The converter to adapt.
     * @return An adaptation of {@code delegate} that matches {@code sourceClass} and {@code targetClass}.
     * @throws IllegalArgumentException When no adaptation is possible.
     */
    public static <S, T> Converter<S, T> adapt(Class<S> sourceClass,
                                               Class<T> targetClass,
                                               Converter<?, ?> delegate) {
        Checks.isNotNull(sourceClass, "sourceClass");
        Checks.isNotNull(targetClass, "targetClass");
        Checks.isNotNull(delegate, "delegate");

        if (sourceClass.equals(delegate.getSourceClass())
                && targetClass.equals(delegate.getTargetClass())) {
            // Classes match. We can simply cast.
            @SuppressWarnings("unchecked")
            final Converter<S, T> tmp = (Converter<S, T>) delegate;
            return tmp;
        } else {
            // Classes don't exactly match. We create an adapter.
            return new ConverterAdapter<>(sourceClass, targetClass, delegate);
        }
    }

    public static <C extends Converter<S, T>, S, T> Factory<C> singleton(C instance) {
        return Factory.singleton(instance,
                                 Args.builder()
                                     .arg(SOURCE_CLASS, instance.getSourceClass())
                                     .arg(TARGET_CLASS, instance.getTargetClass())
                                     .build());
    }
}