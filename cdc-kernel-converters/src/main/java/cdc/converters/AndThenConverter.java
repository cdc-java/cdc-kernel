package cdc.converters;

import cdc.args.Args;

/**
 * Converter that is the composition of 2 Converters.
 *
 * @author Damien Carbonne
 *
 * @param <S> The source/input type of first and composed converter.
 * @param <X> The intermediate type.<br>
 *            It is compliant with target/result type if first converter.<br>
 *            It is compliant with source/input type of second converter.
 * @param <T> The target/result type of second and composed converter.
 */
public final class AndThenConverter<S, X, T> implements Converter<S, T> {
    private final Converter<S, ? extends X> before;
    private final Converter<? super X, T> after;

    public AndThenConverter(Converter<S, ? extends X> before,
                            Converter<? super X, T> after) {
        this.before = before;
        this.after = after;
    }

    @Override
    public T apply(S arg) {
        return after.apply(before.apply(arg));
    }

    @Override
    public Class<S> getSourceClass() {
        return before.getSourceClass();
    }

    @Override
    public Class<T> getTargetClass() {
        return after.getTargetClass();
    }

    @Override
    public Args getParams() {
        return Args.NO_ARGS;
    }

    /**
     * @return The converter that is applied first.
     */
    public Converter<S, ? extends X> getBefore() {
        return before;
    }

    /**
     * @return The converter that is applied second.
     */
    public Converter<? super X, T> getAfter() {
        return after;
    }

    @Override
    public String toString() {
        return "[" + getClass().getSimpleName() + " " + getBefore() + " o " + getAfter() + "]";
    }
}