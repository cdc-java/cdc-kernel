package cdc.converters;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Factories;
import cdc.converters.defaults.BigDecimalToString;
import cdc.converters.defaults.BigIntegerToString;
import cdc.converters.defaults.BooleanArrayToString;
import cdc.converters.defaults.BooleanListToString;
import cdc.converters.defaults.BooleanToString;
import cdc.converters.defaults.ByteArrayToString;
import cdc.converters.defaults.ByteListToString;
import cdc.converters.defaults.ByteToString;
import cdc.converters.defaults.CharToString;
import cdc.converters.defaults.ClassToString;
import cdc.converters.defaults.DoubleArrayToString;
import cdc.converters.defaults.DoubleListToString;
import cdc.converters.defaults.DoubleToString;
import cdc.converters.defaults.EnumToString;
import cdc.converters.defaults.FileToString;
import cdc.converters.defaults.FloatArrayToString;
import cdc.converters.defaults.FloatListToString;
import cdc.converters.defaults.FloatToString;
import cdc.converters.defaults.Identity;
import cdc.converters.defaults.IntegerArrayToString;
import cdc.converters.defaults.IntegerListToString;
import cdc.converters.defaults.IntegerToString;
import cdc.converters.defaults.LocalDateTimeToString;
import cdc.converters.defaults.LocalDateToString;
import cdc.converters.defaults.LocalTimeToString;
import cdc.converters.defaults.LocaleToString;
import cdc.converters.defaults.LongArrayToString;
import cdc.converters.defaults.LongListToString;
import cdc.converters.defaults.LongToString;
import cdc.converters.defaults.ObjectToString;
import cdc.converters.defaults.ShortArrayToString;
import cdc.converters.defaults.ShortListToString;
import cdc.converters.defaults.ShortToString;
import cdc.converters.defaults.StringFiller;
import cdc.converters.defaults.StringRandomizer;
import cdc.converters.defaults.StringToBigDecimal;
import cdc.converters.defaults.StringToBigInteger;
import cdc.converters.defaults.StringToBoolean;
import cdc.converters.defaults.StringToBooleanArray;
import cdc.converters.defaults.StringToBooleanList;
import cdc.converters.defaults.StringToByte;
import cdc.converters.defaults.StringToByteArray;
import cdc.converters.defaults.StringToByteList;
import cdc.converters.defaults.StringToChar;
import cdc.converters.defaults.StringToClass;
import cdc.converters.defaults.StringToConverter;
import cdc.converters.defaults.StringToDate;
import cdc.converters.defaults.StringToDouble;
import cdc.converters.defaults.StringToDoubleArray;
import cdc.converters.defaults.StringToDoubleList;
import cdc.converters.defaults.StringToEnum;
import cdc.converters.defaults.StringToFile;
import cdc.converters.defaults.StringToFloat;
import cdc.converters.defaults.StringToFloatArray;
import cdc.converters.defaults.StringToFloatList;
import cdc.converters.defaults.StringToInteger;
import cdc.converters.defaults.StringToIntegerArray;
import cdc.converters.defaults.StringToIntegerList;
import cdc.converters.defaults.StringToLocalDate;
import cdc.converters.defaults.StringToLocalDateTime;
import cdc.converters.defaults.StringToLocalTime;
import cdc.converters.defaults.StringToLocale;
import cdc.converters.defaults.StringToLong;
import cdc.converters.defaults.StringToLongArray;
import cdc.converters.defaults.StringToLongList;
import cdc.converters.defaults.StringToShort;
import cdc.converters.defaults.StringToShortArray;
import cdc.converters.defaults.StringToShortList;
import cdc.converters.defaults.ToLowerCase;
import cdc.converters.defaults.ToUpperCase;
import cdc.util.debug.Printable;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;
import cdc.util.lang.NotFoundException;

/**
 * Registry of converters.
 *
 * @author Damien Carbonne
 *
 */
public final class Converters {
    private static final Logger LOGGER = LogManager.getLogger(Converters.class);
    private static final String CONVERTER = "converter";
    private static final String SOURCE_CLASS = "sourceClass";
    private static final String TARGET_CLASS = "targetClass";
    private static final Map<String, Converter<?, ?>> MAP = new HashMap<>();
    public static final Printable PRINTER = new Printer();

    static {
        Printables.register(Converters.class, PRINTER);
    }

    private static class Key {
        private final Class<?> source;
        private final Class<?> target;

        public Key(Class<?> source,
                   Class<?> target) {
            this.source = Introspection.wrap(source);
            this.target = Introspection.wrap(target);
        }

        @Override
        public int hashCode() {
            return source.hashCode() + 31 * target.hashCode();
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Key)) {
                return false;
            }
            final Key other = (Key) object;
            return source.equals(other.source)
                    && target.equals(other.target);
        }
    }

    private static class Bucket {
        private final List<Converter<?, ?>> converters = new ArrayList<>();
        private Converter<?, ?> defaultConverter = null;

        public Bucket() {
            super();
        }

        public List<Converter<?, ?>> getConverters() {
            return converters;
        }

        public void add(Converter<?, ?> converter) {
            converters.add(converter);
        }

        public void setDefaultConverter(Converter<?, ?> converter) {
            defaultConverter = converter;
        }

        public boolean hasDefaultConverter() {
            return defaultConverter != null;
        }

        public Converter<?, ?> getSpecialConverter(SpecialConverterKind kind) {
            if (defaultConverter != null) {
                return defaultConverter;
            } else if (kind == SpecialConverterKind.DEFAULT_OR_SINGLE && converters.size() == 1) {
                return converters.get(0);
            } else {
                return null;
            }
        }
    }

    private static final Map<Key, Bucket> BUCKETS = new HashMap<>();

    static {
        LOGGER.debug("<clinit>()");
        Factories.register(BooleanArrayToString.FACTORY);
        Factories.register(BooleanListToString.FACTORY);
        Factories.register(BooleanToString.FACTORY);
        Factories.register(BigDecimalToString.FACTORY);
        Factories.register(BigIntegerToString.FACTORY);
        Factories.register(ByteArrayToString.FACTORY);
        Factories.register(ByteListToString.FACTORY);
        Factories.register(ByteToString.FACTORY);
        Factories.register(CharToString.FACTORY);
        Factories.register(ClassToString.FACTORY);
        Factories.register(DoubleArrayToString.FACTORY);
        Factories.register(DoubleListToString.FACTORY);
        Factories.register(DoubleToString.FACTORY);
        Factories.register(EnumToString.FACTORY);
        Factories.register(FileToString.FACTORY);
        Factories.register(FloatArrayToString.FACTORY);
        Factories.register(FloatListToString.FACTORY);
        Factories.register(FloatToString.FACTORY);
        Factories.register(Identity.FACTORY);
        Factories.register(IntegerArrayToString.FACTORY);
        Factories.register(IntegerListToString.FACTORY);
        Factories.register(IntegerToString.FACTORY);
        Factories.register(LocaleToString.FACTORY);
        Factories.register(LocalDateToString.FACTORY);
        Factories.register(LocalDateTimeToString.FACTORY);
        Factories.register(LocalTimeToString.FACTORY);
        Factories.register(LongArrayToString.FACTORY);
        Factories.register(LongListToString.FACTORY);
        Factories.register(LongToString.FACTORY);
        Factories.register(ObjectToString.FACTORY);
        Factories.register(ShortArrayToString.FACTORY);
        Factories.register(ShortListToString.FACTORY);
        Factories.register(ShortToString.FACTORY);
        Factories.register(StringFiller.FACTORY);
        Factories.register(StringRandomizer.FACTORY);
        Factories.register(StringToBoolean.FACTORY);
        Factories.register(StringToBooleanArray.FACTORY);
        Factories.register(StringToBooleanList.FACTORY);
        Factories.register(StringToBigDecimal.FACTORY);
        Factories.register(StringToBigInteger.FACTORY);
        Factories.register(StringToByte.FACTORY);
        Factories.register(StringToByteArray.FACTORY);
        Factories.register(StringToByteList.FACTORY);
        Factories.register(StringToChar.FACTORY);
        Factories.register(StringToClass.FACTORY);
        Factories.register(StringToConverter.FACTORY);
        Factories.register(StringToDate.FACTORY);
        Factories.register(StringToDouble.FACTORY);
        Factories.register(StringToDoubleArray.FACTORY);
        Factories.register(StringToDoubleList.FACTORY);
        Factories.register(StringToEnum.FACTORY);
        Factories.register(StringToFile.FACTORY);
        Factories.register(StringToFloat.FACTORY);
        Factories.register(StringToFloatArray.FACTORY);
        Factories.register(StringToFloatList.FACTORY);
        Factories.register(StringToInteger.FACTORY);
        Factories.register(StringToIntegerArray.FACTORY);
        Factories.register(StringToIntegerList.FACTORY);
        Factories.register(StringToLocale.FACTORY);
        Factories.register(StringToLocalDate.FACTORY);
        Factories.register(StringToLocalDateTime.FACTORY);
        Factories.register(StringToLocalTime.FACTORY);
        Factories.register(StringToLong.FACTORY);
        Factories.register(StringToLongArray.FACTORY);
        Factories.register(StringToLongList.FACTORY);
        Factories.register(StringToShort.FACTORY);
        Factories.register(StringToShortArray.FACTORY);
        Factories.register(StringToShortList.FACTORY);
        Factories.register(ToLowerCase.FACTORY);
        Factories.register(ToUpperCase.FACTORY);

        Converters.register(BooleanToString.INSTANCE, true);
        Converters.register(ByteToString.INSTANCE, true);
        Converters.register(BigDecimalToString.INSTANCE, true);
        Converters.register(BigIntegerToString.INSTANCE, true);
        Converters.register(CharToString.INSTANCE, true);
        Converters.register(ClassToString.INSTANCE, true);
        Converters.register(DoubleToString.INSTANCE, true);
        Converters.register(FileToString.INSTANCE, true);
        Converters.register(FloatToString.INSTANCE, true);
        Converters.register(Identity.INSTANCE, true);
        Converters.register(IntegerToString.INSTANCE, true);
        Converters.register(LocaleToString.INSTANCE, true);
        Converters.register(LongToString.INSTANCE, true);
        Converters.register(ObjectToString.INSTANCE, true);
        Converters.register(ShortToString.INSTANCE, true);
        Converters.register(StringToBoolean.INSTANCE, true);
        Converters.register(StringToBigDecimal.INSTANCE, true);
        Converters.register(StringToBigInteger.INSTANCE, true);
        Converters.register(StringToByte.INSTANCE, true);
        Converters.register(StringToChar.INSTANCE, true);
        Converters.register(StringToClass.INSTANCE, true);
        Converters.register(StringToConverter.INSTANCE, true);
        Converters.register(StringToDouble.INSTANCE, true);
        Converters.register(StringToFile.INSTANCE, true);
        Converters.register(StringToFloat.INSTANCE, true);
        Converters.register(StringToInteger.INSTANCE, true);
        Converters.register(StringToLocale.INSTANCE, true);
        Converters.register(StringToLong.INSTANCE, true);
        Converters.register(StringToShort.INSTANCE, true);
        Converters.register(ToUpperCase.INSTANCE, false);
        Converters.register(ToLowerCase.INSTANCE, false);
    }

    private Converters() {
    }

    public static void elaborate() {
        // Ignore
    }

    private static String noConverterFound(String name) {
        return "No '" + name + "' converter found";
    }

    private static String noMatchingConverterFound(String name,
                                                   Class<?> sourceClass,
                                                   Class<?> targetClass) {
        return "No " + (name == null ? "" : "'" + name + "' ")
                + "converter maching "
                + (sourceClass == null ? "?" : sourceClass.getSimpleName())
                + " "
                + (targetClass == null ? "?" : targetClass.getSimpleName())
                + " found";
    }

    private static String noCompliantConverterFound(String name,
                                                    Class<?> sourceClass,
                                                    Class<?> targetClass) {
        return "No " + (name == null ? "" : "'" + name + "' ")
                + "converter compliant with "
                + (sourceClass == null ? "?" : sourceClass.getSimpleName())
                + " "
                + (targetClass == null ? "?" : targetClass.getSimpleName())
                + " found";
    }

    /**
     * Registers a converter.
     *
     * @param converter The converter.
     * @param name The converter name.
     * @param isDefault If {@code true}, this converter is a default one for target/source classes.
     * @param reaction The reaction to adopt in case of failure.
     * @throws IllegalArgumentException when c{@code converter} is {@code null},
     *             or a converter with the same name is already registered,
     *             or a default converter is already registered.
     */
    public static void register(Converter<?, ?> converter,
                                String name,
                                boolean isDefault,
                                FailureReaction reaction) {
        LOGGER.debug("register({}, {}, {})", converter, name, isDefault);
        Checks.isNotNull(converter, CONVERTER);
        Checks.isNotNull(name, "name");
        if (MAP.containsKey(name)) {
            FailureReaction.onError("Duplicate name '" + name + "'", LOGGER, reaction, IllegalArgumentException::new);
        }

        MAP.put(name, converter);
        final Key key = new Key(converter.getSourceClass(), converter.getTargetClass());
        final Bucket bucket = BUCKETS.computeIfAbsent(key, k -> new Bucket());
        bucket.add(converter);
        if (isDefault) {
            if (bucket.hasDefaultConverter()) {
                FailureReaction.onError("A default converter is already associated to ("
                        + converter.getSourceClass().getSimpleName()
                        + ", " + converter.getTargetClass().getSimpleName()
                        + ")",
                                        LOGGER,
                                        reaction,
                                        IllegalArgumentException::new);
            }
            bucket.setDefaultConverter(converter);
        }
    }

    /**
     * Registers a converter using its class simple name as name.
     *
     * @param converter The converter.
     * @param isDefault If {@code true}, this converter is a default one for target/source classes.
     * @param reaction The reaction to adopt in case of failure.
     * @throws IllegalArgumentException when c{@code converter} is {@code null},
     *             or a converter with the same name is already registered,
     *             or a default converter is already registered.
     */
    public static void register(Converter<?, ?> converter,
                                boolean isDefault,
                                FailureReaction reaction) {
        LOGGER.debug("register({}, {})", converter, isDefault);
        Checks.isNotNull(converter, CONVERTER);
        register(converter, converter.getClass().getSimpleName(), isDefault, reaction);
    }

    public static void register(Converter<?, ?> converter,
                                boolean isDefault) {
        register(converter, isDefault, FailureReaction.FAIL);
    }

    /**
     * @return A set of all registered converters names.
     */
    public static Set<String> getNames() {
        return MAP.keySet();
    }

    /**
     * Returns {@code true} when a converter with a given name exists.
     *
     * @param name The converter name.
     * @return {@code true} when a converter named {@code name} exists.
     */
    public static boolean hasConverter(String name) {
        return MAP.containsKey(name);
    }

    /**
     * Returns {@code true} when some converters matching source and target classes are registered.
     *
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return {@code true} when converters matching {@code sourceClass} and {@code targetClass} are registered.
     */
    public static boolean hasMatchingConverters(Class<?> sourceClass,
                                                Class<?> targetClass) {
        final Key key = new Key(sourceClass, targetClass);
        return BUCKETS.containsKey(key);
    }

    /**
     * Returns {@code true} when some converters compliant with source and target classes are registered.
     *
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return {@code true} when converters compliant with {@code sourceClass} and {@code targetClass} are registered.
     */
    public static boolean hasCompliantConverters(Class<?> sourceClass,
                                                 Class<?> targetClass) {
        for (final Map.Entry<Key, Bucket> entry : BUCKETS.entrySet()) {
            for (final Converter<?, ?> converter : entry.getValue().getConverters()) {
                if (converter.areCompliantSourceAndTargetClasses(sourceClass, targetClass)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns {@code true} when a special converter matches a pair of classes.
     * <p>
     * <b>WARNING:</b> Only exact matching of classes is tested.
     *
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @param kind The special converter kind.
     * @return {@code true} when a special converter from {@code sourceClass} to {@code targetClass} exists.
     */
    public static boolean hasSpecialConverter(Class<?> sourceClass,
                                              Class<?> targetClass,
                                              SpecialConverterKind kind) {
        Checks.isNotNull(kind, "kind");
        final Key key = new Key(sourceClass, targetClass);
        final Bucket bucket = BUCKETS.get(key);
        return bucket != null && bucket.getSpecialConverter(kind) != null;
    }

    /**
     * Returns {@code true} when a converter is declared as a special one.
     *
     * @param converter The converter.
     * @param kind The special converter kind.
     * @return {@code true} when {@code converter} is a special converter.
     */
    public static boolean isSpecialConverter(Converter<?, ?> converter,
                                             SpecialConverterKind kind) {
        Checks.isNotNull(kind, "kind");
        if (converter == null) {
            return false;
        } else {
            return getMatchingSpecialConverter(converter.getSourceClass(),
                                               converter.getTargetClass(),
                                               kind,
                                               FailureReaction.DEFAULT) == converter;
        }
    }

    /**
     * Returns {@code true} if a name is the name of a default converter.
     *
     * @param name The name.
     * @param kind The special converter kind.
     * @return {@code true} if {@code name} is the name of a default converter.
     */
    public static boolean isSpecialConverterName(String name,
                                                 SpecialConverterKind kind) {
        Checks.isNotNull(kind, "kind");
        return isSpecialConverter(getConverter(name, FailureReaction.DEFAULT), kind);
    }

    /**
     * Returns the converter that has a given name.
     *
     * @param name The name.
     * @param reaction The reaction to adopt when not converter is found.
     * @return The converter named {@code name} or {@code null}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL} and no converter named {@code name} exists.
     */
    public static Converter<?, ?> getConverter(String name,
                                               FailureReaction reaction) {
        return NotFoundException.onResult(MAP.get(name),
                                          noConverterFound(name),
                                          LOGGER,
                                          reaction,
                                          null);
    }

    /**
     * Returns the converter that has a given name.
     *
     * @param name The name.
     * @return The converter named { @code name}.
     * @throws NotFoundException When no converter named {@code name} exists.
     */
    public static Converter<?, ?> getConverter(String name) {
        return getConverter(name, FailureReaction.FAIL);
    }

    /**
     * Returns the converter that has a given name and matches source and target classes.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param name The converter name.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @param reaction The reaction to adopt when not converter is found.
     * @return The matching converter or {@code null}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL}
     *             and no converter named {@code name}, matching {@code sourceClass} and {@code targetClass} exists.
     */
    public static <S, T> Converter<S, T> getMatchingConverter(String name,
                                                              Class<S> sourceClass,
                                                              Class<T> targetClass,
                                                              FailureReaction reaction) {
        final Converter<?, ?> tmp = MAP.get(name);
        if (tmp != null) {
            if (tmp.areMatchingSourceAndTargetClasses(sourceClass, targetClass)) {
                return Introspection.uncheckedCast(tmp);
            } else {
                return NotFoundException.onError("'" + name + "' converter does not match " + sourceClass.getCanonicalName() + " "
                        + targetClass.getCanonicalName(),
                                                 LOGGER,
                                                 reaction,
                                                 null);
            }
        } else {
            return NotFoundException.onError(noConverterFound(name),
                                             LOGGER,
                                             reaction,
                                             null);
        }
    }

    /**
     * Returns the converter that has a given name and matches source and target classes.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param name The converter name.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return The matching converter or {@code null}.
     * @throws NotFoundException When no converter named {@code name}, matching {@code sourceClass} and {@code targetClass} exists.
     */
    public static <S, T> Converter<S, T> getMatchingConverter(String name,
                                                              Class<S> sourceClass,
                                                              Class<T> targetClass) {
        return getMatchingConverter(name, sourceClass, targetClass, FailureReaction.FAIL);
    }

    /**
     * Returns the converter that has a given name and is compliant with source and target classes.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param name The converter name.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @param reaction The reaction to adopt when not converter is found.
     * @return The compliant converter or {@code null}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL}
     *             and no converter named {@code name}, compliant with {@code sourceClass} and {@code targetClass} exists.
     */
    public static <S, T> Converter<? super S, ? extends T> getCompliantConverter(String name,
                                                                                 Class<S> sourceClass,
                                                                                 Class<T> targetClass,
                                                                                 FailureReaction reaction) {
        final Converter<?, ?> tmp = MAP.get(name);
        if (tmp != null) {
            if (tmp.areCompliantSourceAndTargetClasses(sourceClass, targetClass)) {
                return Introspection.uncheckedCast(tmp);
            } else {
                return NotFoundException.onError("'" + name + "' converter not compliant with " + sourceClass.getCanonicalName()
                        + " " + targetClass.getCanonicalName(),
                                                 LOGGER,
                                                 reaction,
                                                 null);
            }
        } else {
            return NotFoundException.onError(noConverterFound(name),
                                             LOGGER,
                                             reaction,
                                             null);
        }
    }

    /**
     * Returns the converter that has a given name and is compliant with source and target classes.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param name The converter name.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return The compliant converter or {@code null}.
     * @throws NotFoundException When no converter named {@code name}, compliant with {@code sourceClass} and {@code targetClass}
     *             exists.
     */
    public static <S, T> Converter<? super S, ? extends T> getCompliantConverter(String name,
                                                                                 Class<S> sourceClass,
                                                                                 Class<T> targetClass) {
        return getCompliantConverter(name, sourceClass, targetClass, FailureReaction.FAIL);
    }

    public static <S> List<Converter<S, ?>> getSourceMatchingConverters(Class<S> sourceClass) {
        final List<Converter<S, ?>> result = new ArrayList<>();
        for (final Map.Entry<Key, Bucket> entry : BUCKETS.entrySet()) {
            for (final Converter<?, ?> converter : entry.getValue().getConverters()) {
                if (converter.isMatchingSourceClass(sourceClass)) {
                    result.add(Introspection.uncheckedCast(converter));
                }
            }
        }
        return result;
    }

    public static <T> List<Converter<?, T>> getTargetMatchingConverters(Class<T> targetClass) {
        final List<Converter<?, T>> result = new ArrayList<>();
        for (final Map.Entry<Key, Bucket> entry : BUCKETS.entrySet()) {
            for (final Converter<?, ?> converter : entry.getValue().getConverters()) {
                if (converter.isMatchingTargetClass(targetClass)) {
                    result.add(Introspection.uncheckedCast(converter));
                }
            }
        }
        return result;
    }

    /**
     * Returns a list of all converters from a source class to a target class.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @return A list of all converters from {@code sourceClass} to {@code targetClass}.
     */
    public static <S, T> List<Converter<S, T>> getMatchingConverters(Class<S> sourceClass,
                                                                     Class<T> targetClass) {
        final Key key = new Key(sourceClass, targetClass);
        final Bucket bucket = BUCKETS.get(key);
        if (bucket == null) {
            return Collections.emptyList();
        } else {
            return Introspection.uncheckedCast(bucket.getConverters());
        }
    }

    public static <S> Set<Converter<? super S, ?>> getSourceCompliantConverters(Class<S> sourceClass,
                                                                                SpecialConverterKind kind) {
        final Set<Converter<? super S, ?>> result = new HashSet<>();
        for (final Map.Entry<Key, Bucket> entry : BUCKETS.entrySet()) {
            if (kind == null) {
                for (final Converter<?, ?> converter : entry.getValue().getConverters()) {
                    if (converter.isCompliantSourceClass(sourceClass)) {
                        result.add(Introspection.uncheckedCast(converter));
                    }
                }
            } else {
                final Converter<?, ?> converter = entry.getValue().getSpecialConverter(kind);
                if (converter != null && converter.isCompliantSourceClass(sourceClass)) {
                    result.add(Introspection.uncheckedCast(converter));
                }
            }
        }
        return result;
    }

    public static <T> Set<Converter<?, ? extends T>> getTargetCompliantConverters(Class<T> targetClass,
                                                                                  SpecialConverterKind kind) {
        final Set<Converter<?, ? extends T>> result = new HashSet<>();
        for (final Map.Entry<Key, Bucket> entry : BUCKETS.entrySet()) {
            if (kind == null) {
                for (final Converter<?, ?> converter : entry.getValue().getConverters()) {
                    if (converter.isCompliantTargetClass(targetClass)) {
                        result.add(Introspection.uncheckedCast(converter));
                    }
                }
            } else {
                final Converter<?, ?> converter = entry.getValue().getSpecialConverter(kind);
                if (converter != null && converter.isCompliantTargetClass(targetClass)) {
                    result.add(Introspection.uncheckedCast(converter));
                }
            }
        }
        return result;
    }

    public static <S, T> Set<Converter<? super S, ? extends T>> getCompliantConverters(Class<S> sourceClass,
                                                                                       Class<T> targetClass,
                                                                                       SpecialConverterKind kind) {
        final Set<Converter<? super S, ? extends T>> result = new HashSet<>();
        for (final Map.Entry<Key, Bucket> entry : BUCKETS.entrySet()) {
            if (kind == null) {
                for (final Converter<?, ?> converter : entry.getValue().getConverters()) {
                    if (converter.areCompliantSourceAndTargetClasses(sourceClass, targetClass)) {
                        result.add(Introspection.uncheckedCast(converter));
                    }
                }
            } else {
                final Converter<?, ?> converter = entry.getValue().getSpecialConverter(kind);
                if (converter != null && converter.areCompliantSourceAndTargetClasses(sourceClass, targetClass)) {
                    result.add(Introspection.uncheckedCast(converter));
                }
            }
        }
        return result;
    }

    /**
     * Returns the default converter associated to a pair of classes, if any.
     * <p>
     * <b>WARNING:</b> Only exact matching of classes is tested.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @param kind The special converter kind.
     * @param reaction The reaction to adopt when not converter is found.
     * @return The default converter from {@code sourceClass} to {@code targetClass} or {@code null}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL} and no matching converter is found.
     */
    public static <S, T> Converter<S, T> getMatchingSpecialConverter(Class<S> sourceClass,
                                                                     Class<T> targetClass,
                                                                     SpecialConverterKind kind,
                                                                     FailureReaction reaction) {
        final Key key = new Key(sourceClass, targetClass);
        final Bucket bucket = BUCKETS.get(key);
        if (bucket == null || bucket.getSpecialConverter(kind) == null) {
            return NotFoundException.onError(noMatchingConverterFound(null, sourceClass, targetClass),
                                             LOGGER,
                                             reaction,
                                             null);
        } else {
            return Introspection.uncheckedCast(bucket.getSpecialConverter(kind));
        }
    }

    public static <S, T> Converter<S, T> getMatchingSpecialConverter(Class<S> sourceClass,
                                                                     Class<T> targetClass,
                                                                     SpecialConverterKind kind) {
        return getMatchingSpecialConverter(sourceClass, targetClass, kind, FailureReaction.FAIL);
    }

    /**
     * Returns a compliant default converter for a pair of classes.
     * <p>
     * <b>WARNING:</b> When there is a matching converter, it is returned.<br>
     * Otherwise, when several compliant converters exist, the returned one
     * is selected using an non deterministic algorithm.
     *
     * @param <S> The source type.
     * @param <T> The target type.
     * @param sourceClass The source class.
     * @param targetClass The target class.
     * @param kind The special converter kind.
     * @param reaction The reaction to adopt when not converter is found.
     * @return A compliant default converter from {@code sourceClass} to {@code targetClass} or {@code null}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL} and no compliant converter is found.
     */
    public static <S, T> Converter<? super S, ? extends T> getCompliantSpecialConverter(Class<S> sourceClass,
                                                                                        Class<T> targetClass,
                                                                                        SpecialConverterKind kind,
                                                                                        FailureReaction reaction) {
        final Converter<S, T> matching = getMatchingSpecialConverter(sourceClass, targetClass, kind, FailureReaction.DEFAULT);
        if (matching != null) {
            return matching;
        } else {
            final Set<Converter<? super S, ? extends T>> converters =
                    getCompliantConverters(sourceClass,
                                           targetClass,
                                           kind);
            if (converters.isEmpty()) {
                return NotFoundException.onError(noCompliantConverterFound(null, sourceClass, targetClass),
                                                 LOGGER,
                                                 reaction,
                                                 null);
            } else {
                if (converters.size() != 1) {
                    LOGGER.warn("Several compliant converters from {} to {}",
                                sourceClass.getCanonicalName(),
                                targetClass.getCanonicalName());
                    for (final Converter<?, ?> converter : converters) {
                        LOGGER.warn("   {}", converter);
                    }
                }
                // TODO sort result. criterion?
                return converters.iterator().next();
            }
        }
    }

    public static <S, T> Converter<? super S, ? extends T> getCompliantSpecialConverter(Class<S> sourceClass,
                                                                                        Class<T> targetClass,
                                                                                        SpecialConverterKind kind) {
        return getCompliantSpecialConverter(sourceClass,
                                            targetClass,
                                            kind,
                                            FailureReaction.FAIL);
    }

    /**
     * Converts a source object to a target object using a converter known by its name and parameters.
     *
     * @param name The converter name.
     * @param source The object to convert.
     * @return The conversion of {@code source} using the converter named {@code name} with parameters {@code params}.
     * @throws ConversionException when no converter is found.
     */
    public static Object convertRaw(String name,
                                    Object source) {
        final Converter<?, ?> converter = getConverter(name);
        if (converter == null) {
            throw new ConversionException(noConverterFound(name));
        } else {
            return converter.applyRaw(source);
        }
    }

    /**
     * Converts an object to a target class using an appropriate default converter.
     * <p>
     * The default converter search is narrowed by source and target classes.
     * <p>
     * {@code null} is converted to {@code null}.
     * <p>
     * <b>WARNING:</b> it is the caller responsibility to use a valid {@code sourceClass},
     * compliant with {@code source}.
     *
     * @param source The source object.
     * @param sourceClass The source class, used to narrow search of compliant converters.
     *            It must be compliant with {@code source}.
     * @param targetClass The target class.
     * @return The conversion of {@code source} to {@code targetClass}.
     * @throws ConversionException when no matching default converter is found.
     */
    public static Object convertRaw(Object source,
                                    Class<?> sourceClass,
                                    Class<?> targetClass) {
        Checks.isNotNull(sourceClass, SOURCE_CLASS);
        Checks.isNotNull(targetClass, TARGET_CLASS);
        if (source == null) {
            return null;
        } else {
            final Converter<?, ?> converter =
                    getCompliantSpecialConverter(sourceClass,
                                                 targetClass,
                                                 SpecialConverterKind.DEFAULT_OR_SINGLE,
                                                 FailureReaction.DEFAULT);
            if (converter == null) {
                if (targetClass.isAssignableFrom(source.getClass())) {
                    return source;
                } else {
                    throw new ConversionException("No default or compliant converter from '" + sourceClass.getSimpleName()
                            + "' to '" + targetClass.getSimpleName() + "' is registered");
                }
            } else {
                return converter.applyRaw(source);
            }
        }
    }

    /**
     * Converts an object to a target class using an appropriate default converter.
     * <p>
     * The default converter search is narrowed by class of {@code source} object and target class.
     * <p>
     * {@code null} is converted to {@code null}.
     *
     * @param source The source object.
     * @param targetClass The target class.
     * @return The conversion of {@code source} to {@code targetClass}.
     * @throws ConversionException when no matching default converter is found.
     */
    public static Object convertRaw(Object source,
                                    Class<?> targetClass) {
        Checks.isNotNull(targetClass, TARGET_CLASS);
        if (source == null) {
            return null;
        } else {
            return convertRaw(source, source.getClass(), targetClass);
        }
    }

    /**
     * Converts an object to a target class using an appropriate default converter narrowed from a source class
     * <p>
     * <b>WARNING:</b> it is the caller responsibility to use a valid {@code sourceClass},
     * compliant with {@code source}.
     *
     * @param <T> The target type.
     * @param source The source object.
     * @param sourceClass The source class, used to narrow search of compliant converters.
     *            It must be compliant with {@code source}.
     * @param targetClass The target class.
     * @return The conversion of {@code source} to {@code targetClass}.
     * @throws ConversionException when no matching default converter is found.
     */
    public static <T> T convert(Object source,
                                Class<?> sourceClass,
                                Class<T> targetClass) {
        Checks.isNotNull(sourceClass, SOURCE_CLASS);
        Checks.isNotNull(targetClass, TARGET_CLASS);

        return targetClass.cast(convertRaw(source, sourceClass, targetClass));
    }

    /**
     * Converts an object to a target class using an appropriate default converter.
     *
     * @param <T> The target type.
     * @param source The source object.
     * @param targetClass The target class.
     * @return The conversion of {@code source} to {@code targetClass}.
     * @throws ConversionException when no matching default converter is found.
     */
    public static <T> T convert(Object source,
                                Class<T> targetClass) {
        Checks.isNotNull(targetClass, TARGET_CLASS);

        return targetClass.cast(convertRaw(source, targetClass));
    }

    /**
     * Converts a collection using a converter.
     *
     * @param <T> The elements type.
     * @param input The input collection.
     * @param converter The converter to use.
     * @param output The output collection.
     */
    public static <T> void convert(Collection<?> input,
                                   Converter<?, ? extends T> converter,
                                   Collection<T> output) {
        Checks.isNotNull(input, "input");
        Checks.isNotNull(converter, CONVERTER);
        Checks.isNotNull(output, "output");

        for (final Object o : input) {
            final T t = converter.applyRaw(o);
            output.add(t);
        }
    }

    /**
     * Converts an object to a string using default or single converters.
     * <p>
     * If {@code object} is {@code null}, {@code null} is returned.<br>
     * {@code toString()} is used when no converter is found and {@code reaction} is not FAIL.
     *
     * @param object The object.
     * @param reaction The reaction to adopt when not converter is found.
     * @return The conversion of {@code object} to string using a compliant default or single converter, or {@code toString()}.
     * @throws NotFoundException When {@code reaction} is {@link FailureReaction#FAIL} and no compliant converter is found.
     */
    public static String toString(Object object,
                                  FailureReaction reaction) {
        if (object == null) {
            return null;
        } else {
            @SuppressWarnings("unchecked")
            final Converter<?, String> converter =
                    (Converter<?, String>) getCompliantSpecialConverter(object.getClass(),
                                                                        String.class,
                                                                        SpecialConverterKind.DEFAULT_OR_SINGLE,
                                                                        String.class.equals(object.getClass())
                                                                                ? FailureReaction.DEFAULT
                                                                                : reaction);
            if (converter == null) {
                return object.toString();
            } else {
                return converter.applyRaw(object);
            }
        }
    }

    public static String toString(Object object) {
        return toString(object, FailureReaction.FAIL);
    }

    public static List<Converter<?, ?>> flatten(Converter<?, ?> converter) {
        Checks.isNotNull(converter, CONVERTER);
        try {
            final List<Converter<?, ?>> list = new ArrayList<>();
            flatten(converter, list);
            return list;
        } catch (final RuntimeException e) {
            LOGGER.error("Failed to flatten {}, {}", converter, e.getMessage());
            throw e;
        }
    }

    private static void flatten(Converter<?, ?> converter,
                                List<Converter<?, ?>> list) {
        if (converter instanceof AndThenConverter) {
            flatten(((AndThenConverter<?, ?, ?>) converter).getBefore(), list);
            flatten(((AndThenConverter<?, ?, ?>) converter).getAfter(), list);
        } else if (converter instanceof ConverterAdapter) {
            flatten(((ConverterAdapter<?, ?>) converter).getDelegate(), list);
        } else {
            list.add(converter);
        }
    }

    protected static class Printer implements Printable {
        @Override
        public void print(PrintStream out,
                          int level) {
            indent(out, level);
            out.println("Named converters (" + Converters.getNames().size() + ")");
            for (final String name : CollectionUtils.toSortedList(Converters.getNames())) {
                final Converter<?, ?> converter = Converters.getConverter(name);
                indent(out, level + 1);
                if (Converters.isSpecialConverter(converter, SpecialConverterKind.DEFAULT)) {
                    out.print("D");
                } else {
                    out.print(" ");
                }
                if (Converters.isSpecialConverter(converter, SpecialConverterKind.DEFAULT_OR_SINGLE)) {
                    out.print("S ");
                } else {
                    out.print("  ");
                }
                out.println(name);
                indent(out, level + 2);
                out.println(converter);
            }
        }
    }
}