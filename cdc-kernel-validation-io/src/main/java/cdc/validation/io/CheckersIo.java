package cdc.validation.io;

import java.util.ArrayList;
import java.util.List;

import cdc.args.Args;
import cdc.args.Factories;
import cdc.args.io.ArgsIo;
import cdc.converters.Converter;
import cdc.converters.io.ConvertersIo;
import cdc.io.data.Element;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.util.lang.FailureReaction;
import cdc.validation.checkers.Checker;
import cdc.validation.checkers.Checkers;
import cdc.validation.checkers.RawRefChecker;

public final class CheckersIo {
    public static final String AFTER = "after";
    public static final String AND = "and";
    public static final String CHECKER = "checker";
    public static final String CHECKERS = "checkers";
    public static final String CONVERTER = "converter";
    public static final String CLASS = "class";
    public static final String DEF = "def";
    public static final String NAME = "name";
    public static final String NOT = "not";
    public static final String OR = "or";
    public static final String REF = "ref";

    private CheckersIo() {
    }

    public static class DataLoader extends AbstractResourceLoader<Void> {
        private final ConvertersIo.DataLoader convertersLoader;
        private final ArgsIo.DataLoader argsLoader;

        public DataLoader(FailureReaction reaction) {
            super(reaction);
            this.convertersLoader = new ConvertersIo.DataLoader(reaction);
            this.argsLoader = new ArgsIo.DataLoader(reaction);
        }

        @Override
        protected Void loadRoot(Element root) {
            loadAndRegisterNamedCheckers(root);
            return null;
        }

        private static Checker<?> and(List<Checker<?>> checkers) {
            Checker<?> checker = null;
            for (final Checker<?> next : checkers) {
                if (checker == null) {
                    checker = next;
                } else {
                    checker = checker.andRaw(next);
                }
            }
            return checker;
        }

        private static Checker<?> or(List<Checker<?>> checkers) {
            Checker<?> checker = null;
            for (final Checker<?> next : checkers) {
                if (checker == null) {
                    checker = next;
                } else {
                    checker = checker.orRaw(next);
                }
            }
            return checker;
        }

        public void loadAndRegisterNamedCheckers(Element element) {
            if (CHECKERS.equals(element.getName())) {
                for (final Element child : element.getElements()) {
                    if (CHECKER.equals(child.getName())) {
                        loadAndRegisterNamedChecker(child);
                    } else {
                        unexpectedElement(child, CHECKER);
                    }
                }
            } else {
                unexpectedElement(element, CHECKERS);
            }
        }

        public void loadAndRegisterNamedChecker(Element element) {
            if (CHECKER.equals(element.getName())) {
                if (element.getChildrenCount(Element.class) == 1) {
                    final Checker<?> checker = loadChecker(element.getChild(Element.class));
                    if (checker != null) {
                        final String name = element.getAttributeValue(NAME);
                        if (Checkers.hasChecker(name)) {
                            onError("A checker named '" + name + "' is already registered");
                        } else {
                            Checkers.register(checker, name);
                        }
                    }
                } else {
                    onError("Exactly one child expected under " + element);
                }
            } else {
                unexpectedElement(element, CHECKER);
            }
        }

        public Checker<?> loadChildChecker(Element parent) {
            if (parent.getChildrenCount(Element.class) == 1) {
                final Element child = parent.getChildAt(Element.class, 0);
                return loadChecker(child);
            } else {
                return onError("No child checker found", null);
            }
        }

        public Checker<?> loadChecker(Element element) {
            try {
                switch (element.getName()) {
                case AFTER:
                    final Converter<?, ?> converter = convertersLoader.loadAnonymousConverter(element.getChildAt(Element.class, 0));
                    final Checker<?> checker = loadChecker(element.getChildAt(Element.class, 1));
                    return checker.afterRaw(converter);

                case AND:
                    if (element.getChildrenCount(Element.class) == 0) {
                        return onError("At least one child expected under " + AND, null);
                    } else {
                        return and(loadChildrenCheckers(element));
                    }

                case OR:
                    if (element.getChildrenCount(Element.class) == 0) {
                        return onError("At least one child expected under " + OR, null);
                    } else {
                        return or(loadChildrenCheckers(element));
                    }

                case NOT:
                    if (element.getChildrenCount(Element.class) == 1) {
                        return loadChecker(element.getChild(Element.class)).negate();
                    } else {
                        return onError("Exactly one child expected under " + NOT, null);
                    }

                case DEF:
                    final Args args = argsLoader.loadOptionalChildArgs(element);
                    final String className = element.getAttributeValue(CLASS);
                    return (Checker<?>) Factories.create(className, args);

                case REF:
                    final String name = element.getAttributeValue(NAME);
                    return new RawRefChecker(name);

                default:
                    unexpectedElement(element, AFTER, AND, OR, NOT, DEF, REF);
                    return null;
                }
            } catch (final RuntimeException e) {
                getLogger().error("loadChecker({}) failed", element);
                throw e;
            }
        }

        private List<Checker<?>> loadChildrenCheckers(Element element) {
            final List<Checker<?>> result = new ArrayList<>();
            for (final Element child : element.getElements()) {
                result.add(loadChecker(child));
            }
            return result;
        }
    }
}