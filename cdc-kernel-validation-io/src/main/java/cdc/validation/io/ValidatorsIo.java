package cdc.validation.io;

import cdc.io.data.Element;
import cdc.io.data.util.AbstractResourceLoader;
import cdc.util.lang.FailureReaction;
import cdc.validation.Validator;
import cdc.validation.ValidatorImpl;
import cdc.validation.Validators;
import cdc.validation.checkers.Checker;

public final class ValidatorsIo {
    public static final String ERROR = "error";
    public static final String NAME = "name";
    public static final String VALIDATOR = "validator";
    public static final String VALIDATORS = "validators";
    public static final String WARNING = "warning";

    private ValidatorsIo() {
    }

    public static class DataLoader extends AbstractResourceLoader<Void> {
        private final CheckersIo.DataLoader checkersLoader;

        public DataLoader(FailureReaction reaction) {
            super(reaction);
            this.checkersLoader = new CheckersIo.DataLoader(reaction);
        }

        @Override
        protected Void loadRoot(Element root) {
            if (VALIDATORS.equals(root.getName())) {
                loadAndRegisterNamedValidators(root);
            } else {
                unexpectedElement(root, VALIDATORS);
            }
            return null;
        }

        private void loadAndRegisterNamedValidators(Element element) {
            for (final Element child : element.getElements()) {
                if (VALIDATOR.equals(child.getName())) {
                    loadAndRegisterNamedValidator(child);
                } else {
                    unexpectedElement(child, VALIDATOR);
                }
            }
        }

        private void loadAndRegisterNamedValidator(Element element) {
            final String name = element.getAttributeValue(NAME, null, getReaction());
            final Validator<?> validator = loadValidator(element);
            if (validator != null) {
                if (Validators.hasValidator(name)) {
                    onError("A validator named '" + name + "' is already registered");
                } else {
                    Validators.register(validator, name);
                }
            }
        }

        public Validator<?> loadValidator(Element element) {
            final Checker<?> error = loadChecker(element, ERROR);
            final Checker<?> warning = loadChecker(element, WARNING);
            final Validator<?> validator = ValidatorImpl.createUnchecked(error, warning);
            if (validator == null) {
                return onError("Failed to create validator", null);
            } else {
                return validator;
            }
        }

        private Checker<?> loadChecker(Element parent,
                                       String name) {
            if (parent.hasChildren(Element.class, Element.named(name))) {
                final Element wrapper = parent.getChildAt(Element.class, Element.named(name), 0);
                return checkersLoader.loadChildChecker(wrapper);
            } else {
                // Normal, no need to alert
                return null;
            }
        }
    }
}