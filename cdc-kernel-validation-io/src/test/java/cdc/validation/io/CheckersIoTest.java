package cdc.validation.io;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.args.Factories;
import cdc.converters.ArgsConversion;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;
import cdc.validation.checkers.Checkers;

class CheckersIoTest {
    private static final Logger LOGGER = LogManager.getLogger(CheckersIoTest.class);

    public CheckersIoTest() {
        Factories.setConverter(ArgsConversion::convert);
    }

    @Test
    void test() throws IOException {
        final CheckersIo.DataLoader loader = new CheckersIo.DataLoader(FailureReaction.FAIL);
        loader.loadXml("src/test/resources/checkers-test.xml");
        for (final String name : CollectionUtils.toSortedList(Checkers.getNames())) {
            LOGGER.debug("{}: {}", name, Checkers.getChecker(name).explain());
        }
        assertTrue(true);
    }
}