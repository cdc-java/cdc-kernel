package cdc.validation.io;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.args.Factories;
import cdc.converters.ArgsConversion;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;
import cdc.validation.Validators;
import cdc.validation.checkers.Checkers;

class ValidatorsIoTest {
    private static final Logger LOGGER = LogManager.getLogger(ValidatorsIoTest.class);

    public ValidatorsIoTest() {
        Factories.setConverter(ArgsConversion::convert);
        Checkers.elaborate();
    }

    @Test
    void test() throws IOException {
        final ValidatorsIo.DataLoader loader = new ValidatorsIo.DataLoader(FailureReaction.FAIL);
        loader.loadXml("src/test/resources/validators-test.xml");
        for (final String name : CollectionUtils.toSortedList(Validators.getNames())) {
            LOGGER.debug("{}: {}", name, Validators.getValidator(name));
        }
        Validators.PRINTER.print(System.out);
        assertTrue(true);
    }
}